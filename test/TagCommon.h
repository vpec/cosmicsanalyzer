#ifndef TAGCOMMON_H
#define TAGCOMMON_H

// active volume bounding box
const float boundary_pdsp[][2] = {
    {-357.791, 357.791},
    {   7.275, 607.499},
    {-0.49375, 695.286}
};
const float boundary_pdhd[][2] = {
    {-352.212, 352.012},
    {   7.275, 607.499},
    {-0.59375, 463.126}
};



const float* GetBoundary(TString whichdet = "pdsp") {

    whichdet.ToLower();
    if (whichdet == "pdsp") {
	return (float*)boundary_pdsp;
    } else if (whichdet == "pdhd") {
	return (float*)boundary_pdhd;
    }
    return 0;
}

enum Tag_e {
    kOrig = 0b01,
    kApaEnter = 0b10,
    kApaExit = 0b100,
    kCPA = 0b1000,
};


#endif
