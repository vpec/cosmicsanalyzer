/**
 * Script to help tuning flash visualisation histogram binning.
 **/

void test_flash_vis_binning() {

    Float_t binsz[] = {
	// first APA - Arapuca on Jura side
	0, 29., 39., 49., 59., 70., 80., 90., 100., 110., 115., 135., 156., 176., 197.,233.,
	// second APA - Arapuca on Salev side
	261., 271., 281., 291., 302., 312., 322., 332., 342., 347., 367., 388., 408., 429., 466.,
	// third APA - no Arapuca
	700.
    };
    Int_t  binnumz = sizeof(binsz)/sizeof(Float_t) - 1;

    // Bar centres in Y direction (either side in X))
    const Float_t opdetcentrey[] = {
	35.492, 94.692, 153.892, 213.092, 272.292,
	331.492, 390.692, 449.892, 509.092, 568.292
    };

    const size_t sizey = sizeof(opdetcentrey)/sizeof(Float_t);
    Float_t binsy[sizey*4+2] = {0};
    Int_t  binnumy = sizey*4 + 1;
    for (size_t i = 0; i < sizey; ++i) {
	binsy[2*i+1] = opdetcentrey[i] - 10 - 600.;
	binsy[2*i+2] = opdetcentrey[i] + 10 - 600.;
	binsy[2*(i+sizey)+1] = opdetcentrey[i] - 10;
	binsy[2*(i+sizey)+2] = opdetcentrey[i] + 10;
    }
    binsy[0] = -600.;
    binsy[binnumy] = 600.;

    // debug:
    // std::cout<<"Bin edges in Y direction:"<<std::endl;
    // for (int i = 0; i <= binnumy; ++i)
    // 	std::cout<<binsy[i]<<", ";
    // std::cout<<std::endl;



    auto h = new TH2D("h", "",
		      binnumz, binsz,
		      binnumy, binsy);
    h->SetOption("colz");


    float bincontent = 0.;
    for (size_t iy = 1; iy < h->GetNbinsY()+1; ++iy) {
	for (size_t ix = 1; ix < h->GetNbinsX()+1; ++ix) {
	    int bin = iy*(h->GetNbinsX()+2) + ix;
	    h->SetBinContent(bin, (float) bin);
	}
    }


    gStyle->SetNumberContours(256);

    h->Draw("colz text");

}
