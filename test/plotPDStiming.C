
#ifndef FOR
#define FOR(i, size) for (int i = 0; i < size; ++i)
#endif

#include "LogScale.h"

#include "../cosmicsanalyzer/CosmicEventTree.h"

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec);
std::ostream& operator<<(std::ostream& outs, const TVector3& vec);

// active volume bounding box
const float boundary[][2] = {
    {-357.791, 357.791},
    {   7.275, 607.499},
    {-0.49375, 695.286}
};


void plotPDStiming(const char* fname = "", const char* outpref = "plots/", size_t maxevents = 0, size_t startentry = 0 )
{
    gStyle->SetOptStat(1110);
    gStyle->SetNumberContours(255);


    // prepare bounding box dimensions
    double top[3] = {};
    double bot[3] = {};
    for (int i = 0; i < 3; i++) {
	top[i] = std::max(boundary[i][0], boundary[i][1]);
	bot[i] = std::min(boundary[i][0], boundary[i][1]);

	cout<<"Bounds in direction "<<i<<": "<<bot[i]<<" -- "<<top[i]<<endl;
    }


    // input tree
    auto tree = new TChain("OpDetAna/opdetana");
    auto result = tree->Add(fname);
    if (!result) {
	cerr<<"Unable to get tree from file "<<fname<<endl;
	return;
    }
    auto evt = new CosmicEventTree::CosmicEventTree_t;
    CosmicEventTree::setAddresses(tree, evt);

    // Turn on only some of the branches
    tree->SetBranchStatus("*", 0);
    vector<string> keepOn = {
	// "n_reco_tracks",
	// "trk_hasT0",
	// "trk_T0",
	// "trk_startPoint",
	// "trk_endPoint",
	// "trk_length",
	// "trk_mcMuIdx",
	"n_muons",
	"gen_pos",
	"n_flashes",
	"flash_time",
	"flash_abs_time",
	"flash_tot_pe",
	"flash_mcMuIdx",
    };
    for (auto s: keepOn) tree->SetBranchStatus(s.c_str(), 1);

    // PE threshold
    float pe_thld = 400.;


    // Histograms
    vector<TH1*> hists;
    vector<unsigned> logaxes; // bits, 0b01 = X, 0b10 = Y,...
    enum {
	kLogX = 0b01,
	kLogY = 0b10,
	kLogZ = 0b100,
    };
#define mkhist1d(h, htype, title, dim, low, hi, log) auto h = new htype(#h, title, dim, low, hi); hists.push_back(h); logaxes.push_back(log)
#define mkhist2d(h, htype, title, dim1, low1, hi1, dim2, low2, hi2, log) auto h = new htype(#h, title, dim1, low1, hi1, dim2, low2, hi2); hists.push_back(h); logaxes.push_back(log); h->SetStats(0)

    cout<<"Creating hitograms..."<<endl;

    // all flashes and all muons
    mkhist1d(h1, TH1F, "Flash time - true time;#DeltaT [#mus]", 700, -3.5e3, 3.5e3, 0);
    mkhist1d(h2, TH1F, "Flash abs time - true time;#DeltaT [#mus]", 700, -3.5e3, 3.5e3, 0);
    mkhist1d(h5, TH1F, "Flash time - true time;#DeltaT [#mus]", 300, -500, 100, 0);
    mkhist1d(h6, TH1F, "Flash abs time - true time;#DeltaT [#mus]", 300, -500, 100, 0);

    // only backtracked muons
    mkhist1d(h3, TH1F, "Flash time - backtracked true time;#DeltaT [#mus]", 400, -100, 300, 0);
    mkhist1d(h4, TH1F, "Flash abs time - backtracked true time;#DeltaT [#mus]", 400, -100, 300, 0);

    cout<<"histograms done."<<endl;

    // Loop over entries in the tree
    cout<<"Estimating number of entries..."<<endl;
    tree->GetEntry(0);
    auto size = tree->GetTree()->GetEntries();
    auto ntrees = tree->GetNtrees();
    cout<<"Chain has "<<ntrees<<" trees. Single input tree has "<<size<<" entries."<<endl;
    size *= ntrees;
    cout<<"Estimated total number of entries: "<<size<<endl;
    if (maxevents > 0 && size > maxevents)
	size = maxevents;


    float fiftieth = size/50.;
    cout<<"Fiftieth: "<<fiftieth<<endl;
    cout<<"|                                                  |\r|";
    cout.flush();
    int nth = 0;

    for (auto i = size - size + startentry; i < size; i++) {
	if (!tree->GetEntry(i)) break; // break the loop if no data read

	if (fiftieth && (i+1)%fiftieth == 0) {
	    int nth = (i+1)/fiftieth;
	    cout<<"\r|"<<string(nth, '-')<<string(50-nth, ' ')<<"| "<<(2*nth)<<"%";
	    cout.flush();
	}

	FOR(iflash, evt->n_flashes) {
	    int& muIdx = evt->flash_mcMuIdx[iflash];
	    if (muIdx >= 0) {
		h3->Fill(evt->flash_time[iflash] - evt->gen_pos[muIdx][3]*1e-3);
		h4->Fill(evt->flash_abs_time[iflash] - evt->gen_pos[muIdx][3]*1e-3);
	    }
	    FOR(imuon, evt->n_muons) {
		h1->Fill(evt->flash_time[iflash] - evt->gen_pos[imuon][3]*1e-3);
		h2->Fill(evt->flash_abs_time[iflash] - evt->gen_pos[imuon][3]*1e-3);
		h5->Fill(evt->flash_time[iflash] - evt->gen_pos[imuon][3]*1e-3);
		h6->Fill(evt->flash_abs_time[iflash] - evt->gen_pos[imuon][3]*1e-3);
	    }
	}

    } // event loop

    cout<<endl;

    for (int i = 0; i < hists.size(); ++i) {
	auto c = new TCanvas(Form("c%d",i), "");
	if (hists[i]->IsA()->InheritsFrom("TH2"))
	    hists[i]->Draw("colz");
	else
	    hists[i]->Draw();

	if (logaxes[i] & kLogX)
	    c->SetLogx();
	if (logaxes[i] & kLogY)
	    c->SetLogy();
	if (logaxes[i] & kLogZ)
	    c->SetLogz();
	c->SaveAs(Form("%s%s.pdf", outpref, hists[i]->GetName()));
	c->SaveAs(Form("%s%s.png", outpref, hists[i]->GetName()));
    }

}
