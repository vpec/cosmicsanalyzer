
WHICHDET=pdsp

INPUTFILE=$1
OUTPUTPREF=$2

[ -n "$3" ] && WHICHDET=$3


OUTPUTDIR=${OUTPUTPREF%/*}
mkdir -p $OUTPUTDIR

echo INPUTFILE=$INPUTFILE
echo OUTPUTPREF=$OUTPUTPREF
echo OUTPUTDIR=$OUTPUTDIR
#ls $OUTPUTDIR
echo WHICHDET=$WHICHDET

OUTFNAME=$(basename $INPUTFILE)
OUTFILE=$OUTPUTPREF${OUTFNAME%.root}_tag_cpa.root
myroot -b -q test/TagCPAcrossers.C $INPUTFILE $OUTFILE 0 0 $WHICHDET

INPUTFILE=$OUTFILE
OUTFILE=${OUTFILE%.root}_apain.root
myroot -b -q test/TagAPAcrossers.C $INPUTFILE $OUTFILE 0 0 $WHICHDET

INPUTFILE=$OUTFILE
OUTFILE=${OUTFILE%.root}_apaout.root
myroot -b -q test/TagAPAOutCrossers.C $INPUTFILE $OUTFILE 0 0 $WHICHDET

INPUTFILE=$OUTFILE
OUTFILE=${OUTFILE%.root}_matched.root
myroot -b -q test/matchTPCtoPDS.C $INPUTFILE $OUTFILE


INPUTFILE=$OUTFILE
OUTFILE=${OUTFILE%.root}_stopped.root
myroot -b -q test/selectStoppingMus.C $INPUTFILE $OUTFILE 0 0 $WHICHDET
