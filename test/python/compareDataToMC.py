#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/compare_data_to_mc/v2/'

import selections as sel, tools as tls
from tools import DataToPlot as DTP

import copy

def plotComparison(datatree, mctree, d) :

    func = None
    if isinstance(d.data, tuple) :
        func = d.data[1]
        dstring = d.data[0]
    else :
        dstring = d.data

    data = datatree.arrays('a', aliases={'a':dstring})['a']
    mc   =   mctree.arrays('a', aliases={'a':dstring})['a']

    if func is not None :
        data = func(data)
        mc   = func(mc)

    toPlot = copy.deepcopy(d)
    # if isinstance(d.bins, dict) :
    #     d_data = { key:val for key,val in zip(*np.unique(ak.ravel(data), return_counts=True)) }
    #     d_mc = { key:val for key,val in zip(*np.unique(ak.ravel(mc), return_counts=True)) }
    #     toPlot.data = d.bins.keys()
    #     toPlot.bins = d.bins.values()
    #     toPlot.weights = [d_data[key] for key in d.bins.keys()]
    # else:
    #     toPlot.data = data

    outs = outpref
    if (d.outs is None) :
        outs += d.data
    else :
        outs += d.outs

    toPlot.ylabel = 'count/no.events'
    toPlot.kwargs['histtype'] = 'step'
    #toPlot.kwargs['density'] = True
    entries_mc = ak.num(mc, axis=0)
    entries_data = ak.num(data, axis=0)

    toPlot.data = mc
    toPlot.stats={'pos':(0.98, 0.7), 'ent':True}
    toPlot.label = 'MC'
    toPlot.outs = None
    tls.plotHist(toPlot, scale=1./entries_mc, fill=True)

    toPlot.clf = False
    toPlot.cla = False

    toPlot.data = data
    toPlot.stats={'pos':(0.98, 0.55), 'ent':True}
    toPlot.label = 'data'
    toPlot.outs = outs
    if toPlot.log :
        toPlot.outs = toPlot.outs + '_log'
    if toPlot.xlog :
        toPlot.outs = toPlot.outs + '_xlog'
    toPlot.outs = toPlot.outs+'.png'
    tls.plotHist(toPlot, legend=True, scale=1./entries_data, histtype='step')



def longTracks(d) :
    return ak.sum(d>20., axis=-1)


def process_tree(datatree, mctree) :

    toCompare = [
        DTP('n_reco_tracks', title='Number of reco tracks per event', xlabel='No.tracks per event', range=(60, 300), bins=240),
        DTP('trk_length', title='Track length', xlabel='Track length [cm]', range=(0, 1500), bins=150),
        DTP('trk_length', title='Track length', xlabel='Track length [cm]', range=(0, 1500), bins=150, log=True),
        DTP('trk_hasT0', title='T0 tag', xlabel='T0 tag', range=(0, 9), bins=9, log=True),
        DTP(('trk_length', longTracks), outs='n_long_tracks',title='Number of reco tracks > 20 cm per event', xlabel='No.tracks per event', range=(0, 200), bins=200),
        DTP('n_flashes', title='Number of reco flashes per event', xlabel='No.flashes per event', range=(0, 300), bins=300),
        DTP('flash_nhits', title='Number of reco ophits per flash', xlabel='No.ophits per flash', range=(0, 200), bins=200, log=True),
        DTP('flash_tot_pe', title='Number of reco PE per flash', xlabel='No.PE per flash', range=(0, 200), bins=200, log=True),
        DTP('flash_tot_pe', title='Number of reco PE per flash', xlabel='No.PE per flash', range=(1, 1e6), bins=120, log=True, xlog=True),
        DTP('n_hits', title='Number of reco ophits per event', xlabel='No.ophits per event', range=(1, 1e5), bins=250, log=True, xlog=True),
        DTP('hit_pe', title='Number of reco PE per ophit', xlabel='No.PE per ophit', range=(0, 500), bins=500, log=True),
        DTP('hit_pe', title='Number of reco PE per ophit', xlabel='No.PE per ophit', range=(0.1, 1e4), bins=250, log=True, xlog=True),
    ]

    for d in toCompare :
        plotComparison(datatree, mctree, d)


def run(args):
    print(args[1], args[2])


    if len(args) > 3 :
        global outpref
        outpref = args[3]

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as datatree:
        with ur.open(args[2]+':OpDetAna/opdetana') as mctree:
            process_tree(datatree, mctree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
