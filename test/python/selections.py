import uproot as ur, awkward as ak, numpy as np
import numba as nb

### Auxiliary
boundary = {'pdsp': ( (-357.791, 357.791),
                      (   7.275, 607.499),
                      (-0.49375, 695.286),
                     ),
            'pdhd': ( (-352.212, 352.012),
                      (   7.275, 607.499),
                      (-0.59375, 463.126),
                     ),
            }

top = []
bot = []
whichdet = 'pdsp'

def SetDetector(fwhichdet):
    # prepare bounding box dimensions
    global top
    global bot
    global whichdet
    whichdet=fwhichdet
    print(f'Setting boundary for detector {fwhichdet}')
    print(top, bot)
    top = [max(a[0],a[1]) for a in boundary[fwhichdet] ]
    bot = [min(a[0],a[1]) for a in boundary[fwhichdet] ]
    print(top, bot)

SetDetector(whichdet)

class T0Type(object):
    Orig = 0b01
    APAIn = 0b10
    APAOut = 0b100
    CPA = 0b1000


import tools as tls


### MC truth based
def MCStartsOutside(t) :
    gen_pos = t['gen_pos'].array()
    x,y,z = gen_pos[...,0], gen_pos[...,1], gen_pos[...,2]
    return ( (x < bot[0]) | (x > top[0]) ) | \
           ( (y < bot[1]) | (y > top[1]) ) | \
           ( (z < bot[2]) | (z > top[2]) )

def MaskCPACrossersInMC(t) :
    start = t['mc_startPoint_tpcAV'].array()
    end = t['mc_endPoint_tpcAV'].array()
    return np.sign(start[...,0]) != np.sign(end[...,0])

def MaskAPAInCrossersInMC(t) :
    mc_startPoint_tpcAV = t['mc_startPoint_tpcAV'].array()
    y,z = mc_startPoint_tpcAV[...,1], mc_startPoint_tpcAV[...,2]
    starts_oustide = MCStartsOutside(t)
    return starts_oustide & (z > bot[2] + 1.) & (z < top[2] - 1.) & (y > bot[1] + 1.) & (y < top[1] - 1.)

def MaskAPAOutCrossersInMC(t) :
    b = t.arrays(['mc_endPoint_tpcAV', 'mc_exited_tpcAV'])
    y,z = b.mc_endPoint_tpcAV[...,1], b.mc_endPoint_tpcAV[...,2]
    return b.mc_exited_tpcAV & (z > bot[2] + 1.) & (z < top[2] - 1.) & (y > bot[1] + 1.) & (y < top[1] - 1.)


### Mixed reco and MC truth

def GetMCMatched(t, entry_stop=None) :

    try:
        b = t['trk_mcMuIdx_best'].array(entry_stop=entry_stop)
        return b
    except KeyInFileError :
        b = t.arrays(['trk_mcMuIdx', 'trk_ncalopts'], entry_stop=entry_stop)

    mcidx = b.trk_mcMuIdx
    ncalos = b.trk_ncalopts

    # mcIdx for individual planes
    pl0 = b.trk_mcMuIdx[:,:,0]
    pl1 = b.trk_mcMuIdx[:,:,1]
    pl2 = b.trk_mcMuIdx[:,:,2]

    ## use mcidx from at least 2 planes if they agree
    result  = ak.where(pl0 == pl1, pl0, -1)
    result  = ak.where(pl0 == pl2, pl0, result)
    result  = ak.where(pl2 == pl1, pl1, result)

    ## if mcidx still -1, try to get best plane
    # determine best plane for each track
    bestplane = ak.argmax(ncalos, -1, keepdims=True)
    # get mcIdx by best plane
    bestplane_idxs = ak.firsts(mcidx[bestplane], axis=-1)
    #bestplane_idxs = SelectByIdx(mcidx, bestplane)

    final_idxs = ak.where(result == -1, bestplane_idxs, result)

    return final_idxs


@nb.njit
def _BuildRecoPerTrue(layout, builder, trkMcIdx) :
    for ievt in range(len(layout)) :
        builder.begin_list()
        for  imu in range(len(layout[ievt])) :
            builder.begin_list()
            for itrk in range(len(trkMcIdx[ievt])) :
                if trkMcIdx[ievt][itrk] == imu :
                    builder.integer(itrk)

            builder.end_list()
        builder.end_list()

def BuildRecoPerTrue(t,trkMcIdx=None) :

    builder = ak.ArrayBuilder()
    if trkMcIdx is None :
        idx = GetMCMatched(t)
    else :
        idx = trkMcIdx

    if isinstance(t, ur.TTree) :
        layout = t['mc_length_tpcAV'].array()
    elif isinstance(t, ak.Array) :
        layout = t['mc_length_tpcAV']
    else :
        raise
    _BuildRecoPerTrue(layout, builder, idx)
    return builder.snapshot()

def BuildRecoPerTrueAlt(t,trkMcIdx=None) :
    if trkMcIdx is None :
        idx = GetMCMatched(t)
    else :
        idx = trkMcIdx

    print('trk_mcMuIdx')
    idx.show()

    trk_idx = ak.local_index(idx,axis=-1)
    idx_sort = ak.argsort(idx)
    idx_sort = idx_sort[idx[idx_sort] != -1]
    runs = ak.run_lengths(idx[idx_sort])

    print('idx_sort')
    idx_sort.show()
    print('idx[idx_sort]')
    idx[idx_sort].show()
    print('trk_idx[idx_sort]')
    trk_idx[idx_sort].show()

    return ak.unflatten(trk_idx[idx_sort], ak.ravel(runs), axis=-1)


def BuildFlashPerTrue(t,flashMcIdx=None,entry_stop=None) :

    builder = ak.ArrayBuilder()
    if flashMcIdx is None :
        idx = t['flash_mcMuIdx'].array(entry_stop=entry_stop)
    else :
        idx = flashMcIdx

    # print(f'{idx = }')

    size = ak.num(idx,axis=0)
    layout = t['mc_length_tpcAV'].array(entry_stop=size)
    _BuildRecoPerTrue(layout, builder, idx)
    return builder.snapshot()



### Reco based
#### CPA Stitching
def GetCPAMatchedPairs(t) :

    @nb.njit
    def _GetCPAMatchedPairs(trk_stitched, builder):
        for evt in trk_stitched:
            itrk = -1
            builder.begin_list()
            for trk in evt :
                itrk += 1
                if trk  < 0 :
                    continue
                if trk < itrk :
                    continue
                builder.begin_tuple(2)
                builder.index(0).integer(itrk)
                builder.index(1).integer(trk)
                builder.end_tuple()

            builder.end_list()


    builder = ak.ArrayBuilder() # must use builder to create new AwkwardArray
    _GetCPAMatchedPairs(t['trk_stitched'].array(), builder)
    return builder.snapshot()

# tracks and flashes matching

def GetTrueFlashesPerTrack(tree, trkMcIdx=None, flashesPerTrue=None):
    '''
    Returrn all flashes backtracked to the same true muon as the track, for each reco track.
    Dimensions [nevts, nrecotracks]
    '''
    if flashesPerTrue is None :
        flashesPerTrue = BuildFlashPerTrue(tree)
    if trkMcIdx is None :
        trkMcIdx       = GetMCMatched(tree)

    # flashes matched to trk in truth
    return ak.where(trkMcIdx>-1, flashesPerTrue[trkMcIdx], ak.singletons(ak.mask(trkMcIdx,trkMcIdx>-1),axis=-1))



# Track and PDS association
# associated wfms
def GetTrkWfms(data, tracks) :
    trk_flashes = GetMatchedFlashes(data, tracks)
    return GetFlashWfms(data,trk_flashes)


def GetMatchedFlashes(data, tracks) :
    trk_flashes = data.match_flash_idx[data.trk_matched[tracks]]
    return trk_flashes

def GetFlashWfms(data, flashes) :
    # first get flash hits

    flash_hit_idx = tls.ClipData(data.flash_hit_idx[flashes], data.flash_nhits[flashes])
    flashwfmsidx = tls.Slice(data.hit_wfmIdx,flash_hit_idx)
    hit_chan  = tls.Slice(data.hit_channel,flash_hit_idx)
    # flash_hit_idx.show(limit_cols=100,type=True)
    # flashhits.show(limit_cols=100,type=True)
    #flashwfms.show(limit_cols=100,type=True)
    wfms      = tls.Slice(data.wfm,flashwfmsidx,axis=-2)
    wfmsizes  = tls.Slice(data.wfm_size,flashwfmsidx)
    wfmtimes  = tls.Slice(data.wfm_time,flashwfmsidx)

    flashTime = tls.Slice(data.flash_abs_time, flashes)

    depth_limit = wfms.ndim-1

    return ak.zip({'chan':hit_chan, 'wfms':tls.ClipData(wfms,wfmsizes),
                   'wfm_time':wfmtimes, 'flash_time':flashTime},depth_limit=depth_limit)


## PDS matching related
class PDSMatchSelector :
    def __init__(self, tree):
        self.tree = tree

        # Look at T0 vs PDS time!
        trkMcIdx = tree['trk_mcMuIdx'].array()
        self.flashMcIdx = tree['flash_mcMuIdx'].array()


        #   tracks
        self.trk_hasT0 = tree['trk_hasT0'].array()
        hasT0 = self.trk_hasT0 > 0
        self.trk_hasT0 = self.trk_hasT0[hasT0]
        trk_t = tree['trk_T0'].array()[hasT0]*1e-3
        self.trkT0McIdx = trkMcIdx[hasT0]
        trkLen = tree['trk_length'].array()[hasT0]
        isLong = trkLen > 100 # at least 1 m

        #   flashes
        self.flash_t = tree['flash_abs_time'].array()
        self.flash_pe = tree['flash_tot_pe'].array()

        trk_idx = ak.local_index(trk_t)
        flash_idx = ak.local_index(self.flash_t)
        self.all_pairs = ak.cartesian({'a':trk_idx, 'b':flash_idx}, nested=True)
        self.all_pairs_dt = tls.Slice(trk_t, self.all_pairs['a']) - tls.Slice(self.flash_t,self.all_pairs['b'])

    def ApplyCorrections(self,offsets) :
        all_pairs_tags = tls.Slice(self.trk_hasT0, self.all_pairs['a'])
        self.all_pairs_dt = tls.CorrectOffsetsByTag(self.all_pairs_dt, all_pairs_tags, offsets)
        return self.all_pairs_dt

    def ApplyTimeRange(self, tlow, thi) :
        range_mask = (self.all_pairs_dt>tlow) & (self.all_pairs_dt<thi)
        self.all_pairs    = self.all_pairs[range_mask]
        self.all_pairs_dt = self.all_pairs_dt[range_mask]

    def GetMatchedPair(self) :
        atrkmcidx = tls.Slice(self.trkT0McIdx,self.all_pairs['a'])
        aflashmcidx = tls.Slice(self.flashMcIdx,self.all_pairs['b'])
        self.matched_pair_mask = (atrkmcidx > -1) & (atrkmcidx == aflashmcidx)
        return self.matched_pair_mask

    def GetTimeMatchedPairMask(self, low=-20., high=30.) :
        '''
        impose cut on DT to be (-20,30) us
        '''
        return (self.all_pairs_dt > low) & (self.all_pairs_dt < high)
    def GetTimeMatchedPairDT(self, low=-20., high=30.) :
        return self.all_pairs_dt[self.GetTimeMatchedPairMask(low, high)]


    # dt for pairs that share the same true muon
    def GetMatchedPairDT(self) :
        return self.all_pairs_dt[self.matched_pair_mask]




# branches_to_process = [
#     'gen_pos',
#     'mc_exited_tpcAV',
#     'mc_startPoint_tpcAV',
#     'mc_endPoint_tpcAV',
#     'trk_length',
#     'trk_hasT0',
#     'trk_T0',
#     'trk_startPoint',
#     'trk_endPoint',
#     'trk_startTpc',
#     'trk_endTpc',
#     'trk_mcMuIdx',
#     'trk_ncalopts',
#     'trk_stitched'
# ]
