#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

import tools as tls

# use batch graphics mode
#mpl.use('agg')

ticks_per_us = 150. # PDS clock rate for MC of PDSP (prod4)


def run(args):
    fname = args.fname
    global outpref, apply_offset, ticks_per_us
    outpref, apply_offset, ticks_per_us = (args.outpref, bool(args.apply_offset), args.clock)


    # get input tree
    branches = ['flash_hit_idx', 'flash_nhits', 'hit_wfmIdx', 'wfm','wfm_time','wfm_size']

    data = 0
    with ur.open(fname+':OpDetAna/opdetana') as tree :
        # get relevant data
        data = tree.arrays(branches, entry_stop=args.nmax)

    flsh_hit_idx = tls.ClipData(data.flash_hit_idx, data.flash_nhits)
    flsh_hit_wfm_idx = tls.Slice(data.hit_wfmIdx, flsh_hit_idx)

    wfms = tls.ClipData(data.wfm, data.wfm_size)
    wfm_data = ak.zip({'wfms':wfms, 'wfm_time':data.wfm_time}, depth_limit=2)

    #wfm_data.show(type=True)

    flsh_wfm_data = tls.Slice(wfm_data, flsh_hit_wfm_idx)
    flsh_wfm_offset = flsh_wfm_data.wfm_time - flsh_wfm_data.wfm_time[...,0]
    flsh_wfm_data = ak.zip({'wfms':flsh_wfm_data.wfms, 'wfm_offset':flsh_wfm_offset}, depth_limit=3)
    #flsh_wfm_data.show(type=True)

    evtid = ak.local_index(data, axis=0)
    flashid = ak.local_index(data.flash_nhits, axis=-1)

    data_to_plot = ak.zip({'evtid':evtid, 'flashid':flashid, 'wfm_data':flsh_wfm_data},depth_limit=2)
    #data_to_plot.show(type=True)

    print(f'Flashes in events: {ak.num(data_to_plot,axis=1)}')
    PlotWfmsByFlash(data_to_plot)



def PlotWfmsByFlash(d) :
    global apply_offset
    for evt in d :
        for flash in evt :
            evtid = flash.evtid
            flashid = flash.flashid
            fig,axs=plt.subplots(2,1,sharex=True)
            axs[0].set_title(f'Evt {evtid}, Flash {flashid}, {len(flash.wfm_data)} Waveforms')
            axs[1].set_xlabel('Time [$\\mu s$]')
            for wfmd in flash.wfm_data :
                y=wfmd.wfms.to_numpy()
                x=np.arange((len(y)-0.9)/ticks_per_us,step=1./ticks_per_us)
                if apply_offset :
                    x = x+wfmd.wfm_offset
                axs[0].plot(x,y)
            axs[1].hist(flash.wfm_data.wfm_offset)
            plt.savefig(outpref+f'wfms_evt{evtid}_flash{flashid}.png')
            #plt.show()

if __name__ == '__main__':
    print('Running processing...')

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('fname')
    parser.add_argument('outpref',nargs='?',default='')
    parser.add_argument('-n','--nmax', default=10, type=int)
    parser.add_argument('--clock', default=150, type=float)
    parser.add_argument('-o', '--apply-offset', default=1, type=int)

    args = parser.parse_args()

    # make sure the output directory exists
    os.makedirs(os.path.dirname(args.outpref), exist_ok=True)

    run(args)
