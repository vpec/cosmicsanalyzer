#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

def AddStatBox(mean, std) :
    ax = plt.gca()
    ax.text(0.98, 0.88, f'mean = {mean:5.2f}\n std.dev. = {std:5.2f}',
                       transform = ax.transAxes, ha='right', fontfamily = 'monospace')

outpref = 'plots/python/reco_to_mc_matching'

import selections as sel
import tools as tls
from selections import GetMCMatched


def GetReconstructableMask(tree) :
    DriftVel = 0.1565e3 # in cm/ns // from ProtoDUNE fhicl //at 87.68 K and 486.7 V/cm as for ProtoDUNE
    # nominal at 87 K and normal pressure: 0.1648;
    InverseDriftVel = 1./DriftVel

    b = tree.arrays(['mc_startPoint_tpcAV',
                     'mc_endPoint_tpcAV'])

    true_t = b.mc_startPoint_tpcAV[...,3]
    x1 = b.mc_startPoint_tpcAV[...,0]
    x2 = b.mc_endPoint_tpcAV[...,0]
    absx1 = ak.where(x1 > 0., x1, -x1)
    absx2 = ak.where(x2 > 0., x2, -x2)
    # xmax = ak.where( absx1 > absx2, absx1, absx2)
    # xmin = ak.where( absx1 > absx2, absx1, absx2)
    xmax = ak.max( [absx1, absx2], axis=0 )
    xmin = ak.min( [absx1, absx2], axis=0 )
    # absxzipped = ak.zip([absx1, absx2])
    # xmax = ak.max( absxzipped, axis=2 )
    # xmin = ak.min( absxzipped, axix=2 )
    upperLimit = true_t < (2.75e6 - (358. - xmax)*InverseDriftVel)
    lowerLimit = true_t > (-250e3 - (358. - xmin)*InverseDriftVel)

    crossers = sel.MaskCPACrossersInMC(tree)

    return ak.where(crossers,
                    (true_t > -2.5e6) & upperLimit, # 2.5e6 ns is max drift time + 250e3 ns pretrigger
                    # not a CPA crosser
                    lowerLimit & upperLimit)

def GetTrimmed(b) :
    # trimmed by readout window; -1: trimmed at low TDC, 1: at high TDC
    buf = 50 # how many TDC counts from the edge to consider track trimmed
    trimmed_low = ( (b.trk_startPoint[...,3] < buf) | (b.trk_endPoint[...,3] < buf) )
    trimmed_hi  = ( (b.trk_startPoint[...,3] > (6000-buf)) | (b.trk_endPoint[...,3] > (6000- buf)) )

    return (trimmed_low, trimmed_hi)



def process_tree(args):
    print(args[1])
    print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        sel.SetDetector(args[3])

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    # make sure the output directory exists
    os.makedirs(outpref, exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:

        branches = tree.arrays(['mc_startPoint_tpcAV',
                                'mc_endPoint_tpcAV',
                                'trk_length',
                                'trk_hasT0',
                                'trk_T0',
                                'trk_startPoint',
                                'trk_endPoint',
                                'trk_startTpc',
                                'trk_endTpc',
                                'trk_mcMuIdx',
                                'trk_ncalopts',
                                'trk_stitched'
                                ])

        trk_mcMuIdx      = GetMCMatched(tree)
        cpa_matches      = sel.GetCPAMatchedPairs(tree)
        nstitched        = ak.num(cpa_matches, axis = -1)
        empty_event_mask = (nstitched > 0)

        first,second = ak.unzip(cpa_matches)
        cpa_stitched_first_mcidx = trk_mcMuIdx[first]
        cpa_stitched_second_mcidx = trk_mcMuIdx[second]
        # true CPA crossers
        true_crosser = sel.MaskCPACrossersInMC(tree)
        true_match = ((cpa_stitched_first_mcidx == cpa_stitched_second_mcidx) & true_crosser[cpa_stitched_first_mcidx])

        # Count
        match_sum = ak.sum(true_match)
        no_match_sum = ak.sum(~true_match)
        total_stitched = match_sum + no_match_sum
        print(f'{match_sum = }')
        print(f'{no_match_sum = }')
        print(f'{total_stitched = }')
        print(f'purity = {match_sum/total_stitched*100:.1f}%')


        # Time diffs between the true time and CPA T0
        true_times = branches.mc_startPoint_tpcAV[cpa_stitched_first_mcidx[true_match]][...,3]
        cpa_t0 = branches.trk_T0[first[true_match]]
        true_match_dt = (cpa_t0 - true_times)


        true_times_nomatch = branches.mc_startPoint_tpcAV[cpa_stitched_first_mcidx[~true_match]][...,3]
        cpa_t0_nomatch = branches.trk_T0[first[~true_match]]
        no_match_dt = cpa_t0_nomatch - true_times_nomatch


        # Reconstructable truth
        reconable = GetReconstructableMask(tree)

        # for each muon find all backtracked reco tracks
        muRecoTracks = sel.BuildRecoPerTrue(tree, trk_mcMuIdx)

        # track end-point side of CPA
        trk_tpc_start = branches.trk_startTpc % 4
        trk_tpc_end = branches.trk_endTpc % 4

        flattened = ak.flatten(muRecoTracks, axis=-1)
        flattened_tpc_start = branches.trk_startTpc[flattened]
        flattened_tpc_start = trk_tpc_start[flattened]
        longtrack_mask = branches.trk_length[flattened] > 20.

        # print(branches.trk_startTpc)
        # print(flattened)
        # print(flattened_tpc_start)
        # print(flattened_tpc_start)
        # print(longtrack_mask)

        # Stitchable tracks cossing CPA
        from select_stitchable import SelectStitchable

        # find if relevant tracks are trimmed by RO window
        trk_trimmed_mask_low, trk_trimmed_mask_hi = GetTrimmed(branches)
        trksPerMu = muRecoTracks
        trksPerMu_flat = ak.flatten(trksPerMu, axis=-1)
        trimmedPerMu = tls.RebuildFromFlat(ak.flatten(trk_trimmed_mask_hi[trksPerMu_flat]), tls.GetLayout(trksPerMu))
        hasTrimmedPerMu = ak.sum(trimmedPerMu, axis=-1) > 1 # at least two tracks are trimmed at high TDC
        # shouldn't I check if they are on both sides of APA?



        stitchable_mask = SelectStitchable(tree)
        # stitchable_mask.type.show()
        # print(ak.num(stitchable_mask, axis=-1))
        # hasTrimmedPerMu.type.show()
        # print(ak.num(hasTrimmedPerMu, axis=-1))
        stitchable_trimmed_mask = stitchable_mask & hasTrimmedPerMu
        #print(f'{stitchable_mask = }')
        stitched_mask = branches.trk_hasT0 == 8 # indexed in reco tracks
        stitched_and_matched_trks = first[cpa_stitched_first_mcidx == cpa_stitched_second_mcidx] # indexed in matched tracks
        stitched_and_matched_mcidx = trk_mcMuIdx[stitched_and_matched_trks] # indexed in matched tracks
        stitched_and_matched_and_stitchable_mask = stitchable_mask[stitched_and_matched_mcidx] # indexed in matched tracks
        #print(f'{ak.to_list(stitched_and_matched_and_stitchable_mask) = }')

        stitchable_original_t0_mask = stitchable_mask[trk_mcMuIdx[branches.trk_hasT0 == 1]]

        count_stitched_and_stitchable = ak.sum(stitched_and_matched_and_stitchable_mask)
        count_stitchable              = ak.sum(stitchable_mask)
        count_stitchable_trimmed      = ak.sum(stitchable_trimmed_mask)
        count_stitched                = ak.count(first)
        count_stitchable_originalT0   = ak.sum(stitchable_original_t0_mask)
        count_tracks                      = ak.sum(tree['n_reco_tracks'].array())
        count_stitched_tracks             = ak.sum(stitched_mask)
        count_recod_mus                   = ak.sum(ak.num(muRecoTracks, axis=-1) > 0)
        count_stitched_and_matched_tracks = ak.count(stitched_and_matched_trks)

        print('='*30)
        print(f'{count_stitched_and_stitchable = }')
        print(f'{count_stitchable              = }')
        print(f'{count_stitchable_trimmed      = }')
        print(f'{count_stitched                = }')
        print(f'{count_stitchable_originalT0   = }')

        print('='*30)
        print(f'Efficiency         = {count_stitched_and_stitchable/count_stitchable = :.3}')
        print(f'Efficiency trimmed = {count_stitched_and_stitchable/count_stitchable_trimmed = :.3}')
        print(f'Efficiency tracks  = {count_stitched_tracks/count_tracks = :.3}')
        print(f'Efficiency muons   = {count_stitched_and_matched_tracks/count_recod_mus = :.3}')
        print('='*30)
        print(f'Purity             = {count_stitched_and_stitchable/count_stitched       = :.3}')
        print(f'Purity muons       = {count_stitched_and_matched_tracks/count_stitched   = :.3}')
        print('='*30)


        ### Get info about inefficient events
        stitched_truth_mask = tls.IdxToMask(stitchable_mask, stitched_and_matched_mcidx)
        inefficient_mask = stitchable_mask & ~stitched_truth_mask
        muidx = ak.local_index(branches.mc_startPoint_tpcAV, axis=1)
        evtidx =  ak.local_index(muidx, axis=0)
        muoncoord = ak.zip([evtidx, muidx])

        # print(f'{stitchable_mask = }')
        # print(f'{stitched_truth_mask = }')
        # print(f'{inefficient_mask = }')
        # print(f'{ak.sum(inefficient_mask) = }')
        # print(f'{muidx = }')
        # print(f'{evtidx = }')
        # print(f'{muoncoord = }')
        # print(ak.to_list(muoncoord[inefficient_mask])) # lists Evt Number and Muon number for stitchable muon which was not stitched


        #### Plotting ####
        dt = true_match_dt*1e-3
        tls.plotHist(dt, bins=200, range=(-1000,1000),
                     title='MC Matched', xlabel=r'$\Delta$T [$\mu$s]',
                     stats=True, outs=outpref+'stitched_2_dt.png')
        mean,std = tls.plotZoomed(dt, bins=120, range=(-60,60),
                                  title='MC Matched', xlabel=r'$\Delta$T [$\mu$s]',
                                  stats=True, outs=outpref+'stitched_2_dt_zoom.png')

        plt.cla()
        plt.hist(ak.flatten(dt*1e-3), label='MC Matched',
                 bins=300, range=(-3,3), histtype='step')
        plt.hist(ak.flatten(no_match_dt)*1e-6, label='No Match',
                 bins=300, range=(-3,3), histtype='step')
        plt.legend()
        plt.xlabel(r'$\Delta$T [ms]')
        plt.savefig(outpref + 'stitched_dt.png')

        # plt.xlim((-200,200))
        # plt.savefig(outpref + 'stitched_dt_zoom.png')


        # get fraction of outliers id DT
        def outliers(data, label, mean, std):
            count_outliers = ak.sum(abs(data - mean) > 3*std)
            fraction_of_outliers = count_outliers / ak.count(data)
            print (f'{label:15} {fraction_of_outliers:.3f}')

        print('='*30)
        print('DT outliers ( |Dt-mean| > 3sigma )')
        outliers(dt, 'CPA', mean, std)
        print('='*30)


        ### original tag for tracks that cross CPA
        orig_t0_cpa_crossing = (branches.trk_startTpc%4 + branches.trk_endTpc%4 == 3)[branches.trk_hasT0 == 1]
        ### original tag for tracks that cross APA
        orig_t0_apa_crossing = ( (branches.trk_startTpc%4 == 0) | (branches.trk_endTpc%4 == 3) )[branches.trk_hasT0 == 1]



        def plotData(data, bins=16, range=(-0.5,15.5),
                     title='', xlabel='Count per event', outs=None) :
            plt.cla()
            counts, edges, bars = plt.hist(data, bins=bins, range=range, histtype='bar')
            # plt.cla()
            # plt.stairs(counts, edges)
            # plt.bar_label(bars)
            plt.xlabel(xlabel)
            plt.title(title)
            if not outs is None :
                plt.savefig(outpref + outs + '.png')


        data_to_plot =[
            (nstitched, 'Number of stitched pairs', 'num_stitched'),
            (ak.sum(stitched_mask, axis=-1)*0.5, 'Number of stitched pairs (by hasT0)', 'num_stitched_hasT0'),
            (ak.sum(branches.trk_hasT0==1, axis=-1), 'Number of tracks w/ original tag', 'original_t0'),
            (ak.sum(orig_t0_cpa_crossing, axis=-1), 'Number of orig-tag tracks CPA crossing', 'original_t0_cpa_crossing'),
            (ak.sum(orig_t0_apa_crossing, axis=-1), 'Number of orig-tag tracks APA crossing', 'original_t0_apa_crossing'),
        ]

        for data,title,outs in data_to_plot :
            #plotData(data, title=title, outs=outs)
            tls.plotHist(data, bins=16, range=(-0.5,15.5),
                         title=title,  xlabel='Count per event',
                         outs=outpref+outs+'.png')


if __name__ == '__main__':
    print('Running processing...')
    process_tree(sys.argv)
