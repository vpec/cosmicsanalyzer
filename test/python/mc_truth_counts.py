#!/bin/env python

import sys
import uproot as ur
import matplotlib.pyplot as plt
import numpy as np
import numba as nb
import awkward as ak
import vector

import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

import tools as tls

gDriftVel = 0.1565; # in cm/us // from ProtoDUNE fhicl //at 87.68 K and 486.7 V/cm as for ProtoDUNE


boundary = (
    (-357.791, 357.791),
    (   7.275, 607.499),
    (-0.49375, 695.286)
)


# prepare bounding box dimensions
top = [max(a[0],a[1]) for a in boundary ]
bot = [min(a[0],a[1]) for a in boundary ]



def createHist(data, figure, bins = 1, range = (0,1), title = '', color = 'blue') :
    ax = plt.gca()
    ax.set_xlabel('Number of muons per event')
    ax.set_title(title)

    h, bins, patches = ax.hist(data, bins = bins, range = range, histtype='step', color=color)
    mean = ak.mean(data)
    std = ak.std(data)
    tls.AddStatBox(mean, std, int(ak.sum(h)), {'ent':True})
    total = np.sum(data)

    return (h, bins, title, total, mean, std)


def replot(h, bins, title, color, total, mean, std) :
    ax = plt.gca()
    ax.set_xlabel('Number of muons per event')
    ax.set_title(title)
    plt.stairs(h, bins)
    tls.AddStatBox(mean, std, int(ak.sum(h)), {'ent':True})

def SelectReconableMuons(branches):
    # count reconstructable muons
    true_muon_mask = branches.mc_length_tpcAV >= 20.
    x1 = branches.mc_startPoint_tpcAV[:,:,0]
    x2 = branches.mc_endPoint_tpcAV[:,:,0]
    true_t = branches.mc_startPoint_tpcAV[:,:,3]

    xmin = np.minimum(np.abs(x1), np.abs(x2))
    xmax = np.maximum(np.abs(x1), np.abs(x2))

    upperLimit = true_t < (2.75e6 - (358. - xmax)/gDriftVel*1e3)
    lowerLimit = true_t > (-250e3 - (358. - xmin)/gDriftVel*1e3)
    t_min_cut = true_t > -2.5e6

    cpa_crosser_mask = ((x1 < 0.) & (x2 > 0.)) | ((x1 > 0.) & (x2 < 0.))

    return ak.where(cpa_crosser_mask, (t_min_cut & upperLimit), (upperLimit & lowerLimit)) & true_muon_mask


def SelectReconableCPACrossers(branches) :
    # count reconable CPA crossers
    true_muon_mask = branches.mc_length_tpcAV >= 20.
    x1 = branches.mc_startPoint_tpcAV[:,:,0]
    x2 = branches.mc_endPoint_tpcAV[:,:,0]
    true_t = branches.mc_startPoint_tpcAV[:,:,3]

    xmax = np.maximum(np.abs(x1), np.abs(x2))

    upperLimit = true_t < (2.75e6 - (358. - xmax)/gDriftVel*1e3)
    t_min_cut = true_t > -2.5e6

    cpa_crosser_mask = ((x1 < 0.) & (x2 > 0.)) | ((x1 > 0.) & (x2 < 0.))

    return true_muon_mask & cpa_crosser_mask & t_min_cut & upperLimit


def SelectReconableAPAIn(branches) :
    start = vector.zip({'x': branches.mc_startPoint_tpcAV[:,:,1],
                        'y': branches.mc_startPoint_tpcAV[:,:,2]})
    true_muon_mask = branches.mc_length_tpcAV >= 20.
    reconable = SelectReconableMuons(branches)

    return true_muon_mask & (start.y > bot[2] + 1.) & (start.y < top[2] - 1.) & (start.x > bot[1] + 1.) & (start.x < top[1] - 1.) & reconable

def SelectReconableAPAOut(branches) :
    end = vector.zip({'x': branches.mc_endPoint_tpcAV[:,:,1],
                      'y': branches.mc_endPoint_tpcAV[:,:,2]})
    exited = branches.mc_exited_tpcAV
    true_muon_mask = branches.mc_length_tpcAV >= 20.
    reconable = SelectReconableMuons(branches)

    return true_muon_mask & exited & (end.y > bot[2] + 1.) & (end.y < top[2] - 1.) & (end.x > bot[1] + 1.) & (end.x < top[1] - 1.) & reconable


# def GetMCIdx(planes) :
#     if planes[0].x == planes[1].x :
#         return planes[0].x
#     if planes[0].x == planes[2].x :
#         return planes[0].x
#     if planes[1].x == planes[2].x :
#         return planes[1].x

#     return planes[np.argmax(planes[:].y)].x

# def MapMCIdxPerTrack(branches) :
#     mcidx = branches.trk_mcMuIdx
#     ncalos = branches.trk_ncalopts

#     return np.apply_along_axis( GetMCIdx, 2, vector.zip({'x': mcidx, 'y': ncalos}) )




def process_tree(args):
    outpref='plots/python/true_muon_counting/'

    if len(args) > 2 :
        outpref = args[2]

    # make sure the output directory exists
    #print(os.path.dirname(outpref))
    import os
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    print(args[1])
    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        # prepare output figure
        #plt.().xlabel('Number of muons per event')
        fig,axes = plt.subplots(2, 3, figsize = (15,10))

        totals = []
        means = []

        branches = tree.arrays(['n_muons',
                                'mc_length_tpcAV',
                                'mc_startPoint_tpcAV',
                                'mc_endPoint_tpcAV',
                                'mc_exited_tpcAV',
                                'trk_mcMuIdx',
                                'trk_ncalopts'])

        hist_data = { #(histdata, title, color)
            # count all gen muons
            'gen_mu':     (branches.n_muons,                                     'Generated true muons', 'tab:green'),
            # muons over 20 cm
            'long_mu':    (np.sum(branches.mc_length_tpcAV >= 20., axis      = 1),   'True muons > 20 cm',   'tab:blue'),
            # reconstructable muons
            'reco_mu':    (np.sum(SelectReconableMuons(branches), axis       = 1),       'Reconstructable',      'tab:purple'),
            # reconstructable CPA crossers
            'reco_cpa':   (np.sum(SelectReconableCPACrossers(branches), axis = 1), 'CPA crossers',         'tab:cyan'),
            # reconstructable APA entering and exiting
            'reco_apain': (np.sum(SelectReconableAPAIn(branches), axis       = 1),       'APA entering',         'tab:gray'),
            'reco_apaout': (np.sum(SelectReconableAPAOut(branches), axis     = 1),      'APA exiting',          'tab:orange'),
            'mu_len': (branches.mc_length_tpcAV,      'Muon length in TPC AV',          'tab:orange'),
            #(MapMCIdxPerTrack(branches),                           'MC muon index per reco track', 'red'),


        }

        # plots hists on a single figure
        iaxis = 0
        hists = {}
        for key, (data,title, color) in hist_data.items() :
            if iaxis > 5 :
                break
            fig.sca(fig.axes[iaxis])
            iaxis += 1
            h, bins, title, total, mean, std = createHist(data,
                                                          fig, title = title,
                                                          bins = 150, range = (-0.5,149.5), color=color)
            hists[key] = (h, bins, title, color, total, mean, std)

            totals.append((title,total))
            means.append((title, mean, std))

        plt.savefig(f'{outpref}hists.png')
        plt.savefig(f'{outpref}hists.pdf')


        # plot hists in separate figures and save those
        iaxis = 0
        for key, hist in hists.items() :
            fig,axes = plt.subplots()
            replot(*hist)
            fig.savefig(f'{outpref}{key}_hist.png')


        import tools as tls
        tls.plotHist(hist_data['reco_cpa'][0], bins=21, range=(-.5,20.5),
                     title='Reconstructable true CPA crossers',
                     xlabel='Count per event',
                     outs=f'{outpref}reconable_cpa_crossers.png')

        tls.plotHist(hist_data['mu_len'][0], bins=100, range=(0,2000),
                     title=hist_data['mu_len'][1],
                     xlabel='Length [cm]',
                     outs=f'{outpref}mu_len.png')


        print('='*30)
        print('Total counts:')
        print('-'*30)
        for tot in totals :
            print(f'{tot[0]:>20s} = {tot[1]:6d}')

        print('Counts per Event:')
        print('-'*30)
        for mean in means :
            print(f'{mean[0]:>20s} = {mean[1]:6.2f} (stddev = {mean[2]:.2f})')
        print('='*30)





        # runs = tree["run"].array()
        #pos = tree["mc_startPoint_tpcAV"].array()

        # for evt in tree:
        # plt.hist(runs, 200)
        # plt.show()

        # print(runs)

        # tree["gen_pos"].show()
        # print(pos)

        # plt.hist(ak.flatten(pos[:-1,:-1,0]), bins=100, range=(-600, 600))

        # plt.xlabel('x [cm]')

        # plt.show()



if __name__ == '__main__':
    print('Running processing...')
    process_tree(sys.argv)
