import awkward as ak
import numpy as np

### plotting tools
from matplotlib import pyplot as plt


class DataToPlot :
    def __init__(self, data=None, bins='auto', range=None,
                 label=None, title=None, xlabel=None, ylabel=None,
                 xlog=False, log=False,
                 stats=True, barlabels=False,zoomed=False,
                 outs=None, weights=None, **kwargs) :
        self.data      = data
        self.bins      = bins
        self.range     = range
        self.label     = label
        self.title     = title
        self.xlabel    = xlabel
        self.ylabel    = ylabel
        self.xlog      = xlog
        self.log       = log
        self.stats     = stats
        self.barlabels = barlabels
        self.zoomed    = zoomed
        self.outs      = outs
        self.weights   = weights
        self.kwargs    = kwargs


def AddStatBox(mean, std, entries, stats=None) :
    ax = plt.gca()
    figures = 2
    if std >= 10 :
        figures = 0

    showentries = False
    x,y =  (0.98, 0.98)
    if (isinstance(stats, tuple)) :
        x,y = stats
    elif isinstance(stats, dict) :
        if 'pos' in stats :
            x,y = stats['pos']
        if 'ent' in stats :
            showentries = stats['ent']

    ax.text(x, y, (f'entries = {entries:5d}\n' if showentries else '') + \
            f'mean = {mean:5.{figures}f}\n' + \
            f'std.dev. = {std:5.{figures}f}',
            transform = ax.transAxes,
            va='top', ha='right',
            fontfamily = 'monospace')
    return mean, std

def processPlotArgs(data, **kwargs) :
    defaults = {
        'bins': 'auto', 'range': None,
        'title': '', 'xlabel': '', 'ylabel':'',
        'stats': True,
        'barlabels': False,
        'outs': None, 'show': False,
        'xlog': False,
        'cla': True, 'clf': True,
        'zoomed': False,
        'legend': False,
        'scale': None,
        'weights': None,
    }

    args = kwargs
    if isinstance(data, DataToPlot):
        datts = dict(data.__dict__)
        datts.update(datts['kwargs'])
        del datts['kwargs']
        datts.update(**kwargs)
        args = datts
        del args['data']
    defaults.update(**args)
    args = defaults

    from argparse import Namespace
    ns = Namespace(**args)

    # clear figure or axes if requested
    if ns.cla :
        plt.cla()
    if ns.clf :
        plt.clf()

    return ns


def plotHist(data=None,
             **kwargs) :

    ns = processPlotArgs(data, **kwargs)

    if isinstance(data,DataToPlot) :
        d = data.data
    else :
        d = data
    # if data is a function, get the resulting data
    if callable(d) :
        d = d()

    if d.ndim > 1:
        x = ak.ravel(d)
    else :
        x = d

    if ns.range is None :
        low = ak.min(x)
        hi = ak.max(x)
    else :
        low,hi = ns.range

    if low < 0 or hi < 0 :
        ns.xlog = False

    if ns.xlog :
        lbins = np.logspace(np.log10(low),np.log10(hi), ns.bins+1 if ns.bins is not None else 40)
    else :
        lbins = ns.bins if ns.bins is not None else 40

    if ns.zoomed:
        d = d[(d>ns.range[0]) & (d<ns.range[1])]

    dont_pass = ['bins', 'range', 'title', 'xlabel', 'ylabel', 'stats',
                 'barlabels', 'outs', 'show', 'xlog', 'cla', 'clf',
                 'zoomed', 'legend','weights', 'scale']
    kwargs_to_plot = { key:value for key,value in ns.__dict__.items() if key not in dont_pass }

    counts,edges = np.histogram(x, bins=lbins, range=(low,hi))

    if ns.scale is not None :
        if isinstance(ns.scale, str) :
            print('scale is a string')
            if ns.scale.lower() == 'event' :
                print('scaling by number of events')
                nevts = ak.num(d, axis=0)
                counts,edges,bars=plt.hist(edges[:-1], edges, weights=counts/float(nevts), **kwargs_to_plot)
        else:
            counts,edges,bars=plt.hist(edges[:-1], edges, weights=ns.scale*counts, **kwargs_to_plot)
    else :
        counts,edges,bars=plt.hist(edges[:-1], edges,weights=counts, **kwargs_to_plot)

    plt.title(ns.title)
    plt.xlabel(ns.xlabel)
    plt.ylabel(ns.ylabel)
    if ns.xlog :
        plt.xscale('log')
    if ns.barlabels :
        plt.bar_label(bars)
    mean, std, entries = (ak.mean(d), ak.std(d), ak.count(d))
    if ns.stats :
        mean, std = AddStatBox(mean, std, entries, ns.stats)

    if ns.legend :
        plt.legend(loc='best')

    if ns.outs :
        plt.savefig(ns.outs)
    if ns.show :
        plt.show()

    return mean, std



def plotZoomed(data, **kwargs) :
    return plotHist( data, zoomed=True, **kwargs )



### awkward array handling tools

# def SelectByIdx(data, indxs) :
#     '''
#     data: awkward array with data
#     indxs: array with indices
#     '''
#     for i in range(len(data)) :
#         builder.begin_list()
#         for j in range(len(data[i])):
#             k = int(indxs[i][j])
#             builder.append(data[i][j][k])
#         builder.end_list()

def Fit(counts, bins, func, fit_range=None, **kwargs) :
    from scipy.optimize import curve_fit

    if fit_range :
        range_mask = (bins > fit_range[0]) & (bins < fit_range[1])
        if len(bins) == len(counts)+1 :
            counts = counts[range_mask[:-1]]
        else :
            counts = counts[range_mask]
        bins   = bins[range_mask]


    if 'sigma' not in kwargs.keys() :
        # set histogram
        kwargs['sigma'] = np.sqrt(counts)

    # treat bins as x coordinate
    x = bins
    if len(bins) == len(counts)+1 :
        # take bin centres
        x = np.array([(bins[i]+bins[i+1])*0.5 for i in range(len(bins)-1)])

    result = curve_fit(func, x, counts, **kwargs)
    print('Fit results:\n', result)

    return (x, func(x, *result[0]), result)


def MultiNormal(x, *params) :
    y = np.zeros_like(x)
    for i in range(0, len(params)-1, 3):
        amp = params[i]
        mean = params[i+1]
        sig = params[i+2]
        y = y + amp * np.exp( -0.5*((x - mean)/sig)**2)
    if (len(params) % 3) == 1 :
        y += float(params[-1])
    return y



def PlotFitResults(results, loc=None):
    # results is a tuple of a list of labels, array of fit parameters and their covariance matrix
    from matplotlib.offsetbox import AnchoredText

    lloc = 'upper right'
    if loc :
        lloc=loc
    s = ''
    labels, params, cov = results
    errs = np.sqrt(np.diag(cov))
    for i in range(len(labels)) :
        s += fr'{labels[i]}={params[i]:.4g}$\pm${errs[i]:.4g}' + '\n'
    s = s[:-1] # remove trailing \n
    text = AnchoredText(s, loc=lloc)
    plt.gca().add_artist(text)

def PlotStatbox(stats, loc=None):
    # stats is a dict of statistics
    from matplotlib.offsetbox import AnchoredText

    lloc = 'upper right'
    if loc :
        lloc=loc
    s = ''
    for label,value in stats.items() :
        s += fr'{label}={value:.4g}' + '\n'
    s = s[:-1] # remove trailing \n
    text = AnchoredText(s, loc=lloc)
    plt.gca().add_artist(text)



def SaveToGZ(fname, data) :
    from gzip import open as gzopen
    from pickle import dump
    with gzopen(fname, 'wb', compressionlevel=3) as outf :
        dump(data, outf)

def CorrectOffsetsByTag(data, tags, offsets) :
    '''Data and tags need to have the same shape. Offsets is a
    dict translating tag to an offset.
    '''
    offsets_arr = np.array( [0.]*(max(list(offsets.keys()))+1) )
    for key,val in offsets.items() :
        offsets_arr[key] = val
    offsets_arr = ak.Array(offsets_arr)

    mapped_offsets = offsets_arr[ak.ravel(tags)]
    layout = GetLayout(tags)
    mapped_offsets = RebuildFromLayout(mapped_offsets, layout)

    return data - mapped_offsets


def Slice(data, idx, axis=-1) :
    '''
    Slices data with idx. idx must have the same structure down to
    the most inner dimension of data. Any further dimensions may be
    arbitrary. The result will have the dimensions of idx.
    Axis determines at what level of data the operations should be done.
    '''
    new_idx = idx
    ddim = axis+1
    if axis < 0 :
        ddim = data.ndim + axis + 1
    for i in range(ddim,idx.ndim):
        new_idx = ak.flatten(new_idx, axis=-1)
    result = data[new_idx]
    layout = GetLayout(idx)

    return RebuildFromLayout(result, layout, axis)

def RangeArray(arr, low, hi) :
    return arr[(arr > low) & (arr < hi)]

def GetLayout(array) :
    divisions = []
    for i in reversed(range(1,array.ndim)) :
        if i < 2 :
            divisions.append(ak.num(array, axis = i))
        else :
            #divisions.append(ak.flatten(ak.num(array, axis = i)))
            divisions.append(ak.ravel(ak.num(array, axis = i)))
    return divisions

def RebuildFromFlat(arr_flat, divs) :
    result = arr_flat
    for div in divs :
       result  = ak.unflatten(result, div, axis=0)
    return result

def RebuildFromLayout(arr, divs, axis=-1):
    ddim = axis
    if axis < 0 :
        ddim = arr.ndim + axis
    while  ddim > 0 :
        arr = ak.flatten(arr, axis=ddim)
        ddim -= 1
    return RebuildFromFlat(arr, divs)

def ClipData(arr, nelements) :
    '''
    Clips arr at nfields inner most dimension according to nfields
    values as counts of elements at the axis.
    '''
    n = ak.sum(nelements)
    idcs = ak.Array(np.ones(n))
    # unflatten
    idcs = ak.unflatten(idcs,ak.ravel(nelements))
    for idim in range(nelements.ndim-1) :
        idcs = ak.unflatten(idcs,ak.ravel(ak.num(nelements,axis=-idim-1)))
    idcs = ak.local_index(idcs)

    return arr[idcs]



def IdxToMask(orig, idxs) :
    '''
    Builds output of the shape of the original orig. Takes indexes
    of the same shape up to last dimension. Last dimension specifies
    index within that dimension of item to be included (True), the
    rest is masked out (False).
    Only useful when looking for an inverse of such a mask. Otherwise,
    direct indexing works.
    '''

    # build layout of the original array
    divisions = GetLayout(orig)

    n = ak.count(orig)
    glob_idx = ak.Array(range(n))
    glob_idx = RebuildFromFlat(glob_idx, divisions)

    mask = np.zeros(n, dtype=np.bool_)
    #print(f'{mask = }')
    # print(f'{orig = }')
    # print(f'{glob_idx[idxs] = }')
    mask[ak.flatten(glob_idx[idxs], axis=None)] = True

    mask = RebuildFromFlat(mask, divisions)

    return mask


def AverageWfms(d,apply_offset=True, ticks_per_us=150) :
    '''
    Requires data to have a set of wfms and wfm_time.
    dim(wfms) = dim(wfm_time)+1
    '''

    wfms=d.wfms
    if apply_offset :
        ### apply offsets
        # get offsets w.r.t. min trigger time
        offsets = d.wfm_time - ak.min(d.wfm_time,axis=-1)
        offsets = ak.values_astype(offsets*ticks_per_us,np.int32)
        #mean_offset = ak.mean(offsets,axis=-1)
        max_roll=ak.max(offsets)
        width = ak.ravel(ak.num(wfms,axis=-1))[0]
        # remove pedestal
        wfms = wfms - 1500. # PDSP prod4 MC pedestal
        # pad wfms before shifting
        wfms = ak.fill_none(ak.pad_none(wfms,width+max_roll,axis=-1), 0.)
        wfms=Roll(wfms,offsets)

    return ak.sum(wfms, axis=-2) #/ ak.num(wfms, axis=-2)

def Roll(data, offsets) :
    idcs = ak.local_index(data, axis=-1)
    #idcs.show(limit_cols=100,type=True)
    # shift indices to the right = subtract offset!
    idcs = (idcs-offsets)%ak.num(data,axis=-1)
    #idcs.show(limit_cols=100,type=True)
    return data[idcs]
