#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

import tools as tls


# use batch graphics mode
mpl.use('agg')


def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())
    fname = args[1]

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    #import selections as sel
    import dt_offsets_by_tag as offsets
    import selections as sel

    br = tree.arrays(['match_trk_idx', 'trk_T0', 'trk_hasT0', 'trk_length', 'trk_mcMuIdx_best', 'gen_pos'])

    matches = br['match_trk_idx']
    t0 = br['trk_T0'][matches]*1e-3
    hasT0 = br['trk_hasT0'][matches]
    mcIdx = br['trk_mcMuIdx_best'][matches]
    hasMc = mcIdx > -1
    mcT = br['gen_pos'][mcIdx[hasMc]]*1e-3
    dt = t0[hasMc] - mcT
    dt_corrected = tls.CorrectOffsetsByTag(dt, hasT0[hasMc], offsets.offsets)


    print(f'{t0    = }')
    print(f'{hasT0 = }')
    print(f'{mcIdx = }')
    print(f'{t0   [hasMc] = }')
    print(f'{hasT0[hasMc] = }')
    print(f'{mcIdx[hasMc] = }')



    plt.xlabel(r'$\Delta T_\text{T0-MC}$ [$\mu$s]')
    common_settings={'bins':240, 'range':(-30,30), 'histtype':'step', 'cla':False, 'stats':False}
    counts,bins = plot(dt,           label='Uncorrected', **common_settings)
    counts,bins = plot(dt_corrected, label='Corrected',   **common_settings)
    plt.legend()
    plt.savefig(outpref+'compare_dt_corrected_uncorrected.png')


    # split by tag
    tags = {1:'orig', 2:'APAIn', 4:'APAOut', 8:'CPA'}
    dt_by_tag = [ dt[hasT0[hasMc] == i] for i in tags.keys() ]
    dt_corrected_by_tag = [ dt_corrected[hasT0[hasMc] == i] for i in tags.keys() ]

    plt.cla()
    for label,data in zip(tags.values(),dt_by_tag) :
        counts,bins = plot(data, label=label, **common_settings)
    plt.legend()
    plt.savefig(outpref+'uncorrected_dt_by_tag.png')

    plt.cla()
    for label,data in zip(tags.values(),dt_corrected_by_tag) :
        counts,bins = plot(data, label=label, **common_settings)
    plt.legend()
    plt.savefig(outpref+'corrected_dt_by_tag.png')



def plot(data, fname=None, stats=True, clf=False, cla=True, xlabel=False, ylabel=False, legend=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()
    if cla :
        plt.cla()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)

    data = ak.nan_to_none(ak.ravel(data))
    data = ak.drop_none(data)
    counts,bins,bars = plt.hist(data, **kwargs)

    if legend :
        plt.legend()


    if stats :
        labels=('Entries','Mean','Std')
        st = (ak.count(data), ak.mean(data), ak.std(data))
        tls.PlotStatbox( dict(zip(labels,st)) )


    if fname not in (None, '') :
        plt.savefig(outpref + fname)
    return counts, bins




if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
