#!/bin/env python

import sys, os, argparse
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

import tools as tls
import selections as sel

# use batch graphics mode
mpl.use('agg')

def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('stoptagfname', nargs='?', default='')
    #parser.add_argument('-d', default='pdsp')
    parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()

    #sel.SetDetector(args.d)

    MAX_EVENTS = args.nmax
    PER_ITERATION = 10000

    print('Input:', args.fname)
    if args.stoptagfname != '' :
        print('Input 2:', args.stoptagfname)

    to_read = ['trk_hasT0', 'trk_mcMuIdx_best',
               'trk_endPoint', 'trk_startPoint',
               'trk_endTpc', 'trk_startTpc',
               'trk_length', 'trk_matched',
               'mc_length_tpcAV',
               'mc_exited_tpcAV', 'end_pos',
               'mc_endPoint_tpcAV']


    count_entries = 0
    counter = 0

    def process_tree(tree) :
        nonlocal count_entries, counter
        if MAX_EVENTS != -1 and count_entries >= MAX_EVENTS :
            return 0
        print(f'Reading data in iteration {counter}')
        count_entries += len(tree)
        counter += 1


        mc_stopped_mask = tree.mc_exited_tpcAV == 0
        #trksPerStoppedMu = sel.BuildRecoPerTrue(tree, tree.trk_mcMuIdx_best)[mc_stopped_mask]

        #trksPerStoppedMu.show()
        trk_masks = [
            tree.trk_length > 20.,
            (tree.trk_length > 20.) & (tree.trk_hasT0 > 0),
            tree.trk_matched > -1,
        ]
        labels = [
            'long',
            'T0\'d long',
            'matched',
        ]

        print('='*30)
        print(f'Efficiency for tracks')
        print( '---------------------')
        for label,trk_mask in zip(labels, trk_masks) :
            trkTrueStopper = mc_stopped_mask[tree.trk_mcMuIdx_best[trk_mask]]
            trkStopped     = tree.trk_stopped[trk_mask] == 1

            eff = ak.sum(trkTrueStopper & trkStopped)/ak.sum(trkTrueStopper)

            print(f' {label:15} = {eff:.3f}')
        print('='*30)


        # purity: number of selected true stoppers out of selected stoppers
        trkTrueStopper = mc_stopped_mask[tree.trk_mcMuIdx_best]
        trkStopped     = tree.trk_stopped == 1
        purity = ak.sum(trkStopped & trkTrueStopper)/ak.sum(trkStopped)
        print(f'Purity = {purity:.3f}')
        print('='*30)

        print('Efficiency and purity by tag:')
        taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
        for itag,taglabel in enumerate(taggers, 1) :
            isTag = tree.trk_hasT0 == itag

            trkTrueStopper = mc_stopped_mask[tree.trk_mcMuIdx_best[isTag]]
            trkStopped     = (tree.trk_stopped == 1)[isTag]

            eff = ak.sum((trkTrueStopper & trkStopped))/ak.sum(trkTrueStopper)
            pur = ak.sum((trkTrueStopper & trkStopped))/ak.sum(trkStopped)
            print(f'{taglabel:16}: {eff:4.2f} '
                  + f'{pur:4.2f}')
        print('='*30)


        return 1


    if args.stoptagfname == '' :
        iterator = ur.iterate(args.fname+':OpDetAna/opdetana', expressions=to_read+['trk_stopped'], step_size=PER_ITERATION)
        for tree in iterator :
           if not process_tree(tree) :
               break
    else :
        iterators = (ur.iterate(args.fname+':OpDetAna/opdetana', expressions=to_read, step_size=PER_ITERATION),
                     ur.iterate(args.stoptagfname+':OpDetAna/opdetana', expressions=['trk_stopped'], step_size=PER_ITERATION) )
        for treea,treeb in  zip(*iterators):
            tree = zip(treea, treeb)
            if not process_tree(tree) :
                break



if __name__ == '__main__':
    print('Running processing...')
    run()
