#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

from mpl_toolkits.mplot3d.art3d import Poly3DCollection


# use batch graphics mode
#mpl.use('agg')


def run():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outpref',nargs='?',default='')
    # parser.add_argument('-d', default='pdsp')
    # parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()
    # make sure the output directory exists
    os.makedirs(os.path.dirname(args.outpref), exist_ok=True)


    fname = args.fname
    global outpref
    outpref = args.outpref

    # get input tree
    tree = ur.open(fname+':OpDetAna/opdetana')

    # get relevant data
    data = tree.arrays(['trk_hasT0', 'trk_stopped'])

    stopped_mask = data.trk_stopped == 1

    plot(data.trk_hasT0, 'tag_distro')
    plt.cla()
    plot(data.trk_hasT0[stopped_mask], 'tag_distro_stopped')



def plot(data, label) :
    global outpref
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]

    plt.hist(ak.ravel(data), bins=7, range=(0.5,7.5))
    ax = plt.gca()
    ax.set_xticks(list(range(1,8)))
    ax.set_xticklabels(taggers, rotation=-20, ha='left')
    plt.savefig(outpref+f'{label}.png')


if __name__ == '__main__':
    print('Running processing...')
    run()
