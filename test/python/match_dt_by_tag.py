#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak
import argparse


# use batch graphics mode
mpl.use('agg')


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outpref',nargs='?',default='')
    parser.add_argument('--pe-cut',default='100',type=float,help='Threshold on number of flash PEs to select PDS flash for matching to TPC.')
    args=parser.parse_args()


    #print(mlp.matplotlib_fname())
    fname = args.fname
    print('Input:', fname)

    global OUTPREF,PE_CUT

    PE_CUT=args.pe_cut
    OUTPREF = args.outpref
    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(OUTPREF))
    os.makedirs(os.path.dirname(OUTPREF), exist_ok=True)




    with ur.open(fname+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    global PE_CUT, OUTPREF
    #import selections as sel
    import tools as tls

    trk_hasT0 = tree['trk_hasT0'].array()
    trk_t0    = tree['trk_T0'].array()*1e-3

    # preselect
    T0Mask    = trk_hasT0 > 0
    trk_hasT0 = trk_hasT0[T0Mask]
    trk_t0    = trk_t0[T0Mask]

    flash_t  = tree['flash_abs_time'].array()
    flash_pe = tree['flash_tot_pe'].array()

    # preselect
    peMask   = flash_pe > PE_CUT
    flash_t  = flash_t[peMask]
    flash_pe = flash_pe[peMask]

    # pairing trk with T0 with any flash
    trk_idx = ak.local_index(trk_t0)
    flash_idx = ak.local_index(flash_t)
    all_pairs = ak.cartesian({'trk':trk_idx, 'flash':flash_idx},nested=True)
    all_pairs_dt = tls.Slice(trk_t0,all_pairs['trk']) - tls.Slice(flash_t,all_pairs['flash'])

    all_pairs_tags = tls.Slice(trk_hasT0, all_pairs['trk'])

    # tags = {1:'orig', 2:'APAIn', 4:'APAOut', 8:'CPA'} # older tagging (pre-v7)
    # PDSP
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
    npeaks_arr = [1,1,1,1,2,2,2]
    fit_ranges = [(-50,75.), (-1.,1.), (-1.,1.), (-10,5), (-6,-1), (-10,0), (-10,0)] # data PDSP
    # PDHD
    taggers =  ['pandora', 'crttag', 'crtreco', 'CPA', 'APAIn', 'APAOut' ]
    npeaks_arr = [1,1,2,2,2,2]
    fit_ranges = [(25,50.), (-1.,1.), (-1.,1.), (-25.,75.), (25.,50.), (25,50)]


    tags = dict( enumerate(taggers, 1) )
    all_pairs_dt_by_tag = [ all_pairs_dt[all_pairs_tags == i] for i in tags.keys() ]


    # PDSP
    # PDHD
    #fit_ranges = [(-100,100.), (-1.,1.), (-1.,1.), (-2,12), (0,12), (-1,8), (-1,8)]

    tag_labels = list(tags.values())
    for i in range(len(all_pairs_dt_by_tag)) :
        dts = all_pairs_dt_by_tag[i]
        label = tag_labels[i]
        counts,bins = plot(dts, f'{label}_dt_tpc_pds_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=200, range=(-100,100))

        if label in ['anodepiercerst0','CPA','APAIn','APAOut'] :
            counts,bins = plot(dts, f'{label}_dt_tpc_pds_zoom2.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=100, range=fit_ranges[i])
        if label in ['CPA'] :
            counts,bins = plot(dts, f'{label}_dt_tpc_pds_zoom3.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=150, range=fit_ranges[i])

        doFit = True
        if label in ['crttag','crtreco'] :
            doFit = False

        if doFit :
            try :
                npeaks = npeaks_arr[i]
                x,y,pars = fitData(counts, bins, fit_range=fit_ranges[i], npeaks = npeaks)
                params = pars[0]
                errors = np.sqrt(np.diag(pars[1]))
                plt.plot(x,y, label='fit')
                plt.legend(loc='upper left')
                labels=[]
                for i in range(npeaks) :
                    labels += [f'ampl{i}', f'mean{i}', f'sigma{i}']
                labels += ['const']
                tls.PlotFitResults((labels,*pars))

                plt.savefig(f'{OUTPREF}{label}_fit_dt_tpc_pds_zoom.png')
            except :
                print(repr(sys.exception()))
                print(f"Can't fit {taggers[i]}.")
                #throw(


def fitData(counts, bins, fit_range=None, npeaks=1) :
    #from scipy import curve_fit as cvf
    from scipy.stats import norm
    import tools as tls

    range_mask = (bins > fit_range[0]) & (bins < fit_range[1])

    counts = counts[range_mask[:-1]]
    bins   = bins[range_mask]

    binmax = np.argmax(counts)
    guess = []
    for i in range(npeaks) :
        guess += [counts[binmax], bins[binmax]+i*10, 3.]

    guess += [counts[0]]

    return tls.Fit(counts, bins, tls.MultiNormal, p0=guess)



def plot(data, fname, clf=True, xlabel=False, ylabel=False, **kwargs) :
    global OUTPREF

    if clf :
        plt.clf()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)
    counts,bins,bars = plt.hist(ak.ravel(data), **kwargs)
    plt.savefig(OUTPREF + fname)
    return counts, bins

def isCPAcrosser(array) :
    # input is array of pairs: start and end X coordinate of a track
    return np.sign(array['start']) != np.sign(array['end'])


if __name__ == '__main__':
    print('Running processing...')
    run()
