import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak

import matplotlib as mpl
#mpl.use('qtagg')


def run():

    fname=sys.argv[1]


    with ur.open(fname+':OpDetAna/opdetana') as t :
        b = t.arrays(['trk_hasT0', 'trk_nsps', 'trk_sp'])


    hasT0 = b.trk_hasT0 > 0
    tagtypes = b.trk_hasT0[hasT0]
    nsps = b.trk_nsps[hasT0]
    sps  = b.trk_sp[hasT0]
    sps  = sps[ak.local_index(sps, axis=-2) < nsps]

    fig,axes = plt.subplots(1,3)

    type_names = {1:'APAIn', 2:'CPA', 3:'APAOut', 4:'CRTreco', 5:'AnodePiercer', 6:'pandora', 7:'CRTtag'}

    for evtsps,evttagtypes in zip(sps,tagtypes) :
        for trksps,trktagtype in zip(evtsps,evttagtypes) :
            typename = type_names[trktagtype]
            plotTrack(axes, trksps, typename)


def plotTrack(axes, sps, typename):

    proj = [(0,1), (2,1), (2,0)]
    proj_names = ['xy', 'zy', 'zx']
    labels = [('x [cm]','y [cm]'),('z [cm]','y [cm]'),('z [cm]','x [cm]')]

    for i in range(3) :
        ax = axes[i]
        ax.cla()
        ax.plot(sps[...,proj[i][0]],sps[...,proj[i][1]])
        ax.set_xlabel(labels[i][0])
        ax.set_ylabel(labels[i][1])

    plt.gcf().suptitle(typename)

    plt.savefig('plots/python/track_interactive.png')
    input()

if __name__ == '__main__' :
    run()
