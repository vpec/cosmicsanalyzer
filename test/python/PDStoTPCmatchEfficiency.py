#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak

import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/pds_to_tpc_match_efficiency/'

import selections as sel, tools as tls




def process_tree(tree) :
    tagType        = tree['trk_hasT0'].array()
    hasT0          = tagType > 0
    longTrk        = tree['trk_length'].array() > 20.
    flashesPerTrue = sel.BuildFlashPerTrue(tree)
    trkMcIdx       = sel.GetMCMatched(tree)
    # flashes matched to trk in truth
    flashesPerTrk  = sel.GetTrueFlashesPerTrack(tree, trkMcIdx, flashesPerTrue)

    flashT   = tree['flash_abs_time'].array()
    trkT0    = tree['trk_T0'].array()*1e-3
    matchedTrk   = tree['match_trk_idx'].array()
    matchedFlash = tree['match_flash_idx'].array()

    # print(ak.sum(ak.any(flashesPerTrk[hasT0 & longTrk],axis=-1),axis=-1))

    # long track with T0 has at least one flash
    matchable = hasT0 & longTrk & ak.any(flashesPerTrk, axis=-1)
    # at least one matchable flash is the matched flash
    # FIXME: implement longTrk into matchedTrk
    matched_matchable = matchable[matchedTrk]
    has_true_match = ak.any(flashesPerTrk[matchedTrk] == matchedFlash, axis=-1)
    matched_matchable_true_match = matched_matchable & has_true_match

    count_matched_matchable = ak.sum(matched_matchable_true_match)
    count_matchable = ak.sum(matchable)
    count_matched = ak.count(matchedTrk)
    eff = count_matched_matchable/count_matchable
    pur = count_matched_matchable/count_matched

    print('='*30)
    print(f'Matchable         = {count_matchable}')
    print(f'Matched_Matchable = {count_matched_matchable}')
    print(f'Matched           = {count_matched}')
    print('-'*30)
    print(f'Efficiency        = {eff:.3}')
    print(f'Purity            = {pur:.3}')
    print('='*30)

    print('Efficiency and purity by tag:')
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
    for itag,taglabel in enumerate(taggers, 1) :
        isTag = tagType == itag
        tag_matched = isTag[matchedTrk]
        tag_matchable = matchable & isTag
        tag_matched_matchable = matched_matchable & isTag[matchedTrk]
        tag_matched_matchable_true_match =tag_matched_matchable & has_true_match
        print(f'{taglabel:16}: {ak.sum(tag_matched_matchable)/ak.sum(tag_matchable):4.2f} '
              + f'{ak.sum(tag_matched_matchable)/ak.sum(tag_matched):4.2f}')
    print('='*30)



    #dt_label    = '$\Delta$T$_{T0-flash}$ [$\mu$s]'
    trk_label   = 'No. tracks per event'
    flash_label = 'No. flashes per track'
    from tools import DataToPlot as Data
    to_plot = [
        Data(ak.num(matchedTrk, axis=-1),
             title='Number of reco tracks matched to flash',
             xlabel=trk_label ,
             bins=50, range=(-0.5,49.5),
             outs='matched_trks_per_event.png'),
        Data(ak.sum(matched_matchable, axis=-1),
             title='Number of reco tracks matched to matchable flash',
             xlabel=trk_label,
             bins=50, range=(-0.5,49.5),
             outs='matched_matchable_trks_per_event.png'),
        Data(ak.sum(matchable, axis=-1),
             title='Number of reco tracks matchable to flash',
             xlabel=trk_label,
             bins=50, range=(-0.5,49.5),
             outs='matchable_trks_per_event.png'),
        Data(ak.num(flashesPerTrk[matchedTrk], axis=-1),
             title='Number of true flashes per matched reco track',
             xlabel=flash_label,
             bins=10,range=(-0.5,9.5),
             outs='number_true_flashes_per_matched_track.png'),

        Data(ak.num(flashesPerTrk, axis=-1),
             title='Number of true flashes per reco track',
             xlabel=flash_label,
             bins=10,range=(-0.5,9.5),
             outs='number_true_flashes_per_track.png'),
    ]


    for d in to_plot :
        d.outs = outpref+d.outs
        tls.plotHist(d)



def run(args):
    # print(args[1])
    # print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    # make sure the output directory exists
    #print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    #print('Running processing...')
    run(sys.argv)
