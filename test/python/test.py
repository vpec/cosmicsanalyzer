import sys

import uproot as ur, awkward as ak

import selections as sel

with ur.open(sys.argv[1]+':OpDetAna/opdetana') as tree:
    #print(sel.GetMCMatched(tree))
    #print(sel.BuildRecoPerTrue(tree))
    #print(sel.GetCPAMatchedPairs(tree))
    #print(sel.MaskTrueCPACrossers(tree))
    print(sel.MaskTrueCPACrossers(tree))
