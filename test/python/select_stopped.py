#!/bin/env python

import sys, os, argparse
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak
import vector
vector.register_awkward()

import tools as tls
import selections as sel

# use batch graphics mode
mpl.use('agg')


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outfname')
    parser.add_argument('-d', default='pdsp')
    parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()

    fname = args.fname
    outfname = args.outfname
    whichdet = args.d
    sel.SetDetector(whichdet)

    MAX_EVENTS = args.nmax
    PER_ITERATION = 10000

    print('Input:', fname)
    # make sure the output directory exists
    print('Output:', outfname)
    outdir=os.path.dirname(outfname)
    if len(outdir) > 0 :
        os.makedirs(outdir, exist_ok=True)


    # output root file to hold track stopping selection
    outf = ur.recreate(outfname)
    outf.mktree('OpDetAna/opdetana', {'trk_stopped':'var*bool'})

    to_read = ['trk_hasT0',
               'trk_endPoint', 'trk_startPoint',
               'trk_endTpc', 'trk_startTpc',
               ]

    ## This may not work for string with wilde cards
    total_entries = 0
    for entries in ur.num_entries(fname+":OpDetAna/opdetana") :
        total_entries += entries[2]
    print(f'Will be processing total of {entries} entries.')

    count_entries = 0
    counter = 0
    for tree in ur.iterate(fname+':OpDetAna/opdetana', expressions=to_read, step_size=PER_ITERATION) :
        if MAX_EVENTS != -1 and count_entries >= MAX_EVENTS :
            break
        count_entries += len(tree)
        print(f'Reading data in iteration {counter}')
        reco_stopped_mask = selectRecoStopped(tree)
        # fill stopped selection
        outf['OpDetAna/opdetana'].extend({'trk_stopped':reco_stopped_mask})
        counter +=1

    print(f'Processed {count_entries} entries.')
    print(f'Closing output tree ROOT file with {outf["OpDetAna/opdetana"].num_entries} entries')
    outf.close()


def selectRecoStopped(br) :
    reco_end      = createVector4D(br.trk_endPoint)
    reco_start    = createVector4D(br.trk_startPoint)
    trk_tpc_start = br.trk_startTpc
    trk_tpc_end   = br.trk_endTpc
    hasT0         = br.trk_hasT0 > 0

    bottom_mask = getBottomMask(reco_start, reco_end)
    bottom_end = ak.where(bottom_mask, reco_end, reco_start)
    bottom_tpc = ak.where(bottom_mask, trk_tpc_end, trk_tpc_start)
    reco_tpc   = ak.where(bottom_end['x'] < 0, 1, 2)

    correct_tpc_mask = reco_tpc == (bottom_tpc % 4)
    reco_stopped_mask = hasT0 & correct_tpc_mask & insideTpc(bottom_end, 50.) & notTrimmed(bottom_end, 10)

    return reco_stopped_mask


#############
### Tools ###
#############
def createVector4D(data) :
    # expects multi-D array with last dimension of len 4
    return ak.zip({'x':data[...,0],
                   'y':data[...,1],
                   'z':data[...,2],
                   't':data[...,3] },
                  with_name='Vector4D')


def insideTpc(end, buffer, whichdet='pdsp') :
    import selections as sel
    a = [low for low,_  in sel.boundary[whichdet] ]
    b = [hi  for   _,hi in sel.boundary[whichdet] ]
    end3d = end.to_3D()
    coords=(('x',0),('y',1),('z',2))
    result=True
    for coord,idx in coords :
        result = result & (end3d[coord] > a[idx] + buffer) & (end3d[coord] < b[idx] - buffer)
    return result
           # (end3d['y'] > a[1] + buffer) & (end3d['y'] < b[1] - buffer) & \
           # (end3d['z'] > a[2] + buffer) & (end3d['z'] < b[2] - buffer)

def getBottomMask(start, end): # returns 1 for end point at the bottom, 0 otherwise
    return end['y'] < start['y']

def notTrimmed(end, buffer) :
    return (end['t'] < 6000 - buffer) & (end['t'] > buffer)


if __name__ == '__main__':
    print('Running processing...')
    run()
