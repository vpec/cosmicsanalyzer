#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/compare_T0_data_to_mc/v2/'

import selections as sel, tools as tls
from tools import DataToPlot as DTP
from selections import T0Type
import math

import copy


def plotWLabels(data, labels, width=1., offset=0., scale=1., type='bar', log=False, **kwargs):
    d = np.unique(ak.ravel(data), return_counts=True)
    dd = { key:val for key,val in zip(*d) }
    dd[T0Type.CPA] /= 2
    x = np.arange(len(labels))
    y = [ dd[y]*scale for y in labels.keys() ]
    yerr = [ math.sqrt(val*scale) for val in y ]


    if type == 'bar' :
        plt.bar(x+offset, y, width=width, **kwargs)
    elif type == 'steps':
        plt.stairs(y, np.append(x,[x[-1]+1])-0.5, **kwargs)
    elif type == 'errorbar':
        plt.errorbar(x, y, yerr, fmt='o', capsize=4, **kwargs)

def plotComparison(datatree, mctree, d) :

    dstring = d.data

    data = datatree.arrays('a', aliases={'a':dstring})['a']
    mc   =   mctree.arrays('a', aliases={'a':dstring})['a']

    outs = outpref
    if (d.outs is None) :
        outs += d.data
    else :
        outs += d.outs

    events_mc = ak.num(mc, axis=0)
    events_data = ak.num(data, axis=0)
    entries_mc = ak.count(mc)
    entries_data = ak.count(data)

    # plotWLabels(data, d.bins, label=f'data, {entries_data}', width=0.3, offset=-0.15, scale=1./events_data)
    # plotWLabels(mc, d.bins, label=f'MC, {entries_mc}', width=0.3, offset=0.15, scale=1./events_mc)
    plotWLabels(mc,   d.bins, type='bar', label=f'MC',
                width=0.5, offset=0., scale=1./events_mc)
    plotWLabels(data, d.bins, type='errorbar', label=f'data',
                width=0.3, offset=0.3, scale=1./events_data, color='darkorange')
    plt.text(0.2, 0.965, f'{entries_mc:8} entries/{events_mc:7} events\n'+
             f'{entries_data:8d} entries/{events_data:7d} events',
             va='top', ha='left', linespacing=1.6,
             transform=plt.gca().transAxes,
             fontfamily='monospace')

    plt.xticks(np.arange(len(d.bins)), d.bins.values())
    plt.xlabel('T0 tag')
    plt.ylabel('count/no.events')
    #plt.yscale('log')
    plt.legend()

    outs+'.png'

    plt.savefig(outs)


def longTracks(d) :
    return ak.sum(d>20., axis=-1)

def plotComparisonEachTag(data, mc, label, d, **kwargs) :
    data_toplot = copy.deepcopy(d)
    mc_toplot = copy.deepcopy(d)

    data_toplot.data = data
    mc_toplot.data = mc

    events_mc = ak.num(mc, axis=0)
    events_data = ak.num(data, axis=0)
    entries_mc = ak.count(mc)
    entries_data = ak.count(data)

    #print(data, mc)

    labels = {'MC':   f'MC:   {entries_mc:8d} entries /{events_mc:7d} events',
              'data': f'data: {entries_data:8d} entries /{events_data:7d} events'}


    plt.clf()
    tls.plotHist(mc_toplot, outs=None, label=labels['MC'], scale=1./events_mc, fill=True, **kwargs)
    tls.plotHist(data_toplot, outs=None, label=labels['data'], scale=1./events_data, histtype='step', **kwargs)
    plt.legend(prop={'family':'monospace'})

    outs=outpref
    if d.outs is not None :
        outs += f'{d.outs}'
    else :
        outs += d.data[0] if isinstance(d.data, tuple) else d.data
    outs += f'_{label}.png'

    plt.savefig(outs)


def plotComparisonAllTags(datatree, mctree, d, *args) :
    tags_data = datatree['trk_hasT0'].array()
    tags_mc   =   mctree['trk_hasT0'].array()

    func=None
    dstring=''
    if isinstance(d.data, tuple) :
        dstring=d.data[0]
        func=d.data[1]
    else:
        dstring=d.data

    data = datatree[dstring].array()
    mc   =   mctree[dstring].array()

    if func is not None :
        data = func(data, *args)
        mc = func(mc, *args)

    tag_types = (T0Type.Orig, T0Type.CPA, T0Type.APAIn, T0Type.APAOut)
    labels = { T0Type.Orig:'Default', T0Type.CPA:'CPA', T0Type.APAIn:'APAIn', T0Type.APAOut:'APAOut' }

    for i in range(len(tag_types)) :
        label=labels[tag_types[i]]
        plotComparisonEachTag(data[tags_data == tag_types[i]],
                              mc[tags_mc == tag_types[i]],
                              label,
                              d, title=f'{d.title}: {label}')
    plotComparisonEachTag(data[tags_data > 0],
                          mc[tags_mc > 0],
                          'Any',
                          d, title=f'{d.title}: {label}')



def process_tree(datatree, mctree) :
    labels = { T0Type.Orig:'Default', T0Type.CPA:'CPA', T0Type.APAIn:'APAIn', T0Type.APAOut:'APAOut' }

    toCompare = [
        DTP('trk_hasT0', title='T0 tag', xlabel='T0 tag', bins=labels, log=True),
    ]

    for d in toCompare :
        plotComparison(datatree, mctree, d)


    # compare distributions for individual T0 tags
    toCompare = [
        DTP('trk_length', title='Track length', xlabel='Track length [cm]', bins=150, range=(0,1500)),
        DTP('trk_T0', title='Reconstructed T0', xlabel='T0 [ns]', bins=130, range=(-2.6e6, 2.6e6)),
    ]

    for d in toCompare :
        d.clf = False
        d.cla = False
        d.stats = False
        d.ylabel='Tracks per event'

        plotComparisonAllTags(datatree, mctree, d)


    def getCoord(data, coord) :
        return data[..., coord]

    toCompare = [
        DTP(('trk_startPoint', getCoord), title='Track start position', bins=140, range=(-7e2,7e2)),
        DTP(('trk_endPoint', getCoord), title='Track end position', bins=140, range=(-7e2,7e2)),
    ]

    coord_label = ('x', 'y', 'z')
    bins=(200, 125, 200)
    limits = ((-400,400), (-50,650), (-50,750))

    for d in toCompare :
        d.clf = False
        d.cla = False
        d.stats = False
        d.ylabel='Tracks per event'

        for coord in range(3) :
            d.outs = f'{d.data[0]}/{d.data[0]}_{coord_label[coord]}'
            d.xlabel = f'{coord_label[coord]} [cm]'
            d.bins = bins[coord]
            d.range = limits[coord]
            plotComparisonAllTags(datatree, mctree, d, coord)



def run(args):
    print(args[1], args[2])

    if len(args) > 3 :
        global outpref
        outpref = args[3]

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    os.makedirs(os.path.dirname(outpref)+'/trk_startPoint', exist_ok=True)
    os.makedirs(os.path.dirname(outpref)+'/trk_endPoint', exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as datatree:
        with ur.open(args[2]+':OpDetAna/opdetana') as mctree:
            process_tree(datatree, mctree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
