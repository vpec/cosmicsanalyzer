#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak


# use batch graphics mode
mpl.use('agg')


def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())
    fname = args[1]

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    import selections as sel
    global outpref

    #trk_to_mc = sel.GetMCMatched(tree)
    trk_to_mc = tree['trk_mcMuIdx_best'].array()
    hasMcMask = trk_to_mc > -1
    trk_tag   = tree['trk_hasT0'].array()
    trk_t0    = tree['trk_T0'].array()*1e-3
    mc_t      = tree['mc_startPoint_tpcAV'].array()[...,3]*1e-3
    #stopped_mask = tree['trk_stopped'].array() == 1

    trkMask = hasMcMask & (trk_tag > 0) #& stopped_mask
    dt = trk_t0[trkMask] - mc_t[trk_to_mc[trkMask]]

    # print(f'{trk_to_mc = }')
    # print(f'{hasMcMask = }')
    # print(f'{trk_tag   = }')
    # print(f'{trk_t0    = }')
    # print(f'{mc_t      = }')
    # print(f'{trkMask   = }')
    # print(f'{mc_t[trk_to_mc[trkMask]] = }')
    # print(f'{len(dt)   = }')
    # print(f'{ak.num(dt)= }')
    # print(f'{ak.sum(trk_tag > 0, axis=1)= }')

    plot(dt, 'all_t0_dt.png', bins=100)
    plot(dt, 'all_t0_dt_zoom.png', bins=140, range=(-10,60))

    # PDSP
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
    # PDHD
    taggers =  ['pandora', 'crttag', 'crtreco', 'CPA', 'APAIn', 'APAOut' ]
    #tags = {1:'orig', 2:'APAIn', 4:'APAOut', 8:'CPA'}
    tags = dict(enumerate(taggers,1))

    mcMaskTags = trk_tag[trkMask]
    dts = [ dt[mcMaskTags == i] for i in tags.keys() ]

    for idt,label in zip(dts,tags.values()) :
        plot(idt, f'{label}_t0_dt.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=120, range=(-3000,3000))
        plot(idt, f'{label}_t0_dt_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=100, range=(-100,100))

    # start_x = tree['trk_startPoint'].array()[...,0]
    # end_x   = tree['trk_endPoint'  ].array()[...,0]
    # orig_mask = ak.zip({'start': start_x[hasMcMask & (trk_tag == 1)],
    #                     'end'  : end_x  [hasMcMask & (trk_tag == 1)]})
    # orig_cpa_mask = isCPAcrosser(orig_mask)
    # dt_orig_cpa = dt[mcMaskTags == 1][orig_cpa_mask]
    # dt_orig_apa = dt[mcMaskTags == 1][~orig_cpa_mask]

    # plot(dt_orig_cpa, f'orig_cpa_t0_dt_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=140, range=(-10,60))
    # plot(dt_orig_apa, f'orig_apa_t0_dt_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=140, range=(-10,60))


    # # print info on APA piercers
    # apa_start_x = start_x[hasMcMask & (trk_tag == 1)][~orig_cpa_mask]
    # apa_end_x   =   end_x[hasMcMask & (trk_tag == 1)][~orig_cpa_mask]
    # cpa_start_x = start_x[hasMcMask & (trk_tag == 1)][orig_cpa_mask]
    # cpa_end_x   =   end_x[hasMcMask & (trk_tag == 1)][orig_cpa_mask]

    # apa_max_x = np.maximum(abs(apa_start_x), abs(apa_end_x))
    # cpa_max_x = np.maximum(abs(cpa_start_x), abs(cpa_end_x))
    # plot(apa_max_x, 'apa_max_absx.png', bins=100, range=(350,370))
    # plot(cpa_max_x, 'cpa_max_absx.png', bins=100, range=(350,370))


def plot(data, fname, clf=True, xlabel=False, ylabel=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)
    plt.hist(ak.ravel(data), **kwargs)
    plt.savefig(outpref + fname)


def isCPAcrosser(array) :
    # input is array of pairs: start and end X coordinate of a track
    return np.sign(array['start']) != np.sign(array['end'])


if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
