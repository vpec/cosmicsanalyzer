#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/apa_tag_eff/'

import selections as sel, tools as tls

def test(tree):
    trkMcIdx = sel.GetMCMatched(tree)
    print(f'{trkMcIdx = }')

def process_tree(tree) :
    '''
    See how well original tag was working.
    - overall efficiency = tagged vs all reconstructed true muons
    - efficiency for CPA crossers = num of tagged true crossers / reconable true crossers
    - efficiency for APA crossers = dtto
    '''

    # useful data
    # trk dim
    trk_mcMuIdx            = sel.GetMCMatched(tree)
    t0_in_mask             = tree['trk_hasT0'].array() == 2
    t0_out_mask            = tree['trk_hasT0'].array() == 4
    t0_mask                = t0_in_mask | t0_out_mask

    # mc dim
    trksPerMu              = sel.BuildRecoPerTrue(tree, trk_mcMuIdx)
    muHasReco              = ak.num(trksPerMu, axis=-1) > 0

    cpaCrossers            = sel.MaskCPACrossersInMC(tree)
    apaInCrossers          = sel.MaskAPAInCrossersInMC(tree)
    apaOutCrossers         = sel.MaskAPAOutCrossersInMC(tree)

    cpaCrossersWithReco    = cpaCrossers & muHasReco
    apaInCrossersWithReco  = apaInCrossers & muHasReco
    apaOutCrossersWithReco = apaOutCrossers & muHasReco

    # true muons have a T0'd track
    layout             = tls.GetLayout(trksPerMu)
    trksPerMu_flat     = ak.flatten(trksPerMu, axis=-1)
    t0_in_perMu_mask_flat = t0_in_mask[trksPerMu_flat]
    t0_in_perMu_mask      = tls.RebuildFromFlat(ak.flatten(t0_in_perMu_mask_flat), layout)
    t0_in_perMu_any_mask  = ak.any(t0_in_perMu_mask, axis=-1)
    t0_out_perMu_mask_flat = t0_out_mask[trksPerMu_flat]
    t0_out_perMu_mask      = tls.RebuildFromFlat(ak.flatten(t0_out_perMu_mask_flat), layout)
    t0_out_perMu_any_mask  = ak.any(t0_out_perMu_mask, axis=-1)
    t0_perMu_mask_flat = t0_mask[trksPerMu_flat]
    t0_perMu_mask      = tls.RebuildFromFlat(ak.flatten(t0_perMu_mask_flat), layout)
    t0_perMu_any_mask  = ak.any(t0_perMu_mask, axis=-1)

    # tagged trks dim
    ## APA In
    t0_in_mcMuIdx            = trk_mcMuIdx[t0_in_mask]
    t0_in_cpaCrosser_mask    = cpaCrossersWithReco[   t0_in_mcMuIdx]
    t0_in_apaInCrosser_mask  = apaInCrossersWithReco[ t0_in_mcMuIdx]
    t0_in_apaOutCrosser_mask = apaOutCrossersWithReco[t0_in_mcMuIdx]
    ## APA Out
    t0_out_mcMuIdx            = trk_mcMuIdx[t0_out_mask]
    t0_out_cpaCrosser_mask    = cpaCrossersWithReco[   t0_out_mcMuIdx]
    t0_out_apaInCrosser_mask  = apaInCrossersWithReco[ t0_out_mcMuIdx]
    t0_out_apaOutCrosser_mask = apaOutCrossersWithReco[t0_out_mcMuIdx]
    ## APA
    t0_mcMuIdx            = trk_mcMuIdx[t0_mask]
    t0_cpaCrosser_mask    = cpaCrossersWithReco[t0_mcMuIdx]
    t0_apaInCrosser_mask  = apaInCrossersWithReco[t0_mcMuIdx]
    t0_apaOutCrosser_mask = apaOutCrossersWithReco[t0_mcMuIdx]


    # tagged and CPA crossers
    count_data = [
        ('cpa_with_reco'          ,   cpaCrossersWithReco   ),
        ('apaIn_with_reco'        ,   apaInCrossersWithReco ),
        ('apaOut_with_reco'       ,   apaOutCrossersWithReco),
        ('apa_with_reco'          ,   apaInCrossersWithReco | apaOutCrossersWithReco),
        ('tracks'                 ,   [ak.count(t0_mask)]),
        ('recod_mus'              ,   muHasReco),
        ('tagged_in'              ,   t0_in_mask),
        ('tagged_in_with_muons'   ,   t0_in_mask[trk_mcMuIdx > -1]),
        ('tagged_in_any_muons'    ,   t0_in_perMu_any_mask),
        ('tagged_in_muons'        ,   t0_in_perMu_mask),
        ('tagged_in_true_CPA'     ,   t0_in_cpaCrosser_mask),
        ('tagged_in_true_APAIn'   ,   t0_in_apaInCrosser_mask),
        ('tagged_in_true_APAOut'  ,   t0_in_apaOutCrosser_mask),
        ('tagged_in_true_APA'     ,   t0_in_apaInCrosser_mask | t0_in_apaOutCrosser_mask),
        ('tagged_out'             ,   t0_out_mask),
        ('tagged_out_with_muons'  ,   t0_out_mask[trk_mcMuIdx > -1]),
        ('tagged_out_any_muons'   ,   t0_out_perMu_any_mask),
        ('tagged_out_muons'       ,   t0_out_perMu_mask),
        ('tagged_out_true_CPA'    ,   t0_out_cpaCrosser_mask),
        ('tagged_out_true_APAIn'  ,   t0_out_apaInCrosser_mask),
        ('tagged_out_true_APAOut' ,   t0_out_apaOutCrosser_mask),
        ('tagged_out_true_APA'    ,   t0_out_apaInCrosser_mask | t0_out_apaOutCrosser_mask),
        ('tagged'                 ,   t0_mask),
        ('tagged_with_muons'      ,   t0_mask[trk_mcMuIdx > -1]),
        ('tagged_any_muons'       ,   t0_perMu_any_mask),
        ('tagged_muons'           ,   t0_perMu_mask),
        ('tagged_true_CPA'        ,   t0_cpaCrosser_mask),
        ('tagged_true_APAIn'      ,   t0_apaInCrosser_mask),
        ('tagged_true_APAOut'     ,   t0_apaOutCrosser_mask),
        ('tagged_true_APA'        ,   t0_apaInCrosser_mask | t0_apaOutCrosser_mask),
    ]


    print('='*30)
    counts = { label: ak.sum(count) for label,count in count_data }
    for label, count in counts.items():
        print(f'{label:22} = {count}')
    # for label,arr in counts:
    #     print(f'{label:20} = {ak.sum(arr)}')

    efficiencies = [
        ('eff_in_trks'     , counts['tagged_in']/ak.count(t0_mask)             ),
        ('eff_in_reco_mus' , counts['tagged_in_any_muons']/counts['recod_mus'] ),
        ('eff_in_CPA' , counts['tagged_in_true_CPA']/counts['cpa_with_reco'] ),
        ('eff_in_APAIn' , counts['tagged_in_true_APAIn']/counts['apaIn_with_reco'] ),
        ('eff_in_APAOut' , counts['tagged_in_true_APAOut']/counts['apaOut_with_reco'] ),
        ('eff_in_APA' , counts['tagged_in_true_APA']/counts['apa_with_reco'] ),
        ('eff_out_trks'     , counts['tagged_out']/ak.count(t0_mask)             ),
        ('eff_out_reco_mus' , counts['tagged_out_any_muons']/counts['recod_mus'] ),
        ('eff_out_CPA' , counts['tagged_out_true_CPA']/counts['cpa_with_reco'] ),
        ('eff_out_APAIn' , counts['tagged_out_true_APAIn']/counts['apaIn_with_reco'] ),
        ('eff_out_APAOut' , counts['tagged_out_true_APAOut']/counts['apaOut_with_reco'] ),
        ('eff_out_APA' , counts['tagged_out_true_APA']/counts['apa_with_reco'] ),
        ('eff_trks'     , counts['tagged']/ak.count(t0_mask)             ),
        ('eff_reco_mus' , counts['tagged_any_muons']/counts['recod_mus'] ),
        ('eff_CPA' , counts['tagged_true_CPA']/counts['cpa_with_reco'] ),
        ('eff_APAIn' , counts['tagged_true_APAIn']/counts['apaIn_with_reco'] ),
        ('eff_APAOut' , counts['tagged_true_APAOut']/counts['apaOut_with_reco'] ),
        ('eff_APA' , counts['tagged_true_APA']/counts['apa_with_reco'] ),
    ]

    print('='*30)
    print('Efficiencies:')
    print('-------------')
    for label, count in efficiencies :
        print(f'{label:20} = {count:.3f}')



    # purity
    # How many of the tagged tracks are muons, how many are CPA crossers and how many are APA crossers
    purities = [
        ('in_muons'   , counts['tagged_in_any_muons']/counts['tagged_in']),
        ('in_CPA'     , counts['tagged_in_true_CPA']/counts['tagged_in']),
        ('in_APAIn'   , counts['tagged_in_true_APAIn']/counts['tagged_in']),
        ('in_APAOut'  , counts['tagged_in_true_APAOut']/counts['tagged_in']),
        ('in_APA'     , counts['tagged_in_true_APA']/counts['tagged_in']),
        ('out_muons'  , counts['tagged_out_any_muons']/counts['tagged_out']),
        ('out_CPA'    , counts['tagged_out_true_CPA']/counts['tagged_out']),
        ('out_APAIn'  , counts['tagged_out_true_APAIn']/counts['tagged_out']),
        ('out_APAOut' , counts['tagged_out_true_APAOut']/counts['tagged_out']),
        ('out_APA'    , counts['tagged_out_true_APA']/counts['tagged_out']),
        ('muons'      , counts['tagged_any_muons']/counts['tagged']),
        ('CPA'        , counts['tagged_true_CPA']/counts['tagged']),
        ('APAIn'      , counts['tagged_true_APAIn']/counts['tagged']),
        ('APAOut'     , counts['tagged_true_APAOut']/counts['tagged']),
        ('APA'        , counts['tagged_true_APA']/counts['tagged']),
    ]

    print('='*30)
    print('Purities:')
    print('---------')
    for label, count in purities :
        print(f'{label:20} = {count:.3f}')

    ### plotting ###
    plt.ioff()
    for label,data in count_data :
        if not type(data) is ak.Array :
            continue
        if data.ndim < 2 :
            continue
        tls.plotHist(ak.sum(data, axis=1), bins=51, range=(-0.5,50),
                     title=label, xlabel='Count per event',
                     outs=outpref+'perEventCounts_'+label+'.png')


    # plot DT0 (T0 - Ttrue) for original tag
    masks = [
        ('APA_in', t0_in_mask),
        ('APA_out', t0_out_mask),
        ('APA', t0_mask),
    ]

    trk_T0 = tree['trk_T0'].array()
    mc_t = tree['mc_startPoint_tpcAV'].array()[trk_mcMuIdx][...,3]
    dts = []
    for label,mask in masks :
        t0 = trk_T0[mask]
        true_time = mc_t[mask]

        dt = (t0 - true_time)*1e-3
        tls.plotHist(dt, bins=100, range=(-500,500),
                     title=f'{label} T0 vs true time',
                     xlabel='$\Delta T0$ [$\mu$s]',
                     outs=outpref+f'dT0_{label}.png')

        mean,std = tls.plotZoomed(dt, bins=40, range=(-20,20),
                                  title=f'{label} T0 vs true time',
                                  xlabel='$\Delta T0$ [$\mu$s]',
                                  outs=outpref+f'dT0_{label}_zoom.png')
        dts.append((label, dt, mean, std))

    # get fraction of outliers id DT
    def outliers(data, label, mean, std):
        count_outliers = ak.sum(abs(data - mean) > 3*std)
        fraction_of_outliers = count_outliers / ak.count(data)

        print (f'{label:15} {fraction_of_outliers:.3f}')

    print('='*30)
    print('DT outliers ( |Dt-mean| > 3sigma )')
    for label,dt,mean,std in dts :
        outliers(dt, label, mean, std)
    print('='*30)




    return

    # plot track length for tagger tracks
    trklen = b.trk_length[t0_mask]
    tls.plotHist(trklen, bins=120, range=(0,1200),
                 title='Length of T0 tagged tracks',
                 xlabel='Length [cm]',
                 outs=outpref+'trklen.png')


    hists = [(cpaCrossersWithReco,    'True CPA crossing muons with any reco track',     'true_cpa'),
             (apaInCrossersWithReco,  'True APAIn crossing muons with any reco track',  'true_apain'),
             (apaOutCrossersWithReco, 'True APAOut crossing muons with any reco track', 'true_apaout'),
             (t0_cpaCrosser_mask,     'T0 tagged true CPA crossers', 't0_cpa'),
             (t0_apaInCrosser_mask,     'T0 tagged true APAIn crossers', 't0_apain'),
             (t0_apaOutCrosser_mask,     'T0 tagged true APAOut crossers', 't0_apaout'),
             ]
    for h,t,s in hists :
        tls.plotHist(ak.sum(h,axis=-1), bins=41, range=(-0.5,40.5),
                     title=t,
                     xlabel='Count per event',
                     outs=outpref+f'{s}_crossers.png')



def run(args):
    print(args[1])
    print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]


    if len(args) > 3 :
        sel.SetDetector(args[3])


    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
