#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

def AddStatBox(mean, std) :
    ax = plt.gca()
    ax.text(0.98, 0.88, f'mean = {mean:5.2f}\n std.dev. = {std:5.2f}',
                       transform = ax.transAxes, ha='right', fontfamily = 'monospace')





OUTDIR = 'plots/python/reco_to_mc_matching'

def process_tree(args):
    print(args[1])
    print(mlp.matplotlib_fname())

    # make sure the output directory exists
    os.makedirs(OUTDIR, exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:

        branches = tree.arrays(['trk_mcMuIdx',
                                'trk_ncalopts'
                                ])

        trk_mcMuIdx = GetMCMatched(branches)
        print(trk_mcMuIdx)

        # h = hist.Hist(ak.flatten(trk_mcMuIdx))
        # h.plot()


if __name__ == '__main__':
    print('Running processing...')
    process_tree(sys.argv)
