#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

mpl.use('agg')

import tools as tls, selections as sel
import pdsp_opdet_map as chmap

import vis_pds_event as vis


def run(args):
    fname = args.fname
    global outpref, apply_offset, ticks_per_us, close_to_anode_only, is_data
    outpref, apply_offset, ticks_per_us,close_to_anode_only,is_data = (args.outpref,args.apply_offset, args.clock, args.close_to_anode_only,args.is_data)

    # get input tree
    branches = [
        'run', 'event',
        'trk_startPoint','trk_endPoint','trk_hasT0', 'trk_mcMuIdx_best',
        'mc_endPoint_tpcAV', 'mc_exited_tpcAV', 'mc_hasME', 'mc_ME_en','mc_pdg', 'mc_ME_dt',
        'trk_stopped', 'trk_matched', 'match_flash_idx',
        'flash_abs_time', 'flash_nhits', 'flash_hit_idx',
        'hit_pe', 'hit_channel']

    tree = ur.open(fname+':OpDetAna/opdetana')
    # get relevant data
    data_to_plot = 0
    first=True
    last_evt = -1
    counter = 0
    for data in tree.iterate(branches, step_size=200, entry_stop=args.nmax) :
#    data = tree.arrays(branches, entry_stop=args.nmax)
        print(f'Iterating over batch {counter}')
        counter += 1

        tracks = (data.trk_stopped == 1)

        trk_data = data[['run', 'event',
                         'trk_startPoint','trk_endPoint',
                         'trk_hasT0', 'trk_mcMuIdx_best',
                         'trk_stopped', 'trk_matched']]


        trk_endPoint = data.trk_endPoint
        trk_startPoint = data.trk_startPoint
        trk_endPoint = ak.where( trk_endPoint[...,1]<trk_startPoint[...,1], trk_endPoint, trk_startPoint )
        # select only tracks reco'd close to apa
        if args.close_to_anode_only :
            trk_close_mask = (geometry[0][1] - abs(data.trk_endPoint[...,0])) < 100. # track's end point max 1 m from an APA
            tracks = tracks & trk_close_mask

        trkid = ak.local_index(data.trk_stopped, axis=1)

        #trk_data.show(type=True)
        trk_data = dict(zip(ak.fields(trk_data)+['trkid'],list(ak.unzip(trk_data))+[trkid]))
        #print(trk_data)
        trk_data = ak.zip(trk_data, depth_limit=2)
        trk_data = trk_data[tracks]
        #trk_data.show(type=True)



        flashes = data.match_flash_idx[trk_data.trk_matched]
        hits = tls.ClipData(data.flash_hit_idx[flashes],data.flash_nhits[flashes])

        channels = tls.Slice(data.hit_channel, hits)
        pes      = tls.Slice(data.hit_pe, hits)

        PlotFlashHitMap(ak.zip({'chan':channels, 'PEs':pes, 'run':trk_data.run, 'event':trk_data.event, 'trk':trk_data.trkid}, depth_limit=2))



def PlotFlashHitMap(d) :
    # prepare output histogram grid
    global outpref

    for evt in d :
        for trk in evt :
            #trk.show(type=True)
            vis.visualize_light_intensity(dict(zip(trk.chan,trk.PEs)),chmap.OpChannelToCoord)
            plt.savefig(outpref+f'procsd_avg_wfm_run{trk.run}_evt{trk.event}_trk{trk.trk}.png')
            plt.close(plt.gcf())




if __name__ == '__main__':
    print('Running processing...')

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outpref',nargs='?',default='')
    # parser.add_argument('-d', default='pdsp')
    parser.add_argument('-n','--nmax', default=10, type=int)
    parser.add_argument('--clock', default=150, type=float) # 150 ticks/us ... PDSP, prod4 MC
    parser.add_argument('-o', '--apply-offset',action='store_true')
    parser.add_argument('-c','--close-to-anode-only',action='store_true')
    parser.add_argument('-d','--is-data',action='store_true')



    args = parser.parse_args()

    # make sure the output directory exists
    outdir = os.path.dirname(args.outpref)
    if outdir != '' :
        os.makedirs(outdir, exist_ok=True)


    #import cProfile
    #cProfile.run('run()', 'plot_wfms_of_stopped.py.profile_stats')
    run(args)
