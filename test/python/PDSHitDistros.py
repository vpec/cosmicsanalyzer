#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/pdsHitDistros/'

import selections as sel, tools as tls

#import dunestyle.matplotlib as dunestyle

def process_tree(tree) :
    nhits = tree['n_hits'].array()
    nhitsperflash = tree['flash_nhits'].array()
    npesperhit = tree['hit_pe'].array()
    npesperflash = tree['flash_tot_pe'].array()

    to_plot = [
        tls.DataToPlot(nhits, bins=200, #(3e3,10e3),
                       title='No. OpDet hits per readout',
                       xlabel='No. hits',
                       outs='nhits'),
        tls.DataToPlot(nhits, bins=200, range=(1,1e5), #(3e3,10e3),
                       title='No. OpDet hits per readout',
                       xlabel='No. hits', xlog=True,
                       outs='nhits_xlog'),
        tls.DataToPlot(nhits, bins=150, range=(1e2,1e5), #(3e3,10e3),
                       title='No. OpDet hits per readout',
                       xlabel='No. hits', xlog=True,log=True,
                       outs='nhits_loglog'),
        tls.DataToPlot(nhitsperflash,bins=250,range=(0,250),
                       log=True,
                       title='No. OpDet hits per reco flash',
                       xlabel='No. hits', outs='nhits_perflash'),
        tls.DataToPlot(nhitsperflash,bins=150,range=(1,1e3),
                       log=True,xlog=True,
                       title='No. OpDet hits per reco flash',
                       xlabel='No. hits', outs='nhits_perflash_loglog'),
        tls.DataToPlot(npesperhit, bins=250, range=(0.1,5e4), #(3e3,10e3),
                       title='No. PE hits per OpDet hit',
                       xlabel='No. PE', xlog=True, log=True,
                       outs='hit_pe'),
        tls.DataToPlot(npesperflash, bins=250, range=(0.1,5e4), #(3e3,10e3),
                       title='No. PE hits per OpDet flash',
                       xlabel='No. PE', xlog=True, log=True,
                       outs='flash_pe'),
    ]


    for p in to_plot :
        tls.plotHist(p, outs=outpref+p.outs+'.png')

def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
