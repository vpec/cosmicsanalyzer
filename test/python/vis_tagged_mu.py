#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

from mpl_toolkits.mplot3d.art3d import Poly3DCollection


# use batch graphics mode
#mpl.use('agg')


def run():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    # parser.add_argument('-d', default='pdsp')
    # parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()

    fname = args.fname

    # get input tree
    tree = ur.open(fname+':OpDetAna/opdetana')

    # get relevant data
    data = tree.arrays([
        'stopped',
        'reco_start',
        'reco_end',
        'mc_start',
        'mc_end',
        't0',
        'hast0',
        'mcIdx',
        'truExit',
        'flashMcIdx',
        'flashT',
        'trkMatch',
        'matchFlsh',
    ], aliases={
        #'hasT0' : 'trk_hasT0',
        'stopped'    : 'trk_stopped',
        'reco_start' : 'trk_startPoint',
        'reco_end'   : 'trk_endPoint',
        'mc_start'   : 'mc_startPoint_tpcAV',
        'mc_end'     : 'mc_endPoint_tpcAV',
        't0'         : 'trk_T0',
        'hast0'      : 'trk_hasT0',
        'mcIdx'      : 'trk_mcMuIdx_best',
        'truExit'    : "mc_exited_tpcAV",
        'flashMcIdx' : 'flash_mcMuIdx',
        'flashT'     : 'flash_abs_time',
        'trkMatch' : 'trk_matched' ,
        'matchFlsh'  : 'match_flash_idx',
    })

    # print(data[0].__class__)
    # data[0].show(limit_cols=300,type=True)

    trk_mask = (data.mcIdx > 1) & (data.hast0>0)

    evtid = ak.local_index(data.mcIdx,axis=0)
    trkid = ak.local_index(data.mcIdx,axis=1)

    evtid = evtid[ak.any(trk_mask,axis=1)]
    trkid = trkid[trk_mask]

    reindex=[2,0,1]
    reco_start = data.reco_start[trk_mask][...,reindex]
    reco_end   = data.reco_end[trk_mask][...,reindex]

    trkMcIdx = data.mcIdx[trk_mask]

    true_start = data.mc_start[trkMcIdx][...,reindex]
    true_end   = data.mc_end[trkMcIdx][...,reindex]

    truExit = data.truExit[trkMcIdx]
    recoStopped = data.stopped[trk_mask]

    matched = data.trkMatch[trk_mask]
    matched = ak.where(matched > -1, matched, ak.Array([None]))
    matchedFlash = data.matchFlsh[matched]

    flashMcIdx = data.flashMcIdx[matchedFlash]
    flashT = data.flashT[matchedFlash]

    t0 = data.t0[trk_mask]*1e-3
    t0type = data.hast0[trk_mask]

    truT = data.mc_start[data.mcIdx[trk_mask]][...,3]*1e-3

    dt_reco_tru = t0 - truT
    dt_flash_tru = flashT - truT
    dt_trk_flash = t0-flashT

    data_keys = ['true_start','true_end','reco_start','reco_end',
                 'evtid','trkid','t0','t0type','truExit','recoStopped',
                 'dt_reco_tru','dt_flash_tru','dt_trk_flash',
                 'trkMcIdx','flashMcIdx']

    loc = locals()
    data = ak.zip(dict((x,loc[x]) for x in data_keys),depth_limit=2)

    for devt in data :
        for dtrk in devt :
            #d.show(limit_cols=300,type=True)
            create_vis(dtrk)


def create_vis(d) :
    fig, axs = plt.subplots(1,3,subplot_kw={"projection": "3d"},figsize=(12,5),width_ratios=[1,2,2],
                            layout=None)
    fig.suptitle(f'Event {d.evtid}, track {d.trkid}')
    #fig.set_tight_layout(True)
    for tit,ax in zip(['Reco','MC'],axs[1:]) :
        ax.set_xlabel('Z [cm]')
        ax.set_ylabel('X [cm]')
        ax.set_zlabel('Y [cm]')
        ax.set_aspect('auto')
        ax.view_init(elev=30, azim=210)
        ax.set_title(tit)

    # reco track
    plt.sca(axs[1])
    plotTrack(d.reco_start,d.reco_end)

    # d.reco_start.show()
    # d.reco_end.show()

    # mc track
    plt.sca(axs[2])
    plotTrack(d.true_start,d.true_end)


    for ax in axs[1:] :
        draw_detector(ax)

    plt.sca(axs[0])
    AddInfo(d)

    plt.show()

def draw_detector(ax):
    plt.sca(ax)
    # left active volume
    draw_transparent_cube([0,-357,7], [695,357,600], 'blue', 0.1)
    draw_transparent_cube([0,0,7], [695,357,600], 'red', 0.1)


def plotTrack(start, end):
    data = list(zip(start,end))
    plt.plot(*data)

def AddInfo(d): #t0, t0type, truExit, recoStopped,dt_reco_tru, dt_flash_tru, dt_trk_flash) :
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]

    text = \
        f'T0 type = {taggers[d.t0type-1]}\n\n' +\
        f'True stopper = {not d.truExit}\n' +\
        f'Reco stopped = {d.recoStopped==True}\n\n' +\
        f'T0 = {d.t0:.1f} $\\mu$s\n' +\
        f'$\\Delta T_\\text{{trk,true}}$ = {d.dt_reco_tru:0.2f} $\\mu$s\n'
    if d.dt_flash_tru != None :
        text += f'$\\Delta T_\\text{{flash,true}}$ = {d.dt_flash_tru:0.2f} $\\mu$s\n' +\
            f'$\\Delta T_\\text{{trk,flash}}$ = {d.dt_trk_flash:0.2f} $\\mu$s\n'
    else :
        text += f'$\\Delta T_\\text{{flash,true}}$ = none\n' +\
            f'$\\Delta T_\\text{{trk,flash}}$ = none\n'
    text += f'TrkMCIdx = {d.trkMcIdx}\n' +\
        f'FlashMCIdx = {d.flashMcIdx}'

    ax = plt.gca()
    ax.set_axis_off()
    ax.text2D(0.1,0.9,text,transform=ax.transAxes)


def draw_transparent_cube(center, dimensions, color='red', alpha=0.2):
    """Draws a transparent cube."""

    # Define vertices
    vertices = np.array([
        [0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
        [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]
    ])

    vertices = vertices*dimensions
    vertices = vertices + center

    # Define faces (as indices of vertices)
    faces = [
        [0, 1, 2, 3],  # Bottom face
        [4, 5, 6, 7],  # Top face
        [0, 1, 5, 4],  # Front face
        [2, 3, 7, 6],  # Back face
        [1, 2, 6, 5],  # Right face
        [0, 3, 7, 4]   # Left face
    ]

    # Create Poly3DCollection
    poly = Poly3DCollection([vertices[face] for face in faces], alpha=alpha, facecolor=color, edgecolor='black') #alpha for transparency

    # Add the collection to the axes
    plt.gca().add_collection3d(poly)

if __name__ == '__main__':
    print('Running processing...')
    run()
