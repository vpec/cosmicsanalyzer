#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/t0_tag_distro/'


import selections as sel, tools as tls
from selections import T0Type

def process_tree(t):
    hasT0 = t['trk_hasT0'].array()
    hasT0 = ak.ravel(hasT0[hasT0>0])


    #remap = ['', 'Default', 'APAIn', '', 'APAOut', '', '', '', 'CPA']
    #remap = ak.cartesian([hasT0, remap], nested=True, axis=0)['1']
    #remap = ak.cartesian([hasT0, remap], nested=True, axis=1)['1']

    # print(remap.type)
    # labels = ak.flatten(remap[ak.singletons(hasT0)], axis=1)
    # # print(hasT0)
    # print(labels.type)

    d = np.unique(ak.to_numpy(hasT0), return_counts=True)
    print(d)

    dd = { key:val for key,val in zip(*d) }
    dd[T0Type.CPA] /= 2

    print(d)
    x = { T0Type.Orig:'Default', T0Type.CPA:'CPA', T0Type.APAIn:'APAIn', T0Type.APAOut:'APAOut' }
    y = [ dd[y] for y in x.keys() ]
    print(x,y)

    #plt.hist(labels) #, bins=10, range=(0.5,10.5))
    plt.bar(x.values(), y) #, bins=10, range=(0.5,10.5))
    #plt.show()
    plt.savefig(outpref+'t0_distro.png')





def run(args):
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        process_tree(tree)


if __name__ == '__main__':
    #print('Running processing...')
    run(sys.argv)
