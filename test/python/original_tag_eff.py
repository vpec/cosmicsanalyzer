#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/original_tag_eff/'

branches_to_process = [
    'gen_pos',
    'mc_exited_tpcAV',
    'mc_startPoint_tpcAV',
    'mc_endPoint_tpcAV',
    'trk_length',
    'trk_hasT0',
    'trk_T0',
    'trk_startPoint',
    'trk_endPoint',
    'trk_startTpc',
    'trk_endTpc',
    'trk_mcMuIdx',
    'trk_ncalopts',
    'trk_stitched'
]

import tools as tls, selections as sel


def process_tree(tree) :
    '''
    See how well original tag was working.
    - overall efficiency = tagged vs all reconstructed true muons
    - efficiency for CPA crossers = num of tagged true crossers / reconable true crossers
    - efficiency for APA crossers = dtto
    '''

    # useful data
    # trk dim
    trk_mcMuIdx            = sel.GetMCMatched(tree)
    t0_mask                = tree['trk_hasT0'].array() == 1
    trk_len_mask           = tree['trk_length'].array() > 20.
    # mc dim
    trksPerMu              = sel.BuildRecoPerTrue(tree, trk_mcMuIdx)
    muHasReco              = ak.num(trksPerMu, axis=-1) > 0
    cpaCrossers            = sel.MaskCPACrossersInMC(tree)
    apaInCrossers          = sel.MaskAPAInCrossersInMC(tree)
    apaOutCrossers         = sel.MaskAPAOutCrossersInMC(tree)
    cpaCrossersWithReco    = cpaCrossers & muHasReco
    apaInCrossersWithReco  = apaInCrossers & muHasReco
    apaOutCrossersWithReco = apaOutCrossers & muHasReco
    # true muons have a T0'd track
    layout                 = tls.GetLayout(trksPerMu)
    trksPerMu_flat         = ak.flatten(trksPerMu, axis=-1)
    t0_perMu_mask_flat     = t0_mask[trksPerMu_flat]
    t0_perMu_mask          = tls.RebuildFromFlat(ak.flatten(t0_perMu_mask_flat), layout)
    t0_perMu_any_mask      = ak.any(t0_perMu_mask, axis=-1)
    # only for long tracks
    t0_perMu_long_mask_flat     = (trk_len_mask & t0_mask)[trksPerMu_flat]
    t0_perMu_long_mask          = tls.RebuildFromLayout(t0_perMu_long_mask_flat, layout)
    t0_perMu_any_long_mask      = ak.any(t0_perMu_long_mask, axis=-1)

    recLenMaskPerTrue      = tls.RebuildFromLayout(trk_len_mask[trksPerMu_flat], layout)
    muHasLongReco          = ak.any(recLenMaskPerTrue, axis=-1)


    # tagged trks dim
    t0_mcMuIdx            = trk_mcMuIdx[t0_mask]
    t0_cpaCrosser_mask    = cpaCrossersWithReco[t0_mcMuIdx]
    t0_apaInCrosser_mask  = apaInCrossersWithReco[t0_mcMuIdx]
    t0_apaOutCrosser_mask = apaOutCrossersWithReco[t0_mcMuIdx]



    # tagged and CPA crossers
    counts = [
        ('cpa_with_reco'     ,   cpaCrossersWithReco   ),
        ('apaIn_with_reco'   ,   apaInCrossersWithReco ),
        ('apaOut_with_reco'  ,   apaOutCrossersWithReco),
        ('apa_with_reco'     ,   apaInCrossersWithReco | apaOutCrossersWithReco),
        ('tagged'            ,   t0_mask),
        ('tagged_with_muons' ,   t0_mask[trk_mcMuIdx > -1]),
        ('tagged_any_muons'  ,   t0_perMu_any_mask),
        ('tagged_muons'      ,   t0_perMu_mask),
        ('tracks'            ,   [ak.count(t0_mask)]),
        ('recod_mus'         ,   muHasReco),
        ('tagged_true_CPA'   ,   t0_cpaCrosser_mask),
        ('tagged_true_APA'   ,   t0_apaInCrosser_mask | t0_apaOutCrosser_mask),
    ]


    print('='*30)
    counts = { label: ak.sum(count) for label,count in counts }
    for label, count in counts.items():
        print(f'{label:20} = {count}')
    # for label,arr in counts:
    #     print(f'{label:20} = {ak.sum(arr)}')


    # Goodnest of T0 tag base on DT
    t0        = tree['trk_T0'].array()[t0_mask]
    true_time = tree['mc_startPoint_tpcAV'].array()[trk_mcMuIdx][...,3][t0_mask]

    dt = (t0 - true_time)*1e-3
    # assuming good tag if -10 us < Dt < 30 us
    # good_tag_mask = abs(dt-10.) < 20 # for PDSP Prod4
    good_tag_mask = abs(dt-42.) < 10 # for PDHD2024a
    trk_len_mask           = tree['trk_length'].array() > 20.

    efficiencies = [
        ('eff_long_trks_reco_mus', ak.sum(t0_perMu_any_long_mask)/ak.sum(muHasLongReco)             ),
        ('eff_long_trks', ak.sum(trk_len_mask & t0_mask)/ak.sum(trk_len_mask)             ),
        ('eff_long_good', ak.sum(good_tag_mask & trk_len_mask[t0_mask])/ak.sum(trk_len_mask) ),
        ('eff_trks'     , counts['tagged']/ak.count(t0_mask)             ),
        ('eff_reco_mus' , counts['tagged_any_muons']/counts['recod_mus'] ),
        # efficiency speciffically for CPA/APA crossers
        ('eff_CPA' , counts['tagged_true_CPA']/counts['cpa_with_reco'] ),
        ('eff_APA' , counts['tagged_true_APA']/counts['apa_with_reco'] ),
    ]

    print('='*30)
    print('Efficiencies:')
    print('-------------')
    for label, count in efficiencies :
        print(f'{label:20} = {count:.3f}')



    # purity
    # How many of the tagged tracks are muons, how many are CPA crossers and how many are APA crossers
    purities = [
        ('good'  , ak.sum(good_tag_mask)/counts['tagged']),
        ('good_long'  , ak.sum(good_tag_mask & trk_len_mask[t0_mask])/ak.sum(t0_mask & trk_len_mask)),
        ('muons' , counts['tagged_any_muons']/counts['tagged']),
        ('CPA'   , counts['tagged_true_CPA']/counts['tagged']),
        ('APA'   , counts['tagged_true_APA']/counts['tagged']),
    ]

    print('='*30)
    print('Purities:')
    print('---------')
    for label, count in purities :
        print(f'{label:20} = {count:.3f}')

    print('='*30)


    ### plotting
    # plot DT0 (T0 - Ttrue) for original tag
    tls.plotHist(dt, bins=100, range=(-500,500),
                 title='Original T0 vs true time',
                 xlabel=r'$\Delta T0$ [$\mu$s]',
                 outs=outpref+'dT0.png',
                 stats={'ent':True})
    tls.plotHist(dt, bins=100, range=(-500,500),
                 title='Original T0 vs true time',
                 xlabel=r'$\Delta T0$ [$\mu$s]',
                 outs=outpref+'dT0_log.png',
                 log=True,
                 stats={'ent':True})

    mean, std = tls.plotZoomed(dt, bins=160, range=(-30,50),
                               title='Original T0 vs true time',
                               xlabel=r'$\Delta T0$ [$\mu$s]',
                               outs=outpref+'dT0_zoom.png',
                               stats={'ent':True})
    tls.plotZoomed(dt, bins=160, range=(-30,50),
                   title='Original T0 vs true time',
                   xlabel=r'$\Delta T0$ [$\mu$s]',
                   outs=outpref+'dT0_zoom_log.png',
                   log=True,
                   stats={'ent':True})

    # print more stuff on dt
    count_outliers = ak.sum(abs(dt - mean) > 3*std)
    fraction_of_outliers = count_outliers / counts['tagged']

    print (f'DT outliers ( |Dt-mean| > 3sigma )  = {fraction_of_outliers:.3f}')
    print('='*30)

    # plot track length for tagger tracks
    trklen = tree['trk_length'].array()[t0_mask]
    tls.plotHist(trklen, bins=120, range=(0,1200),
                 title='Length of T0 tagged tracks',
                 xlabel='Length [cm]',
                 outs=outpref+'trklen.png',
                 stats={'ent':True})


    hists = [(cpaCrossersWithReco,    'True CPA crossing muons with any reco track',     'true_cpa'),
             (apaInCrossersWithReco,  'True APAIn crossing muons with any reco track',  'true_apain'),
             (apaOutCrossersWithReco, 'True APAOut crossing muons with any reco track', 'true_apaout'),
             (t0_cpaCrosser_mask,     'T0 tagged true CPA crossers', 't0_cpa'),
             (t0_apaInCrosser_mask,     'T0 tagged true APAIn crossers', 't0_apain'),
             (t0_apaOutCrosser_mask,     'T0 tagged true APAOut crossers', 't0_apaout'),
             ]
    for h,t,s in hists :
        tls.plotHist(ak.sum(h,axis=-1), bins=41, range=(-0.5,40.5),
                     title=t,
                     xlabel='Count per event',
                     outs=outpref+f'{s}_crossers.png',
                     stats={'ent':True})



def run(args):
    print(args[1])
    print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        sel.SetDetector(args[3])

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
