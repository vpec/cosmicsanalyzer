#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

from mpl_toolkits.mplot3d.art3d import Poly3DCollection


# use batch graphics mode
#mpl.use('agg')


def run():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    # parser.add_argument('-d', default='pdsp')
    # parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()

    fname = args.fname

    # get input tree
    tree = ur.open(fname+':OpDetAna/opdetana')

    # get relevant data
    data = tree.arrays([
        'stopped',
        'reco_start',
        'reco_end',
        'mc_start',
        'mc_end',
        't0',
        'hast0',
        'mcIdx',
        'truExit',
        'flashT',
        'trkMatch',
        'matchFlsh',
    ], aliases={
            #'hasT0' : 'trk_hasT0',
            'stopped'    : 'trk_stopped',
            'reco_start' : 'trk_startPoint',
            'reco_end'   : 'trk_endPoint',
            'mc_start'   : 'mc_startPoint_tpcAV',
            'mc_end'     : 'mc_endPoint_tpcAV',
            't0'         : 'trk_T0',
            'hast0'      : 'trk_hasT0',
            'mcIdx'      : 'trk_mcMuIdx_best',
            'truExit'    : "mc_exited_tpcAV",
            'flashT'     : 'flash_abs_time',
        'trkMatch' : 'trk_matched' ,
        'matchFlsh'  : 'match_flash_idx',
    })

    trk_mask = (data.mcIdx > 1) & (data.stopped==1)

    evtid = ak.local_index(data.mcIdx,axis=0)
    trkid = ak.local_index(data.mcIdx,axis=1)

    evtid = evtid[ak.any(trk_mask,axis=1)]
    trkid = trkid[trk_mask]

    reco_start = data.reco_start[trk_mask][...,:3]
    reco_end   = data.reco_end[trk_mask][...,:3]

    true_start = data.mc_start[data.mcIdx[trk_mask]][...,:3]
    true_end   = data.mc_end[data.mcIdx[trk_mask]][...,:3]

    truExit = data.truExit[data.mcIdx[trk_mask]]

    flashT = data.flashT[data.matchFlsh[data.trkMatch[trk_mask]]]


    t0 = data.t0[trk_mask]*1e-3
    t0type = data.hast0[trk_mask]

    truT = data.mc_start[data.mcIdx[trk_mask]][...,3]*1e-3

    dt_reco_tru = t0 - truT
    dt_flash_tru = flashT - truT

    for ievt in range(len(reco_start)) :
        for itrk in range(len(reco_start[ievt])) :
            fig, axs = plt.subplots(1,3,subplot_kw={"projection": "3d"},figsize=(12,5),width_ratios=[1,2,2])
            fig.suptitle(f'Event {evtid[ievt]}, track {trkid[ievt,itrk]}')
            fig.set_tight_layout(True)
            for tit,ax in zip(['Reco','MC'],axs[1:]) :
                ax.set_xlabel('X [cm]')
                ax.set_ylabel('Y [cm]')
                ax.set_zlabel('Z [cm]')
                ax.set_aspect('auto')
                ax.view_init(elev=-70, azim=120, roll=-35)
                ax.set_title(tit)

            # reco track
            plt.sca(axs[1])
            plotTrack(reco_start[ievt,itrk],reco_end[ievt,itrk])

            # mc track
            plt.sca(axs[2])
            plotTrack(true_start[ievt,itrk],true_end[ievt,itrk])



            for ax in axs[1:] :
                plt.sca(ax)
                # left active volume
                draw_transparent_cube([-357,7,0], [357,600,695], 'blue', 0.1)
                draw_transparent_cube([0,7,0], [357,600,695], 'red', 0.1)


            plt.sca(axs[0])
            AddInfo(t0[ievt,itrk], t0type[ievt,itrk], truExit[ievt,itrk], dt_reco_tru[ievt,itrk], dt_flash_tru[ievt,itrk])

            plt.show()



def plotTrack(start, end):
    data = list(zip(start,end))
    plt.plot(*data)

def AddInfo(t0, t0type, truExit, dt_reco_tru, dt_flash_tru) :
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]

    text = \
        f'True stopper = {not truExit}\n' +\
        f'T0 = {t0:.1f} $\\mu$s\n' +\
        f'T0 type = {taggers[t0type-1]}\n' +\
        f'$\\Delta T_\\text{{reco,true}}$ = {dt_reco_tru:0.2f} $\\mu$s\n' +\
        f'$\\Delta T_\\text{{reco,true}}$ = {dt_flash_tru:0.2f} $\\mu$s\n' +\
        f''

    ax = plt.gca()
    ax.set_axis_off()
    ax.text2D(0.1,0.99,text,transform=ax.transAxes)


def draw_transparent_cube(center, dimensions, color='red', alpha=0.2):
    """Draws a transparent cube."""

    # Define vertices
    vertices = np.array([
        [0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
        [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]
    ])

    vertices = vertices*dimensions
    vertices = vertices + center

    # Define faces (as indices of vertices)
    faces = [
        [0, 1, 2, 3],  # Bottom face
        [4, 5, 6, 7],  # Top face
        [0, 1, 5, 4],  # Front face
        [2, 3, 7, 6],  # Back face
        [1, 2, 6, 5],  # Right face
        [0, 3, 7, 4]   # Left face
    ]

    # Create Poly3DCollection
    poly = Poly3DCollection([vertices[face] for face in faces], alpha=alpha, facecolor=color, edgecolor='black') #alpha for transparency

    # Add the collection to the axes
    plt.gca().add_collection3d(poly)

if __name__ == '__main__':
    print('Running processing...')
    run()
