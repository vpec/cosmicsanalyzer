import uproot as ur
import awkward as aw
from matplotlib import pyplot as plt
import numpy as np
import matplotlib as mpl
mpl.use('agg')

import argparse

import sys

parser = argparse.ArgumentParser()
parser.add_argument('fname')
parser.add_argument('outpref', nargs='?', default='')
parser.add_argument('-w', default=0, type=int)
parser.add_argument('-e', default=0, type=int)
args = parser.parse_args()

outpref = args.outpref

with ur.open(f'{args.fname}:OpDetAna/opdetana') as tree :
    data = tree.arrays(['wfm','wfm_size','hit_channel','wfm_hit'])
    print(f'Number of waveforms in this event: {len(data.wfm[args.e])}')

    # wfms = tree['wfm'].array()
    # hit_channels = tree['hit_channel'].array()
    # wfm_hit_idx =  tree['wfm_hit'].array()
    size = data.wfm_size[args.e,args.w]
    wfm = data.wfm[args.e,args.w,:size]
    chan = data.hit_channel[args.e,data.wfm_hit[args.e,args.w]]
    plt.plot(wfm)
    plt.savefig(outpref+f'waveform_evt{args.e}_wfm{args.w}_ch{chan}.png')

    outfname = 'test_wfm_hists.root'
    try :
        f = ur.update(outfname)
    except OSError:
        f = ur.recreate(outfname)

    f[f'wfm{args.e}_{args.w}_ch{chan}'] = (wfm.to_numpy(),np.arange(len(wfm)+1))
