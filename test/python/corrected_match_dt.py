#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak


# use batch graphics mode
mpl.use('agg')


def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())
    fname = args[1]

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    #import selections as sel
    import tools as tls
    import dt_offsets_by_tag as offsets

    trk_hasT0 = tree['trk_hasT0'].array()
    trk_t0    = tree['trk_T0'].array()*1e-3

    # preselect
    T0Mask    = trk_hasT0 > 0
    trk_hasT0 = trk_hasT0[T0Mask]
    trk_t0    = trk_t0[T0Mask]

    flash_t  = tree['flash_abs_time'].array()
    flash_pe = tree['flash_tot_pe'].array()

    # preselect
    peMask   = flash_pe > 400
    flash_t  = flash_t[peMask]
    flash_pe = flash_pe[peMask]

    # pairing trk with T0 with any flash
    trk_idx = ak.local_index(trk_t0)
    flash_idx = ak.local_index(flash_t)
    all_pairs = ak.cartesian({'trk':trk_idx, 'flash':flash_idx},nested=True)
    all_pairs_dt = tls.Slice(trk_t0,all_pairs['trk']) - tls.Slice(flash_t,all_pairs['flash'])
    all_pairs_tags = tls.Slice(trk_hasT0, all_pairs['trk'])

    tags = {1:'orig', 2:'APAIn', 4:'APAOut', 8:'CPA'}


    # do corrections
    all_pairs_dt_corrected = tls.CorrectOffsetsByTag(all_pairs_dt, all_pairs_tags, offsets.offsets)

    counts,bins = plot(all_pairs_dt, f'uncorrected_dt_tpc_pds_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=160, range=(-40,40))
    counts,bins = plot(all_pairs_dt_corrected, f'corrected_dt_tpc_pds_zoom.png', xlabel=r'$\Delta T$ [$\mu$s]', bins=160, range=(-40,40))

    fit = tls.Fit(counts, bins, tls.MultiNormal, fit_range=(-10,20), p0=(5e4, 0., 3., 5e3))
    plt.plot(fit[0], fit[1])
    tls.PlotFitResults((['amp','mean','sigma', 'const'], *fit[2]))
    plt.savefig(f'{outpref}fit_corrected_dt_tpc_pds_zoom.png')


def plot(data, fname, clf=True, xlabel=False, ylabel=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)
    counts,bins,bars = plt.hist(ak.ravel(data), **kwargs)
    plt.savefig(outpref + fname)
    return counts, bins

if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
