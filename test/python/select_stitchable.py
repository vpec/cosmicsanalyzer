#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'


outpref = 'plots/python/select_stitchable/'


import selections as sel

@nb.njit
def _GetCPAMatchedPairs(trk_stitched, builder):
    for evt in trk_stitched:
        itrk = -1
        builder.begin_list()
        for trk in evt :
            itrk += 1
            if trk  < 0 :
                continue
            if trk < itrk :
                continue
            builder.begin_tuple(2)
            builder.index(0).integer(itrk)
            builder.index(1).integer(trk)
            builder.end_tuple()

        builder.end_list()


def GetCPAMatchedPairs(b) :
        builder = ak.ArrayBuilder() # must use builder to create new AwkwardArray
        _GetCPAMatchedPairs(b.trk_stitched, builder)
        return builder.snapshot()

@nb.njit
def _BuildRecoPerTrue(b, mcidx, builder) :
    for ievt in range(len(b.mc_startPoint_tpcAV)) :
        builder.begin_list()
        for  imu in range(len(b.mc_startPoint_tpcAV[ievt])) :
            builder.begin_list()
            for itrk in range(len(mcidx[ievt])) :
                if mcidx[ievt][itrk] == imu :
                    builder.integer(itrk)

            builder.end_list()
        builder.end_list()

def BuildRecoPerTrue(b, mcidx) :
    builder = ak.ArrayBuilder()
    _BuildRecoPerTrue(b, mcidx, builder)
    return builder.snapshot()

def MaskTrueCrossers(b) :
    return np.sign(b.mc_startPoint_tpcAV[...,0]) != np.sign(b.mc_endPoint_tpcAV[...,0])

def GetReconstructableMask(b) :
    DriftVel = 0.1565e3 # in cm/ns // from ProtoDUNE fhicl //at 87.68 K and 486.7 V/cm as for ProtoDUNE
    # nominal at 87 K and normal pressure: 0.1648;
    InverseDriftVel = 1./DriftVel

    true_t = b.mc_startPoint_tpcAV[...,3]
    x1 = b.mc_startPoint_tpcAV[...,0]
    x2 = b.mc_endPoint_tpcAV[...,0]
    absx1 = ak.where(x1 > 0., x1, -x1)
    absx2 = ak.where(x2 > 0., x2, -x2)
    xmax = ak.max( [absx1, absx2], axis=0 )
    xmin = ak.min( [absx1, absx2], axis=0 )
    upperLimit = true_t < (2.75e6 - (sel.top[0] - xmax)*InverseDriftVel)
    lowerLimit = true_t > (-250e3 - (sel.top[0] - xmin)*InverseDriftVel)

    crossers = MaskTrueCrossers(b)

    return ak.where(crossers,
                    (true_t > -2.5e6) & upperLimit, # 2.5e6 ns is max drift time + 250e3 ns pretrigger
                    # not a CPA crosser
                    lowerLimit & upperLimit)

def SelectStitchable(t) :
    # for each muon find all backtracked reco tracks
    mcidx = sel.GetMCMatched(t) # for each reco track get best muon MC idx (reco track dimensions)
    muRecoTracks = sel.BuildRecoPerTrue(t, mcidx) # get list of resulting tracks for each true muon (mc muon dimensions))

    # track end-point side of CPA
    trk_tpc_start = t['trk_startTpc'].array() % 4
    trk_tpc_end = t['trk_endTpc'].array() % 4

    mu2trkidx_flat = ak.flatten(muRecoTracks, axis=-1) # mc mu flat dim
    trkcounts       = ak.num(muRecoTracks, axis=-1) # mc mu dim, counts number of tracks per mc muon
    trkcounts_flat  = ak.flatten(trkcounts) # mc mu dim, flat per event, counts of how the track
    mucounts = ak.num(muRecoTracks, axis=1) # evt dim, counts number of muons per event

    # long track
    longtrack_mask_flat = t['trk_length'].array()[mu2trkidx_flat] > 20. # relevant tracks are longer than 20 cm (mc mu flat dim)
    longtrack_mask = ak.unflatten(ak.unflatten(ak.flatten(longtrack_mask_flat), trkcounts_flat), mucounts, axis=0)


    tpc_start_flat = trk_tpc_start[mu2trkidx_flat]
    tpc_end_flat   = trk_tpc_end[mu2trkidx_flat]
    # same drift volume
    same_drift_flat = tpc_start_flat == tpc_end_flat
    tpc_sum_flat = tpc_start_flat+tpc_end_flat
    cpa_crosser_flat = ~same_drift_flat & (tpc_sum_flat == 3)
    apa_crosser_flat = ~same_drift_flat & ((tpc_sum_flat == 1) | (tpc_sum_flat == 5))
    # cpa_crosser = ak.unflatten(ak.unflatten(ak.flatten(cpa_crosser_flat), trkcounts_flat), mucounts, axis=0)
    # apa_crosser = ak.unflatten(ak.unflatten(ak.flatten(apa_crosser_flat), trkcounts_flat), mucounts, axis=0)

    # check if muon has tracks on either side
    beam_left_flat = same_drift_flat & (tpc_start_flat == 2)
    beam_right_flat = same_drift_flat & (tpc_start_flat == 1)
    # unflatten cannot deal with 0-length additions in nested dimensions, cannot know if attaching at the beginning or end of the list.
    # need to flatten completely and use the original shape to unflatten from the lowest dimension
    beam_left = ak.unflatten(ak.unflatten(ak.flatten(beam_left_flat), trkcounts_flat), mucounts, axis=0)
    beam_right = ak.unflatten(ak.unflatten(ak.flatten(beam_right_flat), trkcounts_flat), mucounts, axis=0)
    trk_both_sides = ak.any(beam_left, axis=-1) & ak.any(beam_left, axis=-1) # dim mc mu

    long_beam_left = ak.unflatten(ak.unflatten(ak.flatten(longtrack_mask_flat & beam_left_flat), trkcounts_flat), mucounts, axis=0)
    long_beam_right = ak.unflatten(ak.unflatten(ak.flatten(longtrack_mask_flat & beam_right_flat), trkcounts_flat), mucounts, axis=0)
    long_trk_both_sides = ak.any(long_beam_left, axis=-1) & ak.any(long_beam_right, axis=-1) # dim mc mu



    # # trimmed by readout window; -1: trimmed at low TDC, 1: at high TDC
    # buf = 50 # how many TDC counts from the edge to consider track trimmed
    # trimmed_low = ( (b.trk_startPoint[...,3] < buf) | (b.trk_endPoint[...,3] < buf) )[mu2trkidx_flat]
    # trimmed_hi  = ( (b.trk_startPoint[...,3] > (6000-buf)) | (b.trk_endPoint[...,3] > (6000- buf)) )[mu2trkidx_flat]


    true_crosser_mask = sel.MaskCPACrossersInMC(t)

    long_trk_both_sides_not_cpa_crosser = long_trk_both_sides & ~true_crosser_mask
    trk_both_sides_not_cpa_crosser      = trk_both_sides & ~true_crosser_mask
    #stitchable = long_trk_both_sides & true_crosser_mask
    stitchable = trk_both_sides & true_crosser_mask


    # print(f'{b.trk_startTpc = }')
    # print(f'{ak.to_list(muRecoTracks[:3]) = }')
    # print(f'{ak.to_list(mu2trkidx_flat[:3]) = }')
    # print(f'{ak.to_list(trkcounts[:3]) = }')
    # start = 0 #ak.sum(ak.num(mucounts[:2], axis=1))
    # stop  = start + ak.sum(ak.num(trkcounts[:3], axis=1))
    # print(f'{ak.to_list(trkcounts_flat[start:stop]) = }')

    # print(f'{tpc_start_flat = }')
    # print(f'{tpc_end_flat = }')
    # print(f'{tpc_sum_flat = }')
    # print(f'{cpa_crosser_flat = }')
    # print(f'{apa_crosser_flat = }')

    # print(f'{beam_left = }')
    # print(f'{beam_right = }')
    # print(f'{trk_both_sides = }')

    # print(f'{long_beam_left = }')
    # print(f'{long_beam_right = }')
    # print(f'{long_trk_both_sides = }')
    # print(f'{long_trk_both_sides_not_cpa_crosser = }')
    # print(f'{ak.any(long_trk_both_sides_not_cpa_crosser, axis=1) = }')

    print(f'{ak.sum(true_crosser_mask) = }')
    print(f'{ak.sum(trk_both_sides) = }')
    print(f'{ak.sum(trk_both_sides_not_cpa_crosser) = }')
    print(f'{ak.sum(long_trk_both_sides) = }')
    print(f'{ak.sum(long_trk_both_sides_not_cpa_crosser) = }')
    print(f'{ak.sum(stitchable) = }')


    # muidx = ak.local_index(long_trk_both_sides_not_cpa_crosser, axis=1)
    # evtidx =  ak.local_index(long_trk_both_sides_not_cpa_crosser, axis=0)


    # investigate non CPA crossing muon with long tracks on both sides of CPA
    # evts = ak.any(long_trk_both_sides_not_cpa_crosser, axis=1)
    # print(f'{ak.to_list(muRecoTracks[long_trk_both_sides_not_cpa_crosser][evts]) = }')


    # print(f'{ak.num(b.mc_startPoint_tpcAV, axis=1) = }')
    # print(f'{ak.num(trk_both_sides, axis=1) = }')
    # print(f'{ak.num(muRecoTracks, axis=1) = }')
    # print(f'{ak.num(beam_left, axis=1) = }')

    # print(f'{longtrack_mask = }')


    # plot track start TPC%4
    # edges, values, bars = plt.hist(ak.flatten(tpc_start_flat), bins=4, range=(-0.5,3.5))
    # plt.bar_label(bars)
    # plt.show()

    # plot counts of true CPA crossers
    # edges, values, bars = plt.hist(ak.num(true_crosser_mask,axis=-1))
    # plt.bar_label(bars)
    # plt.show()

    return stitchable

def process_tree(args):
    # print(args[1])
    # print(mlp.matplotlib_fname())

    if len(args) > 2 :
        sel.SetDetector(args[2])

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:

        branches = tree.arrays(['mc_startPoint_tpcAV',
                                'mc_endPoint_tpcAV',
                                'trk_length',
                                'trk_hasT0',
                                'trk_T0',
                                'trk_startPoint',
                                'trk_endPoint',
                                'trk_startTpc',
                                'trk_endTpc',
                                'trk_mcMuIdx',
                                'trk_ncalopts',
                                'trk_stitched'
                                ])

        # Reconstructable truth
        #reconable = GetReconstructableMask(branches)

        stitchable = SelectStitchable(tree)

        # plot counts of stitchable muons
        values, edges, bars = plt.hist(ak.sum(stitchable,axis=-1), bins=11, range=(-0.5,10.5))
        plt.cla()
        plt.stairs(values, edges)
        plt.bar_label(bars)
        plt.title("Number of CPA stitchable true muons")
        plt.xlabel('Count per event')
        plt.savefig(outpref+'stitchable_counts.png')


if __name__ == '__main__':
    print('Running processing...')
    process_tree(sys.argv)
