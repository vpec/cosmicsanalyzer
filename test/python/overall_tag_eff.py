#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/overall_tag_eff/'

import selections as sel, tools as tls


def process_tree(tree) :


    # useful data
    # trk dim
    trk_mcMuIdx            = sel.GetMCMatched(tree)
    trkHasMC_mask          = trk_mcMuIdx > -1
    hasT0                  = tree['trk_hasT0'].array()
    t0_mask                = hasT0 > 0
    trk_len_mask           = tree['trk_length'].array() > 20.

    trkHasMCandT0_mask = trkHasMC_mask & t0_mask
    longtrkHasMCandT0_mask = trkHasMC_mask & t0_mask & trk_len_mask

    # mc dim
    trksPerMu              = sel.BuildRecoPerTrue(tree, trk_mcMuIdx[trkHasMC_mask])
    muHasReco              = ak.num(trksPerMu, axis=-1) > 0
    muHasLongReco          = ak.sum(tls.Slice(trk_len_mask, trksPerMu), axis=-1) > 0

    hasT0_perMu        = tls.Slice(hasT0,trksPerMu)


    # dt
    trk_T0 = tree['trk_T0'].array()
    mc_t = tree['mc_startPoint_tpcAV'].array()[trk_mcMuIdx][...,3]
    dt = (trk_T0[longtrkHasMCandT0_mask] - mc_t[longtrkHasMCandT0_mask])*1e-3

    # assuming good tag if -10 us < Dt < 30 us
    good_tag_mask = abs(dt-10.) < 20 # for PDSP prod4 sample
    # good_tag_mask = abs(dt-40.) < 10 # for PDHD MC?

    # calculaate dt for tracks per muon
    hasT0perMu = tls.Slice(t0_mask,trksPerMu)
    T0perMu = tls.Slice(trk_T0,trksPerMu)
    muT = tree['mc_startPoint_tpcAV'].array()[...,3]
    # T0perMu[hasT0perMu].show(type=True)
    # mc_t.show(type=True)
    # ak.num(trksPerMu,axis=1).show(type=True)
    # ak.num(T0perMu[hasT0perMu],axis=1).show(type=True)
    # ak.num(mc_t,axis=1).show(type=True)
    dtPerMu = (T0perMu[hasT0perMu]-muT)*1e-3
    good_tag_perMu_mask = abs(dtPerMu-10.) < 20
    any_good_tag_perMu = ak.any(good_tag_perMu_mask, axis=-1)


    ## truth based selections
    cpaCrossers            = sel.MaskCPACrossersInMC(tree)
    apaInCrossers          = sel.MaskAPAInCrossersInMC(tree)
    apaOutCrossers         = sel.MaskAPAOutCrossersInMC(tree)

    cpaCrossersWithReco    = cpaCrossers & muHasReco
    apaInCrossersWithReco  = apaInCrossers & muHasReco
    apaOutCrossersWithReco = apaOutCrossers & muHasReco

    cpaCrossersWithLongReco    = cpaCrossers & muHasLongReco
    apaInCrossersWithLongReco  = apaInCrossers & muHasLongReco
    apaOutCrossersWithLongReco = apaOutCrossers & muHasLongReco

    # true muons have a T0'd track
    t0_perMu_mask      = tls.Slice(t0_mask,trksPerMu)
    t0_perMu_any_mask  = ak.any(t0_perMu_mask, axis=-1)
    longt0_perMu_mask      = tls.Slice(t0_mask&trk_len_mask,trksPerMu)
    longt0_perMu_any_mask  = ak.any(longt0_perMu_mask, axis=-1)

    # tagged trks dim
    t0d_mcMuIdx            = trk_mcMuIdx[trkHasMCandT0_mask]
    t0d_cpaCrosser_mask    = cpaCrossersWithReco[t0d_mcMuIdx]
    t0d_apaInCrosser_mask  = apaInCrossersWithReco[t0d_mcMuIdx]
    t0d_apaOutCrosser_mask = apaOutCrossersWithReco[t0d_mcMuIdx]
    longt0d_cpaCrosser_mask    = cpaCrossersWithLongReco[t0d_mcMuIdx]    & any_good_tag_perMu[t0d_mcMuIdx]
    longt0d_apaInCrosser_mask  = apaInCrossersWithLongReco[t0d_mcMuIdx]  & any_good_tag_perMu[t0d_mcMuIdx]
    longt0d_apaOutCrosser_mask = apaOutCrossersWithLongReco[t0d_mcMuIdx] & any_good_tag_perMu[t0d_mcMuIdx]


    # tagged and CPA crossers
    count_data = [
        ('cpa_with_reco'          ,   cpaCrossersWithLongReco   ),
        ('apaIn_with_reco'        ,   apaInCrossersWithLongReco ),
        ('apaOut_with_reco'       ,   apaOutCrossersWithLongReco),
        ('apa_with_reco'          ,   apaInCrossersWithLongReco | apaOutCrossersWithLongReco),
        ('matched_long_tracks'    ,   trkHasMC_mask & trk_len_mask),
        ('recod_mus'              ,   muHasLongReco),
        ('tagged'                 ,   t0_mask),
        ('tagged_long'            ,   t0_mask&trk_len_mask),
        ('tagged_good'            ,   good_tag_mask),
        ('tagged_bad'             ,   ~good_tag_mask),
        ('tagged_with_muons'      ,   t0_mask[trk_mcMuIdx > -1]),
        ('tagged_long_with_muons' ,   (t0_mask&trk_len_mask)[trk_mcMuIdx > -1]),
        ('tagged_any_muons'       ,   t0_perMu_any_mask),
        ('any_good_tag_perMu'     ,   any_good_tag_perMu),
        ('tagged_muons'           ,   t0_perMu_mask),
        ('tagged_long_any_muons'  ,   longt0_perMu_any_mask),
        ('tagged_long_muons'      ,   longt0_perMu_mask),
        ('tagged_long_true_CPA'        ,   longt0d_cpaCrosser_mask),
        ('tagged_long_true_APAIn'      ,   longt0d_apaInCrosser_mask),
        ('tagged_long_true_APAOut'     ,   longt0d_apaOutCrosser_mask),
        ('tagged_long_true_APA'        ,   longt0d_apaInCrosser_mask | longt0d_apaOutCrosser_mask),
    ]


    print('='*30)
    counts = { label: ak.sum(count) for label,count in count_data }
    for label, count in counts.items():
        print(f'{label:22} = {count}')

    efficiencies = [
        # ('eff_good'     , counts['tagged_good']/counts['tagged']                  ),
        # ('eff_long_good', ak.sum(good_tag_mask & trk_len_mask[t0_mask])/ak.sum(trk_len_mask) ),
        ('eff_true_mu'   , counts['any_good_tag_perMu']/counts['recod_mus'] ),
        ('eff_long_trks' , counts['tagged_good']/counts['matched_long_tracks']        ),
        #('eff_reco_mus' , counts['tagged_any_muons']/counts['recod_mus']          ),
        ('eff_CPA'       , counts['tagged_long_true_CPA']/counts['cpa_with_reco']       ),
        ('eff_APAIn'     , counts['tagged_long_true_APAIn']/counts['apaIn_with_reco']   ),
        ('eff_APAOut'    , counts['tagged_long_true_APAOut']/counts['apaOut_with_reco'] ),
        ('eff_APA'       , counts['tagged_long_true_APA']/counts['apa_with_reco']       ),
    ]

    print('='*30)
    print('Efficiencies:')
    print('-------------')
    for label, count in efficiencies :
        print(f'{label:20} = {count:.3f}')


    # efficiency per tag: pandora et al.
    # out of all muons with a long reco track, how many have a tag of certain type
    # 'all muons with a long reco track' = counts['recod_mus']
    # need separate longt0_perMu_any_mask by tag.

    print('-'*20)
    print('Efficiencies by T0 tagger:')
    print('-'*20)
    print('True muons:')
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
    for itag,taglabel in enumerate(taggers, 1) :
        mu_has_t0_long_trk_mask = tls.Slice(t0_mask&trk_len_mask&(hasT0==itag),trksPerMu)[hasT0perMu] & good_tag_perMu_mask
        mu_has_any_mask  = ak.any(mu_has_t0_long_trk_mask, axis=-1)
        n_mus_with_t0_long_track = ak.sum(mu_has_any_mask)
        print(f'  {taglabel:20}: {n_mus_with_t0_long_track/counts["recod_mus"]:6.3f}')



    # purity
    # How many of the tagged tracks are muons, how many are CPA crossers and how many are APA crossers
    purities = [
        ('muons'      , counts['any_good_tag_perMu']/counts['tagged_long']),
        ('tracks'       , counts['tagged_good']/counts['tagged_long']),
        ('CPA'        , counts['tagged_long_true_CPA']/counts['tagged_long']),
        ('APAIn'      , counts['tagged_long_true_APAIn']/counts['tagged_long']),
        ('APAOut'     , counts['tagged_long_true_APAOut']/counts['tagged_long']),
        ('APA'        , counts['tagged_long_true_APA']/counts['tagged_long']),
    ]

    print('='*30)
    print('Purities:')
    print('---------')
    for label, count in purities :
        print(f'{label:20} = {count:.3f}')
    print('-'*30)

    print('Purities by tag')
    print('---------------')
    for itag,taglabel in enumerate(taggers, 1) :
        isTag = (hasT0 == itag)
        tag_good = good_tag_mask & isTag[longtrkHasMCandT0_mask]
        tagged_long = isTag[trk_len_mask]
        print(f'  {taglabel:20}: {ak.sum(tag_good)/ak.sum(tagged_long):6.3f}')

    print('='*30)



    # ### plotting ###
    # plt.ioff()

    # # DT
    # tls.plotHist(dt, bins=200, range=(-3e3,3e3),
    #              title='T0 vs true T',
    #              xlabel=r'$\Delta T = T0 - T_{True} [\mu s]$',
    #              outs=outpref+'dt.png',
    #              stats={'ent':True})
    # tls.plotHist(dt, bins=200, range=(-3e3,3e3),
    #              title='T0 vs true T',
    #              xlabel=r'$\Delta T = T0 - T_{True} [\mu s]$',
    #              outs=outpref+'dt_log.png',
    #              log=True,
    #              stats={'ent':True})
    # tls.plotZoomed(dt, bins=160, range=(-30,50),
    #                title='T0 vs True T',
    #                xlabel=r'$\Delta T = T0 - T_{True} [\mu s]$',
    #                outs=outpref+'dt_zoomed.png',
    #                stats={'ent':True})
    # tls.plotZoomed(dt, bins=160, range=(-30,50),
    #                title='T0 vs True T',
    #                xlabel=r'$\Delta T = T0 - T_{True} [\mu s]$',
    #                outs=outpref+'dt_zoomed_log.png',
    #                log=True,
    #                stats={'ent':True})

    # # all data
    # for label,data in count_data :
    #     if not type(data) is ak.Array :
    #         continue
    #     if data.ndim < 2 :
    #         continue
    #     tls.plotHist(ak.sum(data, axis=1), bins=51, range=(-0.5,50),
    #                  title=label, xlabel='Count per event',
    #                  outs=outpref+'perEventCounts_'+label+'.png',
    #                  stats={'ent':True})


    # return



def run(args):
    print(args[1])
    print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        sel.SetDetector(args[3])


    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
