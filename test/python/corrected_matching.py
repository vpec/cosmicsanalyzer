#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak


# use batch graphics mode
mpl.use('agg')


def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())
    fname = args[1]

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    #import selections as sel
    import tools as tls
    import dt_offsets_by_tag as offsets
    import selections as sel

    tlow = -40.
    thi  =  40.


    pdsmatch = sel.PDSMatchSelector(tree)
    # flash candidates for all tracks with T0
    ncandidates = ak.sum(pdsmatch.GetTimeMatchedPairMask(-5.,5.), axis=-1)
    uncorrected_dt = tls.RangeArray(pdsmatch.all_pairs_dt, tlow, thi)
    pdsmatch.ApplyCorrections(offsets.offsets)
    pdsmatch.ApplyTimeRange(tlow, thi)
    candidates   = pdsmatch.GetTimeMatchedPairMask(-5.,5.)
    candidates_dt = pdsmatch.all_pairs_dt[candidates]
    firsts_dt = ak.firsts(candidates_dt, axis=-1)

    candidates_pe = tls.Slice(pdsmatch.flash_pe,pdsmatch.all_pairs['b'][candidates])
    firsts_pecut_dt = ak.firsts(candidates_dt[candidates_pe > 400], axis=-1)

    max_pe_dt = candidates_dt[ak.argmax(candidates_pe,axis=-1,keepdims=1)]
    max_pe_wcut_dt = candidates_dt[(candidates_pe > 400)][ak.argmax(candidates_pe[(candidates_pe > 400)],axis=-1,keepdims=1)]


    common_settings={'bins':160, 'range':(-40,40), 'cla': False}
    plt.cla()
    counts,bins = plot(uncorrected_dt, xlabel=r'$\Delta T$ [$\mu$s]', label='uncorrected', **common_settings)
    counts,bins = plot(pdsmatch.all_pairs_dt, histtype='step', label='corrected', **common_settings)
    plt.legend()
    plt.savefig(outpref+'compare_dt_corrected_uncorrected.png')

    plt.cla()
    common_settings['histtype'] = 'step'
    common_settings['range'] = (-5,5)
    counts,bins = plot(firsts_dt,       label='firsts'      , **common_settings )
    counts,bins = plot(max_pe_dt,       label='maxPE'       , **common_settings )
    counts,bins = plot(firsts_pecut_dt, label='firsts+PEcut', **common_settings )
    counts,bins = plot(max_pe_wcut_dt,  label='maxPE+PEcut' , **common_settings )
    plt.legend()
    plt.savefig(outpref+'compare_dt_corrected_selections.png')

    counts,bins = plot(ncandidates, fname=f'n_flash_candidates_-5_5.png', bins=20, range=(0,20))

    plt.cla()
    plt.title('')
    plt.xlabel('Number of PDS-to-TPC matches')
    common_settings['bins'] = 41
    common_settings['range'] = (-0.5,40.5)
    counts,bins = plot(ak.num(ak.drop_none(firsts_dt),       axis=-1), label='firsts'      , **common_settings )
    counts,bins = plot(ak.num(ak.drop_none(firsts_pecut_dt), axis=-1), label='firsts+PEcut', **common_settings )
    plt.legend()
    plt.savefig(outpref+'compare_number_of_matches.png')




def plot(data, fname=None, clf=False, cla=True, xlabel=False, ylabel=False, legend=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()
    if cla :
        plt.cla()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)
    counts,bins,bars = plt.hist(ak.ravel(data), **kwargs)

    if legend :
        plt.legend()

    if fname not in (None, '') :
        plt.savefig(outpref + fname)
    return counts, bins

if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
