#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/compare_match_data_to_mc/'

import selections as sel, tools as tls
from tools import DataToPlot as DTP
from selections import T0Type
import math

import copy

tag_types = (T0Type.Orig, T0Type.CPA, T0Type.APAIn, T0Type.APAOut)
tag_labels = { T0Type.Orig:'Default', T0Type.CPA:'CPA', T0Type.APAIn:'APAIn', T0Type.APAOut:'APAOut' }


def longTracks(d) :
    return ak.sum(d>20., axis=-1)

def plotComparisonEachTag(data, mc, label, d, func=None, funcargs=None, **kwargs) :
    data_toplot = copy.deepcopy(d)
    mc_toplot = copy.deepcopy(d)

    data_toplot.data = data
    mc_toplot.data = mc

    events_mc = ak.num(mc, axis=0)
    events_data = ak.num(data, axis=0)
    entries_mc = ak.count(mc)
    entries_data = ak.count(data)

    print(f'{ak.ravel(data) = }\n{ak.ravel(mc) = }')

    labels = {'MC':   f'MC:   {entries_mc:8d} entries /{events_mc:7d} events',
              'data': f'data: {entries_data:8d} entries /{events_data:7d} events'}


    plt.clf()
    tls.plotHist(mc_toplot, outs=None, label=labels['MC'], scale=1./events_mc, fill=True, **kwargs)
    tls.plotHist(data_toplot, outs=None, label=labels['data'], scale=1./events_data, histtype='step', **kwargs)
    plt.legend(prop={'family':'monospace'})

    outs=outpref
    if d.outs is not None :
        outs += f'{d.outs}'
    else :
        outs += d.data[0] if isinstance(d.data, tuple) else d.data
    outs += f'_{label}.png'

    plt.savefig(outs)


def plotComparisonAllTags(datatree, mctree, d, **kwargs) :
    func=None
    dstring=''
    if isinstance(d.data, tuple) :
        dstring=d.data[0]
        func=d.data[1]
    else:
        dstring=d.data

    data = datatree[dstring].array()
    mc   =   mctree[dstring].array()

    print(f'Plot all tags: {ak.ravel(data) = }\n{ak.ravel(mc) = }')


    for i in range(len(tag_types)) :
        label=tag_labels[tag_types[i]]
        if func is not None :
            print('Data has associated function')
            data_final = func(data, tree=datatree, t0type=tag_types[i])
            mc_final = func(mc, tree=mctree, t0type=tag_types[i])
        else:
            print('Data does not have associated function')
            data_final = data
            mc_final = mc

        plotComparisonEachTag(data_final,
                              mc_final,
                              label,
                              d, title=f'{d.title}: {label}')

def GetMatchedByT0(d, **kwargs):
    t = kwargs['tree']
    t0type = kwargs['t0type']
    t0mask = (t['trk_hasT0'].array() == t0type)
    t0matchedmask = (t['trk_matched'].array() > 0) & t0mask
    print(f'{t0type = }')
    print(f'{t0mask = }')
    print(f'{t0matchedmask = }')
    print(f'{ak.sum(t0mask) = }')
    print(f'{ak.sum(t0matchedmask) = }')
    print(f'{ak.num(t0matchedmask,axis=1) = }')
    print(f'{ak.num(d,axis=1) = }')
    print(f'{ak.sum(ak.num(d[t0matchedmask],axis=1) != 0) = }')
    return  d[t0matchedmask]




def process_tree(datatree, mctree) :


    d_n_matches = DTP('n_matches')

    # compare distributions for individual T0 tags
    toCompare = [
        DTP(('trk_length', GetMatchedByT0), title='Track length', xlabel='Track length [cm]', bins=150, range=(0,1500)),
    ]

    for d in toCompare :
        d.clf = False
        d.cla = False
        d.stats = False
        d.ylabel='Tracks per event'

        plotComparisonAllTags(datatree, mctree, d, )




def run(args):
    print(args[1], args[2])

    if len(args) > 3 :
        global outpref
        outpref = args[3]
    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as datatree:
        with ur.open(args[2]+':OpDetAna/opdetana') as mctree:
            process_tree(datatree, mctree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
