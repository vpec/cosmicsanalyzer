#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/pds_to_tpc_match/matched_v6/'
ismc = True
time_low = 1
time_hi = 7


import selections as sel, tools as tls


def process_tree(tree) :
    branches = {
        'flash_abs_time'      : {'xlabel': r'Flash time [$\mu$s]',           'bins': 100 , 'range': None, 'xlog': False, 'ylog': False   },
        'flash_tot_pe'        : {'xlabel': 'No PEs per Flash', 'bins': 50  , 'range': (2,2e5), 'xlog': True , 'ylog': True                  },
        'flash_mcMuIdx'       : {'xlabel': None,            'bins': None, 'range': None, 'xlog': False, 'ylog': False                   },
        'mc_startPoint_tpcAV' : {'xlabel': None, 'bins': None, 'range': None, 'xlog': False, 'ylog': False },
        'match_trk_idx'       : {'xlabel': None, 'bins': None, 'range': None, 'xlog': False, 'ylog': False },
        'match_flash_idx'     : {'xlabel': None, 'bins': None, 'range': None, 'xlog': False, 'ylog': False },
        'trk_T0'              : {'xlabel': None, 'bins': None, 'range': None, 'xlog': False, 'ylog': False },
    }

    entry_stop = 1000
    # getting only a portion of the data
    data_to_get = [name for name in branches.keys()] + ['trk_hasT0', 'trk_length', 'trk_mcMuIdx_best']
    tree_data = tree.iterate(data_to_get, entry_stop = entry_stop).__next__()

    for name,a in branches.items():
        a['data']  = tree_data[name]
        a['label'] = name
        a['title'] = tree[name].title

    data = { name: a['data'] for name, a in branches.items() }


    #trkMcIdx = sel.GetMCMatched(tree, entry_stop=entry_stop)
    trkMcIdx = tree_data['trk_mcMuIdx_best']
    flashMcIdx = data['flash_mcMuIdx']

    match_trk_idx = data['match_trk_idx']
    match_flash_idx = data['match_flash_idx']

    if ismc :
        trueMatchMask = (trkMcIdx[match_trk_idx] > -1) & (trkMcIdx[match_trk_idx] == flashMcIdx[match_flash_idx])

        count_matched = ak.count(trueMatchMask)
        count_matched_true = ak.sum(trueMatchMask)

        print(f'{count_matched = }')
        print(f'{count_matched_true = }')
        print(f'{count_matched_true/count_matched = :.3f}')


    # Look at T0 vs PDS time!
    #   tracks
    trk_hasT0 = tree_data['trk_hasT0']
    hasT0 = trk_hasT0 > 0
    trk_hasT0 = trk_hasT0[hasT0]
    trk_t = data['trk_T0'][hasT0]*1e-3
    trkT0McIdx = trkMcIdx[hasT0]
    trkLen = tree_data['trk_length'][hasT0]
    isLong = trkLen > 100 # at least 1 m

    #   flashes
    flash_t = data['flash_abs_time']
    flash_pe = data['flash_tot_pe']

    # pairing trk with T0 with any flash
    trk_idx = ak.local_index(trk_t)
    flash_idx = ak.local_index(flash_t)
    all_pairs = ak.cartesian({'trk':trk_idx, 'flash':flash_idx},nested=True)
    all_pairs_dt = tls.Slice(trk_t,all_pairs['trk']) - tls.Slice(flash_t,all_pairs['flash'])


    print(f'{ak.sum(hasT0) = }')
    print(f'{ak.sum(trk_hasT0 > 0) = }')
    print(f'{ak.sum(trk_hasT0 == 1) = }')
    print(f'{ak.sum(trk_hasT0 == 2) = }')
    print(f'{ak.sum(trk_hasT0 == 4) = }')
    print(f'{ak.sum(trk_hasT0 == 8) = }')

    ak.zip({'TrkIdx':all_pairs['trk'][0],
            'T0Tag':trk_hasT0[0],
            'DT': all_pairs_dt[0],
            'PE':tls.Slice(flash_pe,all_pairs['flash'])[0],
            })[abs(all_pairs_dt[0]) < 100.].show()
    print(f'{all_pairs = }')
    print(f'{all_pairs_dt = }')
    print(f'{ak.num(all_pairs_dt,axis=1) = }')

    def GetMatchedPair() :
        atrkmcidx = tls.Slice(trkT0McIdx,all_pairs['trk'])
        aflashmcidx = tls.Slice(flashMcIdx,all_pairs['flash'])
        return (atrkmcidx > -1) & (atrkmcidx == aflashmcidx)

    matched_pair_mask = GetMatchedPair()

    # n_flashes = ak.num(flash_t, axis=-1)
    # all_pairs_by_track = ak.unflatten(all_pairs, ak.cartesian({'trk':trk_idx, 'flash':n_flashes})['flash'])

    def GetTimeMatchedPair(low=-20., high=30.) :
        '''
        impose cut on DT to be (-20,30) us
        '''
        return (all_pairs_dt > low) & (all_pairs_dt < high)


    # dt for pairs that share the same true muon
    def GetMatchedPairDT() :
        return all_pairs_dt[matched_pair_mask]


    # PE for pairs that share the same true muon
    def GetMatchedPairPE() :
        return tls.Slice(flash_pe,all_pairs['flash'])[matched_pair_mask]
    def GetUnMatchedPairPE() :
        return tls.Slice(flash_pe,all_pairs['flash'])[~matched_pair_mask]
    def GetTimeMatchedPairPE(low=-20., high=30.) :
        return tls.Slice(flash_pe,all_pairs['flash'])[GetTimeMatchedPair(low,high)]


    PECUT=400

    # find pairs for flashes above 200 PE
    def GetHiPEDt():
        hiPE_mask = tls.Slice(flash_pe,all_pairs['flash'])>PECUT
        print(f'{hiPE_mask = }')
        return all_pairs_dt[hiPE_mask]
    def GetHiPEbyT0Dt(tag):
        hiPE_mask = tls.Slice(flash_pe,all_pairs['flash'])>PECUT
        tagMask = tls.Slice(trk_hasT0,all_pairs['trk']) == tag
        return all_pairs_dt[tagMask & hiPE_mask]


    def GetLongTrackTrueMatchedPE():
        longMask = tls.Slice(trkLen,all_pairs['trk']) > 100
        return tls.Slice(flash_pe,all_pairs['flash'])[longMask & matched_pair_mask]

    # PE distribution of matched flashes
    #match_good_flash_idx = data['match_flash_idx']
    matched_tot_pe = flash_pe[match_flash_idx]


    # DT between matched track and flash (in us)
    trkT0  = tree_data['trk_T0']*1e-3
    flashT = tree_data['flash_abs_time']
    matched_dt = trkT0[match_trk_idx] - flashT[match_flash_idx]


    hiPEDt = GetHiPEDt()
    hiPEDtFirsts = ak.firsts(hiPEDt[(hiPEDt > -10.) & (hiPEDt < 20.)], axis=-1)
    # number of canadidates within a time window
    count_hiPE_candidates = ak.sum((hiPEDt > -10.) & (hiPEDt < 20.), axis=-1)


    time_label = r'$\Delta$T$_{T0-flash}$ [$\mu$s]'
    pe_label = 'No. PEs per flash'
    from tools import DataToPlot as Data
    to_plot = [
        Data(all_pairs_dt,
             bins=200, range=(-3e3,3e3),
             title='Trk T0 vs flash time - all combinations',
             xlabel=time_label,
             outs='all_dt.png',),
        Data(all_pairs_dt,
             bins=200, range=(-300,300),
             title='Trk T0 vs flash time - all combinations',
             xlabel=time_label,
             outs='all_dt_zoomed.png',
             zoomed=True),
        Data(all_pairs_dt,
             bins=200, range=(-20,20),
             title='Trk T0 vs flash time - all combinations',
             xlabel=time_label,
             outs='all_dt_zoomed2.png',
             zoomed=True),
        Data(hiPEDt,
             bins=200, range=(-100,100),
             title=f'Trk T0 vs flash time - flash PE > {PECUT}',
             xlabel=time_label,
             outs=f'high_pe_{PECUT}_dt_zoomed.png',
             zoomed=True),
        Data(hiPEDt,
             bins=200, range=(-10,10),
             title=f'Trk T0 vs flash time - flash PE > {PECUT}',
             xlabel=time_label,
             outs=f'high_pe_{PECUT}_dt_zoomed2.png',
             zoomed=True),
        Data(count_hiPE_candidates,
             bins=7, range=(-0.5,6.5),
             scale=1./ak.num(count_hiPE_candidates,axis=0),
             title=fr'Number of flash candidates with PE > {PECUT} and 0 $\mu$s < $\Delta$T < 25 $\mu$s',
             xlabel=fr'Number of flash candidates with PE > {PECUT} and 0 $\mu$s < $\Delta$T < 25 $\mu$s',
             ylabel='Number of tracks per trigger',
             outs=f'count_high_pe_{PECUT}.png',
             zoomed=True),
        Data(hiPEDtFirsts,
             bins=300, range=(-10,20),
             title=f'Trk T0 vs flash time - flash PE > {PECUT} & 1st candidate',
             xlabel=time_label,
             outs=f'high_pe_{PECUT}_firsts_dt_zoomed2.png',
             zoomed=True),
        # high PE dt by T0 tag
        # original
        Data(GetHiPEbyT0Dt(1),
             title='T0 vs flash time - original T0 tag',
             xlabel=time_label ,
             bins=200, range=(-100,100),
             label='Matched',
             zoomed=True,
             outs='hiPEDt_orig_T0.png'),
        # APAIn
        Data(GetHiPEbyT0Dt(2),
             title='T0 vs flash time - APAIn T0 tag',
             xlabel=time_label ,
             bins=200, range=(-100,100),
             label='Matched',
             zoomed=True,
             outs='hiPEDt_APAIn_T0.png'),
        # APAOut
        Data(GetHiPEbyT0Dt(4),
             title='T0 vs flash time - APAOut T0 tag',
             xlabel=time_label ,
             bins=200, range=(-100,100),
             label='Matched',
             zoomed=True,
             outs='hiPEDt_APAOut_T0.png'),
        # CPA
        Data(GetHiPEbyT0Dt(8),
             title='T0 vs flash time - CPA T0 tag',
             xlabel=time_label ,
             bins=200, range=(-100,100),
             label='Matched',
             zoomed=True,
             outs='hiPEDt_CPA_T0.png'),
        # compare to mnatched
        Data(hiPEDt,
             bins=200, range=(-100,100),
             title=f'Trk T0 vs flash time - flash PE > {PECUT}',
             xlabel=time_label,
             label=f'Candidate > {PECUT}',
             stats=False),
        Data(matched_dt,
             title='T0 vs matched flash time',
             xlabel=time_label ,
             bins=200, range=(-100,100),
             clf=False, cla=False,
             legend=True,
             label='Matched',
             outs='matched_dt.png'),
    ] + ([
        Data(GetMatchedPairDT,
             bins=200, range=(-100,100),
             title='Trk T0 vs flash time - true match',
             xlabel=time_label,
             outs='true_dt_zoomed.png',
             zoomed=True),
        Data(ak.firsts(GetMatchedPairDT(), axis=-1),
             bins=200, range=(-100,100),
             title='Trk T0 vs flash time - true first match',
             xlabel=time_label,
             outs='true_dt_firsts_zoomed.png',
             zoomed=True),
        Data(GetMatchedPairPE,
             bins=120, range=(1,1e6),
             title='Total PE in flash matched in truth with a track',
             xlabel=pe_label,
             xlog=True, log=True,
             outs='true_match_pe.png'),
        Data(GetUnMatchedPairPE,
             bins=120, range=(1,1e6),
             title='Total PE in flash NOT matched in truth with a track',
             xlabel=pe_label,
             xlog=True, log=True,
             outs='true_no_match_pe.png'),
        Data(GetLongTrackTrueMatchedPE,
             bins=120, range=(1,1e6),
             title='Total PE in flash matched in truth with a track > 1 m',
             xlabel=pe_label,
             xlog=True, log=True,
             outs='long_true_match_pe.png'),
    ] if ismc else []) + [
        Data(GetTimeMatchedPairPE,
             bins=120, range=(1,1e6),
             title='Total PE in flash matched in time (-20,30) with a track',
             xlabel=pe_label,
             xlog=True, log=True,
             outs='time_match_pe.png'),
        Data(GetTimeMatchedPairPE(time_low,time_hi),
             bins=120, range=(1,1e6),
             title=f'Total PE in flash matched in time ({time_low},{time_hi}) with a track',
             xlabel=pe_label,
             xlog=True, log=True),
        Data(GetTimeMatchedPairPE(time_low,time_hi),
             bins=68, range=(10**(52/20),1e6),
             title=f'Total PE in flash matched in time ({time_low},{time_hi}) with a track',
             xlabel=pe_label,
             xlog=True, log=True, cla=False, clf=False, stats=False,
             outs=f'time_{time_low}_{time_hi}_match_pe_selected.png'),
        Data( matched_tot_pe,
              title='Total PE in flash matched with track',
              xlabel=pe_label,
              bins=60, range=(2e2,2e5),
              xlog=True,
              log=True,
              outs = 'matched_tot_pe.png',
             ),
    ]

    for d in to_plot[:] :
        if d.stats :
            d.stats = {'ent':True}
        tls.plotHist(d, outs=outpref+d.outs if d.outs is not None else None)
        #print(d.data)


def run(args):
    print(args[1])
    print(mlp.matplotlib_fname())


    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global ismc
        if args[3].lower() == 'mc' :
            ismc = True
        elif args[3].lower() == 'data' :
            ismc = False


    print('Processing ' + ('MC' if ismc else 'data'))

    # make sure the output directory exists
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
