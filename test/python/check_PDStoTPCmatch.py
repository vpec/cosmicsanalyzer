#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

import tools as tls


# use batch graphics mode
mpl.use('agg')


def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())
    fname = args[1]

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



def process_tree(tree) :
    #import selections as sel
    import dt_offsets_by_tag as offsets
    import selections as sel


    n_matches = tree['n_matches'].array()
    trk_idx   = tree['match_trk_idx'].array()
    flash_idx = tree['match_flash_idx'].array()

    trk_t     = tree['trk_T0'].array()[trk_idx]*1e-3
    flash_t   = tree['flash_abs_time'].array()[flash_idx]

    trk_tag   = tree['trk_hasT0'].array()[trk_idx]

    print(f'{trk_t   = }')
    print(f'{flash_t = }')


    dt = trk_t - flash_t
    dt_corrected = tls.CorrectOffsetsByTag(dt, trk_tag, offsets.offsets)

    # print(f'{np.isnan(dt) = }')
    # print(f'{np.sum(np.isnan(dt)) = }')
    # nan_index = ak.local_index(dt)[np.isnan(dt)]
    # mask = ak.num(nan_index)>0
    # print(f'{nan_index[mask] = }')
    # print(f'{trk_t[np.isnan(dt)][mask] = }')
    # print(f'Event: {ak.local_index(trk_t, axis=0)[mask]}, '+
    #       f'Tracks: {nan_index[mask]}')



    counts,bins = plot(n_matches, fname='n_matches.png', bins=41, range=(-0.5,40.5))
    counts,bins = plot(dt, fname='dt_uncorrected.png', bins=100, range=(-10,10))
    counts,bins = plot(dt_corrected, fname='dt_corrected.png', bins=100, range=(-10,10))






def plot(data, fname=None, stats=True, clf=False, cla=True, xlabel=False, ylabel=False, legend=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()
    if cla :
        plt.cla()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)

    data = ak.nan_to_none(ak.ravel(data))
    data = ak.drop_none(data)
    counts,bins,bars = plt.hist(data, **kwargs)

    if legend :
        plt.legend()


    if stats :
        labels=('Entries','Mean','Std')
        st = (ak.count(data), ak.mean(data), ak.std(data))
        tls.PlotStatbox( dict(zip(labels,st)) )


    if fname not in (None, '') :
        plt.savefig(outpref + fname)
    return counts, bins




if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
