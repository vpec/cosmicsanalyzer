#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

mpl.use('agg')

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

import tools as tls

# use batch graphics mode
#mpl.use('agg')

geometry = ((-357.791,357.791),(7.275,607.499), (-0.49375,695.286))


def run(args):
    fname = args.fname
    global outpref, apply_offset, ticks_per_us, close_to_anode_only, is_data
    outpref, apply_offset, ticks_per_us,close_to_anode_only,is_data = (args.outpref,args.apply_offset, args.clock, args.close_to_anode_only,args.is_data)

    # get input tree
    branches = [
        'trk_startPoint','trk_endPoint','trk_hasT0', 'trk_mcMuIdx_best',
        'mc_endPoint_tpcAV', 'mc_exited_tpcAV', 'mc_hasME', 'mc_ME_en','mc_pdg', 'mc_ME_dt',
        'trk_stopped', 'trk_matched', 'match_flash_idx',
        'flash_abs_time', 'flash_nhits', 'flash_hit_idx',
        'hit_wfmIdx', 'wfm', 'wfm_time','wfm_size', 'wfm_hit', 'hit_channel']
    #data = ur.iterate(fname+':OpDetAna/opdetana', branches,step_size=20)

    tree = ur.open(fname+':OpDetAna/opdetana')
    # get relevant data
    data_to_plot = 0
    first=True
    last_evt = -1
    counter = 0
    for data in tree.iterate(branches, step_size=200, entry_stop=args.nmax) :
#    data = tree.arrays(branches, entry_stop=args.nmax)
        print(f'Iterating over batch {counter}')
        counter += 1

        tracks = (data.trk_stopped == 1)

        trk_endPoint = data.trk_endPoint
        trk_startPoint = data.trk_startPoint
        trk_endPoint = ak.where( trk_endPoint[...,1]<trk_startPoint[...,1], trk_endPoint, trk_startPoint )
        if args.close_to_anode_only :
            trk_close_mask = (geometry[0][1] - abs(data.trk_endPoint[...,0])) < 100. # track's end point max 1 m from an APA
            tracks = tracks & trk_close_mask

        evtid = ak.local_index(data, axis=0)
        evtid = evtid + last_evt + 1
        last_evt = evtid[-1]
        trkid = ak.local_index(data.trk_stopped, axis=1)[tracks]

        wfms = GetTrkWfms(data,tracks)
        #wfms.show(limit_cols=100,type=True)
        #ak.num(wfms, axis=-2).show()

        # # get rid of empty events
        # wfm_mask=ak.num(wfms, axis=2) > 0
        # wfms = wfms[wfm_mask]
        # wfm_mask.show(type=True)
        # wfms.show(limit_cols=100,type=True)

        avg_wfms = MakeAverage(wfms)
        tmp_dict = {
            't0type':data.trk_hasT0[tracks],
            'reco_xyz':trk_endPoint[tracks][...,:3],
            'chan':wfms.chan,'wfms':wfms.wfms,'avg_wfms':avg_wfms,'flash_time':ak.firsts(wfms.flash_time,axis=-1),
            'evtid':evtid,'trkid':trkid}
        if not args.is_data :
            tmp_dict['mu_pdg'] = data.mc_pdg[data.trk_mcMuIdx_best[tracks]]
            tmp_dict['trueStopper'] = data.mc_exited_tpcAV[data.trk_mcMuIdx_best[tracks]]==0
            tmp_dict['true_xyz'] = data.mc_endPoint_tpcAV[data.trk_mcMuIdx_best[tracks]][...,:3]
            tmp_dict['true_Michel'] = data.mc_hasME[data.trk_mcMuIdx_best[tracks]] == 1
            tmp_dict['true_Michel_en'] = data.mc_ME_en[data.trk_mcMuIdx_best[tracks]]*1e3
            tmp_dict['true_Michel_dt'] = data.mc_ME_dt[data.trk_mcMuIdx_best[tracks]]

        dd = ak.zip(tmp_dict, depth_limit=2)

        if first :
            data_to_plot = dd
            first=False
        else :
            data_to_plot = ak.concatenate([data_to_plot,dd])

    PlotWfms(data_to_plot)
    SaveWfms(data_to_plot)



def MakeAverage(d) :
    wfms=d.wfms
    if apply_offset :
        ### apply offsets
        # get offsets w.r.t. min trigger time
        offsets = d.wfm_time - ak.min(d.wfm_time,axis=-1)
        offsets = ak.values_astype(offsets*ticks_per_us,np.int32)
        #mean_offset = ak.mean(offsets,axis=-1)
        max_roll=ak.max(offsets)
        width = ak.ravel(ak.num(wfms,axis=-1))[0]
        # remove pedestal
        wfms = wfms - 1500. # PDSP prod4 MC pedestal
        # pad wfms before shifting
        wfms = ak.fill_none(ak.pad_none(wfms,width+max_roll,axis=-1), 0.)
        wfms=Roll(wfms,offsets)

    return ak.sum(wfms, axis=-2) #/ ak.num(wfms, axis=-2)




def Roll(data, offsets) :
    idcs = ak.local_index(data, axis=-1)
    #idcs.show(limit_cols=100,type=True)
    # shift indices to the right = subtract offset!
    idcs = (idcs-offsets)%ak.num(data,axis=-1)
    #idcs.show(limit_cols=100,type=True)
    return data[idcs]

# def SumWfms(wfms) :
#     return ak.sum(wfms,axis=-2)

def SaveWfms(d) :
    f = ur.recreate(outpref+f'pds_wfms_hists.root')
    for evt in d :
        for trk in evt :
            #trk.avg_wfms.show(type=True)
            f[f'evt{trk.evtid}/trk{trk.trkid}/avgwfm'] = ToHist(trk.avg_wfms)
            for i,wfm in enumerate(zip(trk.wfms,trk.chan)) :
                f[f'evt{trk.evtid}/trk{trk.trkid}/wfm{i}_ch{wfm[1]}'] = ToHist(wfm[0])
    f.close()

def ToHist(wfm) :
    return (wfm.to_numpy(), np.arange(len(wfm)+1))


def PlotWfms(d):
    for evt in d :
        for trk in evt :
            y = ak.ravel(trk.avg_wfms)
            x = np.arange(len(y)) / ticks_per_us
            plt.cla()
            plt.title(f'Summed waveform - evt {trk.evtid}, trk {trk.trkid}')
            plt.xlabel('Time [$\\mu$s]')
            plt.plot(x,y)
            AddInfo(trk)
            plt.savefig(outpref+f'avg_wfm_evt{trk.evtid}_trk{trk.trkid}.png')

def AddInfo(d) :
    global is_data
    taggers =  ['pandora', 'crttag', 'crtreco', 'anodepiercerst0', 'CPA', 'APAIn', 'APAOut' ]
    mu_label = {13:'mu-', -13:'mu+'}
    text=f'# waveforms = {len(d.wfms)}'
    if not is_data :
        text+='\n'+f'{mu_label[d.mu_pdg]}'
        text+='\n'+f'True stopper = {d.trueStopper}'
        if d.trueStopper :
            text+='\n'+f'Has Michel = {d.true_Michel}'
            if d.true_Michel :
                text+='\n'+f'Michel energy = {d.true_Michel_en:.1f} MeV'
                text+='\n'+rf'Michel $\Delta$T = {d.true_Michel_dt:.1f} $\mu$s'
    text+='\n'+f'Tag = {taggers[d.t0type-1]}'
    text+='\n'+f'Reco end = {d.reco_xyz} cm'
    if not is_data :
        text+='\n'+f'True end = {d.true_xyz} cm'
    text+='\n'+f'Reco from APA = {geometry[0][1] - abs(d.reco_xyz[0]):.0f} cm'
    if not is_data :
        text+='\n'+f'True from APA = {geometry[0][1] - abs(d.true_xyz[0]):.0f} cm'

    ax=plt.gca()
    ax.text(0.95,0.95, text, va='top',ha='right',transform=ax.transAxes)


def GetTrkWfms(data, tracks) :
    trk_flashes = GetMatchedFlashes(data, tracks)
    return GetFlashWfms(data,trk_flashes)


def GetMatchedFlashes(data, tracks) :
    trk_flashes = data.match_flash_idx[data.trk_matched[tracks]]
    return trk_flashes

def GetFlashWfms(data, flashes) :
    # first get flash hits

    flash_hit_idx = tls.ClipData(data.flash_hit_idx[flashes], data.flash_nhits[flashes])
    flashwfmsidx = tls.Slice(data.hit_wfmIdx,flash_hit_idx)
    hit_chan  = tls.Slice(data.hit_channel,flash_hit_idx)
    # flash_hit_idx.show(limit_cols=100,type=True)
    # flashhits.show(limit_cols=100,type=True)
    #flashwfms.show(limit_cols=100,type=True)
    wfms      = tls.Slice(data.wfm,flashwfmsidx,axis=-2)
    wfmsizes  = tls.Slice(data.wfm_size,flashwfmsidx)
    wfmtimes  = tls.Slice(data.wfm_time,flashwfmsidx)

    flashTime = tls.Slice(data.flash_abs_time, flashes)

    return ak.zip({'chan':hit_chan, 'wfms':tls.ClipData(wfms,wfmsizes),
                   'wfm_time':wfmtimes, 'flash_time':flashTime},depth_limit=3)


if __name__ == '__main__':
    print('Running processing...')

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outpref',nargs='?',default='')
    # parser.add_argument('-d', default='pdsp')
    parser.add_argument('-n','--nmax', default=10, type=int)
    parser.add_argument('--clock', default=150, type=float) # 150 ticks/us ... PDSP, prod4 MC
    parser.add_argument('-o', '--apply-offset',action='store_true')
    parser.add_argument('-c','--close-to-anode-only',action='store_true')
    parser.add_argument('-d','--is-data',action='store_true')



    args = parser.parse_args()

    # make sure the output directory exists
    os.makedirs(os.path.dirname(args.outpref), exist_ok=True)


    import cProfile
    #cProfile.run('run()', 'plot_wfms_of_stopped.py.profile_stats')
    run(args)
