#!/bin/env python

import sys
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector

import tools as tls, selections as sel

import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'


gDriftVel = 0.1565; # in cm/us // from ProtoDUNE fhicl //at 87.68 K and 486.7 V/cm as for ProtoDUNE


boundary = (
    (-357.791, 357.791),
    (   7.275, 607.499),
    (-0.49375, 695.286)
)


# prepare bounding box dimensions
top = [max(a[0],a[1]) for a in boundary ]
bot = [min(a[0],a[1]) for a in boundary ]

def AddStatBox(mean, std) :
    ax = plt.gca()
    ax.text(0.98, 0.88, f'mean = {mean:5.2f}\n std.dev. = {std:5.2f}',
                       transform = ax.transAxes, ha='right', fontfamily = 'monospace')


def createHist(data, figure, bins = 1, range = (0,1), title = '', color = 'blue') :

    ax = plt.gca()
    ax.set_xlabel('Number of muons per event')
    ax.set_title(title)

    h, bins, patches = ax.hist(data, bins = bins, range = range, histtype='step', color=color)
    mean = ak.mean(data)
    std = ak.std(data)
    AddStatBox(mean, std)
    total = np.sum(data)

    return (h, bins, title, total, mean, std)


def replot(h, bins, title, color, total, mean, std) :
    ax = plt.gca()
    ax.set_xlabel('Number of muons per event')
    ax.set_title(title)
    plt.stairs(h, bins)
    AddStatBox(mean, std)



outpref = 'plots/python/reco_trk_counting/v6_tag'

def process_tree(args):
    print(args[1])
    if len(args) > 2 :
        global outpref
        outpref = args[2]
    # make sure the output directory exists
    import os
    print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)


    print(mlp.matplotlib_fname())
    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        totals = []
        means = []

        branches = tree.arrays(['n_reco_tracks',
                                'trk_length',
                                'trk_mcMuIdx',
                                'trk_hasT0',
                                'trk_matched',
                                ])

        all_tracks              = branches.n_reco_tracks
        long_tracks_cut         = branches.trk_length >= 20.
        matched_tracks_cut      = np.sum(branches.trk_mcMuIdx >= 0, axis = -1) > 1
        matched_long_tracks_cut = long_tracks_cut & matched_tracks_cut
        original_tag_cut        = branches.trk_hasT0 == 1
        new_CPA_tag_cut         = (branches.trk_hasT0 == 8)
        APAIn_tag_cut           = branches.trk_hasT0 == 2
        APAOut_tag_cut          = branches.trk_hasT0 == 4
        anytag_cut              = branches.trk_hasT0 > 0
        pds_matched_track_cut   = branches.trk_matched >= 0

        # number of long tracks per true muon
        mu_trks = sel.BuildRecoPerTrue(tree)
        mu_trks_len_mask = tls.Slice(branches.trk_length, mu_trks) > 20.
        num_long_trks_per_mu = ak.sum(mu_trks_len_mask,axis=-1)


        hist_labels = [
            'reco_tracks',
            'long_tracks',
            'long_tracks_origtag',
            'long_tracks_cpatag',
            'long_tracks_apaintag',
            'long_tracks_apaouttag',
            'long_tracks_anytag',
            'long_tracks_pds_matched_origtag',
            'long_tracks_pds_matched_cpatag',
            'long_tracks_pds_matched_apaintag',
            'long_tracks_pds_matched_apaouttag',
            'long_tracks_pds_matched_anytag',
            'long_tracks_per_true_mu',
        ]

        hist_data = [
            all_tracks,
            np.sum(long_tracks_cut, axis = 1),
            np.sum(original_tag_cut, axis = 1),
            cpa_long_summed := np.sum(new_CPA_tag_cut, axis = 1) / 2,
            np.sum(APAIn_tag_cut, axis = 1),
            np.sum(APAOut_tag_cut, axis = 1),
            np.sum(anytag_cut, axis = 1) - cpa_long_summed,
            np.sum(original_tag_cut & pds_matched_track_cut, axis = 1),
            cpa_long_matched_summed := np.sum(new_CPA_tag_cut & pds_matched_track_cut, axis = 1) / 2,
            np.sum(APAIn_tag_cut & pds_matched_track_cut, axis = 1),
            np.sum(APAOut_tag_cut & pds_matched_track_cut, axis = 1),
            np.sum(anytag_cut & pds_matched_track_cut, axis = 1) - cpa_long_summed,
            ak.ravel(num_long_trks_per_mu),
        ]

        hist_titles = [
            'Reconstructed tracks',
            'Reco tracks > 20 cm',
            'Reco tracks > 20 cm \nwith T0',
            'Reco tracks > 20 cm \nwith T0',
            'Reco tracks > 20 cm \nwith T0',
            'Reco tracks > 20 cm \nwith T0',
            'Reco tracks > 20 cm \nwith T0',
            'Reco tracks > 20 cm \nwith T0 and PDS match',
            'Reco tracks > 20 cm \nwith T0 and PDS match',
            'Reco tracks > 20 cm \nwith T0 and PDS match',
            'Reco tracks > 20 cm \nwith T0 and PDS match',
            'Reco tracks > 20 cm \nwith T0 and PDS match',
            'Number of tracks > 20 cm per true muon',
            ]

        # prepare output figure
        #plt.().xlabel('Number of muons per event')
        fig,axes = plt.subplots(3, 5, figsize = (18,12))

        # plots hists on a single figure
        from itertools import cycle
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        hists = {}
        for iaxis, (key,data,title,color) in enumerate(zip(hist_labels, hist_data, hist_titles, cycle(colors))) :
            if iaxis > 1 :
                iaxis += 3
            #fig.sca(fig.axes[iaxis])
            if key == 'long_tracks_per_true_mu' :
                h, bins, title, total, mean, std = createHist(data,
                                                              fig, title = title,
                                                              bins = 11, range = (-0.5,10.5), color=color)
            else :
                h, bins, title, total, mean, std = createHist(data,
                                                              fig, title = title,
                                                              bins = 250, range = (-0.5,249.5), color=color)
            hists[key] = (h, bins, title, color, total, mean, std)

            totals.append((title,total))
            means.append((title, mean, std))




        labels = ['Original tag', 'New CPA stitch', 'APA In', 'APA Out', 'Any']
        for ax,label in zip(fig.axes[5:], labels+labels) :
            leg = ax.legend(labels=[label], loc = 'lower right')

        print('Saving multi-hist figure...')
        fig.tight_layout()
        plt.savefig(f'{outpref}/hists.png', bbox_inches='tight')
        plt.savefig(f'{outpref}/hists.pdf', bbox_inches='tight')


        # plot hists in separate figures and save those
        print('Saving single-hist figures...')
        iaxis = 0
        for key, hist in hists.items() :
            fig,axes = plt.subplots()
            replot(*hist)
            if iaxis > 1 :
                axes.legend(labels=[labels[(iaxis-2)%5]], loc = 'lower right')

            fig.tight_layout()
            fig.savefig(f'{outpref}/{key}_hist.png')
            iaxis += 1





        print('='*30)
        print('Total counts:')
        print('-'*30)
        for label, tot in zip([''*5] + labels, totals) :
            print(f'{tot[0]+","+label:>50s} = {int(tot[1]):6d}'.replace('\n',''))

        print('='*30)
        print('Counts per Event:')
        print('-'*30)
        for label, mean in zip([''*5] + labels, means) :
            print(f"{mean[0]+','+label:>50s} = {mean[1]:6.2f} (stddev = {mean[2]:.2f})".replace('\n', ''))
        print('='*30)





        # runs = tree["run"].array()
        #pos = tree["mc_startPoint_tpcAV"].array()

        # for evt in tree:
        # plt.hist(runs, 200)
        # plt.show()

        # print(runs)

        # tree["gen_pos"].show()
        # print(pos)

        # plt.hist(ak.flatten(pos[:-1,:-1,0]), bins=100, range=(-600, 600))

        # plt.xlabel('x [cm]')

        # plt.show()



if __name__ == '__main__':
    print('Running processing...')
    process_tree(sys.argv)
