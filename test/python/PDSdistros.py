#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
# import vector
# import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/pds_counts/'
ismc = True


import selections as sel, tools as tls

#import dunestyle.matplotlib as dunestyle

entry_stop = 4000

whichdet = 'pdsp'

def process_tree(tree) :
    doBasicPlots(tree)
    if ismc :
        doMCPlots(tree)

def doBasicPlots(tree) :
    branches = {
        'n_flashes'           : {'xlabel': 'No Flashes per event',           'bins': None , 'range': None, 'xlog': False, 'ylog': False},#150 , 'range': (125,275), 'xlog': False, 'ylog': False },
        'flash_abs_time'      : {'xlabel': r'Flash time [$\mu$s]',           'bins': 320 , 'range': (-3.2e3,3.2e3), 'xlog': False, 'ylog': False       },
        'flash_time'      : {'xlabel': r'Flash time [$\mu$s]',           'bins': 320 , 'range': (-3.2e3,3.2e3), 'xlog': False, 'ylog': False       },
        'flash_tot_pe'        : {'xlabel': 'No PEs per Flash', 'ylabel': 'Entries per event', 'bins': 100  , 'range': (2,2e5), 'xlog': True , 'ylog': True, 'scale': 'event' },
        'flash_nhits'         : {'xlabel': 'No Hits per Flash', 'bins': 251, 'range': (-0.5,250.5), 'xlog': False, 'ylog': True             },
        'flash_mcPE'          : {'xlabel': 'No true PEs in Flash', 'bins': 160, 'range': (1,1e8), 'xlog': True, 'ylog': True                 },
        'flash_mcMuIdx'       : {'xlabel': None, 'bins': 121, 'range': (-1,120), 'xlog': False, 'ylog': True                     },
        'mc_startPoint_tpcAV' : {'xlabel': 'True T [ns]', 'bins': None, 'range': None, 'xlog': False, 'ylog': False                         },
        'gen_pos'             : {'xlabel': 'True T_{gen} [ns]', 'bins': None, 'range': None, 'xlog': False, 'ylog': False },
    }


    for name,a in branches.items():
        a['data']  = tree[name].array(entry_stop=entry_stop)
        a['label'] = name.removeprefix('flash_')

    d = branches['flash_mcPE']['data']
    branches['flash_mcPE']['data'] = d[d>0]
    d = branches['mc_startPoint_tpcAV']['data']
    branches['mc_startPoint_tpcAV']['data'] = d[...,3]
    d = branches['gen_pos']['data']
    branches['gen_pos']['data'] = d[...,3]

    ihist = 0
    for key,a in branches.items():
        if (not ismc) and ihist > 4 :
            break
        ihist += 1
        label = a['label']
        xlabel = a['xlabel'] if a['xlabel'] is not None else label
        print(f'{ihist}: Plotting {label}')
        tls.plotHist(a['data'], title='', xlabel=xlabel, ylabel=a['ylabel'] if 'ylabel' in a else '',
                     bins=a['bins'], range=a['range'],
                     outs=f'{outpref}{label}.png',
                     xlog=a['xlog'], log=a['ylog'],
                     stats={'ent':True}, scale=(a['scale']) if 'scale' in a else None)


def doMCPlots(tree) :
    # PE for truth matched flashes
    idx                 = tree['flash_mcMuIdx'].array(entry_stop=entry_stop)
    mc_len              = tree['mc_length_tpcAV'].array(entry_stop=entry_stop)
    mc_long_mask        = mc_len > 20.
    idx_mask            = idx>-1
    pe_per_true_matched = tree['flash_tot_pe'].array(entry_stop=entry_stop)[idx_mask]
    pe_per_true_not_matched = tree['flash_tot_pe'].array(entry_stop=entry_stop)[~idx_mask]
    tls.plotHist(pe_per_true_not_matched, title='', xlabel='', label='MC not matched',
                 bins=50, range=(2, 2e5), histtype='step', stats=False,
                 xlog=True, log=True)
    tls.plotHist(pe_per_true_matched, title='PEs for MC matched and unmatched flashes',
                 xlabel='Flash Total No PE', label='MC matched',
                 bins=50, range=(2, 2e5), histtype='step', stats=False,
                 xlog=True, log=True,  cla=False, clf=False)
    plt.legend()
    plt.savefig(outpref+'pe_per_true_match.png')

    # delta T0
    # time to consider
    ttc = 'flash_abs_time'
    global whichdet
    if whichdet == 'pdhd' :
        ttc = 'flash_time'

    true_t = tree['mc_startPoint_tpcAV'].array(entry_stop=entry_stop)[...,3]
    #true_t = tree['gen_pos'].array(entry_stop=entry_stop)
    reco_t = tree[ttc].array(entry_stop=entry_stop)
    dt = reco_t[idx_mask] - true_t[idx[idx_mask]]*1e-3

    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=240, range=(-3000,3000),
                 log=False, stats={'ent':True},
                 outs=outpref+'dt.png')
    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=240, range=(-3000,3000),
                 log=True, stats={'ent':True},
                 outs=outpref+'dt_log.png')
    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=250, range=(-500,500),
                 log=False, stats={'ent':True},
                 outs=outpref+'dt_zoom.png')
    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=250, range=(-500,500),
                 log=True, stats={'ent':True},
                 outs=outpref+'dt_zoom_log.png')
    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=120, range=(-30,30),
                 log=False, stats={'ent':True},
                 outs=outpref+'dt_zoom2.png')
    tls.plotHist(dt, title='Reco flash time vs true time', xlabel=r'$\Delta$T$_{reco-true}$ [$\mu$s]',
                 bins=120, range=(-30,30),
                 log=True, stats={'ent':True},
                 outs=outpref+'dt_zoom2_log.png')

    # # investigate bi-peaked structure
    # run = tree['run'].array(entry_stop=entry_stop)
    # selected_evtid = ak.local_index(dt, 0)
    # selected_evtids = ak.broadcast_arrays(selected_evtid, dt)[0]
    # print(f'{run = }')
    # print(f'{idx_mask = }')
    # print(f'{dt = }')
    # print(f'{selected_evtid = }')
    # print(f'{selected_evtids = }')
    # mc_masksed_runs = run[ak.any(idx_mask,1)]
    # dt_masked_runs = mc_masksed_runs[ak.ravel(selected_evtids)]
    # print(f'{mc_masksed_runs = }')
    # print(f'{dt_masked_runs = }')
    # tls.plotHist(dt_masked_runs, title='Runs for flash', xlabel='Run ID',
    #              bins=240,
    #              log=False, stats={'ent':True},
    #              outs=outpref+'runs_per_flash.png')



    # return

    # Get flashes per true muon
    flashesPerTrue = sel.BuildFlashPerTrue(tree, idx)[mc_long_mask]
    count_per_true = ak.num(flashesPerTrue, axis=-1)

    # plot the counts
    tls.plotHist(count_per_true,
                 title='Number of reco flashes per true muon', xlabel='No Flashes',
                 bins=16, range=(-0.5,15.5), stats={'ent':True},
                 #log=True,
                 outs=outpref+'n_flashes_per_true.png')

    flash_pe_per_true = tree['flash_tot_pe'].array(entry_stop=entry_stop)[ak.flatten(flashesPerTrue,axis=-1)]
    divs = tls.GetLayout(flashesPerTrue)
    flash_pe_per_true = tls.RebuildFromLayout(flash_pe_per_true, divs)

    # count flashes over threshold of 200 PE
    mask_thld_per_true = flashesPerTrue > 200
    tls.plotHist(ak.sum(mask_thld_per_true, axis=-1),
                 title='Number of reco flashes >200 PE per true muon', xlabel='No Flashes',
                 bins=15, range=(0.5,15.5), stats={'ent':True},
                 #log=True,
                 outs=outpref+'n_flashes_thld_per_true.png')

    # plot PE for first flash per true muon
    flash_pe_per_true_first = ak.firsts(flash_pe_per_true, axis=-1)
    tls.plotHist(flash_pe_per_true_first, title='PEs of first flash for MC muon',
                 xlabel='Flash Total No PE',
                 bins=50, range=(2, 2e5),
                 xlog=True, log=True, stats={'ent':True},
                 outs=outpref+'first_flash_pe_per_muon.png')

    # plot PE for largest flash per true muon
    flash_idx_per_true_pe_sort = ak.argsort(flash_pe_per_true, ascending=False)
    flash_pe_per_true_sort = flash_pe_per_true[flash_idx_per_true_pe_sort]
    flash_pe_per_true_max = ak.firsts(flash_pe_per_true_sort, axis=-1)
    tls.plotHist(flash_pe_per_true_max, title='PEs of largest flash for MC muon',
                 xlabel='Flash Total No PE',
                 bins=50, range=(2, 2e5),
                 xlog=True, log=True, stats={'ent':True},
                 outs=outpref+'max_flash_pe_per_muon.png')

    # the same for second largest flash
    has_two_mask = ak.num(flash_pe_per_true_sort, axis=-1) > 1
    flash_pe_per_true_second = flash_pe_per_true_sort[has_two_mask][...,1]
    tls.plotHist(flash_pe_per_true_second, title='PEs of 2nd largest flash for MC muon',
                 xlabel='Flash Total No PE',
                 bins=50, range=(2, 2e5),
                 xlog=True, log=True, stats={'ent':True},
                 outs=outpref+'second_flash_pe_per_muon.png')

    # Time between first and second largest flash
    flash_t_per_true = tree[ttc].array(entry_stop=entry_stop)[ak.flatten(flashesPerTrue,axis=-1)]
    flash_t_per_true = tls.RebuildFromFlat(ak.flatten(flash_t_per_true), divs)
    flash_t_per_true_pe_sort = flash_t_per_true[flash_idx_per_true_pe_sort]
    flash_t_per_true_pe_sort_two = flash_t_per_true_pe_sort[has_two_mask]
    flash_first_pe_dt = flash_t_per_true_pe_sort_two[...,1] - flash_t_per_true_pe_sort_two[...,0]
    tls.plotHist(flash_first_pe_dt, title='Delay of 2nd largest flash w.r.t. 1st for true MC muon',
                 xlabel=r'$\Delta$T [$\mu$s]',
                 bins=120 , range=(-30,30), stats={'ent':True},
                 outs=outpref+'dt_first_pe_flashes_per_muon.png')


    # Time between second and first flash (in time)
    flash_t_per_true_sort = ak.sort(flash_t_per_true)
    flash_t_per_true_sort_two = flash_t_per_true_sort[has_two_mask]
    flash_first_t_dt = flash_t_per_true_sort_two[...,1] - flash_t_per_true_sort_two[...,0]
    tls.plotHist(flash_first_t_dt, title='Delay of 2nd flash after 1st for true MC muon',
                 xlabel=r'$\Delta$T [$\mu$s]',
                 bins=120 , range=(0,60), stats={'ent':True},
                 outs=outpref+'dt_first_t_flashes_per_muon.png')


    # Dt wrt true time for first, second,... flash
    has_one_mask = ak.num(flash_pe_per_true_sort, axis=-1) > 0
    has_three_mask = ak.num(flash_pe_per_true_sort, axis=-1) > 2
    has_four_mask = ak.num(flash_pe_per_true_sort, axis=-1) > 3
    flash_t_per_true_sort_three = flash_t_per_true_sort[has_three_mask]
    flash_t_per_true_sort_four = flash_t_per_true_sort[has_four_mask]

    from tools import DataToPlot as Data
    to_plot = [
        Data(ak.firsts(flash_t_per_true_sort[has_one_mask], axis=-1) - true_t[has_one_mask]*1e-3, title='first', outs=outpref+'dt_true_first.png'),
        Data(flash_t_per_true_sort_two[...,1] - true_t[has_two_mask]*1e-3, title='second', outs=outpref+'dt_true_second.png'),
        Data(flash_t_per_true_sort_three[...,2] - true_t[has_three_mask]*1e-3, title='third', outs=outpref+'dt_true_third.png'),
        Data(flash_t_per_true_sort_four[...,3] - true_t[has_four_mask]*1e-3, title='fourth', outs=outpref+'dt_true_fourth.png'),
        ]

    for d in to_plot:
        tls.plotHist(d, bins=120, range=(-30,30), stats={'ent':True},
                     title='Reco flash time vs true time: '+d.title+'flash',
                     xlabel=r'$\Delta$T [$\mu$s]')

def run(args):
    print('Input:', args[1])
    #print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    if len(args) > 3 :
        global whichdet
        whichdet = args[3]
        sel.SetDetector(whichdet)

    # make sure the output directory exists
    print('Output prefix:', os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    global ismc
    if len(args) > 3 :
        if args[3].lower() == 'mc' :
            ismc = True
        if args[3].lower() == 'data' :
            ismc = False



    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    print('Running processing...')
    run(sys.argv)
