import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import numpy as np
import awkward as ak

import matplotlib as mpl
#mpl.use('agg')


def run():

    fname=sys.argv[1]


    with ur.open(fname+':OpDetAna/opdetana') as t :
        b = t.arrays(['trk_hasT0', 'trk_startPoint', 'trk_endPoint', 'trk_T0'])


    plt.hist(ak.ravel(b.trk_hasT0), bins=10, range=(-0.5,9.5))
    plt.savefig('plots/python/hasT0.png')


    starts = b.trk_startPoint[b.trk_hasT0>0]
    ends   = b.trk_endPoint[b.trk_hasT0>0]
    x = merge(starts,ends,0)
    y = merge(starts,ends,1)
    plt.plot(x,y)
    plt.savefig(f'plots/python/tracks_xy.png')


    plotTracks(starts, ends, b.trk_hasT0[b.trk_hasT0>0])


def plotTracks(starts, ends, hasT0):
    plt.cla()

    #print(hasT0)

    data=[0,0,0]
    types=[0]*3
    for i,_ in enumerate(data) :
        data[i] = merge(starts, ends, i)
        types[i],data[i] = SortByType(ak.ravel(hasT0), data[i])

    print('Types =', types)
    print('Data =',  data)

    type_names = {1:'APAIn', 2:'CPA', 3:'APAOut', 4:'CRTreco', 5:'AnodePiercer', 6:'pandora', 7:'CRTtag'}

    projections = [(0,1), (2,1), (2,0)]
    proj_names = ['xy', 'zy', 'zx']
    labels = [('x [cm]','y [cm]'),('z [cm]','y [cm]'),('z [cm]','x [cm]')]

    for i,t in enumerate(types[0]):
        for iproj in range(3):
            x = data[projections[iproj][0]][:,i]
            y = data[projections[iproj][1]][:,i]
            plt.cla()
            plt.plot(x, y)
            plt.xlabel(labels[iproj][0])
            plt.ylabel(labels[iproj][1])
            plt.savefig(f'plots/python/tracks_t0type_{type_names[t]}_{proj_names[iproj]}.png')

def merge(starts, ends, axis):
    def toFlat4vectors(d):
        return ak.values_astype(ak.unflatten(ak.ravel(d), 4), np.float64)
    starts = toFlat4vectors(starts)
    ends   = toFlat4vectors(ends)
    merge  = ak.zip([starts[...,axis],ends[...,axis]])
    merge  = ak.unflatten(ak.ravel(merge), 2)
    merge = np.transpose(merge)
    return merge



def SortByType(types, data):
    sorting = ak.argsort(types, axis=-1)
    types_sorted = types[sorting]
    runs = ak.ravel(ak.run_lengths(types_sorted))
    data_sorted = data[:,sorting]
    which_ax = types.ndim
    types = ak.firsts(ak.unflatten(types_sorted,runs,axis=-1), axis=-1)
    result = ak.unflatten(data_sorted, ak.concatenate([runs]*2), axis=which_ax)
    return types,result


if __name__ == '__main__' :
    run()
