#!/bin/env python

import sys, os, argparse
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import awkward as ak

import tools as tls
import selections as sel

# use batch graphics mode
mpl.use('agg')


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('fname')
    parser.add_argument('outpref', nargs='?', default='')
    parser.add_argument('-d', default='pdsp')
    parser.add_argument('-n','--nmax', default=10, type=int)

    args = parser.parse_args()

    global outpref

    fname = args.fname
    outpref = args.outpref
    whichdet = args.d
    sel.SetDetector(whichdet)

    MAX_EVENTS = args.nmax
    PER_ITERATION = 10000

    print('Input:', fname)
    # make sure the output directory exists
    print('Output prefix:', outpref)
    outdir=os.path.dirname(outpref)
    if len(outdir) > 0 :
        os.makedirs(outdir, exist_ok=True)


    # output root file to hold track stopping selection
    global outf
    outf = ur.recreate(outpref+'stopping_selection.root')
    outf.mktree('OpDetAna/opdetana', {'trk_stopped':'var*bool'})

    to_read = ['trk_hasT0', 'trk_mcMuIdx_best',
               'trk_endPoint', 'trk_startPoint',
               'trk_endTpc', 'trk_startTpc',
               'trk_length', 'trk_matched',
               'mc_length_tpcAV',
               'mc_exited_tpcAV', 'end_pos',
               'mc_endPoint_tpcAV']


    collect_all_stopped = argparse.Namespace(
        reco_stopped_mask            = 0.,
        reco_stopped_has_mc_mask     = 0.,
        reco_stopped_is_true_stopped = 0.,
        reconstructableStoppedMu     = 0.,
        stopped_mu_reco_as_stopped   = 0.,
        stopped_mu_hasT0             = 0.,
        stopped_mu_not_selected      = 0.,
    )

    collect_pds_matched = copyNamespace(collect_all_stopped)

    ## This may not work for string with wilde cards
    total_entries = 0
    for entries in ur.num_entries(fname+":OpDetAna/opdetana") :
        total_entries += entries[2]
    print(f'Will be processing total of {entries} entries.')

    count_entries = 0
    global counter,bottom_end,true_bottom_end
    counter = 0
    selected_bottoms = []
    selected_lengths = []
    selected_T0tags  = []
    for tree in ur.iterate(fname+':OpDetAna/opdetana', expressions=to_read, step_size=PER_ITERATION) :
        if MAX_EVENTS != -1 and count_entries >= MAX_EVENTS :
            break
        count_entries += len(tree)
        print(f'Reading data in iteration {counter}')
        all_stopped_ns,pds_matched_ns = \
            process_tree(tree, collect_all_stopped, collect_pds_matched)
        trkIsTrueStopped = GetMask_trkIsTrueStopped(tree)
        selected_bottoms += [ (
            bottom_end[all_stopped_ns.reco_stopped_mask],
            bottom_end[pds_matched_ns.reco_stopped_mask],
            bottom_end[all_stopped_ns.reco_stopped_mask & ~all_stopped_ns.reco_stopped_is_true_stopped],
            bottom_end[pds_matched_ns.reco_stopped_mask & ~pds_matched_ns.reco_stopped_is_true_stopped],
            bottom_end[all_stopped_ns.reco_stopped_is_true_stopped],
            bottom_end[pds_matched_ns.reco_stopped_is_true_stopped],
            true_bottom_end[all_stopped_ns.reconstructableStoppedMu & ~all_stopped_ns.stopped_mu_reco_as_stopped],
            true_bottom_end[pds_matched_ns.reconstructableStoppedMu & ~pds_matched_ns.stopped_mu_reco_as_stopped],
            bottom_end[trkIsTrueStopped & ~all_stopped_ns.reco_stopped_mask],
            bottom_end[trkIsTrueStopped & ~pds_matched_ns.reco_stopped_mask],
        ) ]
        selected_lengths += [ (
            tree.trk_length[all_stopped_ns.reco_stopped_mask],
            tree.trk_length[pds_matched_ns.reco_stopped_mask],
            tree.trk_length[all_stopped_ns.reco_stopped_mask & all_stopped_ns.reco_stopped_is_true_stopped],
            tree.trk_length[pds_matched_ns.reco_stopped_mask & pds_matched_ns.reco_stopped_is_true_stopped],
            tree.trk_length[all_stopped_ns.reco_stopped_mask & ~all_stopped_ns.reco_stopped_is_true_stopped],
            tree.trk_length[pds_matched_ns.reco_stopped_mask & ~pds_matched_ns.reco_stopped_is_true_stopped],
            tree.trk_length[ak.flatten(all_stopped_ns.stopped_mu_not_selected,axis=-1)],
            tree.trk_length[ak.flatten(pds_matched_ns.stopped_mu_not_selected,axis=-1)],
            ) ]
        selected_T0tags += [ (
            tree.trk_hasT0[all_stopped_ns.reco_stopped_mask],
            tree.trk_hasT0[pds_matched_ns.reco_stopped_mask],
            tree.trk_hasT0[all_stopped_ns.reco_stopped_mask & all_stopped_ns.reco_stopped_is_true_stopped],
            tree.trk_hasT0[pds_matched_ns.reco_stopped_mask & pds_matched_ns.reco_stopped_is_true_stopped],
            tree.trk_hasT0[all_stopped_ns.reco_stopped_mask & ~all_stopped_ns.reco_stopped_is_true_stopped],
            tree.trk_hasT0[pds_matched_ns.reco_stopped_mask & ~pds_matched_ns.reco_stopped_is_true_stopped],
        ) ]
        counter +=1

    print(f'Processed {count_entries} entries.')
    print(f'Closing output tree ROOT file with {outf["OpDetAna/opdetana"].num_entries} entries')
    outf.close()

    print('Plotting...')
    names=['', '_pds_matched',
           '_not_true_stopper', '_pds_matched_not_true_stopper',
           '_true_stopper', '_pds_matched_true_stopper',
           '_true_not_reco_stopped', '_true_not_pds_matched_reco_stopped',
           '_trk_not_stopped_true_stopper', '_pds_matched_trk_not_stopped_true_stopper',
           ]
    for idx,name in enumerate(names) :
        plot2Dprojections(data=ak.concatenate([data[idx] for data in selected_bottoms]),fname=f'stopping{name}_endpoint_{{}}{{}}_proj.png')
    # plot2Dprojections(data=ak.concatenate(selected_bottoms[:][1]),fname='stopping_pds_matched_endpoint_{}{}_proj.png')
    # plot2Dprojections(data=bottom_end[reco_stopped_pds_matched_mask],fname='stopping_pds_matched_endpoint_{}{}_proj.png')

    # plotting 1D hists
    names=[
        '', '_pds_matched',
        '_true_stopper', '_pds_matched_true_stopper',
        '_not_true_stopper', '_pds_matched_not_true_stopper',
        '_true_not_reco_stopped', '_true_not_pds_matched_reco_stopped',
    ]
    for idx,name in enumerate(names) :
        plot(data=ak.concatenate([data[idx] for data in selected_lengths]),
             fname=f'stopping{name}_length.png',
             bins=100, range=(0,1000),
             xlabel='Track length [cm]')
    names=[
        '',
        '_true_stopper'
        '_not_true_stopper'
    ]
    for idx,name in enumerate(names) :
        for j,pref in enumerate(['', '_pds_matched']):
            plot(data=ak.concatenate([data[2*idx+j] for data in selected_T0tags]),
                 fname=f'stopping{pref}{name}_T0tag.png',
                 bins=10, range=(0,10),
                 xlabel='T0 tag')


    print('done.')


    def PrintEffs(ns) :
        print(f'Fraction of reco stopped tracks that have association to MC truth: {ns.reco_stopped_has_mc_mask/ns.reco_stopped_mask:.2f}')
        print(f'Stop.Sel.Purity - fraction of reco stopped that are true stoppers: {ns.reco_stopped_is_true_stopped/ns.reco_stopped_has_mc_mask:.2f}')
        print(f'Stop.Sel.Efficiency - true stoppers that are reco\'d as such: {ns.stopped_mu_reco_as_stopped/ns.reconstructableStoppedMu:.2f}')
        print(f'T0 Efficiency - true stoppers that are reco\'d with T0: {ns.stopped_mu_hasT0/ns.reconstructableStoppedMu:.2f}')

    print('Effs for all stopped tracks:')
    PrintEffs(collect_all_stopped)

    print('Effs for all PDS matched tracks:')
    PrintEffs(collect_pds_matched)



def process_tree(br,collect_all_stopped,collect_pds_matched) :
    #import selections as sel
    import dt_offsets_by_tag as offsets
    import selections as sel
    import vector
    vector.register_awkward()

    global bottom_end,true_bottom_end,counter

    true_bottom_end = createVector4D(br.mc_endPoint_tpcAV)


    hasMc = br.trk_mcMuIdx_best > -1
    pds_matched_mask = br.trk_matched > -1

    mc_stopped_mask = br.mc_exited_tpcAV == 0

    def collectCounts(mask=True) :
        ns = argparse.Namespace()
        ns.reco_stopped_mask = selectRecoStopped(br) & mask
        # get associations with true muons
        ns.reco_stopped_has_mc_mask = ns.reco_stopped_mask & hasMc
        # comparte to true stoppers
        true_muons = br.trk_mcMuIdx_best
        ns.reco_stopped_is_true_stopped = ns.reco_stopped_has_mc_mask & mc_stopped_mask[true_muons]
        ### Get reconstructable true stopping muons ###
        trksPerStoppedMu = sel.BuildRecoPerTrue(br, br.trk_mcMuIdx_best)[mc_stopped_mask]
        num_tracks_per_stopped_mu = ak.num(trksPerStoppedMu, axis=-1)
        ns.reconstructableStoppedMu  = num_tracks_per_stopped_mu > 0
        # which true stoppers reconstructed as reco stoppers
        stopped_mu_reco_as_stopped = tls.Slice(ns.reco_stopped_mask, trksPerStoppedMu)
        ns.stopped_mu_reco_as_stopped = ak.any(stopped_mu_reco_as_stopped, axis=-1)
        # how often true stopper has T0
        hasT0_per_stopped_mu = tls.Slice( br.trk_hasT0>0, trksPerStoppedMu )
        ns.stopped_mu_hasT0 = ak.any(hasT0_per_stopped_mu,axis=-1)

        ns.stopped_mu_not_selected = tls.Slice(~ns.reco_stopped_mask,trksPerStoppedMu[hasT0_per_stopped_mu])
        return ns

    all_stopped = collectCounts()
    addToCollectable(collect_all_stopped, sumNamespace(all_stopped))
    # fill stopped selection
    global outf
    outf['OpDetAna/opdetana'].extend({'trk_stopped':all_stopped.reco_stopped_mask})


    pds_matched = collectCounts(pds_matched_mask)
    addToCollectable(collect_pds_matched, sumNamespace(pds_matched))

    return all_stopped, pds_matched


def selectRecoStopped(br) :
    global bottom_end

    end           = createVector4D(br.end_pos)
    reco_end      = createVector4D(br.trk_endPoint)
    reco_start    = createVector4D(br.trk_startPoint)
    trk_tpc_start = br.trk_startTpc
    trk_tpc_end   = br.trk_endTpc
    hasT0         = br.trk_hasT0 > 0

    bottom_mask = getBottomMask(reco_start, reco_end)
    bottom_end = ak.where(bottom_mask, reco_end, reco_start)
    bottom_tpc = ak.where(bottom_mask, trk_tpc_end, trk_tpc_start)
    reco_tpc   = ak.where(bottom_end['x'] < 0, 1, 2)

    correct_tpc_mask = reco_tpc == (bottom_tpc % 4)
    reco_stopped_mask = hasT0 & correct_tpc_mask & insideTpc(bottom_end, 50.) & notTrimmed(bottom_end, 10)

    return reco_stopped_mask


#############
### Tools ###
#############
def createVector4D(data) :
    # expects multi-D array with last dimension of len 4
    return ak.zip({'x':data[...,0],
                   'y':data[...,1],
                   'z':data[...,2],
                   't':data[...,3] },
                  with_name='Vector4D')


def insideTpc(end, buffer, whichdet='pdsp') :
    import selections as sel
    a = [low for low,_  in sel.boundary[whichdet] ]
    b = [hi  for   _,hi in sel.boundary[whichdet] ]
    end3d = end.to_3D()
    coords=(('x',0),('y',1),('z',2))
    result=True
    for coord,idx in coords :
        result = result & (end3d[coord] > a[idx] + buffer) & (end3d[coord] < b[idx] - buffer)
    return result
           # (end3d['y'] > a[1] + buffer) & (end3d['y'] < b[1] - buffer) & \
           # (end3d['z'] > a[2] + buffer) & (end3d['z'] < b[2] - buffer)

def getBottomMask(start, end): # returns 1 for end point at the bottom, 0 otherwise
    return end['y'] < start['y']

def notTrimmed(end, buffer) :
    return (end['t'] < 6000 - buffer) & (end['t'] > buffer)

def GetMask_trkIsTrueStopped(br):
    true_muons = br.trk_mcMuIdx_best
    mc_stopped_mask = br.mc_exited_tpcAV == 0
    hasT0 = br.trk_hasT0 > 0
    return hasT0 & (true_muons != -1) & mc_stopped_mask[true_muons]


def plot(data, fname=None, stats=True, clf=False, cla=True, xlabel=False, ylabel=False, legend=False, **kwargs) :
    global outpref

    if clf :
        plt.clf()
    if cla :
        plt.cla()

    if xlabel is not False :
        plt.xlabel(xlabel)
    if ylabel is not False :
        plt.ylabel(ylabel)

    data = ak.nan_to_none(ak.ravel(data))
    data = ak.drop_none(data)
    counts,bins,bars = plt.hist(data, **kwargs)

    if legend :
        plt.legend()


    if stats :
        labels=('Entries','Mean','Std')
        st = (ak.count(data), ak.mean(data), ak.std(data))
        tls.PlotStatbox( dict(zip(labels,st)) )


    if fname not in (None, '') :
        plt.savefig(outpref + fname)
    return counts, bins

def plotScatter(datax, datay, fname, data=None, xlabel=False, ylabel=False) :
    if xlabel is not False:
        plt.xlabel(xlabel)
    if ylabel is not False:
        plt.ylabel(ylabel)
    if data is None :
        plt.plot(ak.ravel(datax), ak.ravel(datay), ',')
        plt.title(f'{ak.count(datax)} entries')
    else :
        plt.plot(ak.ravel(data[datax]), ak.ravel(data[datay]), ',')
        plt.title(f'{ak.count(data[datax])} entries')
    plt.savefig(outpref+fname)

def plot2Dprojections(data, fname) :
    labels = [ ('x','y'), ('z', 'y'), ('z', 'x')]
    for labelx,labely in labels:
        plt.cla()
        plotScatter(labelx, labely, data=data, fname=fname.format(labely,labelx), xlabel=f'{labelx.capitalize()} [cm]', ylabel=f'{labely.capitalize()} [cm]')


def addToCollectable(target, coll) :
    for key,a in vars(target).items() :
        b = vars(coll)[key]
        setattr(target,key, a+b)

def copyNamespace(ns) :
    ns2 = argparse.Namespace()
    for key,a in vars(ns).items() :
        setattr(ns2,key,a)
    return ns2

def updateNamespace(ns, mask) :
    for key,a in vars(ns).items() :
        setattr(ns,key,a & mask)
def sumNamespace(ns) :
    result = argparse.Namespace()
    for key,a in vars(ns).items() :
        setattr(result,key,ak.sum(a))
    return result

if __name__ == '__main__':
    print('Running processing...')
    run()
