#!/bin/env python

import sys, os
import uproot as ur
import matplotlib.pyplot as plt
import matplotlib as mlp
import numpy as np
import numba as nb
import awkward as ak
import vector
import hist


import matplotlib as mpl
#mpl.rcParams[] =
#plt.rcParams['font.family'] = 'monospace'
#ur.default_library = 'np'

outpref = 'plots/python/pds_to_tpc_match_efficiency_curves/'
PE_CUT  = 400.
#PE_CUT  = 0.

dt_label = r'$\Delta$T$_{T0-flash}$ [$\mu$s]'
pe_label = 'No. PEs per flash'



import selections as sel, tools as tls


def plotNumberOfCandidatesPerTrack(matched_mask) :
    nPerTrk = ak.sum(matched_mask, axis=-1)
    tls.plotHist(nPerTrk,
                 title=f'Number of candidate flashes (> {PE_CUT:.0f} PE) per track (T0, > 20 cm)',
                 xlabel='No. candidates per track',
                 log=True, bins=5, range=(-0.5,4.5),
                 outs=outpref+'n_cand_per_track.png')


def process_tree(tree) :
    hasT0          = tree['trk_hasT0'].array() > 0
    longTrk        = tree['trk_length'].array() > 20.
    hiPE           = tree['flash_tot_pe'].array() > PE_CUT

    trkMask  = hasT0 & longTrk
    trkT0    = tree['trk_T0'].array()[trkMask]*1e-3
    flashT   = tree['flash_abs_time'].array()[hiPE]
    flashPE   = tree['flash_tot_pe'].array()[hiPE]

    trk_idx = ak.local_index(trkT0)
    flash_idx = ak.local_index(flashT)
    all_pairs = ak.cartesian([trk_idx,flash_idx], nested=[0])
    all_pairs_dt = trkT0[ak.flatten(all_pairs['0'],axis=-1)] - flashT[ak.flatten(all_pairs['1'], axis=-1)]
    layout = tls.GetLayout(all_pairs)
    all_pairs_dt = tls.RebuildFromLayout(all_pairs_dt, layout)
    #del all_pairs
    # prune the large matrix, interested only in abs(dt) < 25 us
    small_dt_mask = abs(all_pairs_dt-10) < 20
    # dt's and idx's for pairs to be considered
    all_pairs_dt = all_pairs_dt[small_dt_mask]
    all_pairs = all_pairs[small_dt_mask]

    # flashes matched to trk in truth
    flashesPerTrk  = sel.GetTrueFlashesPerTrack(tree)[trkMask]
    matchable = ak.any(flashesPerTrk, axis=-1)

    count_matchable = ak.sum(matchable)



    trkMcIdx = sel.GetMCMatched(tree)[trkMask]
    flashMcIdx = tree['flash_mcMuIdx'].array()[hiPE]
    hiPeFlashPerLongTrk = sel.GetTrueFlashesPerTrack(None, trkMcIdx, sel.BuildFlashPerTrue(tree, flashMcIdx))

    #print(ak.sum(ak.any(hiPeFlashPerLongTrk,axis=-1),axis=-1))

    # print(flashMcIdx)
    # print(hiPeFlashPerLongTrk)

    def EffPur(cutlow, cuthi, byHighestPE=False):
        matched_mask            = (all_pairs_dt > cutlow) & (all_pairs_dt < cuthi)
        if not byHighestPE :
            matched_trk             = ak.firsts(all_pairs[matched_mask]['0'], axis=-1)
            matched_flash           = ak.firsts(all_pairs[matched_mask]['1'], axis=-1)
        else :
            # find position of highest PE matched pair
            flash_idx_per_trk  = all_pairs[matched_mask]['1']
            layout             = tls.GetLayout(flash_idx_per_trk)
            flashes_pe_per_trk = flashPE[ak.flatten(flash_idx_per_trk, axis=-1)]
            flashes_pe_per_trk = tls.RebuildFromLayout(flashes_pe_per_trk, layout)
            max_pe_idx         = ak.argmax(flashes_pe_per_trk, axis=-1, keepdims=1)
            matched_trk        = ak.firsts(all_pairs[max_pe_idx]['0'], axis=-1)
            matched_flash      = ak.firsts(all_pairs[max_pe_idx]['1'], axis=-1)

        matched_matchable_mask  = ak.any(matched_flash == hiPeFlashPerLongTrk[matched_trk], axis=-1)
        count_matched_matchable = ak.sum(matched_matchable_mask)
        count_matched           = ak.count(matched_flash)
        # print(f'{count_matched = }')

        #print(hiPeFlashPerLongTrk[matched_trk], matched_flash, matched_matchable_mask)
        #print(f'low/hi = {cutlow:.1f}/{cuthi:.1f}, Matchable/Matched_Matchable/Matched = {count_matchable}/{count_matched_matchable}/{count_matched}')

        eff = count_matched_matchable/count_matchable
        pur = count_matched_matchable/count_matched
        return eff, pur, count_matched


    xlabel = r'$\Delta$T0-cut [$\mu$s]'
    ylabel = 'Eff/Pur'

    upper_t_cut_range = np.linspace(3,25,44)
    lower_t_cut_range = np.linspace(-2,4,40)


    plotNumberOfCandidatesPerTrack(matched_mask = (all_pairs_dt > 2) & (all_pairs_dt < 6))

    ## match by first flash
    x = upper_t_cut_range
    eff,pur,count_matched = ak.unzip([EffPur(0, c) for c in x])
    plt.cla()
    plt.plot(x, eff, label='Efficiency')
    plt.plot(x, pur, label='Purity')
    plt.ylim(0.,1.)
    plt.title(r'Upper $\Delta$T0 cut')
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend()
    plt.savefig(outpref+'eff_pur_vs_upper_t_cut.png')
    #plt.show()


    x = lower_t_cut_range
    eff,pur,count_matched = ak.unzip([EffPur(c, 6) for c in x])
    plt.cla()
    plt.plot(x, eff, label='Efficiency')
    plt.plot(x, pur, label='Purity')
    plt.ylim(0.,1.)
    plt.title(r'Lower $\Delta$T0 cut')
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend()
    plt.savefig(outpref+'eff_pur_vs_lower_t_cut.png')

    ## match by highest PE
    x = upper_t_cut_range
    eff,pur,count_matched = ak.unzip([EffPur(0, c, byHighestPE=True) for c in x])
    plt.cla()
    plt.plot(x, eff, label='Efficiency')
    plt.plot(x, pur, label='Purity')
    plt.ylim(0.,1.)
    plt.title(r'Upper $\Delta$T0 cut')
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend()
    plt.savefig(outpref+'eff_pur_vs_upper_t_cut_highest_pe.png')
    #plt.show()


    x = lower_t_cut_range
    eff,pur,count_matched = ak.unzip([EffPur(c, 6, byHighestPE=True) for c in x])
    plt.cla()
    plt.plot(x, eff, label='Efficiency')
    plt.plot(x, pur, label='Purity')
    plt.ylim(0.,1.)
    plt.title(r'Lower $\Delta$T0 cut')
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.legend()
    plt.savefig(outpref+'eff_pur_vs_lower_t_cut_highest_pe.png')






    # print(matched_flash)
    # print(matched_matchable_mask)

    # print(f'Matchable         = {count_matchable}')
    # print(f'Matched_Matchable = {count_matched_matchable}')
    # print(f'Matched           = {count_matched}')
    def printEff(low, hi, byHighestPE=False):
        eff, pur,count_matched = EffPur(low, hi, byHighestPE)
        print(f'At dT0 in ({low} us, {hi} us)')
        print(f'Efficiency = {eff:.3}')
        print(f'Purity     = {pur:.3}')
        print(f'{count_matched = }')

    print('Match first flash in time within range')
    print('======================================')
    printEff(2, 6)
    printEff(2,10)
    print('Match flash with highest PE in time within range')
    print('================================================')
    printEff(2, 6, True)
    printEff(2,10, True)


    print(f'{ak.sum(tree['trk_length'].array() > 20.) = }')

    tls.plotHist(all_pairs_dt,bins=200,range=(-10,25.),
                 xlabel=dt_label, title=f'T0 vs flash time - >20 cm, >{PE_CUT:.0f} PE',
                 outs=outpref+'long_trk_hiPE_flash_dt.png')

    # def GetEfficiencyAndPurity() :
    #     eff = ak.sum(matched_matchable)/ak.sum(matchable)
    #     pur = ak.sum(matched_matchable)/ak.count(matchedTrk)
    #     return eff, pur




def run(args):
    # print(args[1])
    # print(mlp.matplotlib_fname())

    if len(args) > 2 :
        global outpref
        outpref = args[2]

    # make sure the output directory exists
    #print(os.path.dirname(outpref))
    os.makedirs(os.path.dirname(outpref), exist_ok=True)

    with ur.open(args[1]+':OpDetAna/opdetana') as tree:
        #test(tree)
        process_tree(tree)



if __name__ == '__main__':
    #print('Running processing...')
    run(sys.argv)
