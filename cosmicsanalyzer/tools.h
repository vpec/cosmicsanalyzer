#ifndef TOOLS_H
#define TOOLS_H

#include "CosmicEventTree.h"


using CEvent_t = CosmicEventTree::CosmicEventTree_t;

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec);
std::ostream& operator<<(std::ostream& outs, const TVector3& vec);

#ifndef FOR
#define FOR(i, size) for (auto i = (size-size); i < size; ++i)
#endif


enum Tag_e {
    kOrig = 0b01,
    kApaEnter = 0b10,
    kApaExit = 0b100,
    kCPA = 0b1000,
};


int findMatchedMCIdx(CEvent_t* evt, size_t itrk)
{
    auto mcidx = evt->trk_mcMuIdx[itrk];
    auto ncalopts = evt->trk_ncalopts[itrk];

    if (mcidx[0] == mcidx[1])
	return mcidx[0];

    if (mcidx[0] == mcidx[2])
	return mcidx[0];

    if (mcidx[1] == mcidx[2])
	return mcidx[1];

    int best_plane = 0;
    FOR(i, 2) {
	if (ncalopts[i+1] > best_plane)
	    best_plane = i+1;
    }

    return mcidx[best_plane];
}

int isCPACrosser(CEvent_t* evt, size_t imu)
{
    if (evt->mc_startPoint_tpcAV[imu][0]*evt->mc_endPoint_tpcAV[imu][0] < 0.)
	return true;
    return false;
}

int isTrackTrimmed(CEvent_t* evt, size_t itrk, float buffer = 50.)
{
    // returns -1 if trimmed at lower TDC end
    // returns  1 if trimmed at upper TDC end
    // returns  2 if trimmed on both sides
    float start_tdc = evt->trk_startPoint[itrk][3];
    float end_tdc = evt->trk_endPoint[itrk][3];
    bool low = false, up = false;
    if (start_tdc < buffer || end_tdc < buffer)
	low = true;
    if (start_tdc > 6000 - buffer || end_tdc > 6000 - buffer)
	up = true;

    if (low && up) // this should probably never happen
	return 2;
    else if (low)
	return -1;
    else if (up)
	return 1;

    return 0;
}

int isReconstructable(CEvent_t* evt, size_t imu)
{   // any muon which is at least partially reconstructable

    const float gDriftVel = 0.1565; // in cm/us // from ProtoDUNE fhicl //at 87.68 K and 486.7 V/cm as for ProtoDUNE
    // nominal at 87 K and normal pressure: 0.1648;

    float true_t = evt->mc_startPoint_tpcAV[imu][3];
    float x1 = evt->mc_startPoint_tpcAV[imu][0];
    float x2 = evt->mc_endPoint_tpcAV[imu][0];
    float xmax = std::max(abs(x1), abs(x2));
    float xmin = std::min(abs(x1), abs(x2));
    bool result = 0;
    bool upperLimit = true_t < (2.75e6 - (358. - xmax)/gDriftVel*1e3);
    bool lowerLimit = true_t > (-250e3 - (358. - xmin)/gDriftVel*1e3);
    if ((x1 < 0. && x2 > 0.) || (x1 > 0. && x2 < 0.)) { // CPA crossing, need to treat differently
	result = true_t > -2.5e6 && upperLimit; // 2.5e6 ns is max drift time + 250e3 ns pretrigger
    } else { // not a CPA crosser
	result = lowerLimit && upperLimit;
    }

    return result;
}

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec)
{
  outs<<"("<<vec.X()<<", "
      <<vec.Y()<<", "
      <<vec.Z()<<", "
      <<vec.T()<<")";
  return outs;
}

std::ostream& operator<<(std::ostream& outs, const TVector3& vec)
{
  outs<<"("<<vec.X()<<", "
      <<vec.Y()<<", "
      <<vec.Z()<<")";
  return outs;
}


#endif
