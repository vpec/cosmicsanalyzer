#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"

#include "TagCPAcrossers.h"



#ifndef FOR
#define FOR(i, size) for (auto i = (size-size); i < size; ++i)
#endif

#include "tools.h"

#include "CosmicEventTree.h"

// const float gDriftVel = 0.1561; // in cm/us //at 87.68 K and 486.7 V/cm as for ProtoDUNE
// nominal at 87 K and normal pressure: 0.1648;


//************************************************************
//****** CUTS ************************************************
const float TDC_CUT = 5850;
const float TRK_LEN_CUT = 20;
const float COS_ANGLE_CUT = 0.98;
const float DR0_CUT = 20.; // cm cut on distance of intersections from the extension of the candidates
const float DR02_CUT = DR0_CUT*DR0_CUT; // 5 cm cut on distance of intersections from the extension of the candidates
//************************************************************
//************************************************************

struct Cuts_t {
    int second; // index in the candidate list of second to the pair
    float X0;
    float cos2d;
    float cos2d1;
    float cos2d2;
    float cos3d;
};


#define MAX_PRINT_COUNT 0

unsigned TagCPAcrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC)
{
    unsigned count = 0;

    // clear unfilled data
    FOR(i, CosmicEventTree::MAX_RECO_TRACKS) {
	evt->trk_stitched[i] = -1;
    }

    // deal with event data
    // loop over reco tracks and save trimmed candidates
    std::vector<size_t> candidate_idx;
    std::vector<short> candidate_cpa_side;
    std::vector<int> candidate_true_mu_idx;
    std::vector<short> candidate_CPA_end; // which part of the track oriented towards CPA (higher TDC) 1 - start / 2 - end
    FOR(itrk, evt->n_reco_tracks) {
	// ignore T0'd tracks
	if (evt->trk_hasT0[itrk])
	    continue;
	// consider only tracks >= 20 cm
	if (evt->trk_length[itrk] < TRK_LEN_CUT)
	    continue;

	// Get the low- and hi-TDC ends of the track
	// Both ends should be on the same side of the CPA, otherwise they would have been stitched and T0 tagged.
	int hiTPC = evt->trk_endTpc[itrk];
	int lowTDC = evt->trk_startPoint[itrk][3];
	int hiTDC = evt->trk_endPoint[itrk][3];
	short cpa_end = 2; // end of track
	if (lowTDC > hiTDC) { // inverted orientation
	    hiTPC = evt->trk_startTpc[itrk];
	    hiTDC = lowTDC;
	    lowTDC = evt->trk_endPoint[itrk][3];
	    cpa_end = 1;
	}

	// test if hi TDC is trimmed
	if (hiTDC < TDC_CUT)
	    continue;

	candidate_cpa_side.push_back(hiTPC % 4);
	candidate_idx.push_back(itrk);
	candidate_CPA_end.push_back(cpa_end);

	if (isMC) {
	    int imu = findMatchedMCIdx(evt, itrk);
	    candidate_true_mu_idx.push_back(imu);
	}
    }

    // store info on candidate pairs
    std::vector<Cuts_t*> pair_cuts(candidate_idx.size(), 0x0);


    /// Start searching through candidates and see if can match
    FOR(ican, int(candidate_idx.size()-1)) {
	//std::cout<<"Candidate: "<<ican<<std::endl;
	auto itrk = candidate_idx[ican];

	// if (ievt < 10)
	// 	std::cout<<candidate_cpa_side[ican]<<", ";

	TVector3 dir1(evt->trk_endPoint[itrk]);
	dir1 -= TVector3(evt->trk_startPoint[itrk]);
	dir1 = dir1.Unit();
	auto dir1yz = dir1; dir1yz.SetX(0.);
	dir1yz = dir1yz.Unit();

	// location of stitching point
	TVector3 point1((candidate_CPA_end[ican] == 1)?evt->trk_startPoint[itrk]:evt->trk_endPoint[itrk]);
	// projection angles
	float tan_xy1 = abs(dir1.X()/dir1.Y());
	float tan_xz1 = abs(dir1.X()/dir1.Z());

	// found X0
	float X0;

	// Search next candidates if any possible stitching match
	std::vector<size_t> pair_candidate;
	for (int jcan = ican+1; jcan < (int)candidate_idx.size(); ++jcan) {
	    if ( candidate_cpa_side[ican] == candidate_cpa_side[jcan] ) // stitching points must be on the opposit side oc the CPA
		continue;
	    // bool mc_matched = 0;
	    // if (candidate_true_mu_idx[ican] >= 0 && candidate_true_mu_idx[ican] == candidate_true_mu_idx[jcan])
	    // 	mc_matched = 1;

	    auto jtrk = candidate_idx[jcan];
	    TVector3 dir2(evt->trk_endPoint[jtrk]);
	    dir2 -= TVector3(evt->trk_startPoint[jtrk]);
	    dir2 = dir2.Unit();

	    // test if directions of the candidates are within a conservative cone
	    float cos3d = dir1.Dot(dir2);

	    // Test if tracks colinear in YZ projection
	    // if the distance at closest aproach is within certain cut

	    // calculate 2D angle between the projections
	    auto dir2yz = dir2; dir2yz.SetX(0.);
	    dir2yz = dir2yz.Unit();
	    float cos2d = dir1yz.Dot(dir2yz);

	    if (abs(cos2d) < COS_ANGLE_CUT)
		continue;

	    // vector connecting the stitching points - porojection in YZ
	    TVector3 point2((candidate_CPA_end[jcan] == 1)?evt->trk_startPoint[jtrk]:evt->trk_endPoint[jtrk]);
	    auto dv = point1 - point2;
	    dv.SetX(0.);
	    // calculate distance of extrapolated parts in the middle
	    auto dv_dir = dv.Unit();
	    float tan1 = abs(dv_dir.Cross(dir1yz).Mag()/dv_dir.Dot(dir1yz));
	    float tan2 = abs(dv_dir.Cross(dir2yz).Mag()/dv_dir.Dot(dir2yz));
	    float d1 = tan1 * dv.Mag()*0.5; // the distance from the connecting line
	    float d2 = tan2 * dv.Mag()*0.5; // the distance from the connecting line

	    float cos2d1 = dv_dir.Dot(dir1yz);
	    float cos2d2 = dv_dir.Dot(dir2yz);

	    // Cut on angle between directions of each segment and the line connecting the stiching points (in 2D projection onto YZ)
	    if (abs(cos2d1) < 0.98 || abs(cos2d2) < 0.98)
		continue;

	    static int print_count = 0;
	    if (print_count < MAX_PRINT_COUNT) {
		++print_count;
		std::cout<<"Trk 1: "<<itrk<<", trk 2: "<<jtrk<<" point1: "<<point1<<", point2: "<<point2<<", "
		    <<"dir1: "<<dir1<<", dir2: "<<dir2<<", dir1yz: "<<dir1yz<<", dir2yz: "<<dir2yz<<std::endl
		    <<"  dv: "<<dv<<", dv_dir: "<<dv_dir<<", tan1: "<<tan1<<", tan2: "<<tan2<<", d1: "<<d1<<", d2: "<<d2<<std::endl;
	    }

	    if (cos3d < COS_ANGLE_CUT)
		continue;


	    // search for T0
	    // Calculate from the y and z distance of the points: stitch must be halfway
	    // location of stitching point
	    float dy = abs(point1.Y() - point2.Y());
	    float dz = abs(point1.Z() - point2.Z());

	    // projection angles
	    float tan_xy2 = abs(dir2.X()/dir2.Y());
	    float tan_xz2 = abs(dir2.X()/dir2.Z());

	    // estimate offset in X
	    float x0_y_1 = 0.5*dy*tan_xy1 + abs(point1.X());
	    float x0_y_2 = 0.5*dy*tan_xy2 + abs(point2.X());
	    float x0_z_1 = 0.5*dz*tan_xz1 + abs(point1.X());
	    float x0_z_2 = 0.5*dz*tan_xz2 + abs(point2.X());

	    float x0[4] = {x0_y_1, x0_y_2, x0_z_1, x0_z_2};

	    float x0_avrg = 0;
	    FOR(ix0, 4)
		x0_avrg += x0[ix0];
	    x0_avrg *= 0.25;
	    X0 = x0_avrg;

	    // float Dz = x0_y_1/tan_xz1 - x0_y_2/tan_xz2;
	    // float Dy = x0_z_1/tan_xy1 - x0_z_2/tan_xy2;

	    // Dz = x0_avrg/tan_xz1 - x0_avrg/tan_xz2;
	    // Dy = x0_avrg/tan_xy1 - x0_avrg/tan_xy2;

	    pair_candidate.push_back(jcan);

	    // test if candidates paired already
	    bool betteri = false;
	    bool betterj = false;
	    if (pair_cuts[ican] && pair_cuts[ican]->cos3d < cos3d) {
		betteri = true;
	    }
	    if (pair_cuts[jcan] && pair_cuts[jcan]->cos3d < cos3d) {
		betterj = true;
	    }

	    if (pair_cuts[ican] && pair_cuts[jcan]) {
		if (betteri && betterj) {
		    // replace both
		    int to_destroy = pair_cuts[ican]->second;
		    // first remove their original matches
		    delete pair_cuts[to_destroy]; pair_cuts[to_destroy] = 0;
		    to_destroy = pair_cuts[jcan]->second;
		    delete pair_cuts[to_destroy]; pair_cuts[to_destroy] = 0;

		    *(pair_cuts[ican]) = {jcan, X0, cos2d, cos2d1, cos2d2, cos3d};
		    *(pair_cuts[jcan]) = {ican, X0, cos2d, cos2d1, cos2d2, cos3d};

		}
	    } else if (betteri || betterj) {
		// replace one and create one
		if (betteri) {
		    int to_destroy = pair_cuts[ican]->second;
		    delete pair_cuts[to_destroy]; pair_cuts[to_destroy] = 0;
		    *(pair_cuts[ican]) = {jcan, X0, cos2d, cos2d1, cos2d2, cos3d};
		    pair_cuts[jcan] = new Cuts_t{ican, X0, cos2d, cos2d1, cos2d2, cos3d};
		} else {
		    int to_destroy = pair_cuts[jcan]->second;
		    delete pair_cuts[to_destroy]; pair_cuts[to_destroy] = 0;
		    *(pair_cuts[jcan]) = {ican, X0, cos2d, cos2d1, cos2d2, cos3d};
		    pair_cuts[ican] = new Cuts_t{jcan, X0, cos2d, cos2d1, cos2d2, cos3d};
		}
	    } else if (!pair_cuts[ican] && !pair_cuts[jcan]) {
		// create a new pair
		pair_cuts[jcan] = new Cuts_t{ican, X0, cos2d, cos2d1, cos2d2, cos3d};
		pair_cuts[ican] = new Cuts_t{jcan, X0, cos2d, cos2d1, cos2d2, cos3d};
	    }

	}


	if (pair_candidate.size() > 1) {
	    std::cout<<"WARNING:  track "<<itrk<<": found > 1 stitching candidates: ";
	    for (auto idx: pair_candidate)
		std::cout<<candidate_idx[idx]<<", ";
	    std::cout<<std::endl;
	}
    } // candidates loop

    // run through pair candidates, calculate T0 and store the results
    FOR(ican, candidate_idx.size()) {
	if (!pair_cuts[ican])
	    continue;

	size_t itrk = candidate_idx[ican];
	size_t jtrk = candidate_idx[pair_cuts[ican]->second];

	auto X0 = pair_cuts[ican]->X0;

	// make sure that T0 shifted track is within the active TPC boundary
	float new_x[2][2] = {};
	size_t trk_idx[2] = {itrk, jtrk};
	FOR(i, 2) {
	    new_x[i][0] = evt->trk_startPoint[trk_idx[i]][0] - X0*(2*(evt->trk_startTpc[trk_idx[i]]%2)-1);
	    new_x[i][1] = evt->trk_endPoint[trk_idx[i]][0] - X0*(2*(evt->trk_endTpc[trk_idx[i]]%2)-1);
	}
	float max_x = 0.;
	// float max_x_i = -1.;
	// float max_x_j = -1.;
	FOR(i, 2) {
	    FOR(j, 2) {
		if (max_x < abs(new_x[i][j])) {
		    max_x = abs(new_x[i][j]);
		    // max_x_i = i;
		    // max_x_j = j;
		}
	    }
	}

	if (max_x  > boundary[1]) {
	    X0 -= max_x - boundary[1];
	}
	float T0 = X0 / driftVelocity;

	// float x0 = (2*(topTPC%2)-1)*gDriftVel*T0; // time 0 is at 500 ticks (out of 6000) //CPAside * boundary[0][1] - top.X();
	if (correctX) {
            FOR(i, 2) {
                evt->trk_startPoint[trk_idx[i]][0] = evt->trk_startPoint[trk_idx[i]][0] - X0*(2*(evt->trk_startTpc[trk_idx[i]]%2)-1);
                evt->trk_endPoint[trk_idx[i]][0] = evt->trk_endPoint[trk_idx[i]][0] - X0*(2*(evt->trk_endTpc[trk_idx[i]]%2)-1);
            }
	}
	evt->trk_hasT0[itrk] = TagID;
	evt->trk_T0[itrk] = T0*1e3;// in [ns]

	evt->trk_stitched[itrk] = jtrk;

        ++count;
    } // end pair_cuts loop


    // clear pair_cuts
    for (auto &can: pair_cuts) {
	delete can;
    }

    return count;
}
