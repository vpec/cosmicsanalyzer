#ifndef OPDETANA_H
#define OPDETANA_H

class OpDetAna;
using Name = std::string;
using NameVector = std::vector<Name>;

class OpDetAna : public art::EDAnalyzer {
private:
    struct Line;
    struct Box;

    struct Line {
        TLorentzVector start, end;

        bool intersects(const Line& l, const Box& b) const;
        bool operator&&(const Box& b) const { return intersects(*this, b); }
        Line operator!() const { return Line({end, start}); }
        TLorentzVector operator[](int i) const;

        bool GetEntryPoint(const Box& b,TLorentzVector& p) const { return GetEntryPoint(*this, b, p);}
        static bool GetEntryPoint(const Line& l, const Box& b, TLorentzVector& p);
        bool GetExitPoint(const Box& b, TLorentzVector& p) const { return GetExitPoint(*this, b, p); }
        static bool GetExitPoint(const Line& l, const Box& b, TLorentzVector& p) { return GetEntryPoint(!l, b, p); }
    };

    struct Box {
        TVector3 corner1, corner2;
        bool operator&&(const Line& l) const { return l.intersects(l, *this); }
        TVector3 operator[](int i) const  {
            if (i == 0) return corner1;
            else if (i == 1) return corner2;
            return TVector3(); };
    };

public:
    explicit OpDetAna(fhicl::ParameterSet const& p);
    ~OpDetAna();
    // The compiler-generated destructor is fine for non-base
    // classes without bare pointers or other resource use.

    // Plugins should not be copied or assigned.
    OpDetAna(OpDetAna const&) = delete;
    OpDetAna(OpDetAna&&) = delete;
    OpDetAna& operator=(OpDetAna const&) = delete;
    OpDetAna& operator=(OpDetAna&&) = delete;

    // Required functions.
    void analyze(art::Event const& e) override;

    // Selected optional functions.
    void beginJob() override;
    void endJob() override;
    void beginRun(art::Run const& r) override;

private:
    void ReadPSet(fhicl::ParameterSet const& p);

    int analyzeMC(detinfo::DetectorClocksData const& clockData, art::Event const& evt);
    int analyzeReco(detinfo::DetectorClocksData const& clockData, art::Event const& evt);
    int analyzeOpDet(detinfo::DetectorClocksData const& clockData, art::Event const& evt);

    bool FilterTaggedOnly(CosmicEventTree::CosmicEventTree_t& evt); // returns true if any tracks left
    bool FilterMatchedOnly(CosmicEventTree::CosmicEventTree_t& evt); // returns true if any tracks left
    void StoreOpWaveforms(CosmicEventTree::CosmicEventTree_t* outevt, art::Event const& evt, detinfo::DetectorClocksData const& clocksData);

    void AddTrack(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichtrk);
    void AddFlashWithHits(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichtrk);
    void AddHit(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichhit);

    static bool inside(const TVector3& pos, const Box& b);
    bool insideTPC(const TVector3& pos) const;
    void GetTPClimits();

    bool IsHitWithinWaveform(float hit_time, const raw::OpDetWaveform & wfm, detinfo::DetectorClocksData const& clocksData);

    void HitsPurity(detinfo::DetectorClocksData const& clockData,
                    std::vector< art::Ptr<recob::Hit> > const& hits,
                    Int_t& trackid,
                    Float_t& purity,
                    Double_t& maxe);
    void OpHitsPurity(detinfo::DetectorClocksData const& clockData,
                      std::vector< art::Ptr<recob::OpHit> > const& hits,
                      Int_t& trackid,
                      Float_t& purity,
                      Double_t& maxpe);

    void  GetMichelEnergy(const simb::MCParticle& mu, art::Handle< std::vector<simb::MCParticle> >& ph, float &en, float &dt);
    bool  MuMinusHasMichel(const simb::MCParticle& mu, art::Handle< std::vector<simb::MCParticle> >& ph, float &en, float &dt);

private:
    typedef std::vector<anab::T0>               T0Vector_t;


    art::ServiceHandle<cheat::ParticleInventoryService> particleinventory;


    // Declare member data here.
    // Configurables
    // input module labels
    std::string fSimModuleLabel;       // Input tag for collection of MC particles
    std::string fRecoTrackModuleLabel;         // Input tag for RecoTrack collection
    std::string fPFPModuleLabel;         // Input tag for RecoTrack collection
    NameVector  fT0ModuleLabels;         // Input tag for RecoTrack collection
    std::string fCaloModuleLabel;         // Input tag for Calorimetry and track association
    NameVector  fOpFlashModuleLabels;       // Input tag for OpFlash collection
    NameVector  fOpHitModuleLabels;         // Input tag for OpHit collection
    NameVector  fOpSimModuleLabels;         // Input tag for std::vector<sim::PhotonBacktrackerRecord> product
    std::string fOpDetWaveformModuleLabel;  // Input tag for OpDetWaveform collection
    std::string fOpWaveformModuleLabel;     // Input tag for OpDetWaveform collection
    std::string fRefTimeStampModuleLabel;   // Input tag for raw::RDTimeStamp collection

    std::vector<std::pair<short,short>> fT0Association; // has size of fT0ModuleLabels; (trk-1/pfp-2, idx) - pair of assn type and index in the assns vector

    std::vector<bool> fT0TagNeedsCorrection;

    // switches
    bool fIsMC;
    bool fDoOpDet;
    bool fStoreMC;
    bool fStoreSPs;
    bool fStoreOpWaveforms;
    bool fTagCPAcrossers;
    bool fTagAPAIn;
    bool fTagAPAOut;
    bool fMatchPDStoTPC;
    bool fCorrectX0;
    bool fSelectOnlyTagged;
    bool fSelectOnlyMatched;
    bool fTagStoppingMu;

    short fAPAOutTag;

    bool fPrintOpDetChannelToSensorPosition;

    // cuts to be used
    float fMatchPECut;
    std::vector<float> fMatchDtOffsets;
    std::vector<float> fMatchDtCuts;


    // internals
    float fDriftVelocity;
    size_t fReadoutWindow;


    TTree* fTree;
    CosmicEventTree::CosmicEventTree_t* fCosmicEvent;

    // Store a map of G4 Track ID to index of primary muon
    std::map<Int_t, Int_t> fTrackIdToPrimary;

    // to store geometry limits of active volume
    geo::Geometry const* fGeometryService;

    float ActiveBounds[6]; // Cryostat boundaries ( neg x, pos x, neg y, pos y, neg z, pos z )
    Box fActiveBox;

    double fExtTrigTime;
    const cheat::ParticleInventory* fPartInv;
};



#endif
