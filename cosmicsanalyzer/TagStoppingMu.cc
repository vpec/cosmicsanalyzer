/**
 * Simple stopping muon selection.
 *
 **/
#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"


#include "CosmicEventTree.h"
#include "tools.h"

bool isInTPC(TVector3 end, float buffer = 20.);

const float* gBoundary;


void TagStoppingMu(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, short APAoutTag)
{
    gBoundary = boundary;

    auto &outevt = evt;
    // clear unfilled data
    std::fill_n(outevt->trk_stopped, CosmicEventTree::MAX_RECO_TRACKS,0);

    // loop over reco tracks to get T0 tagged ones and guess if stopped muon
    FOR(itrk, evt->n_reco_tracks) {
        if (evt->trk_hasT0[itrk] == 0 || evt->trk_hasT0[itrk] == APAoutTag) // no T0 or tagged as exiting
            continue;

        // make sure the bottom end of the track is within the TPC and it is not trimmed by the readout window
        // 1st Find the bottom end
        TLorentzVector end(evt->trk_endPoint[itrk]);
        short which = 1;
        if (evt->trk_startPoint[itrk][1] < end.Y()) {
            end = TLorentzVector(evt->trk_startPoint[itrk]);
            which = 2;
        }

        // 2nd Is the track trimmed by readout window?
        float time_window_buffer = 100.; // in ticks
        if ( end.T() > (6000 - time_window_buffer) || end.T() < (0. + time_window_buffer) )
            continue;

        // 3rd Is it within the volume boundaries (adding a 20 cm buffer)
        if ( !isInTPC(end.Vect(), 50.) )
            continue;

        // check if reconstructed in the correct TPC
        int tpc = evt->trk_endTpc[itrk];
        if (which == 2)
            tpc = evt->trk_startTpc[itrk];
        // assume all passing tracks have end in TPC%4 = 1 or 2
        if ( (1 - 2*(tpc%2))*end.X() < 0 )
            continue;

        outevt->trk_stopped[itrk] = 1;
    }
}

bool isInTPC(TVector3 end, float buffer)
{
    static const float (&boundary)[3][2] = *(const float (*)[3][2])gBoundary;
    FOR(icoord, 3) {
	if ( end[icoord] < boundary[icoord][0] + buffer
	     || end[icoord] > boundary[icoord][1] - buffer )
	    return false;
    }

    return true;
}
