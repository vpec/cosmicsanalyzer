////////////////////////////////////////////////////////////////////////
// Class:       MyTrackEVD
// Plugin Type: analyzer (Unknown Unknown)
// File:        MyTrackEVD_module.cc
//
// Generated at Wed Oct 11 14:35:17 2023 by Viktor Pec using cetskelgen
// from  version .
////////////////////////////////////////////////////////////////////////

#include "art/Framework/Core/EDAnalyzer.h"
#include "art/Framework/Core/ModuleMacros.h"
#include "art/Framework/Principal/Event.h"
#include "art/Framework/Principal/Handle.h"
#include "art/Framework/Principal/Run.h"
#include "art/Framework/Principal/SubRun.h"
#include "canvas/Utilities/InputTag.h"
#include "fhiclcpp/ParameterSet.h"
#include "messagefacility/MessageLogger/MessageLogger.h"

#include "canvas/Persistency/Common/FindManyP.h"
#include "canvas/Persistency/Common/FindOneP.h"


#include "lardataobj/RecoBase/Wire.h"
#include "lardataobj/RecoBase/Hit.h"
#include "lardataobj/RecoBase/Track.h"
#include "lardataobj/RecoBase/PFParticle.h"
#include "lardataobj/RecoBase/SpacePoint.h"


#include <iostream>

#include "TApplication.h"
#include "TSystem.h"
#include "TCanvas.h"
#include "TView.h"
#include "TFrame.h"
#include "TColor.h"
#include "TH3I.h"
#include "TH1I.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TLine.h"
#include "TPolyLine3D.h"
#include "TPolyMarker3D.h"
#include "TBox.h"

#ifndef FOR
#define FOR(i, size) for (int i = 0; i < size; ++i)
#endif

class MyTrackEVD;


class MyTrackEVD : public art::EDAnalyzer {
public:
  explicit MyTrackEVD(fhicl::ParameterSet const& p);
  // The compiler-generated destructor is fine for non-base
  // classes without bare pointers or other resource use.

  // Plugins should not be copied or assigned.
  MyTrackEVD(MyTrackEVD const&) = delete;
  MyTrackEVD(MyTrackEVD&&) = delete;
  MyTrackEVD& operator=(MyTrackEVD const&) = delete;
  MyTrackEVD& operator=(MyTrackEVD&&) = delete;

  // Required functions.
  void analyze(art::Event const& e) override;

  // Selected optional functions.
  void beginJob() override;
  void endJob() override;
  void beginRun(art::Run const& r) override;
  void endRun(art::Run const& r) override;

private:
    void drawTrack(const recob::Track& trk);
    void drawTrackProjections(const recob::Track& trk, const std::vector<art::Ptr<recob::SpacePoint> >& sps);
    void drawSpacePoints(const std::vector<art::Ptr<recob::SpacePoint> >& sps);
    void drawHitsAndWires(const std::vector<art::Ptr<recob::Hit> >& hits, const art::FindOneP<recob::Wire>& wires);
    //void drawWires(const art::FindOneP<recob::Wire>& wires, const std::vector<art::Ptr<recob::Hit> >& hits);
    void drawHits(int itpc, int iplane, std::vector<float>& wires,std::vector<float>& times,std::vector<float>& rmss,
		  float wires_low, float wires_hi, float times_low, float times_hi);
    void drawWires(int itpc, int iplane,
		   std::vector<std::vector<float>>& tdc, std::vector<float>& wires, std::vector<std::vector<float>>& adc,
		   float wires_low, float wires_hi, float times_low, float times_hi);

    void createPlaneTPCCanvases(TCanvas** c, const char* name = "c2d", const char* title = "");
    int pause(const char* prompt = 0);

private:
    TApplication* gApp;

};


MyTrackEVD::MyTrackEVD(fhicl::ParameterSet const& p)
    : EDAnalyzer{p} //,
    //  gApp(0)
  // More initializers here.
{
  // Call appropriate consumes<>() for any products to be retrieved by this module.
}

void MyTrackEVD::analyze(art::Event const& evt)
{
    // Output canvas and waiting loop
    auto c = new TCanvas("c", "");
    c->Draw();

    auto cproj = new TCanvas("cproj", "2D projections", 900, 300);
    cproj->Divide(3,1);
    cproj->Draw();


    art::Handle< std::vector<recob::Track> > trackHandle;
    if (!evt.getByLabel("pandoraTrack", trackHandle))
	{
	    // __LINE__ and __FILE__ are values computed by the compiler.
	    throw cet::exception("MyTrackEVD::analyze()")
		<< " No recob::Track objects in this event - "
		<< " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
	}

    art::Handle< std::vector<recob::PFParticle> > pfpHandle;
    if (!evt.getByLabel("pandora", pfpHandle))
	{
	    // __LINE__ and __FILE__ are values computed by the compiler.
	    throw cet::exception("MyTrackEVD::analyze()")
		<< " No recob::PFParticle objects in this event - "
		<< " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
	}

	// Get hit associations
	art::FindManyP<recob::Hit> fmhits(trackHandle, evt, "pandoraTrack");
	// Get PFParticle associations
	art::FindManyP<recob::PFParticle> fmpfp(trackHandle, evt, "pandoraTrack");
	// Get Spacepoints per PFP
	art::FindManyP<recob::SpacePoint> fmsp(pfpHandle, evt, "pandora");


    TH3I h("hrame", ";Z [cm];X [cm];Y [cm]",
	   1, 0, 700,
	   1, -350, 350,
	   1, 0, 700
	   );
    h.SetStats(0);

    for (size_t itrk = 0; itrk < trackHandle->size(); ++itrk) {
	auto trk = trackHandle->at(itrk);

	// skip if track too short
	if (trk.Length() < 20.)
	    continue;
	std::cout<<"Processing track "<<itrk<<std::endl;

	c->cd();
	// draw frame histogram
	h.Draw();


	// get assoiciated PFP and then the associated list of Space Points
	std::cout<<"Getting associated PFPs."<<std::endl;
	auto pfp = fmpfp.at(itrk).at(0);
	std::cout<<"Getting associated Spacepoints.";
	auto sps = fmsp.at(pfp.key());
	std::cout<<" got "<<sps.size()<<std::endl;
	std::cout<<"Getting associated hits.";
	auto hits = fmhits.at(itrk);
	std::cout<<" got "<<hits.size()<<std::endl;

	drawTrack(trk);
	drawSpacePoints(sps);
	c->Modified(); c->Update();
	c->GetView()->RotateView(15., 70.);

	cproj->cd();
	drawTrackProjections(trk, sps);

	// Plane projections
	// Draw hits and wires
	art::FindOneP<recob::Wire> fow(hits, evt, "hitpdune");
	drawHitsAndWires(hits, fow);

	// drawHits(hits);
	// // Wires
	// drawWires(fmow, hits);


	int toskip = pause("Tracks to skip: ");
	if (toskip == -999) {
	    std::cout<<"Exiting."<<std::endl;
	    exit(0);
	}
	std::cout<<"Skipping "<<toskip<<std::endl;
	itrk += toskip;
    }


   std::cout<<"Press enter to go to next event."<<std::endl;
   pause();

   delete c;
}

void MyTrackEVD::drawTrack(const recob::Track& trk)
{
	// get track info
	auto start = trk.Start();
	auto end = trk.End();

	// draw track's straight line
	double p1[3], p2[3];
	start.GetCoordinates(p1);
	end.GetCoordinates(p2);

	auto l = new TPolyLine3D (2);
	l->SetPoint(0, p1[2], p1[0], p1[1]);
	l->SetPoint(1, p2[2], p2[0], p2[1]);
	l->Draw();

}

void MyTrackEVD::drawSpacePoints(const std::vector<art::Ptr<recob::SpacePoint> >& sps)
{
    // draw space points
    auto pm = new TPolyMarker3D(sps.size(), kFullCircle);
    int i = 0;

    std::cout<<"Getting SPs polymarker."<<std::endl;
    for (auto sp: sps) {
	pm->SetPoint(i, sp->XYZ()[2], sp->XYZ()[0], sp->XYZ()[1]);
	++i;
    }

    pm->Draw();
    std::cout<<"Drawing SPs"<<std::endl;
}


void MyTrackEVD::drawTrackProjections(const recob::Track& trk, const std::vector<art::Ptr<recob::SpacePoint> >& sps)
{
    // projections: 1: Y:X (front), 2: X:Z (top)), 3: Y:Z (side)
    static const short int views[3][2] = {{0,1}, {2,0}, {2,1}};
    static const char* view_title[3] = {"YX view;X [cm];Y [cm]","XZ view;Z [cm];X [cm]","YZ view;Z [cm];Y [cm]"};
    static const float view_limits[3][2] = {{-350, 350}, {0, 700}, {0, 700}};

    // get track info
    auto start = trk.Start();
    auto end = trk.End();

    // draw track's straight line
    double p1[3], p2[3];
    start.GetCoordinates(p1);
    end.GetCoordinates(p2);

    auto canvas = gPad;
    FOR(iview, 3) {
	canvas->cd(iview+1);
	gPad->Clear();
	auto h = new TH1I(Form("hframe_v%d", iview), view_title[iview],
			  1, view_limits[views[iview][0]][0], view_limits[views[iview][0]][1]);
	h->SetMinimum(view_limits[views[iview][1]][0]);
	h->SetMaximum(view_limits[views[iview][1]][1]);
	h->SetStats(0);
	h->Draw();


	auto l = new TLine(p1[views[iview][0]], p1[views[iview][1]], p2[views[iview][0]], p2[views[iview][1]]);
	l->Draw();

	// draw space points
	auto pm = new TGraph(sps.size());
	pm->SetMarkerStyle(kFullCircle);
	pm->SetMarkerSize(0.3);

	int i = 0;
	for (auto sp: sps) {
	    pm->SetPoint(i, sp->XYZ()[views[iview][0]], sp->XYZ()[views[iview][1]]);
	    ++i;
	}

	pm->Draw("P");



	// refresh current pad
	gPad->Modified();
	gPad->Update();
    }
    canvas->cd();
}



void MyTrackEVD::drawHitsAndWires(const std::vector<art::Ptr<recob::Hit> >& hits, const art::FindOneP<recob::Wire>& fow)
{
    std::cout<<"Drawing hits."<<std::endl;

    static TCanvas* c[3] = {};
    static TCanvas* cwires[3] = {};

    createPlaneTPCCanvases(c);
    createPlaneTPCCanvases(cwires, "c2dwires");

    // go through hits
    std::vector<float> wires[3][12];
    std::vector<float> times[3][12];
    std::vector<float> rmss [3][12];
    // for each hit, find sequence of wire TDC and ADC in the ROI around the hit peak time
    std::vector<std::vector<float>> wireadc [3][12];
    std::vector<std::vector<float>> wiretdc [3][12];

    float wires_low [3][12];
    float wires_hi  [3][12];
    float times_low [3][12];
    float times_hi  [3][12];

    FOR(i, 3) FOR(j, 12) {
	wires_low [i][j] = 99999;
	wires_hi  [i][j] = -99999;
	times_low [i][j] = 99999;
	times_hi  [i][j] = -99999;
    }

    for(size_t ihit = 0; ihit < hits.size(); ++ihit) {
	auto hit = hits.at(ihit);
	int itpc = hit->WireID().TPC;
	int iplane = hit->View();

	int peak_tdc = hit->PeakTime();
	float time = (1-2*(itpc%2))*peak_tdc;
	float wire = hit->WireID().Wire;

	wires[iplane][itpc].push_back(wire);
	times[iplane][itpc].push_back(time);
	rmss [iplane][itpc].push_back(hit->RMS());

	if (wire < wires_low[iplane][itpc])
	    wires_low[iplane][itpc] = wire;
	if (wire > wires_hi[iplane][itpc])
	    wires_hi[iplane][itpc] = wire;
	if (time < times_low[iplane][itpc])
	    times_low[iplane][itpc] = time;
	if (time > times_hi[iplane][itpc])
	    times_hi[iplane][itpc] = time;


	// deal with wires
	auto recowire = fow.at(ihit);
	// all signal regions on the wire
	auto rois = recowire->SignalROI();
	size_t iroi = rois.find_range_number(peak_tdc);

	auto roi_it = rois.begin_range() + iroi;
	if (roi_it->empty())
	    continue;
	// start a series of TDC ADC pairs for this hit
	wiretdc[iplane][itpc].push_back({});
	wireadc[iplane][itpc].push_back({});
	auto& tdc_vec = wiretdc[iplane][itpc].back();
	auto& adc_vec = wireadc[iplane][itpc].back();
	int tdc = roi_it->begin_index();
	for (float adc: *roi_it) {
	    tdc_vec.push_back((1-2*(itpc%2))*tdc);
	    adc_vec.push_back(adc);
	    ++tdc;
	}
    }

    // FOR(itpc, 12) {
    // 	int nhits = 0;
    // 	FOR(iplane, 3)
    // 	    nhits += wires[iplane][itpc].size();
    // 	std::cout<<"Found "<<nhits<<" hits in TPC "<<itpc<<std::endl;
    // }

    // prepare 3 frame histograms
    // static TH1I* h[3][12] = {};
    FOR(iplane, 3) {
	std::cout<<"plane "<<iplane<<std::endl;
	FOR(itpc, 12) {
	    // std::cout<<"  TPC "<<itpc<<":"
	    // 	     <<" wires_low="<<wires_low[iplane][itpc]
	    // 	     <<" wires_hi="<<wires_hi  [iplane][itpc]
	    // 	     <<" times_low="<<times_low[iplane][itpc]
	    // 	     <<" times_hi="<<times_hi  [iplane][itpc]
	    // 	     <<std::endl;

	    auto pad = c[iplane]->GetPad(12-itpc);
	    auto pad2 = cwires[iplane]->GetPad(12-itpc);
	    pad->Clear();
	    pad->Modified(); pad->Update();
	    pad2->Clear();
	    pad2->Modified(); pad2->Update();


	    // if(h[iplane][itpc]) {
	    // 	delete h[iplane][itpc];
	    // 	h[iplane][itpc] = 0;
	    // }

	    if (wires[iplane][itpc].size() < 2)
		continue;

	    float dt = (times_hi[iplane][itpc] - times_low[iplane][itpc])*0.1;
	    float dw = (wires_hi[iplane][itpc] - wires_low[iplane][itpc])*0.1;

	    if (dt < 10.)
		dt = 10.;
	    if (dw < 10.)
		dw = 10.;

	    pad->cd();
	    drawHits(itpc, iplane, wires[iplane][itpc], times[iplane][itpc], rmss[iplane][itpc],
		     wires_low[iplane][itpc]-dw, wires_hi[iplane][itpc]+dw, times_low[iplane][itpc]-dt, times_hi[iplane][itpc]+dt);

	    pad->Modified();
	    pad->Update();

	    // draw 2d wire hists
	    pad2->Clear();
	    drawWires(itpc, iplane, wiretdc[iplane][itpc], wires[iplane][itpc], wireadc[iplane][itpc],
		      wires_low[iplane][itpc]-dw, wires_hi[iplane][itpc]+dw, times_low[iplane][itpc]-dt, times_hi[iplane][itpc]+dt);
	    pad2->Modified();
	    pad2->Update();
	}
    }


}


// void MyTrackEVD::drawWires(const art::FindOneP<recob::Wire>& wires, const std::vector<art::Ptr<recob::Hit> >& hits)
// {

// }


void MyTrackEVD::drawHits(int itpc, int iplane, std::vector<float>& wires,std::vector<float>& times, std::vector<float>& rmss,
			  float wires_low, float wires_hi, float times_low, float times_hi)
{
    auto h = new TH1I(Form("h2d%d_%d", iplane, itpc), Form("Hits TPC%d Plane %d;Time tick;Wire", itpc, iplane),
		      1, times_low, times_hi);
    h->SetStats(0);
    h->SetMaximum(wires_hi);
    h->SetMinimum(wires_low);

    h->Draw();


    // draw all hits for this plane
    TBox box;
    box.SetFillStyle(0);
    box.SetLineColor(kRed);
    box.SetLineWidth(1);
    for (size_t ihit = 0; ihit < wires.size(); ++ihit) {
	box.DrawBox(times[ihit]-0.5*rmss[ihit],
		    wires[ihit]-0.5,
		    times[ihit]+0.5*rmss[ihit],
		    wires[ihit]+0.5);
    }

}

void MyTrackEVD::drawWires(int itpc, int iplane,
			   std::vector<std::vector<float>>& tdc, std::vector<float>& wires, std::vector<std::vector<float>>& adc,
			   float wires_low, float wires_hi, float times_low, float times_hi)
{
    auto h = new TH2F(
		      Form("h2dWires%d_%d", iplane, itpc), Form("ADC vs Wire vs TDC TPC%d Plane %d;Time tick;Wire", itpc, iplane),
		      int(times_hi-times_low), int(times_low), int(times_hi),
		      int(wires_hi-wires_low), int(wires_low), int(wires_hi)
		      );
    h->SetStats(0);

    for (size_t ihit = 0; ihit < wires.size(); ++ihit) {
	for (size_t itdc = 0; itdc < tdc[ihit].size(); ++itdc) {
	    int time_bin = int(tdc[ihit][itdc]-times_low) + 1;
	    h->SetBinContent(time_bin, int(wires[ihit] - wires_low) + 1, adc[ihit][itdc]);
	}
    }

    h->Draw("colz");
    gPad->Update();
    gPad->GetFrame()->SetFillColor(TColor::GetColorPalette(0));
}

void MyTrackEVD::createPlaneTPCCanvases(TCanvas** c, const char* name, const char* title)
{

    FOR(i, 3) {
	if (!c[i]) {
	    c[i] = new TCanvas(Form("%s%d",name, i), Form("%s Plane %d", title, i), 800,900);
	    c[i]->Divide(4,3, 0.,0.);

	    float marginz = 0.2;
	    float marginx = 0.5;
	    float height = (1.-marginz)/(3.-marginz);
	    float width  = 0.4;
	    float width_side  = 0.5*(1. - 2.*width);

	    float zhi = 1.;
	    FOR(iz, 3) {
		float zlow = zhi - height - (iz==0 || iz==2)*marginz/(1.-marginz)*height;
		float xlow = 0.;
		FOR(ix, 4) {
		    auto pad = c[i]->GetPad(iz*4 + ix + 1);
		    float xhi = xlow + ((ix==0 || ix==3)?width_side:width);

		    // std::cout<<"width="<<width<<" width_side="<<width_side<<" Pad "<<pad->GetName()
		    // 	     <<" ix="<<ix<<" iz="<<iz<<", xlow="<<xlow<<" xhi="<<xhi
		    // 	     <<" zlow="<<zlow<<" zhi="<<zhi<<std::endl;

		    pad->SetPad(xlow,
				zlow,
				xhi,
				zhi);
		    xlow = xhi;


		    pad->SetMargin(0.05, 0.01, 0.05, 0.01);
		    if (iz == 2)
			pad->SetBottomMargin(marginz);
		    if (iz == 0)
			pad->SetTopMargin(marginz);
		    if (ix == 3)
			pad->SetRightMargin(marginx);
		    if (ix == 0)
			pad->SetLeftMargin(marginx);
		}
		zhi = zlow;
	    }
	    c[i]->Draw();
	}
    }




}


int MyTrackEVD::pause(const char* prompt)
{
    static char buff[255];

    //int key = 0;
    while(1) {

	gSystem->ProcessEvents();

	if (prompt)
	    std::cout<<prompt;

	int result = 0;
	char* status = fgets(buff, 254, stdin);
	if (status != buff)
	    return 0;

	if (sscanf(buff, "%d", &result) != 1) {
	    char str[255];
	    if (sscanf(buff, "%s", str) == 1)
		if (strcmp(str, "q") == 0)
		    return -999;
	    return 0;
	}


	//key = getchar();
	// if((key == '\n') || (key == '\r'))
	//     break;
	return result;
	usleep(500);
    }

    return 0;
}




void MyTrackEVD::beginJob()
{
  // Implementation of optional member function here.
}

void MyTrackEVD::endJob()
{
  // Implementation of optional member function here.
}

void MyTrackEVD::beginRun(art::Run const& r)
{
    // Implementation of optional member function here.
    int argc = 0;
    char** argv = 0;
    gApp = new TApplication("viewerapp", &argc, argv);
   // gApp->Run();
}

void MyTrackEVD::endRun(art::Run const& r)
{
  // Implementation of optional member function here.
    // gApp->Terminate();
}

DEFINE_ART_MODULE(MyTrackEVD)
