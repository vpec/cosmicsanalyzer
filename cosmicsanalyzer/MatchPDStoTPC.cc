
#ifndef FOR
#define FOR(i, size) for (auto i = (size-size); i < size; ++i)
#endif

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"


#include "MatchPDStoTPC.h"

#include "CosmicEventTree.h"
#include "tools.h"


std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec);
std::ostream& operator<<(std::ostream& outs, const TVector3& vec);


const float dt_offsets_us[9] = {0., 8., 3.6, 0., 3.6, 0.,0.,0., 10.};

void MatchPDStoTPC(CosmicEventTree::CosmicEventTree_t* evt,
                   float PEcut,
                   std::vector<float> &dtOffsets, std::vector<float> &dtCuts,
                   CandidateSelection_t select)
{

    // PE threshold
    float pe_thld = PEcut;
    // float dtCutOffset = 0.5*(hiDtCut + lowDtCut);
    // float dtCutWidth  = 0.5*(hiDtCut - lowDtCut);


    // clear unfilled data
    FOR(i, CosmicEventTree::MAX_RECO_TRACKS) {
        evt->trk_matched[i] = -1;
        evt->flash_matched[i] = -1;
    }

    // loop over reco tracks to get T0 tagged ones
    FOR(itrk, evt->n_reco_tracks) {
        if (!evt->trk_hasT0[itrk])
            continue;

        float t0 = evt->trk_T0[itrk];
        unsigned short hasT0 = evt->trk_hasT0[itrk] - 1; // index for the cut values; is lower by 1, tags start from 1.
        float maxPE = 0.;
        int iMatchedFlash = -1;
        FOR(iflash, evt->n_flashes) {
            float flash_t = evt->flash_abs_time[iflash];
            float dt = t0*1e-3 - flash_t; // in us
            float pe = evt->flash_tot_pe[iflash];

            // apply PE cut
            if ( pe < pe_thld )
                continue;

            // correct dt
            dt -= dtOffsets[hasT0];
            // apply dt cut
            if ( abs(dt) > dtCuts[hasT0] )
                continue;

            if (select == kMaxPE) {
                if (pe < maxPE)
                    continue;
                else
                    maxPE = pe;
            }

            iMatchedFlash = iflash; // record this flash index, candidate to be filled

            if (select == kFirst) {
                break;// found first match
            }
        }
        if (iMatchedFlash > -1) {
            evt->match_trk_idx[evt->n_matches] = itrk;
            evt->match_flash_idx[evt->n_matches] = iMatchedFlash;

            // Last recorded matches. This is not necessarily one-to-one match, single track can match with multiple flashes and vice versa.
            evt->trk_matched[itrk] = evt->n_matches;
            evt->flash_matched[iMatchedFlash] = evt->n_matches;
            ++(evt->n_matches);
        }
    }
}
