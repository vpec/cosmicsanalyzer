#ifndef MATCHPDSTOTPC_H
#define MATCHPDSTOTPC_H

namespace CosmicEventTree {
    class CosmicEventTree_t;
}

enum CandidateSelection_t{
    kFirst=1,
    kMaxPE=2
};


// PEcut: used as threshold for considering a PDS flash to match
// dtOffsets: offsets of mean DT between T0 tag and PDS time - each tagging method has its own tag
// dtCuts: +/- interval around the offset to consider match
// select: which PDS flash candidate to choose if more than one
void MatchPDStoTPC(CosmicEventTree::CosmicEventTree_t* evt, float PEcut, std::vector<float> &dtOffsets, std::vector<float> &dtCuts, CandidateSelection_t select);


#endif
