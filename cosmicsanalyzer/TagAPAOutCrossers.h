#ifndef TAGAPAOUTCROSSERS_H
#define TAGAPAOUTCROSSERS_H

namespace CosmicEventTree {
    class CosmicEventTree_t;
}

unsigned TagAPAOutCrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC);


#endif
