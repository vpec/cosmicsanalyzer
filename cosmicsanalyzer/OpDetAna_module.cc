////////////////////////////////////////////////////////////////////////
// Class:       OpDetAna
// Plugin Type: analyzer (Unknown Unknown)
// File:        OpDetAna_module.cc
//
// Generated at Wed Apr 12 14:55:17 2023 by Viktor Pec using cetskelgen
// from  version .
////////////////////////////////////////////////////////////////////////

// Framework headers
#include "art/Framework/Core/EDAnalyzer.h"
#include "art/Framework/Core/ModuleMacros.h"
#include "art/Framework/Principal/Event.h"
#include "art/Framework/Principal/Handle.h"
#include "art/Framework/Principal/Run.h"
#include "art/Framework/Principal/SubRun.h"
#include "canvas/Utilities/InputTag.h"
#include "canvas/Persistency/Common/FindManyP.h"
#include "canvas/Persistency/Common/FindOneP.h"
#include "fhiclcpp/ParameterSet.h"
#include "messagefacility/MessageLogger/MessageLogger.h"
#include "art_root_io/TFileService.h"

// useful services headers
#include "larcore/CoreUtils/ServiceUtil.h"
#include "lardata/DetectorInfoServices/DetectorPropertiesService.h"
#include "lardata/DetectorInfoServices/DetectorClocksService.h"
#include "larcore/Geometry/Geometry.h"
#include "larcore/Geometry/WireReadout.h"

// Sim info access related headers
#include "larsim/MCCheater/BackTrackerService.h"
#include "larsim/MCCheater/PhotonBackTrackerService.h"
#include "larsim/MCCheater/ParticleInventoryService.h"
#include "nusimdata/SimulationBase/MCParticle.h"

// TPC Reco
#include "lardataobj/RawData/RDTimeStamp.h"
#include "lardataobj/RecoBase/Track.h"
#include "lardataobj/RecoBase/PFParticle.h"
#include "lardataobj/AnalysisBase/Calorimetry.h"
#include "lardataobj/AnalysisBase/T0.h"


// OpDet info access
#include "lardataobj/RecoBase/OpFlash.h"
#include "lardataobj/RecoBase/OpFlash.h"
#include "lardataobj/RecoBase/OpHit.h"
#include "lardataobj/RawData/OpDetWaveform.h"

#include "dunecore/DuneObj/ProtoDUNETimeStamp.h"

// ROOT headers
#include "TLorentzVector.h"

#include "CosmicEventTree.h"
#include "TagCPAcrossers.h"
#include "TagAPAInCrossers.h"
#include "TagAPAOutCrossers.h"
#include "MatchPDStoTPC.h"
#include "TagStoppingMu.h"

#include "OpDetAna.h"


std::ostream& operator<<(std::ostream&, const TLorentzVector&);
std::ostream& operator<<(std::ostream& out, const std::vector<bool>& vec);
std::ostream& operator<<(std::ostream& out, const NameVector& vec);
std::ostringstream& operator<<(std::ostringstream& out, NameVector const& vec);


void OpDetAna::ReadPSet(fhicl::ParameterSet const& pset)
{
    fSimModuleLabel	      = pset.get<std::string>("SimModuleLabel");
    fRecoTrackModuleLabel     = pset.get<std::string>("RecoTrackModuleLabel");
    fPFPModuleLabel	      = pset.get<std::string>("PFPModuleLabel");
    fT0ModuleLabels	      = pset.get<NameVector>("T0ModuleLabels");
    fCaloModuleLabel	      = pset.get<std::string>("CaloModuleLabel");
    fOpFlashModuleLabels      = pset.get<NameVector>("OpFlashModuleLabels");
    fOpHitModuleLabels	      = pset.get<NameVector>("OpHitModuleLabels");
    fOpSimModuleLabels	      = pset.get<NameVector>("OpSimModuleLabels");
    fOpDetWaveformModuleLabel = pset.get<std::string>("OpDetWaveformModuleLabel", "");
    fOpWaveformModuleLabel    = pset.get<std::string>("OpWaveformModuleLabel", "");
    fRefTimeStampModuleLabel  = pset.get<std::string>("RefTimeStampModuleLabel", "");


    fT0TagNeedsCorrection     = pset.get<std::vector<bool>>("T0TagNeedsCorrection", {});

    // Other configurables
    fIsMC             = pset.get<bool>("IsMC");
    fDoOpDet          = pset.get<bool>("DoOpDet", true);
    fStoreMC          = pset.get<bool>("StoreMC");
    fStoreSPs         = pset.get<bool>("StoreSPs", false);
    fStoreOpWaveforms = pset.get<bool>("StoreOpWaveforms", false);
    fTagCPAcrossers   = pset.get<bool>("TagCPAcrossers", false);
    fTagAPAIn         = pset.get<bool>("TagAPAIn", false);
    fTagAPAOut        = pset.get<bool>("TagAPAOut", false);
    fMatchPDStoTPC    = pset.get<bool>("MatchPDStoTPC", false);
    fCorrectX0        = pset.get<bool>("CorrectX0", false);
    fSelectOnlyTagged = pset.get<bool>("SelectOnlyTagged", false);
    fSelectOnlyMatched= pset.get<bool>("SelectOnlyMatched", false);
    fTagStoppingMu    = pset.get<bool>("TagStoppingMu", false);

    fMatchPECut	    = pset.get<float>("MatchPECut", 0.);
    fMatchDtOffsets = pset.get<std::vector<float>>("MatchDtOffsets", {});
    fMatchDtCuts    = pset.get<std::vector<float>>("MatchDtCuts", {});

    fPrintOpDetChannelToSensorPosition = pset.get<bool>("PrintOpDetChannelToSensorPosition", false);

    fT0Association = std::vector<std::pair<short,short>>(fT0ModuleLabels.size(), {0,-1});

    if (!fTagAPAOut)
        fAPAOutTag = 0;
    else {
        fAPAOutTag = fT0Association.size()+1;
        if (fTagCPAcrossers) ++fAPAOutTag;
        if (fTagAPAIn) ++fAPAOutTag;
    }

    auto mfi = mf::LogInfo("OpDetAna::ReadPSet()");

    std::cout
	<<"OpDetAna: Configured to get products with the following labels:"<<"\n"
	<< std::setw(20) << "SimModuleLabel      " << std::setw(20) <<       fSimModuleLabel  << "\n"
	<< std::setw(20) << "RecoTrackModuleLabel" << std::setw(20) << fRecoTrackModuleLabel  << "\n"
	<< std::setw(20) << "PFPModuleLabel      " << std::setw(20) <<       fPFPModuleLabel  << "\n"
	<< std::setw(20) << "T0ModuleLabels      " << std::setw(20) <<        fT0ModuleLabels << "\n"
	<< std::setw(20) << "T0ModuleLabels      " << std::setw(20) <<        fT0TagNeedsCorrection << "\n"
	<< std::setw(20) << "CaloModuleLabel     " << std::setw(20) <<      fCaloModuleLabel  << "\n"
	<< std::setw(20) << "OpFlashModuleLabels " << std::setw(20) <<   fOpFlashModuleLabels << "\n"
	<< std::setw(20) << "OpHitModuleLabels   " << std::setw(20) <<     fOpHitModuleLabels << "\n"
	<< std::setw(20) << "OpSimModuleLabels   " << std::setw(20) <<     fOpSimModuleLabels << "\n"
	<< std::setw(20) << "IsMC                " << std::setw(20) << fIsMC             << "\n"
	<< std::setw(20) << "DoOpDet             " << std::setw(20) << fDoOpDet          << "\n"
	<< std::setw(20) << "StoreMC             " << std::setw(20) << fStoreMC          << "\n"
	<< std::setw(20) << "StoreSPs            " << std::setw(20) << fStoreSPs         << "\n"
	<< std::setw(20) << "StoreOpWaveforms    " << std::setw(20) << fStoreOpWaveforms << "\n"
	<< std::setw(20) << "OpDetWaveformModuleLabel    " << std::setw(20) << fOpDetWaveformModuleLabel << "\n"
	<< std::setw(20) << "RefTimeStampModuleLabel    " << std::setw(20) << fRefTimeStampModuleLabel << "\n"
	<< std::setw(20) << "TagCPAcrossers      " << std::setw(20) << fTagCPAcrossers   << "\n"
	<< std::setw(20) << "TagAPAIn            " << std::setw(20) << fTagAPAIn         << "\n"
	<< std::setw(20) << "TagAPAOut           " << std::setw(20) << fTagAPAOut        << "\n"
	<< std::setw(20) << "TagStoppingMu       " << std::setw(20) << fTagStoppingMu    << "\n"
	<< std::setw(20) << "MatchPDStoTPC       " << std::setw(20) << fMatchPDStoTPC    << "\n"
	<< std::setw(20) << "CorrectX0           " << std::setw(20) << fCorrectX0        << "\n"
	<< std::setw(20) << "SelectOnlyTagged    " << std::setw(20) << fSelectOnlyTagged << "\n"
	<< std::setw(20) << "SelectOnlyMatched   " << std::setw(20) << fSelectOnlyMatched<< "\n"
	<< "\n";

}


OpDetAna::OpDetAna(fhicl::ParameterSet const& pset)
    : EDAnalyzer{pset},
      fTree(0),
      fTrackIdToPrimary(),
      fExtTrigTime(0.),
      fPartInv( lar::providerFrom<cheat::ParticleInventoryService>() )
      // More initializers here.
{
    ReadPSet(pset);

    //  if ( fIsMC && fOpFlashModuleLabels.size() != fOpSimModuleLabels.size() ) {
    if ( fOpFlashModuleLabels.size() != fOpHitModuleLabels.size() ) { //|| (fIsMC && fOpFlashModuleLabels.size() != fOpSimModuleLabels.size()) ) {
        throw cet::exception("OpDetAna::OpDetAna()")
            << " Line " << __LINE__ << " in file " << __FILE__
            << " Lists of labels related to PDS don't have the same size "
            // << std::setw(20) << "OpFlashModuleLabels" << std::setw(20) <<   fOpFlashModuleLabels // don't know how to write this operator
            // //<< std::setw(20) << "OpHitModuleLabels" << std::setw(20) <<     fOpHitModuleLabels
            // << std::setw(20) << "OpSimModuleLabels" << std::setw(20) <<     fOpSimModuleLabels
            << std::endl;
    }

    // Get a pointer to the geometry service provider.
    fGeometryService = art::ServiceHandle<geo::Geometry>().get();

    if (fPrintOpDetChannelToSensorPosition) {
        // Attempt to get info on OpDet channels
        geo::WireReadoutGeom const& wireReadout = art::ServiceHandle<geo::WireReadout>()->Get();
        //unsigned nchans = wireReadout.NOpChannels();
        unsigned maxchan = wireReadout.MaxOpChannel();
        auto mfi = mf::LogInfo("OpDetAna::OpDetAnat()");
        mfi << " OpDet channel info:\n";
        for (unsigned ichan = 0; ichan <= maxchan; ++ichan) {
            if (!wireReadout.IsValidOpChannel(ichan))
                continue;
            auto detgeo = wireReadout.OpDetGeoFromOpChannel(ichan);
            auto point = detgeo.GetCenter();
            mfi<<"ch: "<<ichan<<", xyz=("<<point.x()<<","<<point.y()<<","<<point.z()<<"),  "<<detgeo.OpDetInfo()<<"\n";
        }
    }

    GetTPClimits();

    fActiveBox = Box({
            {ActiveBounds[0],ActiveBounds[2],ActiveBounds[4]},
            {ActiveBounds[1],ActiveBounds[3],ActiveBounds[5]}
        });


    consumes<art::Assns<recob::PFParticle,recob::SpacePoint,void>>(fPFPModuleLabel);
    consumes<std::vector<recob::SpacePoint>>(fPFPModuleLabel);
    consumes<art::Assns<recob::PFParticle,recob::Track,void>>(fRecoTrackModuleLabel);
    consumesMany<art::Assns<recob::Track,anab::T0,void>>();
    consumesMany<art::Assns<recob::PFParticle,anab::T0,void>>();
    consumes<art::Assns<recob::Track,anab::Calorimetry,void>>(fCaloModuleLabel);
    consumes<art::Assns<recob::Track,recob::Hit,void>>(fRecoTrackModuleLabel);
    for (auto label: fOpFlashModuleLabels) {
        consumes<art::Assns<recob::OpFlash,recob::OpHit,void>>(label);
        consumes<std::vector<recob::OpFlash>>(label);
    }
    for (auto label: fOpHitModuleLabels)
        consumes<std::vector<recob::OpHit>>(label);
    consumes<std::vector<recob::Track>>(fRecoTrackModuleLabel);
    consumesMany<std::vector<anab::T0>>();

    if (fIsMC) {
        consumes<std::vector<simb::MCParticle>>(fSimModuleLabel);
        for (auto label: fOpSimModuleLabels)
            consumes<std::vector<sim::OpDetBacktrackerRecord>>(label);
    }


    // Detector properties
    auto const detProp = art::ServiceHandle<detinfo::DetectorPropertiesService const>()->DataForJob();
    double efield = detProp.Efield();
    double temp   = detProp.Temperature();
    // Determine the drift velocity from 'efield' and 'temp'; expect to get in cm/us
    fDriftVelocity = detProp.DriftVelocity(efield,temp);
    // Get Readout window length
    fReadoutWindow = detProp.ReadOutWindowSize();


    // prepare the event object to be stored
    fCosmicEvent = new CosmicEventTree::CosmicEventTree_t({});
}

OpDetAna::~OpDetAna()
{
    // return allocated memory
    delete fCosmicEvent;
}

void OpDetAna::analyze(art::Event const& evt)
{
    // clear output data
    *fCosmicEvent = {};
    // needed for the backtracker service
    auto const clockData = art::ServiceHandle<detinfo::DetectorClocksService>()->DataFor(evt);
    // message logger
    auto mfd = mf::LogDebug("OpDetAna::analyze");

    art::RunNumber_t run = evt.id().run();
    art::SubRunNumber_t subrun = evt.id().subRun();
    art::EventNumber_t event = evt.id().event();

    fCosmicEvent->event = event;
    fCosmicEvent->subrun = subrun;
    fCosmicEvent->run = run;


    mfd<<"Run "<<run<<" Sub-run "<<subrun<<" Event "<<event<<"\n";

    // clear map of trackId to index of stored primary
    fTrackIdToPrimary.clear();

    // Fill output event tree? Currently unused.
    bool fill = true;

    if (fIsMC && fStoreMC && ! analyzeMC(clockData, evt) ) // primary muon did not make it into TPC
        return;
    if (! analyzeReco(clockData, evt) ) // no reconstruction?
        return;
    if (fDoOpDet) {
        if (! analyzeOpDet(clockData, evt) ) // no reconstruction?
            return;
    }

    if (fDoOpDet && fMatchPDStoTPC) {
        mf::LogInfo("OpDetAna::analyze()") << "Matching PDS flashes to TPC tracks.";
	MatchPDStoTPC(fCosmicEvent, fMatchPECut, fMatchDtOffsets, fMatchDtCuts, kFirst);
	if (fSelectOnlyMatched) {
            mf::LogVerbatim("OpDetAna::analyze()") << "Selecting only matched PDS flashes and TPC tracks.";
	    FilterMatchedOnly(*fCosmicEvent);
	}
    }

    if (fDoOpDet && fStoreOpWaveforms) {
        mf::LogInfo("OpDetAna::analyze()") << "Storing OpWaveforms.";
	StoreOpWaveforms(fCosmicEvent, evt, clockData);
    }

    if (fTagStoppingMu) {
        mf::LogInfo("OpDetAna::analyze()") << "Selecting stopping muons.";
        TagStoppingMu(ActiveBounds, fCosmicEvent, fAPAOutTag);
    }

    if (fill) {
        mf::LogVerbatim("OpDetAna::analyze()") << "Filling output tree.";
        fTree->Fill();
    }
}

int OpDetAna::analyzeMC(detinfo::DetectorClocksData const& clockData, art::Event const& evt)
{
    art::Handle< std::vector<simb::MCParticle> > particleHandle;
    // Note that if I don't test whether this is successful, and there
    // aren't any simb::MCParticle objects, then the first time we
    // access particleHandle, art will display a "ProductNotFound"
    // exception message and, depending on art settings, it may skip
    // all processing for the rest of this event (including any
    // subsequent analysis steps) or stop the execution.
    if (!evt.getByLabel(fSimModuleLabel, particleHandle))
        {
            // If we have no MCParticles at all in an event, then we're in
            // big trouble. Throw an exception to force this module to
            // stop. Try to include enough information for the user to
            // figure out what's going on. Note that when we throw a
            // cet::exception, the run and event number will automatically
            // be displayed.
            //
            // __LINE__ and __FILE__ are values computed by the compiler.
            throw cet::exception("OpDetAna")
                << " No simb::MCParticle objects in this event - "
                << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
        }

    auto mfi = mf::LogInfo("OpDetAna::analyzeMC");
    auto mfd = mf::LogDebug("OpDetAna::analyzeMC");

    mfd << "Run " << fCosmicEvent->run
        << " subrun " << fCosmicEvent->subrun
        << " event " << fCosmicEvent->event << "\n";

    //int total_muons = 0;
    // int tpc_crossers = 0;
    //int i = 0;
    int iprimarymu = 0;
    for ( auto const& particle: (*particleHandle) ) {
        // mfi<<"Particle mother: "<<particle.Mother()
        //    <<", creation process: "<<particle.Process()
        //    <<"\n";

        // check if this is the primary muon
        if ( particle.Mother() > 0 ) continue; // not a primary; primaries may be interleaved with secondaries in the list
        if ( TMath::Abs(particle.PdgCode()) != 13 ) continue; // not a muon

        iprimarymu++;

        auto traj = particle.Trajectory();

        // generated position and momentum (first in particle's trajectory))
        auto it = traj.begin(); // iterator is a pointer to a pair of 2 TLorentzVectors, pos, mom
        // store generated position and momentum
        auto &nmuons = fCosmicEvent->n_muons;
        it-> first.GetXYZT(fCosmicEvent->gen_pos[nmuons]);
        it->second.GetXYZT(fCosmicEvent->gen_mom[nmuons]);

        particle.EndPosition().GetXYZT(fCosmicEvent->end_pos[nmuons]);

        fCosmicEvent->mc_pdg[nmuons] = particle.PdgCode();

        mfd << "  Primary muon " << iprimarymu++ << "\n";

        mfd << "  Start position: "
            << particle.Position(0)
            << ", end position: "
            << particle.EndPosition()
            << ", momentum: " << particle.Momentum(0)
            << ", end momentum: "
            << particle.EndMomentum()
            << "\n";

        // Check whether track crosses active volume, assumes the track is either sparsified or can be sparsified
        Line segment({particle.Position(0), particle.EndPosition()});

        // store calculated first and last trajectory point in TPC
        TLorentzVector entry, exit;
        bool entered, exited;
        entered = segment.GetEntryPoint(fActiveBox, entry);
        exited  = segment.GetExitPoint(fActiveBox, exit);
        if (!entered) {
            mfd << "  This particle does not intersect with TPC, skipping.\n";
            continue;
        }
        if (!exited) {
            mfd << "  Error: this particle enetered the volume, but never exited nor stopped inside?\n";
        }

        // add this particle to the map of trackID to the tree array index
        fTrackIdToPrimary[particle.TrackId()] = nmuons;

        fCosmicEvent->mc_exited_tpcAV[nmuons] = !insideTPC(particle.EndPosition().Vect());

        fCosmicEvent->mc_length_tpcAV[nmuons] = (exit-entry).Vect().Mag();
        // Fill the entry and exit points
        entry.GetXYZT(fCosmicEvent->mc_startPoint_tpcAV[nmuons]);
        exit.GetXYZT(fCosmicEvent->mc_endPoint_tpcAV[nmuons]);

        // see if Mu decayed
        auto endprocess = particle.EndProcess();
        if (endprocess == "Decay") {
            // mu- decay
            // find Michel electron and its energy
            fCosmicEvent->mc_hasME[nmuons] = 1;
            GetMichelEnergy(particle, particleHandle, fCosmicEvent->mc_ME_en[nmuons], fCosmicEvent->mc_ME_dt[nmuons]);
        } else if (endprocess == "muMinusCaptureAtRest") {
            // mu+, possibly decaying
            // find out if decayed and M.e. energy
            // FIXME: implement
            fCosmicEvent->mc_hasME[nmuons] =
                MuMinusHasMichel(particle, particleHandle, fCosmicEvent->mc_ME_en[nmuons], fCosmicEvent->mc_ME_dt[nmuons]);
        }

        mfd << "  Inside TPC = " << 1
            << ", exited = " << fCosmicEvent->mc_exited_tpcAV[nmuons]
            << "\n";


        mfd << "  Trajectory has " << traj.size() << " points.\n";
        mfd << "  TPC Active:\n"
            << "    Start position =  "
            << entry <<"\n"
            << "    End position = "
            << exit << "\n";

        nmuons++;
    }

    auto truthHandles = evt.getMany<std::vector<simb::MCTruth>>();

    int tot_primaries = 0;
    int tot_cosmics = 0;
    for (auto const& handle: truthHandles) {
        if (handle.isValid())
            for (auto const& truth: *handle)
                tot_primaries += truth.NParticles();
        if (handle.provenance()->inputTag().label() == "cosmicgenerator")
            for (auto const& truth: *handle)
                tot_cosmics += truth.NParticles();
    }

    mfi << "Event has " << iprimarymu << " primary muons "
        << "out of " << tot_cosmics << " primary cosmics "
        << "out of " << tot_primaries << " primary particles and "
        << "out of " << particleHandle->size() << " sim particles.";
    return 1;
} // analyzeMC


int OpDetAna::analyzeReco(detinfo::DetectorClocksData const& clockData, art::Event const& evt)
{
    auto mfi = mf::LogInfo("OpDetAna::analyzeReco");
    auto mfd = mf::LogDebug("OpDetAna::analyzeReco");

    art::Handle< std::vector<recob::Track> > trackHandle;

    if (!evt.getByLabel(fRecoTrackModuleLabel, trackHandle))
        {
            MF_LOG_WARNING("OpDetAna::analyzeReco()")
                << " No recob::Track objects in this event - "
                << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
            return 0;
        }

    art::FindManyP<anab::Calorimetry> fmcalo(trackHandle, evt, fCaloModuleLabel);
    if (!fmcalo.isValid()) {
        // __LINE__ and __FILE__ are values computed by the compiler.
        MF_LOG_WARNING("OpDetAna::analyzeReco()")
            << " No anab::Calorimetry associations to tracks found in "<<fCaloModuleLabel
            << " - "
            << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
        return 0;
    }

    art::Handle< std::vector<recob::PFParticle> > pfpHandle;
    if (!evt.getByLabel(fPFPModuleLabel, pfpHandle))
        {
            // __LINE__ and __FILE__ are values computed by the compiler.
            MF_LOG_WARNING("OpDetAna::analyzeReco()")
                << " No recob::PFParticle objects in this event - "
                << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
            return 0;
        }

    auto spHandle = evt.getValidHandle<std::vector<recob::SpacePoint>>(fPFPModuleLabel);

    art::Handle< std::vector<simb::MCParticle> > particleHandle;
    if (fIsMC) {
        if (!evt.getByLabel(fSimModuleLabel, particleHandle))
            {
                // __LINE__ and __FILE__ are values computed by the compiler.
                throw cet::exception("OpDetAna::analyzeReco")
                    << " No simb::MCParticle objects in this event - "
                    << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
            }
    }

    // Get all hits per each track
    art::FindManyP<recob::Hit> fmht(trackHandle, evt, fRecoTrackModuleLabel);
    // Get PFParticle associations
    art::FindManyP<recob::PFParticle> fmpfp(trackHandle, evt, fRecoTrackModuleLabel);
    // Get Spacepoints per PFP
    art::FindManyP<recob::SpacePoint> fmsp(pfpHandle, evt, fPFPModuleLabel);
    art::FindManyP<recob::Hit> fmhits(spHandle, evt, fPFPModuleLabel); // there should be at least 2 hits per SP


    // get all T0 product handles there are
    //const auto &T0handles = evt.getMany<T0Vector_t>();
    std::vector<art::FindOneP<anab::T0>> T0TrkAssns;
    std::vector<art::FindOneP<anab::T0>> T0PfpAssns;


    // get track->T0 associations
    //for (auto const & handle: T0handles) {
    // clear T0 association table
    static const std::vector<std::pair<short,short>> tmpempty(fT0ModuleLabels.size(), {0,-1});
    fT0Association = tmpempty;
    int ilabel = 0;
    for (auto label: fT0ModuleLabels) {
        const art::FindOneP<anab::T0> findTrkT0(trackHandle,evt,label); // attempt association by track
        if ( findTrkT0.isValid() ) {
            fT0Association[ilabel] = {1, T0TrkAssns.size()};
            T0TrkAssns.push_back(findTrkT0);
            std::cout<<"Using track T0 association from "<<label<<std::endl;
        }
        const art::FindOneP<anab::T0> findPfpT0(pfpHandle,evt,label); // attempt association by pfp
        if ( findPfpT0.isValid() ) {
            fT0Association[ilabel] = {2, T0PfpAssns.size()};
            T0PfpAssns.push_back(findPfpT0);
            std::cout<<"Using PFP T0 association from "<<label<<std::endl;
        }
        ++ilabel;
    }

    unsigned int ntracks = trackHandle->size();
    if (ntracks > CosmicEventTree::MAX_RECO_TRACKS) {
        mfd<<"CalibCosmicsAna: WARNING: Event "
           <<" has "<<ntracks<<" reconstructed tracks. Will only store "<<CosmicEventTree::MAX_RECO_TRACKS<<"\n";
        ntracks = CosmicEventTree::MAX_RECO_TRACKS;
    }

    mfd<<"Run "<<fCosmicEvent->run<<", "
       <<"Subrun "<<fCosmicEvent->subrun<<", "
       <<"Event "<<fCosmicEvent->event<<": "
       <<"storing "<<ntracks<<" reco tracks\n";


    // loop over all tracks in this event
    for (unsigned int itrk = 0; itrk < trackHandle->size(); ++itrk) {
        auto const& track = trackHandle->at(itrk);
        auto& ntrk = fCosmicEvent->n_reco_tracks;
        if (ntrk >= ntracks) break; // store only maximum tracks

        auto start = track.Start();
        auto end = track.End();
        start.GetCoordinates(fCosmicEvent->trk_startPoint[ntrk]);
        end.GetCoordinates(fCosmicEvent->trk_endPoint[ntrk]);

        fCosmicEvent->trk_length[ntrk] = track.Length();

        // associated PFP
        auto const & pfps = fmpfp.at(itrk);
        if (pfps.size() == 0) {
            // ERROR no associated PFParticle?
            MF_LOG_WARNING("OpDetAna::analyzeReco()")
                << " No recob::PFParticle association to track " << itrk << " found in "<<fRecoTrackModuleLabel
                << " - "
                << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
            return 0;
        }
        if (pfps.size() > 1) {
            // WARNING too many PFParticles share the same track!?
            MF_LOG_WARNING("OpDetAna::analyzeReco()") << "WARNING: More than 1 PFParticle associated with track " << itrk << ". Using the first only.";
        }
        auto pfp = pfps.at(0); //Associated PFP


        // associated space points
        auto sps = fmsp.at(pfp.key());

#ifdef DEBUG
        std::cout<<"Track "<<itrk<<":"<<std::endl
                 <<"==========="<<std::endl
                 <<"Looking for track's Hits, associated with space points."<<std::endl;
        std::cout<<"Total SPs: "<<sps.size()<<std::endl;
        //std::cout<<"Got "<<fmhits.size()<<" vectors of hits."<<std::endl;
#endif

        // get lowest and highest drift time, assume hits are ordered as they appear within the track?
        float lowx = 9999.;
        float hix = -9999.;
        float tlow = 0.;
        float thi = 0.;
        int isp = 0;
        int tpclow = 0, tpchi = 0;
        art::Ptr<recob::Hit> hitlow;
        art::Ptr<recob::Hit> hithi;
        auto &trk_sps = fCosmicEvent->trk_sp[ntrk];
        auto &trk_nsps = fCosmicEvent->trk_nsps[ntrk];
        for (auto sp: sps) {
            //std::cout<<"Investigating SP "<<isp<<std::endl;
            auto const& hits = fmhits.at(sp.key());
            if (fStoreSPs && trk_nsps < 8192) {
                for(unsigned i=0; i<3; ++i) {
                    trk_sps[trk_nsps][i] = sp->XYZ()[i];
                }
                trk_nsps += 1;
            }
            if (sp->XYZ()[0] < lowx) {
                lowx = sp->XYZ()[0];
                hitlow = hits.at(0);
            }
            if (sp->XYZ()[0] > hix) {
                hix = sp->XYZ()[0];
                hithi = hits.at(0);
            }
            ++isp;
        }

        tpclow = hitlow->WireID().TPC;
        tpchi = hithi->WireID().TPC;

        // Use hit limits to tell when the track started/ended.
        // TPC1 has x < 0 and lower ticks for smaller X
        if (tpclow%2 == 1)
            tlow = hitlow->StartTick();
        else
            tlow = hitlow->EndTick();

        if (tpchi%2 == 0)
            thi = hithi->StartTick();
        else
            thi = hithi->EndTick();

#ifdef DEBUG
        std::cout<<"  SP x_low = "<<lowx<<", "
                 <<"x_hi = "<<hix<<", "
                 <<"hit t_low = "<<tlow<<", "
                 <<"t_hi = "<<thi<<", "
                 <<"PTC_low = "<<tpclow<<", "
                 <<"TPC_hi = "<<tpchi<<std::endl;
#endif

        if (fCosmicEvent->trk_startPoint[ntrk][0] < fCosmicEvent->trk_endPoint[ntrk][0]) {
            fCosmicEvent->trk_startPoint[ntrk][3] = tlow;
            fCosmicEvent->trk_endPoint[ntrk][3] = thi;
            fCosmicEvent->trk_startTpc[ntrk] = tpclow;
            fCosmicEvent->trk_endTpc[ntrk] = tpchi;
        } else {
            fCosmicEvent->trk_startPoint[ntrk][3] = thi;
            fCosmicEvent->trk_endPoint[ntrk][3] = tlow;
            fCosmicEvent->trk_startTpc[ntrk] = tpchi;
            fCosmicEvent->trk_endTpc[ntrk] = tpclow;
        }


        /// deal with track calorimetry
        mfd<<"  Track "<<itrk<<": \n";
        auto trk_calo = fmcalo.at(itrk);
        float totalE = 0.;
        unsigned short int nplanes = 0, iplane = 0;
        int best_plane_ncalo = 0;
        // std::cout<<"  Track "<<ntrk<<std::endl;
        for (auto const calo: trk_calo) { // loop over all 3 planes (calorimetry stored for all associated hits in each plane)
            mfd<<"    plane "<<iplane   <<": "
               <<"ncalo points = " << calo->dQdx().size()<< ", "
               <<"total kinetic energy = "<<calo->KineticEnergy() << "\n";
            fCosmicEvent->trk_total_charge[ntrk][iplane] = calo->KineticEnergy();
            fCosmicEvent->trk_ncalopts[ntrk][iplane] = calo->dQdx().size();
            // store dEdx at the end of the track
            // std::cout<<"  Plane "<<iplane<<":"<<std::endl;
            // for (auto range: calo->ResidualRange()) {
            //  std::cout<<"    calo range: "<<range<<std::endl;
            // }
            int calosize = calo->ResidualRange().size();
            int imax = std::min(calosize, 20);
            for(int i = 0; i < imax; ++i) {
                fCosmicEvent->trk_end_dedx[ntrk][iplane][i] = calo->dEdx().at(i);
                fCosmicEvent->trk_end_range[ntrk][iplane][i] = calo->ResidualRange().at(i);
                fCosmicEvent->trk_end_ncalo[ntrk][iplane]++;

                fCosmicEvent->trk_start_dedx[ntrk][iplane][i] = calo->dEdx().at(calosize-1-i);
                fCosmicEvent->trk_start_range[ntrk][iplane][i] = calo->ResidualRange().at(calosize-1-i);
                fCosmicEvent->trk_start_ncalo[ntrk][iplane]++;
            }
            if (fCosmicEvent->trk_start_ncalo[ntrk][iplane] > best_plane_ncalo)
                fCosmicEvent->trk_bestPlane[ntrk] = iplane;

            ++iplane;

            if (calo->dQdx().size() == 0) // no calorimetry in this plane
                continue;
            totalE += calo->KineticEnergy();
            nplanes++;
        }
        //float averageE = totalE/nplanes;
        fCosmicEvent->trk_nplanes[ntrk] = nplanes;

        /// Find out if T0 tagged
        // run over all T0s
        // keep track of which producer created T0:
        int whichT0 = 0;
        // - associated with the tack
        for (auto &assn: fT0Association) {
            ++whichT0;
            art::Ptr<anab::T0> t0;
            if (assn.first == 1 )
                // track-T0 association
                t0 = T0TrkAssns[assn.second].at(itrk);
            else if (assn.first == 2 )
                // pfp-T0 assn
                t0 = T0PfpAssns[assn.second].at(pfp.key());
            else
                continue;

            if (t0.isNull())
                continue;

            fCosmicEvent->trk_hasT0[ntrk] = whichT0;
            fCosmicEvent->trk_T0[ntrk] = t0->Time(); // in ns
            // correct track's x if necessary
            if (fCorrectX0 && fT0TagNeedsCorrection.size() && fT0TagNeedsCorrection.at(whichT0-1)) {
                int TPC = fCosmicEvent->trk_startTpc[ntrk];
                float x0 = (2*(TPC%2)-1)*fDriftVelocity*t0->Time()*1e-3; // dift velocity  in cm/us, T0 in ns
                fCosmicEvent->trk_startPoint[ntrk][0] -= x0;
                fCosmicEvent->trk_endPoint[ntrk][0] -= x0;
                if (fStoreSPs) {
                    auto& nsps = fCosmicEvent->trk_nsps[ntrk];
                    auto& sps = fCosmicEvent->trk_sp[ntrk];
                    for (size_t isp = 0; isp < nsps; ++isp) {
                        sps[isp][0] -= x0;
                    }
                }
            }

            break;
        }

        // auto const& t0 = fopt0.at(pfp.key());
        // if (t0.isNull()) // this PFP (which was resolved as the track in question) does not have associated T0
        //   fCosmicEvent->trk_hasT0[ntrk] = 0;
        // else {
        //   fCosmicEvent->trk_hasT0[ntrk] = 1;
        //   fCosmicEvent->trk_T0[ntrk] = t0->Time();
        // }

        // Prepare hits for match in MC and for drift time of first and last point in track
        // separate hits for this track by plane
        const std::vector< art::Ptr<recob::Hit> > &allHits = fmht.at(itrk);
        std::vector< art::Ptr<recob::Hit> > hits[3];
        for(size_t ah = 0; ah < allHits.size(); ++ah){
            if (allHits[ah]->WireID().Plane <  3){
                hits[allHits[ah]->WireID().Plane].push_back(allHits[ah]);
            }
        }

        // get lowest and highest drift time, assume hits are ordered as they appear within the track?
        // std::cout<<"Track "<<ntrk
        //           <<" ("<<start.X()<<","<<start.Y()<<","<<start.Z()<<"),"
        //           <<" ("<<end.X()<<","<<end.Y()<<","<<end.Z()<<")"<<std::endl
        //      <<"Hits drift time: "<<std::endl;
        // for (auto ah: allHits) {
        //   std::cout<<"  "<<ah->PeakTime()<<" TPC ID: "<<ah->WireID().TPC<<" Plane "<<ah->WireID().Plane<<std::endl;
        // }


        /// Get MC particle match
        if (fIsMC) {
            for (unsigned short iplane = 0; iplane < 3; ++iplane) {
                if (hits[iplane].size() == 0)
                    continue;

                Int_t trkid = 0;
                Float_t purity = 0;
                Double_t maxe = 0;

                HitsPurity(clockData, hits[iplane], trkid, purity, maxe);

                mfd << "  plane " << iplane << ": "
                    << "trkid = " << trkid << ", "
                    << "purity = " << purity << ", "
                    << "maxe = " << maxe << ", ";

                fCosmicEvent->trk_g4id[ntrk][iplane] = trkid;
                fCosmicEvent->trk_mcE[ntrk][iplane] = maxe;
		if (fStoreMC) {
		    auto primary = fTrackIdToPrimary.find(trkid);
		    if (primary != fTrackIdToPrimary.end()) {
			fCosmicEvent->trk_mcMuIdx[ntrk][iplane] = primary->second;
		    } else
			fCosmicEvent->trk_mcMuIdx[ntrk][iplane] = -1;
		}

                // search for MC particle
                for (auto const& simp: (*particleHandle) ) {
                    if (simp.TrackId() != trkid) continue;

                    fCosmicEvent->trk_mcpdg[ntrk][iplane] = simp.PdgCode();
                    break;
                }

                mfd << "pdg = " << fCosmicEvent->trk_mcpdg[ntrk][iplane] << "\n";
            }
            // get best match for pdg
            bool done = false;
            for (unsigned short iplane = 0; iplane < 2; ++iplane) {
                for (unsigned short jplane = iplane+1; jplane < 3; ++jplane) {
                    if (fCosmicEvent->trk_mcpdg[ntrk][iplane] == fCosmicEvent->trk_mcpdg[ntrk][jplane]) {
                        fCosmicEvent->trk_mcpdg_best[ntrk] = fCosmicEvent->trk_mcpdg[ntrk][iplane];
                        done = true;
                        break;
                    }
                }
                if (done)
                    break;
            }
            if (!done) {
                // decide by best plane (by n calo points)
                fCosmicEvent->trk_mcpdg_best[ntrk] = fCosmicEvent->trk_mcpdg[ntrk][fCosmicEvent->trk_bestPlane[ntrk]];
            }

            // get best match for MC muon idx
	    if (fStoreMC) {
		done = false;
		for (unsigned short iplane = 0; iplane < 2; ++iplane) {
		    for (unsigned short jplane = iplane+1; jplane < 3; ++jplane) {
			if (fCosmicEvent->trk_mcMuIdx[ntrk][iplane] > -1 && fCosmicEvent->trk_mcMuIdx[ntrk][iplane] == fCosmicEvent->trk_mcMuIdx[ntrk][jplane]) {
			    fCosmicEvent->trk_mcMuIdx_best[ntrk] = fCosmicEvent->trk_mcMuIdx[ntrk][iplane];
			    done = true;
			    break;
			}
		    }
		    if (done)
			break;
		}
		if (!done) {
		    // decide by best plane (by n calo points)
		    fCosmicEvent->trk_mcMuIdx_best[ntrk] = fCosmicEvent->trk_mcMuIdx[ntrk][fCosmicEvent->trk_bestPlane[ntrk]];
		}
	    }
        }

        ntrk++; // advance to next track to be saved
    } // tracks for loop

    // Do the T0 tagging
    int whichT0 = fT0Association.size() + 1; // identification of the next tag
    // CPA crossers
    if (fTagCPAcrossers) {
        mf::LogInfo("OpDetAna::analyzeReco()") << "Running TagCPAcrossers(). Will use tag ID "<<whichT0;
	auto result = TagCPAcrossers(ActiveBounds,fCosmicEvent,whichT0++,fDriftVelocity,fCorrectX0,fIsMC);
        mf::LogInfo("OpDetAna::analyzeReco()")<< "Tagged " << result << " tracks.";
    }
    if (fTagAPAIn) {
        mf::LogInfo("OpDetAna::analyzeReco()") << "Running TagAPAInCrossers(). Will use tag ID "<<whichT0;
	auto result = TagAPAInCrossers(ActiveBounds,fCosmicEvent,whichT0++,fDriftVelocity,fCorrectX0,fIsMC);
        mf::LogInfo("OpDetAna::analyzeReco()")<< "Tagged " << result << " tracks.";
    }
    if (fTagAPAOut) {
        mf::LogInfo("OpDetAna::analyzeReco()") << "Running TagAPAOutCrossers(). Will use tag ID "<<whichT0;
	auto result = TagAPAOutCrossers(ActiveBounds,fCosmicEvent,whichT0++,fDriftVelocity,fCorrectX0,fIsMC);
        mf::LogInfo("OpDetAna::analyzeReco()")<< "Tagged " << result << " tracks.";
    }

    if (fSelectOnlyTagged) {
        //bool result =
	// std::cout<<"Selecting only tagged tracks."<<std::endl;
	// std::cout<<"Will attempt access of fCosmicEvent"<<std::endl;
	// std::cout<<"  fCosmicEvent->n_reco_tracks = "<<fCosmicEvent->n_reco_tracks <<std::endl;
        mf::LogVerbatim("OpDetAna::analyzeReco()") << "Selecting only tagged tracks.";
	FilterTaggedOnly(*fCosmicEvent);
    }

    mfi << "Event has " << fCosmicEvent->n_reco_tracks << " reco tracks.";

    return 1;
} // analyze Reco


int OpDetAna::analyzeOpDet(detinfo::DetectorClocksData const& clockData, art::Event const& evt)
{
    auto mfi = mf::LogInfo("OpDetAna::analyzeOpdet");
    auto mfd = mf::LogDebug("OpDetAna::analyzeOpdet");


    art::Handle< std::vector<recob::OpFlash> > flashHandle;
    art::Handle< std::vector<simb::MCParticle> > particleHandle;
    //art::ServiceHandle<cheat::PhotonBackTrackerService> bt_serv;
    art::Handle< std::vector<dune::ProtoDUNETimeStamp> > trigHandle;


    if (fIsMC) {
        // Backtracker for MC truth handling
        if (!evt.getByLabel(fSimModuleLabel, particleHandle))
            {
                // __LINE__ and __FILE__ are values computed by the compiler.
                throw cet::exception("OpDetAna::analyzeOpDet")
                    << " No simb::MCParticle objects in this event - "
                    << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
            }
    }

    // if (!fIsMC) { // get trigger time stamp for data
    //   // Backtracker for MC truth handling
    //   if (!evt.getByLabel("timingrawdecoder:daq", trigHandle))
    //     {
    //  // __LINE__ and __FILE__ are values computed by the compiler.
    //  throw cet::exception("OpDetAna::analyzeOpDet")
    //    << " No dune::ProtoDUNETimeStamp products in this event - "
    //    << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
    //     }
    //   if (trigHandle->size() != 1)
    //     {
    //  // __LINE__ and __FILE__ are values computed by the compiler.
    //  throw cet::exception("OpDetAna::analyzeOpDet")
    //    << " Number of dune::ProtoDUNETimeStamp objects in this event is "
    //    << trigHandle->size() << " - "
    //    << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
    //     }


    //   const auto timeStamp = trigHandle->at(0);
    //   fExtTrigTime = timeStamp.getTimeStamp()*0.02; // convert to us, assuming 50 MHz clock
    // }

    //get the SSP (or PDS?) external timestamp
    if(!fIsMC){
        // FIXME: handle situation of PD HD - no external trigger
        // product. Need to extract TPC time stamp from
        // pdhdkeepupstage1 raw::RDTimeStamp by daq:trigger.
        // OpDetWaveforms hold time stamp in ticks since epoch (same
        // as the RDTimeStamp), however in double precision, which is
        // only precise to 16 ticks.
        if (fRefTimeStampModuleLabel.empty()) {
            art::Handle<std::vector<raw::OpDetWaveform>> extWaveformHandle;
            evt.getByLabel( fOpDetWaveformModuleLabel+":external", extWaveformHandle);
            if((*extWaveformHandle).size() == 0){
                MF_LOG_WARNING("Empty External Waveforms\n") << "Empty external waveforms for this event. Skipping. \n";
            } else {
                fExtTrigTime=(*extWaveformHandle).at(0).TimeStamp(); //Doesn't matter which one, we have one from each channel all at same time
            }
        } else {
            auto tsHandle = evt.getValidHandle<raw::RDTimeStamp>(fRefTimeStampModuleLabel);
            // WARNING: loss of precision here. In PDHD data, this TS
            // is in DAQ ticks since epoch. ULong64_t is returned. To
            // date, conversion to double cuts off 4 LSBs, changing
            // the precission to 16 ticks.
            fExtTrigTime = tsHandle->GetTimeStamp();
        }
    }

    // auto opHitModuleLabelP = fOpHitModuleLabels.begin();
    for (auto flashModuleLabel: fOpFlashModuleLabels) {
        if (!evt.getByLabel(flashModuleLabel, flashHandle))
            {
                // __LINE__ and __FILE__ are values computed by the compiler.
                MF_LOG_WARNING("OpDetAna::analyzeOpDet()")
                    << " No recob::OpFlash objects in "
                    << flashModuleLabel
                    << " in this event - "
                    << " Line " << __LINE__ << " in file " << __FILE__ << std::endl;
                continue; // in labels for loop
            }


        // Get assosciations between flashes and hits
        art::FindManyP< recob::OpHit > fmHitsPerflash(flashHandle, evt, flashModuleLabel);
        if (!fmHitsPerflash.isValid()) {
            MF_LOG_WARNING("OpDetAna::analyzeOpDet()") << "WARNING: cannot get optical hits associated with flashes.";
            //opHitModuleLabelP++; // advance to next label
            continue; // in labels for loop
        }



        unsigned int nflashes = flashHandle->size();
        if (fCosmicEvent->n_flashes + nflashes > CosmicEventTree::MAX_FLASHES) {
            mfi<<"CalibCosmicsAna: WARNING: Event "
               <<" has "<<nflashes<<" reconstructed flashs in product "
               << flashModuleLabel
               <<" and " << fCosmicEvent->n_flashes << " flashes has been stored.";

            nflashes = CosmicEventTree::MAX_FLASHES - fCosmicEvent->n_flashes;
            if (nflashes < 0)
                nflashes = 0;

            mfi << "Will only store "<<nflashes<<"from this label\n";
        }
        mfd << "Run "     << fCosmicEvent->run << ", "
            << "Subrun "  << fCosmicEvent->subrun << ", "
            << "Event "   << fCosmicEvent->event << ", "
            << "OpFlashLabel " << flashModuleLabel << ": "
            << "storing " << nflashes << " reco flashes\n";

        size_t iflash = 0;
        for (auto const& flash: (*flashHandle) ) {
            auto& nflsh = fCosmicEvent->n_flashes;
            if (iflash >= nflashes) break; // store only maximum flashes

            if (fIsMC) {
                fCosmicEvent->flash_time[nflsh] = flash.Time();
                fCosmicEvent->flash_abs_time[nflsh] = flash.AbsTime();
            } else {
                fCosmicEvent->flash_time[nflsh] = flash.Time() - fExtTrigTime;
                fCosmicEvent->flash_abs_time[nflsh] = flash.AbsTime() - fExtTrigTime;
            }
            fCosmicEvent->flash_time_width[nflsh] = flash.TimeWidth();
            fCosmicEvent->flash_frame[nflsh] = flash.Frame();
            fCosmicEvent->flash_hasx[nflsh] = flash.hasXCenter();

            fCosmicEvent->flash_x[nflsh] = flash.XCenter();
            fCosmicEvent->flash_y[nflsh] = flash.YCenter();
            fCosmicEvent->flash_z[nflsh] = flash.ZCenter();

            fCosmicEvent->flash_x_width[nflsh] = flash.XWidth();
            fCosmicEvent->flash_y_width[nflsh] = flash.YWidth();
            fCosmicEvent->flash_z_width[nflsh] = flash.ZWidth();

            fCosmicEvent->flash_tot_pe[nflsh] = flash.TotalPE();

            // deal with related hits
            auto& nhits = fCosmicEvent->n_hits;
            auto& ihit = fCosmicEvent->flash_nhits[nflsh];
            auto const& hits = fmHitsPerflash.at(iflash);
            for (auto const& hit: hits) {
                if (nhits >= CosmicEventTree::MAX_HITS)
                    break;
                // save individual hit info and store index (nhits) in the flash vector (flash_hit_idx[i])
                fCosmicEvent->hit_channel[nhits] = hit->OpChannel();
                fCosmicEvent->hit_has_start_time[nhits] = hit->HasStartTime();
                fCosmicEvent->hit_start_time[nhits] = hit->StartTime();
                if (fIsMC) {
                    fCosmicEvent->hit_time[nhits] = hit->PeakTime();
                    fCosmicEvent->hit_abs_time[nhits] = hit->PeakTimeAbs();
                } else {
                    fCosmicEvent->hit_time[nhits] = hit->PeakTime() - fExtTrigTime;
                    fCosmicEvent->hit_abs_time[nhits] = hit->PeakTimeAbs() - fExtTrigTime;
                }
                fCosmicEvent->hit_width[nhits] = hit->Width();
                fCosmicEvent->hit_pe[nhits] = hit->PE();

                fCosmicEvent->flash_hit_idx[nflsh][ihit] = nhits;

                ihit++;
                nhits++;
            }

            if (fIsMC) {
                /// Get MC particle match
                Int_t trkid = 0;
                Float_t purity = 0;
                Double_t maxpe = 0;

                OpHitsPurity(clockData, hits, trkid, purity, maxpe);

                mfd << "  local flash " << iflash << ": "
                    << "trkid = " << trkid << ", "
                    << "purity = " << purity << ", "
                    << "maxpe = " << maxpe << ", ";

                fCosmicEvent->flash_g4id[nflsh] = trkid;
                fCosmicEvent->flash_mcPE[nflsh] = maxpe;

		if (fStoreMC) {
		    auto primary = fTrackIdToPrimary.find(trkid);
		    if (primary != fTrackIdToPrimary.end()) {
			fCosmicEvent->flash_mcMuIdx[nflsh] = primary->second;
		    } else
			fCosmicEvent->flash_mcMuIdx[nflsh] = -1;

                    mfd<< "primary idx = " << fCosmicEvent->flash_mcMuIdx[nflsh] << ", ";
		}

                // search for MC particle
                for (auto const& simp: (*particleHandle) ) {
                    if (simp.TrackId() != trkid) continue;

                    fCosmicEvent->flash_mcpdg[nflsh] = simp.PdgCode();
                    break;
                }

                mfd << "pdg = " << fCosmicEvent->flash_mcpdg[nflsh] << "\n";
            }

            ++nflsh;
            ++iflash;
        } // For flashes
    } // for flash labels

    mfi << "Event has " << fCosmicEvent->n_flashes << " reco optical flashes\n"
        << "      and " << fCosmicEvent->n_hits << " reco optical hits.";

    return 1;
} // analyze OpDet

void OpDetAna::beginJob()
{
    art::ServiceHandle<art::TFileService const> tfs;

    fTree = tfs->make<TTree>("opdetana", "");

    CosmicEventTree::createBranches(fTree, fCosmicEvent);
}

void OpDetAna::endJob()
{
    // Implementation of optional member function here.
}

void OpDetAna::beginRun(art::Run const& r)
{
    // Implementation of optional member function here.
}

bool OpDetAna::FilterTaggedOnly(CosmicEventTree::CosmicEventTree_t& evt)
{
    auto tmpevt_p = new CosmicEventTree::CosmicEventTree_t(evt); // need to create on heap... was getting segmentation violation after a call to this function
    auto &tmpevt = *tmpevt_p;

    // clear track reco part
    std::memset((void*)&tmpevt.n_reco_tracks, 0, (char*)&tmpevt.n_flashes - (char*)&tmpevt.n_reco_tracks);

    for (unsigned itrk = 0; itrk < evt.n_reco_tracks; ++itrk) {
        if (!evt.trk_hasT0[itrk])
            continue;
	AddTrack(evt, tmpevt, itrk);
    }

    evt = tmpevt;
    delete tmpevt_p;

    return (evt.n_reco_tracks > 0);
}

bool OpDetAna::FilterMatchedOnly(CosmicEventTree::CosmicEventTree_t& evt)
{
    auto tmpevt_p = new CosmicEventTree::CosmicEventTree_t(evt); // need to create on heap... was getting segmentation violation after a call to this function
    auto &tmpevt = *tmpevt_p;

    // clear data to be rebuilt
    // clear track reco part
    std::memset((void*)&tmpevt.n_reco_tracks, 0, (char*)&tmpevt.n_flashes - (char*)&tmpevt.n_reco_tracks);
    // clear Op det reco part
    std::memset((void*)&tmpevt.n_flashes, 0, (char*)tmpevt.trk_stitched - (char*)&tmpevt.n_flashes);

    mf::LogDebug("FilterMatchedOnly")<<"Cleared OpDet info for output evt:\n"
                                     <<"  n_flashes = "<<tmpevt.n_flashes<<"\n"
                                     <<"  n_hits = "<<tmpevt.n_hits;

    // single flash can be matched to mulitple tracks: keep track of what flashes we keep
    int flash_stored[CosmicEventTree::MAX_FLASHES];
    std::fill_n(flash_stored, CosmicEventTree::MAX_FLASHES, -1);

    for (unsigned imatch = 0; imatch < evt.n_matches; ++imatch) {
	auto itrk = evt.match_trk_idx[imatch];
	auto iflash = evt.match_flash_idx[imatch];

	AddTrack(evt, tmpevt, itrk);
	if (flash_stored[iflash] == -1) {
	    AddFlashWithHits(evt, tmpevt, iflash);
	    flash_stored[iflash] = tmpevt.n_flashes-1;
	}

	auto jtrack = tmpevt.n_reco_tracks-1;
	auto jflash = flash_stored[iflash];

	// rereference matches
	tmpevt.match_trk_idx[imatch] = jtrack;
	tmpevt.match_flash_idx[imatch] = jflash;
	tmpevt.trk_matched[jtrack] = imatch;
	tmpevt.flash_matched[jflash] = imatch; // potentially rewriting previous match. this association is unsafe
    }

    evt = tmpevt;
    delete tmpevt_p;

    return (evt.n_reco_tracks > 0);
}


void OpDetAna::StoreOpWaveforms(CosmicEventTree::CosmicEventTree_t* outevt, art::Event const& evt, detinfo::DetectorClocksData const& clocksData)
{
    mf::LogDebug("StoreOpWaveforms")<<"Starting StoreOpWaveforms";

    using WaveformVec = std::vector<raw::OpDetWaveform>;

    // Find all waveforms for relevant flashes
    std::vector< art::ValidHandle<WaveformVec> > wfmhndls;
    if (!fIsMC && fRefTimeStampModuleLabel.empty()) {
        // case for PDSP data
        for ( auto str: {"internal","external"} ) {
            wfmhndls.push_back(evt.getValidHandle<WaveformVec>(fOpDetWaveformModuleLabel+":"+str));
        }
    } else {
        // case for MC and PDHD data (keepup)
        wfmhndls.push_back(evt.getValidHandle<WaveformVec>(fOpDetWaveformModuleLabel));
    }

    if (fOpHitModuleLabels.size() == 0) {
        // no Reco parts to be stored. Store simply all waveforms
        auto mfd = mf::LogDebug("StoreOpWaveforms");
        mfd << "  fExtTrigTime = " << fExtTrigTime << "\n";
        auto &nwfms = outevt->n_wfms;
        for (const auto & wfms: wfmhndls) {
            for (const auto & opdetwfm: *wfms) {
                if (nwfms < 10) {
                    mfd << "  Channel: " << opdetwfm.ChannelNumber() << "  opdetwfm.TimeStamp() = " << opdetwfm.TimeStamp() << "\n";
                }
                if (nwfms >= CosmicEventTree::MAX_WFMS)
                    break;
                if (fIsMC)
                    outevt->wfm_time[nwfms] = (float)opdetwfm.TimeStamp();
                else
                    outevt->wfm_time[nwfms] = (float)(opdetwfm.TimeStamp() - fExtTrigTime);

                auto &wfm = opdetwfm.Waveform();
                auto &outwfm = outevt->wfm[nwfms];
                auto &size = outevt->wfm_size[nwfms];
                size = std::min((unsigned)wfm.size(),CosmicEventTree::OPDET_WFM_SIZE);
                for (unsigned i = 0; i < size; ++i) {
                    outwfm[i] = wfm[i];
                }
                outevt->wfm_channel[nwfms] = opdetwfm.ChannelNumber();
                ++nwfms;
            }
        }
    } else {
        // Store only waveforms associated with a hit

        // Can there be multiple wfms for each channel in a single event? Maybe. Need to match wfm in time, need to consider all wfms.
        int total_wfms_size = 0;
        std::vector<unsigned> cum_part_sizes;
        for (auto& wfmhndl: wfmhndls) {
            total_wfms_size += wfmhndl->size();
            cum_part_sizes.push_back(total_wfms_size);
        }
        std::vector<int> wfmchannel(total_wfms_size, -1);
        std::map<int,std::vector<unsigned>> channel2wfms;

        int iwfm = 0;
        for (auto &wfmhndl: wfmhndls) {
            auto const &wfms = *wfmhndl;

            for (auto const& wfm: wfms) {
                auto chan = wfm.ChannelNumber();
                wfmchannel[iwfm] = chan;
                channel2wfms[chan].push_back(iwfm);
                ++iwfm;
            }
        }
        mf::LogDebug("StoreOpWaveforms")<<"Filled channel-wfm map";

        std::vector<int> wfm_included(total_wfms_size, -1);
        std::vector<size_t> wfm_to_store;

        auto &hit2wfm_idx = outevt->hit_wfmIdx;
        std::fill_n(hit2wfm_idx, CosmicEventTree::MAX_HITS, -1);
        auto& wfmhit = outevt->wfm_hit;

        mf::LogDebug("StoreOpWaveforms")<<"Initialized hit2wfm indx.";

        // loop over matched flashes
        for (unsigned imatch = 0; imatch < outevt->n_matches; ++imatch) {
            if (wfm_to_store.size() >= CosmicEventTree::MAX_WFMS)
                break;
            const auto &iflash = outevt->match_flash_idx[imatch];
            const auto &nhits =  outevt->flash_nhits[iflash];
            // loop over matched flash hits
            for (unsigned ifhit = 0; ifhit < nhits; ++ifhit) {
                if (wfm_to_store.size() >= CosmicEventTree::MAX_WFMS)
                    break;
                auto ihit = outevt->flash_hit_idx[iflash][ifhit];
                auto chan = outevt->hit_channel[ihit];
                auto hit_time = outevt->hit_abs_time[ihit]; // Use abs time as that one was used to get time offsets
                // find corresponding wfm, identify by timing
                for (auto iwfm: channel2wfms[chan]) {
                    // find the waveform
                    const raw::OpDetWaveform* wfmp;

                    for (unsigned i = 0,offset=0; i<wfmhndls.size(); offset=cum_part_sizes[i],++i) {
                        if (iwfm<cum_part_sizes[i]) {
                            wfmp = &wfmhndls[i].product()->at(iwfm-offset);
                            break;
                        }
                    }
                    auto const &wfm = *wfmp;
                    if (!IsHitWithinWaveform(hit_time, wfm, clocksData))
                        continue;
                    // found the wfm
                    if (wfm_included[iwfm]==-1) {
                        wfm_included[iwfm] = wfm_to_store.size();
                        wfm_to_store.push_back(iwfm);
                    }
                    hit2wfm_idx[ihit] = wfm_included[iwfm];
                    wfmhit[wfm_included[iwfm]] = ihit;
                    break;
                }
            }
        }
        mf::LogDebug("StoreOpWaveforms")<<"Looped over all matched flashes and their hits.\n"
                                        <<"To loop over "<<wfm_to_store.size()<<" waveform to store\n";

        // 3) record hit->wvfm association
        auto &nwfms = outevt->n_wfms;
        for (auto iwfm: wfm_to_store) {
            const raw::OpDetWaveform* wfmp;
            for (unsigned i = 0, offset=0; i<wfmhndls.size(); offset=cum_part_sizes[i],++i) {
                if (iwfm<cum_part_sizes[i]) {
                    wfmp = &wfmhndls[i].product()->at(iwfm-offset);
                    break;
                }
            }
            auto &opdetwfm = *wfmp;
            if (fIsMC)
                outevt->wfm_time[nwfms] = (float)opdetwfm.TimeStamp();
            else
                outevt->wfm_time[nwfms] = (float)opdetwfm.TimeStamp() - fExtTrigTime;

            outevt->wfm_channel[nwfms] = opdetwfm.ChannelNumber();

            auto &wfm = opdetwfm.Waveform();
            auto &outwfm = outevt->wfm[nwfms];
            auto &size = outevt->wfm_size[nwfms];
            size = wfm.size();
            size = std::min((unsigned)size,CosmicEventTree::OPDET_WFM_SIZE);
            for (unsigned i = 0; i < size; ++i) {
                outwfm[i] = wfm[i];
            }
            ++nwfms;
        }
    }
    mf::LogDebug("StoreOpWaveforms")<<"Finished.";
}

void OpDetAna::AddTrack(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichtrk)
{
    auto &newitrk = to.n_reco_tracks;
    // copy over this track info
    // scalar
    to.trk_mcpdg_best   [newitrk]  = from.trk_mcpdg_best  [whichtrk];
    to.trk_mcMuIdx_best [newitrk]  = from.trk_mcMuIdx_best[whichtrk];
    to.trk_startTpc     [newitrk]  = from.trk_startTpc    [whichtrk];
    to.trk_endTpc       [newitrk]  = from.trk_endTpc      [whichtrk];
    to.trk_length       [newitrk]  = from.trk_length      [whichtrk];
    to.trk_hasT0        [newitrk]  = from.trk_hasT0       [whichtrk];
    to.trk_T0           [newitrk]  = from.trk_T0          [whichtrk];
    to.trk_nplanes      [newitrk]  = from.trk_nplanes     [whichtrk];
    to.trk_nsps         [newitrk]  = from.trk_nsps        [whichtrk];
    to.trk_bestPlane    [newitrk]  = from.trk_bestPlane   [whichtrk];
    //to.trk_stitched     [newitrk]  = from.trk_bestPlane   [whichtrk];//FIXME: stitching referencing gets broken when filtering tracks!
    // arrays
    std::memcpy( & to.trk_mcpdg        [newitrk], & from.trk_mcpdg       [whichtrk], (char*) & from.trk_mcpdg       [whichtrk+1] - (char*) & from.trk_mcpdg       [whichtrk] );
    std::memcpy( & to.trk_g4id         [newitrk], & from.trk_g4id        [whichtrk], (char*) & from.trk_g4id        [whichtrk+1] - (char*) & from.trk_g4id        [whichtrk] );
    std::memcpy( & to.trk_mcE          [newitrk], & from.trk_mcE         [whichtrk], (char*) & from.trk_mcE         [whichtrk+1] - (char*) & from.trk_mcE         [whichtrk] );
    std::memcpy( & to.trk_mcMuIdx      [newitrk], & from.trk_mcMuIdx     [whichtrk], (char*) & from.trk_mcMuIdx     [whichtrk+1] - (char*) & from.trk_mcMuIdx     [whichtrk] );
    std::memcpy( & to.trk_startPoint   [newitrk], & from.trk_startPoint  [whichtrk], (char*) & from.trk_startPoint  [whichtrk+1] - (char*) & from.trk_startPoint  [whichtrk] );
    std::memcpy( & to.trk_endPoint     [newitrk], & from.trk_endPoint    [whichtrk], (char*) & from.trk_endPoint    [whichtrk+1] - (char*) & from.trk_endPoint    [whichtrk] );
    std::memcpy( & to.trk_total_charge [newitrk], & from.trk_total_charge[whichtrk], (char*) & from.trk_total_charge[whichtrk+1] - (char*) & from.trk_total_charge[whichtrk] );
    std::memcpy( & to.trk_ncalopts     [newitrk], & from.trk_ncalopts    [whichtrk], (char*) & from.trk_ncalopts    [whichtrk+1] - (char*) & from.trk_ncalopts    [whichtrk] );
    std::memcpy( & to.trk_end_ncalo    [newitrk], & from.trk_end_ncalo   [whichtrk], (char*) & from.trk_end_ncalo   [whichtrk+1] - (char*) & from.trk_end_ncalo   [whichtrk] );
    std::memcpy( & to.trk_end_dedx     [newitrk], & from.trk_end_dedx    [whichtrk], (char*) & from.trk_end_dedx    [whichtrk+1] - (char*) & from.trk_end_dedx    [whichtrk] );
    std::memcpy( & to.trk_end_range    [newitrk], & from.trk_end_range   [whichtrk], (char*) & from.trk_end_range   [whichtrk+1] - (char*) & from.trk_end_range   [whichtrk] );
    std::memcpy( & to.trk_start_ncalo  [newitrk], & from.trk_start_ncalo [whichtrk], (char*) & from.trk_start_ncalo [whichtrk+1] - (char*) & from.trk_start_ncalo [whichtrk] );
    std::memcpy( & to.trk_start_dedx   [newitrk], & from.trk_start_dedx  [whichtrk], (char*) & from.trk_start_dedx  [whichtrk+1] - (char*) & from.trk_start_dedx  [whichtrk] );
    std::memcpy( & to.trk_start_range  [newitrk], & from.trk_start_range [whichtrk], (char*) & from.trk_start_range [whichtrk+1] - (char*) & from.trk_start_range [whichtrk] );
    std::memcpy( & to.trk_sp           [newitrk], & from.trk_sp          [whichtrk], (char*) & from.trk_sp          [whichtrk+1] - (char*) & from.trk_sp          [whichtrk] );

    ++newitrk;
}

void OpDetAna::AddFlashWithHits(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichflash)
{
    auto &newiflash = to.n_flashes;
    // copy over this flash info
    // scalar
    to.flash_time	[newiflash] = from.flash_time [whichflash];
    to.flash_time_width [newiflash] = from.flash_time_width [whichflash];
    to.flash_abs_time	[newiflash] = from.flash_abs_time [whichflash];
    to.flash_frame	[newiflash] = from.flash_frame [whichflash];
    to.flash_hasx	[newiflash] = from.flash_hasx [whichflash];
    to.flash_x		[newiflash] = from.flash_x [whichflash];
    to.flash_y		[newiflash] = from.flash_y [whichflash];
    to.flash_z		[newiflash] = from.flash_z [whichflash];
    to.flash_x_width	[newiflash] = from.flash_x_width [whichflash];
    to.flash_y_width	[newiflash] = from.flash_y_width [whichflash];
    to.flash_z_width	[newiflash] = from.flash_z_width [whichflash];
    to.flash_tot_pe	[newiflash] = from.flash_tot_pe [whichflash];
    to.flash_mcpdg	[newiflash] = from.flash_mcpdg [whichflash];
    to.flash_g4id	[newiflash] = from.flash_g4id [whichflash];
    to.flash_mcPE	[newiflash] = from.flash_mcPE [whichflash];
    to.flash_mcMuIdx	[newiflash] = from.flash_mcMuIdx [whichflash];
    to.flash_nhits	[newiflash] = from.flash_nhits [whichflash];


    // deal with associated hits; expecting only maximum one flash associated to a hit
    unsigned &nhits = to.flash_nhits[newiflash];
    auto &from_flash_hits = from.flash_hit_idx[whichflash];
    auto &to_flash_hits = to.flash_hit_idx[newiflash];
    for (unsigned jhit = 0; jhit < nhits; ++jhit) {
	AddHit(from, to, from_flash_hits[jhit]);
	to_flash_hits[jhit] = to.n_hits-1; // reference the just added hit
    }
    ++newiflash;
}

void OpDetAna::AddHit(CosmicEventTree::CosmicEventTree_t& from, CosmicEventTree::CosmicEventTree_t& to, int whichhit)
{
    auto &newihit = to.n_hits;

    // copy over this flash info
    // scalar
    to.hit_channel		[newihit] = from.hit_channel [whichhit];
    to.hit_has_start_time	[newihit] = from.hit_has_start_time [whichhit];
    to.hit_start_time		[newihit] = from.hit_start_time [whichhit];
    to.hit_time			[newihit] = from.hit_time [whichhit];
    to.hit_abs_time		[newihit] = from.hit_abs_time [whichhit];
    to.hit_width		[newihit] = from.hit_width [whichhit];
    to.hit_pe			[newihit] = from.hit_pe [whichhit];
    to.hit_wfmIdx		[newihit] = from.hit_wfmIdx [whichhit];

    ++newihit;
}



bool OpDetAna::inside(const TVector3& pos, const Box& b)
{
    double bmin[3] = {};
    double bmax[3] = {};

    // sort bounds in order in all 3 directions
    for (unsigned short i = 0; i < 3; ++i) {
        bmin[i] = b.corner1[i];
        bmax[i] = b.corner2[i];
        if (b.corner1[i] > b.corner2[i]) {
            // swap bounds
            bmin[i] = b.corner2[i];
            bmax[i] = b.corner1[i];
        }
    }

    // check if this point is inside active volume of the TPC
    for (unsigned short i = 0; i < 3; ++i) {
        if (pos[i] < bmin[i] || pos[i] > bmax[i])
            return false;
    }

    return true;
}

bool OpDetAna::insideTPC(const TVector3& pos) const
{
    return inside( pos, fActiveBox );
}

void OpDetAna::GetTPClimits()
{
    auto mfi = mf::LogInfo("OpDetAna::GetTPClimits()");

    // Don't remember where I got this definition of the active region.
    mfi << "----> HERE!!!!: N TPCs: " << fGeometryService->NTPC() << "\n";
    mfi << fGeometryService->DetectorName() << "\n";

    ActiveBounds[0] = ActiveBounds[2] = ActiveBounds[4] = FLT_MAX;
    ActiveBounds[1] = ActiveBounds[3] = ActiveBounds[5] = -FLT_MAX;
    // assume single cryostats
    auto const* geom = fGeometryService;
    for (geo::TPCGeo const& TPC: geom->Iterate<geo::TPCGeo>()) {
        if(TPC.DriftDistance() < 25.0) continue; // proper TPC only
        auto bbox = TPC.ActiveBoundingBox();
        double min[3], max[3];
        bbox.Min().GetCoordinates(min);
        bbox.Max().GetCoordinates(max);
        for (int i = 0; i < 3; ++i) {
            if (ActiveBounds[2*i + 0] > min[i]) ActiveBounds[2*i + 0] = min[i];
            if (ActiveBounds[2*i + 1] < max[i]) ActiveBounds[2*i + 1] = max[i];
        }
    } // for all TPC
    mfi << "Active Boundaries: "
        << "\n\tx: " << std::setw(10) << ActiveBounds[0] << " to " << ActiveBounds[1]
        << "\n\ty: " << std::setw(10) << ActiveBounds[2] << " to " << ActiveBounds[3]
        << "\n\tz: " << std::setw(10) << ActiveBounds[4] << " to " << ActiveBounds[5]
        << "\n";
}

bool OpDetAna::IsHitWithinWaveform(float hit_time, const raw::OpDetWaveform & wfm, detinfo::DetectorClocksData const& clocksData)
{
    // hit_time should be absolute, including the timestamp
    double tmstart = wfm.TimeStamp();
    auto wfm_len = wfm.Waveform().size();

    double tmend = tmstart + clocksData.OpticalClock().TickPeriod()*wfm_len;
    if (!fIsMC) {// correct waveform time to be relative to the trigger time, same as stored ophit time
        tmstart -= fExtTrigTime;
        tmend -= fExtTrigTime;
    }
    if (hit_time < tmstart || hit_time > tmend)
	return false;
    return true;
}



TLorentzVector OpDetAna::Line::operator[](int i) const
{
    if (i == 0) return start;
    else if (i == 1) return end;
    return TLorentzVector();
}


//FIXME: this might not work
bool OpDetAna::Line::intersects(const Line& l, const Box& b) const
{
    TLorentzVector entry, exit;
    if ( !l.GetEntryPoint(b, entry) &&
         !l.GetExitPoint(b, exit) )
        return false;

    return true;
}

bool OpDetAna::Line::GetEntryPoint(const Line& l, const Box& b, TLorentzVector& point)
{
    // if the line starts inside, return the start of the line
    if ( inside(l.start.Vect(), b) ) {
        point = l.start;
        return true;
    }

    // prepare bounding box dimensions
    double top[3] = {};
    double bot[3] = {};
    for (int i = 0; i < 3; i++) {
        top[i] = std::max(b[0][i], b[1][i]);
        bot[i] = std::min(b[0][i], b[1][i]);
    }


    // check in each dimension, if line segment crosses one of the perpendicular faces
    for (unsigned short i = 0; i < 3; ++i) {
        double ref = -999.;
        if (l.start[i] > top[i] && l.end[i] < top[i]) // line crosses top face's plane
            ref = top[i];
        else if (l.start[i] < bot[i] && l.end[i] > bot[i] ) // line crosses top face's plane
            ref = bot[i];
        else // segment does not cross these faces' plane, check the other dimensions
            continue;

        float t = (ref - l.end[i])/(l.start[i] - l.end[i]);
        TLorentzVector result;
        bool bounded = true;
        // get bounding box in this plane
        for (unsigned short j = 1; j < 3; ++j) {
            unsigned short coord = (i+j)%3;
            double x = t*(l.start[coord] - l.end[coord]) + l.end[coord];
            if ( x > top[coord] || x < bot[coord] ) { // outside the bounds
                bounded = false;
                break;
            }
            result[coord] = x;
        }
        if (!bounded) continue; // does not intersect any face in i-thdirection

        result[i] = ref; // top or bottom position
        result[3] = l.end[3] + t*(l.start[3] - l.end[3]); // temporal coordinate

        point = result;
        return true;
    }

    return false;
}

// Copied over from dune::AnalysisTree
void OpDetAna::HitsPurity(detinfo::DetectorClocksData const& clockData,
                          std::vector< art::Ptr<recob::Hit> > const& hits, // collection of hits in track (single plane?))
                          Int_t& trackid, // G4 track ID
                          Float_t& purity, // Purity of hits (fraction of hits/energy? from a particle in the hits)
                          Double_t& maxe // Energy of true particle with max energy in this hit collection
                          )
{
    trackid = -1;
    purity = -1;

    art::ServiceHandle<cheat::BackTrackerService> bt_serv;

    // maps deposited energy (double) to G4 track id of responsible particle in this hit collection
    std::map<int,double> trkide;

    for(size_t h = 0; h < hits.size(); ++h){
        art::Ptr<recob::Hit> hit = hits[h];
        //std::vector<sim::IDE> ides;
        //bt_serv->HitToSimIDEs(hit,ides);
        // Returns SimIDE that likely contributed to this hit. Not sure, which
        // method is preferred, HitToSimIDEs or HitToEveTrackIDEs
        //
        // Eve ID is TrackID of particle which is the "ultimate mother" of the
        // actual particle behind the deposition. In the standard algorithm
        // (EveIdCalculator), this would be the primary ancestor.
        //std::vector<sim::TrackIDE> eveIDs = bt_serv->HitToEveTrackIDEs(clockData, hit);
        std::vector<sim::TrackIDE> trkIDs = bt_serv->HitToTrackIDEs(clockData, hit);

        // for each SimIDE, add the energy to the corresponding G4 track id. Sum is made over all hits in this collection.
        for(size_t e = 0; e < trkIDs.size(); ++e){
            //std::cout<<h<<" "<<e<<" "<<eveIDs[e].trackID<<" "<<eveIDs[e].energy<<" "<<eveIDs[e].energyFrac<<std::endl;
            trkide[trkIDs[e].trackID] += trkIDs[e].energy;
        }
    }

    maxe = -1;
    double tote = 0;
    for (auto ii = trkide.begin(); ii!=trkide.end(); ++ii){
        tote += ii->second;
        if ((ii->second)>maxe){
            maxe = ii->second;
            trackid = ii->first;
        }
    }

    //std::cout << "the total energy of this reco track is: " << tote << std::endl;
    if (tote>0){
        purity = maxe/tote;
    }
}

// Based off OpDetAna::HitsPurity
void OpDetAna::OpHitsPurity(detinfo::DetectorClocksData const& clockData,
                            std::vector< art::Ptr<recob::OpHit> > const& hits, // collection of hits in a flash
                            Int_t& trackid, // G4 track ID responsible for the optical flash
                            Float_t& purity, // Purity of hits (fraction of hits/energy? from a particle in the hits)
                            Double_t& maxpe // PEs of true particle with max contribution to this hit collection
                            )
{
    trackid = -1;
    purity = -1;
    maxpe = -1;

    art::ServiceHandle<cheat::PhotonBackTrackerService> bt_serv;
    // Get all SimSDPs for the collection of the hits
    std::vector<std::vector<const sim::SDP*>> hitsdps;
    for (auto const &hit: hits)
	hitsdps.push_back(bt_serv->provider()->OpHitToSimSDPs_Ps(hit));


    /// First get total number of photons per track contributing to this collection

    // maps PEs (double) to G4 track id of responsible particle in this hit collection
    std::map<int,double> trkidpe;

    // loop over hits in this collection
    for (auto &sdps: hitsdps) {
	for(const auto & sdp: sdps) {
	    // for each SDP, add the number of PEs to the corresponding G4 track id. Sum is made over all hits in this collection.
	    auto & trkID = sdp->trackID;
	    auto eveTrkID = fPartInv->ParticleList().EveId(trkID);
	    trkidpe[eveTrkID] += sdp->numPhotons;
	}
    }

    // Get total number of photons contributing to this flash/hit collection
    double totpe = 0;
    for (auto const&ii: trkidpe){
        totpe += ii.second;
        if ((ii.second)>maxpe){
            maxpe = ii.second;
            trackid = ii.first;
        }
    }

    // //std::cout << "the total energy of this reco track is: " << tote << std::endl;
    if (totpe>0){
        purity = maxpe/totpe;
    }
}

void OpDetAna::GetMichelEnergy(const simb::MCParticle& mu,
                                art::Handle< std::vector<simb::MCParticle> >& ph,
                                float &en, float &dt)
{
    // look over muon's daughters, find Michel electron (positron), start from last daughter
    for (int i = mu.NumberDaughters()-1; i >= 0; --i) {
        const auto& d = *particleinventory->TrackIdToParticle_P(mu.Daughter(i));
        if (abs(d.PdgCode()) == 11 && d.Process() == "Decay") {
             en = d.E();
             dt = (d.T() - mu.T())*1e-3;
             return;
        }
    }
}

bool OpDetAna::MuMinusHasMichel(const simb::MCParticle& mu,
                                art::Handle< std::vector<simb::MCParticle> >& ph,
                                float &en, float &dt)
/// Assumes that M.e. and the two neutrinos are the last stored daughters of decaying mu-
{
    // look over muon's daughters, find Michel electron, if there are neutrinos, start from last daughter
    int i = mu.NumberDaughters()-1;
    auto d = particleinventory->TrackIdToParticle_P(mu.Daughter(i));
    if (d->PdgCode() != 14) // last should be nu_mu
        return false;

    ++i; d = particleinventory->TrackIdToParticle_P(mu.Daughter(i));
    if (d->PdgCode() != -12) // next should be nu_e_bar
        return false;

    ++i; d = particleinventory->TrackIdToParticle_P(mu.Daughter(i));
    if (d->PdgCode() != 11) // this should be the electron
        return false;

    en = d->E();
    dt = (d->T() - mu.T())*1e-3;
    return true;
}

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec)
{
    outs<<"("<<vec.X()<<", "
        <<vec.Y()<<", "
        <<vec.Z()<<", "
        <<vec.T()<<")";
    return outs;
}

std::ostream& operator<<(std::ostream& outs, const TVector3& vec)
{
    outs<<"("<<vec.X()<<", "
        <<vec.Y()<<", "
        <<vec.Z()<<")";
    return outs;
}

std::ostream& operator<<(std::ostream& outs, const std::vector<bool>& vec)
{
    outs<<"[";
    bool first = true;
    for (auto item: vec) {
        if (!first)
            outs<<",";
        else
            first = false;
        outs<<(item?"True":"False");
    }
    outs<<"]";
    return outs;
}

std::ostream& operator<<(std::ostream& out, const NameVector& vec)
{
    out << "[";
    bool first = true;
    for (auto s: vec) {
        if (!first)
            out<<",";
        else
            first = false;
        out<<s.c_str();
    }
    out<<"]";

    return out;
}
std::ostringstream& operator<<(std::ostringstream& out, const NameVector& vec)
{
    (std::ostream&)out << vec;
    return out;
}


DEFINE_ART_MODULE(OpDetAna)
