#ifndef TAGAPAINCROSSERS_H
#define TAGAPAINCROSSERS_H

namespace CosmicEventTree {
    class CosmicEventTree_t;
}

unsigned TagAPAInCrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC);


#endif
