
#ifndef FOR
#define FOR(i, size) for (int i = 0; i < size; ++i)
#endif

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"


#include "TagAPAOutCrossers.h"

#include "CosmicEventTree.h"
#include "tools.h"

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec);
std::ostream& operator<<(std::ostream& outs, const TVector3& vec);

//FIXME: get this number from the analyzer
const float TDC_OFFSET = 500.; // where the time 0 starts in terms of TDC ticks

// nominal at 87 K and normal pressure: 0.1648;


unsigned TagAPAOutCrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC)
{
    unsigned count = 0;

    for(unsigned itrk = 0; itrk < evt->n_reco_tracks; ++itrk) {
        // ignore T0'd tracks
        if (evt->trk_hasT0[itrk])
            continue;
        // consider only tracks >= 20 cm
        if (evt->trk_length[itrk] < 20.)
            continue;

        // Get the bottom end of the track
        TVector3 top(evt->trk_startPoint[itrk]);
        TVector3 bottom(evt->trk_endPoint[itrk]);
        int botTPC = evt->trk_endTpc[itrk];
        int botTDC = evt->trk_endPoint[itrk][3];
        if (top.Y() < bottom.Y()) { // inverted orientation
            top = bottom;
            bottom = TVector3(evt->trk_startPoint[itrk]);
            botTPC = evt->trk_startTpc[itrk];
            botTDC = evt->trk_startPoint[itrk][3];
        }

        // test, if track exits through the bottom
        if (bottom.Y() < boundary[2] + 10.) // bottom of active volume is around 7 cm
            continue;
        // test if track does not exit through the front or rear face
        if (bottom.Z() > boundary[5] - 10. || // adding an offset
            bottom.Z() < boundary[4] + 10.)
            continue;

        // test which side of CPA should this track be
        int CPAside = 0;
        if ( top.X() < bottom.X() ) {// must exit through X>0 APA
            CPAside = 1;
        } else { // must exit through X<0 APA
            CPAside = -1;
        }

        // test if the bottom part was really happening in the correct TPC
        if (CPAside < 0 && botTPC%4 != 1)
            continue;
        if (CPAside > 0 && botTPC%4 != 2)
            continue;


        // test if this track could be trimmed by the readout window
        if (botTDC < 100 || botTDC > 5900)
            continue;


        // If here, this track should be T0'd
        // though it may still be a stopping muon! Not sure if there's a way to test if this was a stopping muon (dE/dx? Decay pruducts?)
        float T0 = (botTDC - TDC_OFFSET) * 0.5; // in us // time 0 is at 500 ticks (out of 6000)

        // calculate shift in reco X, adding positive X n the X>0 CPA side and subtracting X on the X<0 side.
        if (correctX) {
            float x0 = (2*(botTPC%2)-1)*driftVelocity*T0;
            evt->trk_startPoint[itrk][0] -= x0;
            evt->trk_endPoint[itrk][0] -= x0;
        }

        evt->trk_hasT0[itrk] = TagID; // indicate, this is a new tag (1 - original T0, 2 - new T0)
        evt->trk_T0[itrk] = T0*1e3;//CPAside * x0/ * 1e3; // in [ns]

        ++count;
    }

    return count;
}
