
#ifndef FOR
#define FOR(i, size) for (int i = 0; i < size; ++i)
#endif

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"


#include "TagAPAInCrossers.h"

#include "CosmicEventTree.h"
#include "tools.h"

std::ostream& operator<<(std::ostream& outs, const TLorentzVector& vec);
std::ostream& operator<<(std::ostream& outs, const TVector3& vec);

//const float gDriftVel = 0.1561; // in cm/us //at 87.68 K and 486.7 V/cm as for ProtoDUNE
// nominal at 87 K and normal pressure: 0.1648;


unsigned TagAPAInCrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC)
{
    unsigned count = 0;
	// loop over reco tracks
    for(unsigned itrk = 0; itrk < evt->n_reco_tracks; ++itrk) {
        // ignore T0'd tracks
        if (evt->trk_hasT0[itrk])
            continue;
        // consider only tracks >= 20 cm
        if (evt->trk_length[itrk] < 20.)
            continue;

        // Get the top end of the track
        TVector3 top(evt->trk_startPoint[itrk]);
        TVector3 bottom(evt->trk_endPoint[itrk]);
        int topTPC = evt->trk_startTpc[itrk];
        int topTDC = evt->trk_startPoint[itrk][3];
        if (top.Y() < bottom.Y()) { // inverted orientation
            top = bottom;
            bottom = TVector3(evt->trk_startPoint[itrk]);
            topTPC = evt->trk_endTpc[itrk];
            topTDC = evt->trk_endPoint[itrk][3];
        }

        // test, if track starts below ceiling
        if (top.Y() > 575) // top of active volume is around 607 cm
            continue;
        // test if track does not enter from the front or rear face
        if (top.Z() > boundary[5] - 10. || // adding an offset
            top.Z() < boundary[4] + 10.)
            continue;

        // test which side of CPA should this track be
        int CPAside = 0;
        if ( top.X() < bottom.X() ) {// negative side of CPA
            CPAside = -1;
        } else {
            CPAside = 1;
        }

        // test if the top part was really happening in the correct TPC
        if (CPAside < 0 && topTPC%4 != 1)
            continue;
        if (CPAside > 0 && topTPC%4 != 2)
            continue;


        // test if this track could be trimmed by the readout window
        if (topTDC < 100 || topTDC > 5900)
            continue;

        // - later times
        // if ( CPAside < 0 && top.X() > 71.)
        // 	continue;
        // if ( CPAside > 0 && top.X() < -71. )
        // 	continue;
        // // - earlier times
        // if ( abs(top.X()) > 395. )
        // 	continue;



        // If here, this track should be T0'd
        float T0 = (topTDC - 500) * 0.5; // in us

        if (correctX) {
            float x0 = (2*(topTPC%2)-1)*driftVelocity*T0; // time 0 is at 500 ticks (out of 6000) //CPAside * boundary[0][1] - top.X();
            evt->trk_startPoint[itrk][0] -= x0;
            evt->trk_endPoint[itrk][0] -= x0;
        }

        evt->trk_hasT0[itrk] = TagID; // indicate, this is a new tag (1 - original T0, 2 - new T0)
        evt->trk_T0[itrk] = T0*1e3;//CPAside * x0/ * 1e3; // in [ns]

        ++count;
    }

    return count;
}
