#ifndef TAGCPACROSSERS_H
#define TAGCPACROSSERS_H

namespace CosmicEventTree {
    class CosmicEventTree_t;
}

unsigned TagCPAcrossers(const float* boundary, CosmicEventTree::CosmicEventTree_t* evt, int TagID, float driftVelocity, bool correctX, bool isMC);

#endif
