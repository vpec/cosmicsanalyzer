// -*- mode: c++; c-basic-offset: 2; -*-
////////////////////////////////////////////////////////////////////////
// Class:       PrintOutOpDetChannelMap
// Plugin Type: analyzer (Unknown Unknown)
// File:        PrintOutOpDetChannelMap_module.cc
//
// Description: simple analyzer to print out a map of channels to
// central position of an optical detector.
//
// Created May 11 2023 by Viktor Pec.
////////////////////////////////////////////////////////////////////////

// c++ libraries
#include <fstream>

// Framework headers
#include "art/Framework/Core/EDAnalyzer.h"
#include "art/Framework/Core/ModuleMacros.h"
#include "art/Framework/Principal/Event.h"
#include "art/Framework/Principal/Handle.h"
#include "art/Framework/Principal/Run.h"
#include "art/Framework/Principal/SubRun.h"
#include "canvas/Utilities/InputTag.h"
#include "canvas/Persistency/Common/FindManyP.h"
#include "canvas/Persistency/Common/FindOneP.h"
#include "fhiclcpp/ParameterSet.h"
#include "messagefacility/MessageLogger/MessageLogger.h"
#include "art_root_io/TFileService.h"

// useful services headers
#include "lardata/DetectorInfoServices/DetectorClocksService.h"
#include "larcore/Geometry/Geometry.h"
#include "larcore/Geometry/WireReadout.h"
#include "larcorealg/Geometry/WireReadoutGeom.h"

// Sim info access related headers
#include "larsim/MCCheater/BackTrackerService.h"
#include "larsim/MCCheater/PhotonBackTrackerService.h"
#include "larsim/MCCheater/ParticleInventoryService.h"
#include "nusimdata/SimulationBase/MCParticle.h"

// TPC Reco
#include "lardataobj/RecoBase/Track.h"
#include "lardataobj/RecoBase/PFParticle.h"
#include "lardataobj/AnalysisBase/Calorimetry.h"
#include "lardataobj/AnalysisBase/T0.h"


// OpDet info access
#include "lardataobj/RecoBase/OpFlash.h"
#include "lardataobj/RecoBase/OpFlash.h"
#include "lardataobj/RecoBase/OpHit.h"
#include "lardataobj/RawData/OpDetWaveform.h"

// ROOT headers
#include "TLorentzVector.h"

#include "CosmicEventTree.h"

class PrintOutOpDetChannelMap;

std::ostream& operator<<(std::ostream&, const TLorentzVector&);

class PrintOutOpDetChannelMap : public art::EDAnalyzer {
private:

public:
  explicit PrintOutOpDetChannelMap(fhicl::ParameterSet const& p);
  // The compiler-generated destructor is fine for non-base
  // classes without bare pointers or other resource use.

  // Plugins should not be copied or assigned.
  PrintOutOpDetChannelMap(PrintOutOpDetChannelMap const&) = delete;
  PrintOutOpDetChannelMap(PrintOutOpDetChannelMap&&) = delete;
  PrintOutOpDetChannelMap& operator=(PrintOutOpDetChannelMap const&) = delete;
  PrintOutOpDetChannelMap& operator=(PrintOutOpDetChannelMap&&) = delete;

  // Required functions.
  void analyze(art::Event const& e) override;

  // Selected optional functions.
  void beginJob() override;
  void endJob() override;
  void beginRun(art::Run const& r) override;

private:
  float displace[288];
};


PrintOutOpDetChannelMap::PrintOutOpDetChannelMap(fhicl::ParameterSet const& pset)
  : EDAnalyzer{pset}
    // More initializers here.
{
}

void PrintOutOpDetChannelMap::beginJob()
{
  auto mfi = mf::LogInfo("PrintOutOpDetChannelMap::beginRun");
  mfi << "Starting geometry retrieval.\n";

  geo::WireReadoutGeom const * geom = &art::ServiceHandle<geo::WireReadout const>()->Get();

  const int nchannels = 290;
  mfi << "Looking for " << nchannels << " channels.\n";

  std::ofstream fout("pdsp_opdettoxyz_channel_map.txt");
  for (int i = 0; i < nchannels; ++i) {
    if ( !geom->IsValidOpChannel(i) ) // skip this channel as it is uninstrumented
      continue;
    auto const opdet = geom->OpDetGeoFromOpChannel(i);
    mfi << i << ": " << opdet.OpDetInfo() << " ";
    auto const xyz = opdet.GetCenter();
    mfi << xyz << "\n";
    fout<<i<<" "<<xyz.X()<<" "<<xyz.Y()<<" "<<xyz.Z()<<std::endl;
  }
  fout.close();
}


void PrintOutOpDetChannelMap::endJob()
{}
void PrintOutOpDetChannelMap::beginRun(art::Run const& r)
{}

void PrintOutOpDetChannelMap::analyze(art::Event const& evt)
{}


DEFINE_ART_MODULE(PrintOutOpDetChannelMap)
