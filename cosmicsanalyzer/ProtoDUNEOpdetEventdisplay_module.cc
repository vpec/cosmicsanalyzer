// -*- mode: c++; c-basic-offset: 2; -*-
// This analyzer writes out a folder containing the
// PDS event display. For every OpFLsh, the OpHits are
// plotted.
//
// 2023 April 27: VP (viktor.pec@fzu.cz)
//
// Modifying original
// duneopdet/OpticalDetector/ProtoDUNE_opdet_eventdisplay_module.cc
// module to split output histograms per flash. The original
// ProtoDUNEopdeteventdisplay module superimposed all flashes onto a
// single histogram. The ProtoDUNE_opdet_eventdisplay module was not
// conforming with art module naming conventions and it never created
// a new histogram for each flash, causing ROOT runtime error.



#ifndef ProtoDUNE_opdet_eventdisplay_H
#define ProtoDUNE_opdet_eventdisplay_H

// ROOT includes
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TTree.h"
#include "TLine.h"
#include "TLegend.h"

// C++ includes
#include <map>
#include <vector>
#include <iostream>
#include <cstring>
#include <sstream>
#include "math.h"
#include <climits>
#include <functional>
#include <algorithm>
#include <map>


// LArSoft includes
#include "larcore/Geometry/Geometry.h"
#include "larcorealg/Geometry/GeometryCore.h"
#include "larcorealg/Geometry/OpDetGeo.h"
#include "larcore/Geometry/WireReadout.h"
#include "larcorealg/Geometry/WireReadoutGeom.h"
#include "lardataobj/RawData/OpDetPulse.h"
#include "lardataobj/RecoBase/OpFlash.h"
#include "lardataobj/RecoBase/OpHit.h"
#include "lardata/DetectorInfoServices/DetectorClocksService.h"

#include "lardataalg/DetectorInfo/DetectorClocks.h"
//#include "OpticalDetector/OpDigiProperties.h"

//#include "larana/OpticalDetector/OpFlashAlg.h"
#include <numeric>

// ART includes.
#include "art/Framework/Core/EDAnalyzer.h"
#include "art/Framework/Principal/Event.h"
#include "fhiclcpp/ParameterSet.h"
#include "art/Framework/Principal/Handle.h"
#include "canvas/Persistency/Common/Ptr.h"
#include "canvas/Persistency/Common/PtrVector.h"
#include "art/Framework/Services/Registry/ServiceHandle.h"
#include "art_root_io/TFileService.h"
#include "art_root_io/TFileDirectory.h"
#include "messagefacility/MessageLogger/MessageLogger.h"
#include "art/Framework/Core/ModuleMacros.h"
#include "canvas/Persistency/Common/FindManyP.h"

namespace opdet {

  class ProtoDUNEOpdetEventdisplay : public art::EDAnalyzer{
  public:

    // Standard constructor and destructor for an ART module.
    ProtoDUNEOpdetEventdisplay(const fhicl::ParameterSet&);
    virtual ~ProtoDUNEOpdetEventdisplay();

    // This method is called once, at the start of the job. In this
    // example, it will define the histogram we'll write.
    void beginJob();

    // The analyzer routine, called once per event.
    void analyze (const art::Event&);

  private:

    // The stuff below is the part you'll most likely have to change to
    // go from this custom example to your own task.

    // The parameters we'll read from the .fcl file.
    std::string fOpFlashModuleLabel;       // Input tag for OpFlash collection
    std::string fOpHitModuleLabel;         // Input tag for OpHit collection
    float fSampleFreq;                     // in MHz
    float fTimeBegin;                      // in us
    float fTimeEnd;                        // in us

    float fYMin, fYMax, fZMin, fZMax;

    int PosHistYRes, PosHistZRes;

    //    bool fMakeFlashTimeHist;
    //    bool fMakeFlashPosHist;
    //    bool fMakePerFlashHists;
    //    bool fMakePerOpHitTree;

    //    TTree * fPerOpHitTree;

    Int_t   fEventID;
    Int_t   fFlashID;
    Int_t   fHitID;
    Float_t fFlashTime;
    //    Float_t fAbsTime;
    //    bool    fInBeamFrame;
    //    int     fOnBeamTime;
    //    Float_t fTotalPE;
    //    Int_t   fFlashFrame;

    //    Float_t fNPe;
    //    Float_t fYCenter;
    //    Float_t fYWidth;
    //    Float_t fZCenter;
    //    Float_t fZWidth;

    Int_t   fOpChannel;
    Float_t fPeakTimeAbs;
    Float_t fPeakTime;
    //    Int_t   fFrame;
    //    Float_t fWidth;
    //    Float_t fArea;
    //    Float_t fAmplitude;
    Float_t fPE;
    //    Float_t fFastToTotal;
    Float_t fYOpCenter;
    Float_t fZOpCenter;
    Float_t fXOpCenter;

    //    int fNFlashes;
    std::vector< int >   fFlashIDVector;
    std::vector< float > fYCenterVector;
    std::vector< float > fZCenterVector;
    std::vector< float > fYWidthVector;
    std::vector< float > fZWidthVector;
    std::vector< float > fFlashTimeVector;
    std::vector< float > fAbsTimeVector;
    std::vector< int >   fFlashFrameVector;
    std::vector< bool >  fInBeamFrameVector;
    std::vector< int >   fOnBeamTimeVector;
    std::vector< float > fTotalPEVector;
    //    int fNChannels;
    std::vector< float > fPEsPerFlashPerChannelVector;
  };

}

#endif // ProtoDUNEOpdetEventdisplay_H

namespace opdet {

  //-----------------------------------------------------------------------
  // Constructor
  ProtoDUNEOpdetEventdisplay::ProtoDUNEOpdetEventdisplay(fhicl::ParameterSet const& pset)
    : EDAnalyzer(pset)
  {

    // Indicate that the Input Module comes from .fcl
    fOpFlashModuleLabel = pset.get<std::string>("OpFlashModuleLabel");
    fOpHitModuleLabel   = pset.get<std::string>("OpHitModuleLabel");

    //    art::ServiceHandle<OpDigiProperties> odp;
    //    fTimeBegin  = odp->TimeBegin();
    //    fTimeEnd    = odp->TimeEnd();
    //    fSampleFreq = odp->SampleFreq();

    auto const clockData = art::ServiceHandle<detinfo::DetectorClocksService const>()->DataForJob();
    fTimeBegin  = clockData.OpticalClock().Time();
    fTimeEnd    = clockData.OpticalClock().FramePeriod();
    fSampleFreq = clockData.OpticalClock().Frequency();

    fYMin = pset.get<float>("YMin");
    fYMax = pset.get<float>("YMax");
    fZMin = pset.get<float>("ZMin");
    fZMax = pset.get<float>("ZMax");

    PosHistYRes = 20;
    PosHistZRes = 42;

    art::ServiceHandle< art::TFileService > tfs;

    fFlashID = 0;
  }

  //-----------------------------------------------------------------------
  // Destructor
  ProtoDUNEOpdetEventdisplay::~ProtoDUNEOpdetEventdisplay()
  {}

  //-----------------------------------------------------------------------
  void ProtoDUNEOpdetEventdisplay::beginJob()
  {}

  //-----------------------------------------------------------------------
  void ProtoDUNEOpdetEventdisplay::analyze(const art::Event& evt)
  {

    // float displace[288]={0};

    // //for(int j=0;j<287;j++) displace[j]=j;
    // for(int j=0;j<284;j+=4){
    //   if((j>=132 && j<=143) || (j>=264 && j<=275)){
    // 	// FIXME: this does not correspond current channel map. Arapuca has 12 different geo volumes in each bar. 8 x 10cm + 4 x 20 cm.
    // 	// displace[j]=-100;
    // 	// displace[j+1]=-75;
    // 	// displace[j+2]=-60;
    // 	// displace[j+3]=-40;
    // 	// displace[j+4]=-20;
    // 	// displace[j+5]=-5;
    // 	// displace[j+6]=5;
    // 	// displace[j+7]=20;
    // 	// displace[j+8]=40;
    // 	// displace[j+9]=60;
    // 	// displace[j+10]=75;
    // 	// displace[j+11]=100;
    // 	j+=8;
    // 	continue;
    //   }
    //   // 4 cells per bar - Double-shift Light Guide or Dip-coated LG.
    //   // Bar is ~210 cm long, 210 cm/4 = 52.5 cm, which should be the separation.
    //   // Why is there variable separation?
    //   displace[j]=-100;
    //   displace[j+1]=-4;
    //   displace[j+2]=5;
    //   displace[j+3]=60;

    // }

    // FIXME: check bin edges;
    // Float_t binsz[] = {0,10, 29, 48, 66, 84, 102, 120, 138, 156, 174, 192, 211, 230, 240, 257, 278, 296, 314, 332, 350, 368, 386, 404, 422, 441, 460, 470, 489, 508, 526, 544, 562, 580, 598, 616, 634, 650, 671, 690, 700};
    Float_t binsz[] = {
      // first APA - Arapuca on Jura side
      0, 29., 39., 49., 59., 70., 80., 90., 100., 110., 115., 135., 156., 176., 197.,233.,
      // second APA - Arapuca on Salev side
      261., 271., 281., 291., 302., 312., 322., 332., 342., 347., 367., 388., 408., 429., 466.,
      // third APA - no Arapuca
      700.
    };
    Int_t  binnumz = sizeof(binsz)/sizeof(Float_t) - 1;

    // Bar centres in Y direction (either side in X))
    const Float_t opdetcentrey[] = {
      35.492, 94.692, 153.892, 213.092, 272.292,
      331.492, 390.692, 449.892, 509.092, 568.292
    };

    const size_t sizey = sizeof(opdetcentrey)/sizeof(Float_t);
    Float_t binsy[sizey*4+2] = {0};
    Int_t  binnumy = sizey*4 + 1;
    for (size_t i = 0; i < sizey; ++i) {
      binsy[2*i+1] = opdetcentrey[i] - 10 - 600.;
      binsy[2*i+2] = opdetcentrey[i] + 10 - 600.;
      binsy[2*(i+sizey)+1] = opdetcentrey[i] - 10;
      binsy[2*(i+sizey)+2] = opdetcentrey[i] + 10;
    }
    binsy[0] = -600.;
    binsy[binnumy] = 600.;

    // debug:
    std::cout<<"Bin edges in Y direction:"<<std::endl;
    for (int i = 0; i <= binnumy; ++i)
      std::cout<<binsy[i]<<", ";
    std::cout<<std::endl;


    // Get flashes from event
     auto FlashHandle = evt.getHandle< std::vector< recob::OpFlash > >(fOpFlashModuleLabel);

    // Get assosciations between flashes and hits
    art::FindManyP< recob::OpHit > Assns(FlashHandle, evt, fOpFlashModuleLabel);

    // Create string for histogram name
    char HistName[256];

    fFlashID = 0;

    // Access ART's TFileService, which will handle creating and writing
    // histograms for us.
        art::ServiceHandle< art::TFileService > tfs;

    //std::vector<TH1D*> FlashHist;
    std::vector<TH2D*> DispHist;

    fEventID = evt.id().event();

    sprintf(HistName, "Event_%d-flash_%%d", evt.id().event());

    geo::WireReadoutGeom const * geom = &art::ServiceHandle<geo::WireReadout const>()->Get();
    //unsigned int NOpChannels = geom->NOpChannels();

    // For every OpFlash in the vector
    for(unsigned int i = 0; i < FlashHandle->size(); ++i) {
      // Get OpFlash
      art::Ptr< recob::OpFlash > TheFlashPtr(FlashHandle, i);
      recob::OpFlash TheFlash = *TheFlashPtr;

      fFlashTime = TheFlash.Time();
      fFlashID   = i; //++;

      std::cout<<"Saving flash id " <<i <<" with " << Assns.at(i).size() << " hits." << std::endl;

      TH2D * hevd = nullptr;
      hevd = tfs->make<TH2D>(Form(HistName, i),"PE from OpHits in OpFlash;z ;DaS             RaS (y)",
			     binnumz, binsz,
			     binnumy, binsy);
      hevd->SetOption("colz");

      for(auto const& hit: Assns.at(i)) {
	//size_t i=0; i!=OpHitHandle->size(); ++i)

	fOpChannel   = hit->OpChannel();
	auto const xyz = geom->OpDetGeoFromOpChannel(fOpChannel).GetCenter();
	fXOpCenter   = xyz.X();
	fYOpCenter   = xyz.Y();
	fZOpCenter   = xyz.Z();
	fPeakTimeAbs = hit->PeakTimeAbs();
	fPeakTime    = hit->PeakTime();
	fPE          = hit->PE();
	//fHitID       = i;
	//fPerOpHitTree->Fill();
	//hevd->Fill(fZOpCenter, fYOpCenter,fPE);
	float y = fYOpCenter;
	if(fXOpCenter < 0){
	  y = fYOpCenter - 600.;
	}
	if ( (fOpChannel>=132 && fOpChannel<=143) || (fOpChannel>=264 && fOpChannel<=275) )
	  // arapuca, no need to displace
	  hevd->Fill(fZOpCenter, y,fPE);
	else{ // others, need to fill the whole bar
	  int binstart, binend;
	  std::cout<<"Hit in channel "<<fOpChannel<<" which is in a full bar."
		   <<" Has " << fPE << " p.e.s" <<std::endl
		   <<"Centre of the bar is "<<xyz;
	  binstart = hevd->FindBin(fZOpCenter, y) / (binnumz+2) * (binnumz+2) + 1; // first bin ath this height
	  binend = binstart + 15; // first bin on the second APA at this height

	  std::cout<<"VisHist bin: " <<hevd->FindBin(fZOpCenter, y)<<" which is supposedly in row "
		   << hevd->FindBin(fZOpCenter, y) / (binnumz+2) << " starting with bin "<< binstart
		   <<". ";

	  if (fZOpCenter > 233. && fZOpCenter < 466.) {// second APA
	    binstart = binend;
	    binend = binstart + 15;
	  } else if (fZOpCenter > 466.) {
	    binstart = binend + 15;
	    binend = binstart + 1;
	  }

	  std::cout<<"Will fill bins "<<binstart<<" through "<<binend<<std::endl;

	  for (int ibin = binstart; ibin < binend; ++ibin) {
	    hevd->AddBinContent(ibin, fPE);
	  }
	}
      }

      TLine *line = new TLine(0,0,700,0);
      line->SetLineColor(kBlack);
      hevd->GetListOfFunctions()->Add(line);

    }


  } // analyze

} // namespace opdet

namespace opdet {
  DEFINE_ART_MODULE(ProtoDUNEOpdetEventdisplay)
}
