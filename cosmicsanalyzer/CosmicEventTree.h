#ifndef COSMICEVENTTREE_H
#define COSMICEVENTTREE_H

#include "TTree.h"

#define EVENT_LIST                              \
    UINT(run, "Run number");                    \
    UINT(subrun, "Sub-run number");             \
    UINT(event, "Event number")                 \

#define MC_TRUTH_LIST                                                   \
    /* Primary muons */                                                 \
    UINT(n_muons, "Number of primary muons");                           \
    /* Generator level */                                               \
    FLOAT_4VEC_ARR(gen_pos, "Muon gen position [cm,ns]", n_muons);      \
    FLOAT_4VEC_ARR(end_pos, "Muon end position [cm,ns]", n_muons);      \
    FLOAT_4VEC_ARR(gen_mom, "Muon gen four momentum [cm,ns]", n_muons); \
    INT_ARR(mc_pdg, "PDG id of primary muon", n_muons);                 \
                                                                        \
    /* Active volume level */                                           \
    FLOAT_4VEC_ARR(mc_startPoint_tpcAV, "Start point in TPC Active Volume [cm,ns]", n_muons); \
    FLOAT_4VEC_ARR(mc_endPoint_tpcAV, "End point in TPC Active Volume [cm,ns]", n_muons); \
    /*    STRING_ARR(mc_endProcess_tpcAV, "Process at end point in TPC Active Volume", n_muons); */ \
    /* MCParticle trajectories are mostly (all?) sparsified,         */ \
    /* leaving only starting and end trajectory points. Can't get    */ \
    /* true momentum at the entry point.                             */ \
    FLOAT_4VEC_ARR(mc_startMom_tpcAV, "Start-point 4-momentum in TPC Active Volume [GeV]", n_muons); \
    FLOAT_4VEC_ARR(mc_endMom_tpcAV, "End-point 4-momentum in TPC Active Volume [GeV]", n_muons); \
    FLOAT_ARR(mc_length_tpcAV, "Length of muon trajectory in TPC Active Volume [cm]", n_muons); \
    USINT_ARR(mc_exited_tpcAV, "Whether muon exited TPC.", n_muons);    \
    USINT_ARR(mc_hasME, "Whether muon has a Miche electron.", n_muons); \
    FLOAT_ARR(mc_ME_en, "Michel electron energy.", n_muons);            \
    FLOAT_ARR(mc_ME_dt, "Michel electron DT from muon in us.", n_muons) \

#define RECO_TRACK_LIST                                                 \
    /* Info on reconstructed tracks */                                  \
    UINT(n_reco_tracks, "Number of Reco Tracks");                       \
    INT_ARR2D(trk_mcpdg, "MC Truth PDG code per plane", n_reco_tracks, 3); \
    INT_ARR(trk_mcpdg_best, "MC Truth PDG code best match", n_reco_tracks); \
    INT_ARR2D(trk_g4id, "MC G4 ID per plane", n_reco_tracks, 3);        \
    FLOAT_ARR2D(trk_mcE, "MC deposited energy contributing to hits in the plane plane", n_reco_tracks, 3); \
    INT_ARR2D(trk_mcMuIdx, "Index in MC n_muons by plane", n_reco_tracks, 3);   \
    INT_ARR(trk_mcMuIdx_best, "Index in MC n_muons", n_reco_tracks);    \
    FLOAT_ARR2D(trk_startPoint, "Start point of reco track", n_reco_tracks, 4); \
    FLOAT_ARR2D(trk_endPoint, "End point of reco track", n_reco_tracks, 4); \
    INT_ARR(trk_startTpc, "Start point TPC", n_reco_tracks);            \
    INT_ARR(trk_endTpc, "End point TPC", n_reco_tracks);                \
    FLOAT_ARR(trk_length, "Length of reco track [cm]", n_reco_tracks);  \
    USINT_ARR(trk_hasT0, "Flag if this track has been assigned T0.", n_reco_tracks); \
    FLOAT_ARR(trk_T0, "T0 of this track [ns].", n_reco_tracks);         \
    FLOAT_ARR2D(trk_total_charge, "Total charge of reco track, per wire plane", n_reco_tracks, 3); \
    USINT_ARR2D(trk_ncalopts, "Number of calorimetry points in each plane", n_reco_tracks, 3); \
    USINT_ARR(trk_nplanes, "Number of planes that have calorimetry", n_reco_tracks); \
    /* end-of-track calorimetry (dE/dx), last 20 hits */                \
    USINT_ARR2D(trk_end_ncalo, "Number of calo points at end of reco track, per wire plane", n_reco_tracks, 3); \
    FLOAT_ARR3D(trk_end_dedx, "dE/dx at end of reco track, per wire plane", n_reco_tracks, 3, 20); \
    FLOAT_ARR3D(trk_end_range, "Res. range at end of reco track, per wire plane", n_reco_tracks, 3, 20); \
    USINT_ARR2D(trk_start_ncalo, "Number of calo points at start of reco track, per wire plane", n_reco_tracks, 3); \
    FLOAT_ARR3D(trk_start_dedx, "dE/dx at start of reco track, per wire plane", n_reco_tracks, 3, 20); \
    FLOAT_ARR3D(trk_start_range, "Res. range at start of reco track, per wire plane", n_reco_tracks, 3, 20); \
                                                                        \
    USINT_ARR(trk_nsps, "Number of spacepoints", n_reco_tracks); \
    FLOAT_ARR3D(trk_sp, "Reconstructed space points associated to the track", n_reco_tracks, 4*2048, 3); \
    USINT_ARR(trk_bestPlane, "Best plane by number of calo points", n_reco_tracks) \


/// Info on reconstructed optical hits and flashes
#define OPDET_FLASH_LIST                                                \
    /* Info on flashes */                                               \
    UINT(n_flashes, "Number of reconstructed falshes");                 \
    FLOAT_ARR(flash_time, "Time of flash in us", n_flashes);            \
    FLOAT_ARR(flash_time_width, "Time width of flash in us", n_flashes); \
    FLOAT_ARR(flash_abs_time, "Absolute time of flash in us", n_flashes); \
    SINT_ARR(flash_frame, "Frame of flash", n_flashes);                 \
    USINT_ARR(flash_hasx, "If flash has X centre", n_flashes);          \
    FLOAT_ARR(flash_x, "Weighted centre of flash in X", n_flashes);     \
    FLOAT_ARR(flash_y, "Weighted centre of flash in Y", n_flashes);     \
    FLOAT_ARR(flash_z, "Weighted centre of flash in Z", n_flashes);     \
    FLOAT_ARR(flash_x_width, "Width of flash in X", n_flashes);         \
    FLOAT_ARR(flash_y_width, "Width of flash in Y", n_flashes);         \
    FLOAT_ARR(flash_z_width, "Width of flash in Z", n_flashes);         \
    FLOAT_ARR(flash_tot_pe, "Total number of PEs in flash", n_flashes); \
    INT_ARR(flash_mcpdg, "PDG code of particle (Eve ID - primary) responsible for this flash", n_flashes); \
    INT_ARR(flash_g4id, "G4 track ID of primary responsible for the flash", n_flashes); \
    FLOAT_ARR(flash_mcPE, "Number of true PEs from the true particle in flash", n_flashes); \
    INT_ARR(flash_mcMuIdx, "Index MC n_muons", n_flashes);      \
    UINT_ARR(flash_nhits, "Number of hits in flash", n_flashes);                \
    UINT_ARR2D(flash_hit_idx, "Index of stored hit which belongs to flash", n_flashes, MAX_HITS_IN_FLASH) \

#define OPDET_HIT_LIST                                                  \
    /* Info on optical hits */                                          \
    UINT(n_hits, "Number of optical hits");                             \
    INT_ARR(hit_channel, "Channel number of hit", n_hits);              \
    USINT_ARR(hit_has_start_time, "If start time calculated", n_hits);  \
    FLOAT_ARR(hit_start_time, "Start time of hit in us", n_hits);       \
    FLOAT_ARR(hit_time, "Peak time of hit in us", n_hits);              \
    FLOAT_ARR(hit_abs_time, "Absolute peak time of hit in us", n_hits); \
    FLOAT_ARR(hit_width, "Time width of hit in us", n_hits);            \
    FLOAT_ARR(hit_pe, "Number of PEs in hit", n_hits);                  \
    INT_ARR(hit_wfmIdx, "Index of respective waveform, if stored", n_hits) \

#define CPA_STITCH_LIST                                                 \
    /* Info of CPA stitching */                                         \
    INT_ARR(trk_stitched, "Idx of track stitch across the TPC.", n_reco_tracks) \

#define TPC_PDS_MATCH_LIST                                              \
    /* Info on TPC to PDS matching */                                   \
    UINT(n_matches, "Number of TPC-PDS matches");                       \
    UINT_ARR(match_trk_idx, "Track index for matched pair", n_matches); \
    UINT_ARR(match_flash_idx, "Flash index for matched pair", n_matches); \
    INT_ARR(trk_matched, "Index in matched pairs", n_reco_tracks);      \
    INT_ARR(flash_matched, "Index in matched pairs", n_flashes)         \

#define TPC_STOPPED_LIST                                                \
    /* info from stopped muon selection */                              \
    INT_ARR(trk_stopped, "This track is a stopped muon", n_reco_tracks) \

#define OPDET_WAVEFORMS_LIST                                            \
    /* Waveforms stored for respective hits */                          \
    UINT(n_wfms, "Number of stored opdet waveforms");                   \
    FLOAT_ARR(wfm_time, "Time of trigger", n_wfms);                     \
    USINT_ARR(wfm_size, "Size of stored opdet waveforms", n_wfms);      \
    USINT_ARR(wfm_channel, "Channel of stored opdet waveforms", n_wfms);   \
    FLOAT_ARR2D(wfm, "Stored opdet waveforms", n_wfms, OPDET_WFM_SIZE); \
    UINT_ARR(wfm_hit, "Index of corresponding hit", n_wfms)             \

#define FULL_LIST                               \
        EVENT_LIST;                             \
        MC_TRUTH_LIST;                          \
        RECO_TRACK_LIST;                        \
        OPDET_FLASH_LIST;                       \
        OPDET_HIT_LIST;                         \
        CPA_STITCH_LIST;                        \
        TPC_PDS_MATCH_LIST;                     \
        TPC_STOPPED_LIST;                       \
        OPDET_WAVEFORMS_LIST                    \

namespace CosmicEventTree {

    const unsigned int MAX_RECO_TRACKS = 1024;
    const unsigned int MAX_PRIMARIES = 1024;
    const unsigned int MAX_FLASHES = 512;
    const unsigned int MAX_HITS = 16*1024;
    const unsigned int MAX_HITS_IN_FLASH = 512;
    const unsigned int OPDET_WFM_SIZE = 1024;
    const unsigned int MAX_WFMS = 4*1024;

    struct CosmicEventTree_t {
#define UINT(var, title) unsigned int var

#define INT_ARR(var, title, size) int var[MAXSIZE]
#define SINT_ARR(var, title, size)  short var[MAXSIZE]
#define USINT_ARR(var, title, size) unsigned short var[MAXSIZE]
#define UINT_ARR(var, title, size) unsigned int var[MAXSIZE]
#define FLOAT_ARR(var, title, size) float var[MAXSIZE]
#define FLOAT_4VEC_ARR(var, title, size) float var[MAXSIZE][4]

#define INT_ARR2D(var, title, size1, size2) int var[MAXSIZE][size2]
#define SINT_ARR2D(var, title, size1, size2) short int var[MAXSIZE][size2]
#define USINT_ARR2D(var, title, size1, size2) unsigned short int var[MAXSIZE][size2]
#define UINT_ARR2D(var, title, size1, size2) unsigned int var[MAXSIZE][size2]
#define FLOAT_ARR2D(var, title, size1, size2) float var[MAXSIZE][size2]
#define FLOAT_ARR3D(var, title, size1, size2, size3) float var[MAXSIZE][size2][size3]

        EVENT_LIST;

#define MAXSIZE MAX_PRIMARIES

        MC_TRUTH_LIST;

#undef MAXSIZE
#define MAXSIZE MAX_RECO_TRACKS

        RECO_TRACK_LIST;

#undef MAXSIZE
#define MAXSIZE MAX_FLASHES

        OPDET_FLASH_LIST;

#undef MAXSIZE
#define MAXSIZE MAX_HITS

        OPDET_HIT_LIST;

#undef MAXSIZE
#define MAXSIZE MAX_RECO_TRACKS

        CPA_STITCH_LIST;
        TPC_PDS_MATCH_LIST;
        TPC_STOPPED_LIST;

#undef MAXSIZE
#define MAXSIZE MAX_WFMS

        OPDET_WAVEFORMS_LIST;

#undef MAXSIZE

#undef UINT
#undef INT_ARR
#undef SINT_ARR
#undef USINT_ARR
#undef UINT_ARR
#undef FLOAT_ARR
#undef FLOAT_4VEC_ARR
#undef INT_ARR2D
#undef SINT_ARR2D
#undef USINT_ARR2D
#undef UINT_ARR2D
#undef FLOAT_ARR2D
#undef FLOAT_ARR3D

    }; // struct Cosmiceventtree


    /// Create branches
    TTree* createBranches(TTree* tree, CosmicEventTree_t* evt)
    {

#define UINT(var, title) tree->Branch(#var, &evt->var, #var"/i")
#define SINT_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"]/S")
#define USINT_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"]/s")
#define UINT_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"]/i")
#define INT_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"]/I")
#define UINT_ARR2D(var, title, size1, size2) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d]/i", size2))
#define INT_ARR2D(var, title, size1, size2) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d]/I", size2))
#define SINT_ARR2D(var, title, size1, size2) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d]/S", size2))
#define USINT_ARR2D(var, title, size1, size2) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d]/s", size2))
#define FLOAT_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"]/F")
#define FLOAT_4VEC_ARR(var, title, size) tree->Branch(#var, evt->var, #var"["#size"][4]/F")
#define FLOAT_ARR2D(var, title, size1, size2) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d]/F", size2))
#define FLOAT_ARR3D(var, title, size1, size2, size3) tree->Branch(#var, evt->var, Form(#var"["#size1"][%d][%d]/F", size2, size3))

        FULL_LIST;

#undef UINT
#undef SINT_ARR
#undef USINT_ARR
#undef UINT_ARR
#undef INT_ARR
#undef INT_ARR2D
#undef UINT_ARR2D
#undef SINT_ARR2D
#undef USINT_ARR2D
#undef FLOAT_ARR
#undef FLOAT_4VEC_ARR
#undef FLOAT_ARR2D
#undef FLOAT_ARR3D

        return tree;
    }


    /// set addresses for tree branches
    TTree* setAddresses(TTree* tree, CosmicEventTree_t* evt)
    {
#define UINT(var, title) if (tree->GetBranch(#var)) tree->SetBranchAddress(#var, &evt->var)

#define ARR_SET_BRANCH(var) if (tree->GetBranch(#var)) tree->SetBranchAddress(#var, evt->var)

#define SINT_ARR(var, title, size) ARR_SET_BRANCH(var)
#define USINT_ARR(var, title, size) ARR_SET_BRANCH(var)
#define UINT_ARR(var, title, size) ARR_SET_BRANCH(var)
#define INT_ARR(var, title, size) ARR_SET_BRANCH(var)
#define INT_ARR2D(var, title, size1, size2) ARR_SET_BRANCH(var)
#define UINT_ARR2D(var, title, size1, size2) ARR_SET_BRANCH(var)
#define SINT_ARR2D(var, title, size1, size2) ARR_SET_BRANCH(var)
#define USINT_ARR2D(var, title, size1, size2) ARR_SET_BRANCH(var)
#define FLOAT_ARR(var, title, size) ARR_SET_BRANCH(var)
#define FLOAT_4VEC_ARR(var, title, size) ARR_SET_BRANCH(var)
#define FLOAT_ARR2D(var, title, size1, size2) ARR_SET_BRANCH(var)
#define FLOAT_ARR3D(var, title, size1, size2, size3) ARR_SET_BRANCH(var)

        FULL_LIST;

#undef UINT
#undef SINT_ARR
#undef USINT_ARR
#undef UINT_ARR
#undef INT_ARR
#undef UINT_ARR2D
#undef SINT_ARR2D
#undef USINT_ARR2D
#undef FLOAT_ARR
#undef FLOAT_4VEC_ARR
#undef FLOAT_ARR2D
#undef FLOAT_ARR3D

        return tree;
    }

} // namespace CosmicEventTree
#endif
